﻿using System.ComponentModel;
using UnityEngine;

internal class Wall : MonoBehaviour
{
    [SerializeField] private WallDescriptor wallDescriptor;

    internal WallDescriptor WallDescriptor
    {
        set
        {
            this.wallDescriptor = value;

            // Modify my position to match the new value.
            SetWallPosition(value.WallPosition);
        }
    }

    private Vector3 StartPosition;
    private Vector3 EndPosition;
    private float startTime;

    internal void SetWallPosition( WallPosition position)
    {
        if (this.wallDescriptor.WallType == WallType.UNDEFINED)
        {
            this.wallDescriptor.WallType = WallType.MOBILE;
        }

        if (this.wallDescriptor.WallType == WallType.MOBILE)
        {            
            bool moving = false;
            float startHeight;
            float endHeight = 0;

            switch (position)
            {
                case WallPosition.DOWN:
                    startHeight = -0.6f;
                    break;
                case WallPosition.UP:
                    startHeight = 0.5f;
                    break;
                case WallPosition.RISING:
                    moving = true;
                    startHeight = -0.6f;
                    endHeight = 0.5f;
                    break;
                case WallPosition.FALLING:
                    moving = true;
                    startHeight = this.transform.position.y;
                    endHeight = startHeight - 1.01f;
                    break;
                default:
                    throw new InvalidEnumArgumentException("That is not a WallPosition!");
            }

            this.transform.position = new Vector3(this.transform.position.x, startHeight, this.transform.position.z);

            if (moving)
            {
                // Set the start position.
                this.StartPosition = this.transform.position;
                this.startTime = Time.time;

                // Set the end position.
                this.EndPosition = new Vector3(this.transform.position.x, endHeight, this.transform.position.z);
            }
            // Start the movement.
            this.wallDescriptor.WallPosition = position;
        }
    }

    // Update is called once per frame
    internal void Update()
    {
        // Lerp if the wall segment is currently rising.
        if (this.wallDescriptor.WallPosition == WallPosition.RISING)
        {
            float timeFraction = (Time.time - this.startTime) / MazeManager.MoveTime;

            if (timeFraction >= 1)
            {
                timeFraction = 1;
                this.wallDescriptor.WallPosition = WallPosition.UP;
            }

            this.transform.position = Vector3.Lerp(this.StartPosition, this.EndPosition, timeFraction);
        }

        // Lerp if the wall segment is currently falling.
        else if (this.wallDescriptor.WallPosition == WallPosition.FALLING)
        {
            float timeFraction = (Time.time - this.startTime) / MazeManager.MoveTime;

            if (this.transform.position.y < -0.5f)
            {
                timeFraction = 1;
                this.wallDescriptor.WallPosition = WallPosition.DOWN;
            }

            this.transform.position = Vector3.Lerp(this.StartPosition, this.EndPosition, timeFraction);
        }

        // If the wall is down (hiding), just return it to the factory. We can make a new one if we want to raise it later.
        else if (this.wallDescriptor.WallPosition == WallPosition.DOWN)
        {
            WallManager.DestroyWall(PositionManager.WorldToGridPoint(this.transform.position));
        }
    }
}
