﻿using Callouts;
using UnityEngine;

internal class MazeManager : MonoBehaviour
{
    private static MazeManager Me;

    [SerializeField] private Wall wallTemplate;
    [SerializeField] private readonly int DefaultMazeSize = 50;
    [SerializeField] private float moveTime = 1f;
    [SerializeField] private WallManager walls;
    private bool hasMazeChanged = false;

    /// <summary>
    /// Delegate callout to populate an empty maze.
    /// </summary>
    internal SetMazeHeights SetMazeHeights_X { private get; set; }

    /// <summary>
    /// Returns the size of the maze.
    /// </summary>
    /// <returns>Vector2Int maze size</returns>
    internal static Vector2Int MazeSize { get => Me.walls.MazeSize; set => Me.walls.MazeSize = value; }

    /// <summary>
    /// Returns true if a maze change has been registered since the last time
    /// this flag was checked, false otherwise.
    /// </summary>
    internal static bool HasMazeChanged
    {
        get
        {
            bool value = Me.hasMazeChanged;
            Me.hasMazeChanged = false;
            return value;
        }
    }
    
    /// <summary>
    /// The time it takes a wall segment to move up or down.
    /// </summary>
    public static float MoveTime { get => Me.moveTime; set => Me.moveTime = value; }

    /// <summary>
    /// The prefab template used to create wall segments in the maze.
    /// </summary>
    internal Wall WallTemplate { get => this.wallTemplate; set => this.wallTemplate = value; }

    internal void Awake()
    {
        if (Me == null)
            Me = this;
        else
            throw new SingletonReInstantiatedException("There is already a MazeManager in the scene! Use MazeManager.Manager to get me!");

        if (this.walls == null)
            this.walls = FindObjectOfType<WallManager>();

        this.walls.CreateNewGrid(new Vector2Int(this.DefaultMazeSize, this.DefaultMazeSize));
    }

    private void Start()
    {
        this.SetMazeHeights_X = MazeGeneratorCallouts.SetMazeHeights;
        WallFactory.WallPrefab = this.WallTemplate;
    }

    /// <summary>
    /// Create a map of the specified size. If no size is given, the last used size will be used.
    /// </summary>
    internal void CreateMaze() => CreateMaze(MazeSize);
    internal void CreateMaze(int size) => CreateMaze(new Vector2Int(size, size));
    internal void CreateMaze(Vector2Int mazeSize)
    {
        this.walls.CreateNewGrid(mazeSize);
        this.SetMazeHeights_X(mazeSize);
    }

    /// <summary>
    /// Loads a saved maze from the provided save game data. This will overwrite any existing maze.
    /// </summary>
    /// <param name="saveData"></param>
    internal static void LoadMaze(MazeSaveData saveData)
    {

        Me.DestroyMaze();

        if (saveData.MazeCells == null || saveData.MazeCells.Count == 0)
        {
            throw new LoadFileException("The save file either does not contain a maze descriptor array, or the array size is zero!");
        }
        else
        {
            Logging.Log("Unpacking save data after loading");
            MazeSize = saveData.MazeSize;

            Me.walls.CreateNewGrid(MazeSize);

            for (int y = 0; y < MazeSize.y; y++)
                for (int x = 0; x < MazeSize.x; x++)
                {
                    if (saveData.MazeCells[x + (y * saveData.MazeSize.x)].WallPosition != WallPosition.DOWN)
                    {
                        WallManager.CreateWall(new Vector2Int(x, y), saveData.MazeCells[x + (y * saveData.MazeSize.x)].WallPosition);
                    }
                }
        }
    }

    /// <summary>
    /// Populates the save game data with the current maze's metadata.
    /// </summary>
    /// <param name="saveData"></param>
    internal static void SaveMaze(MazeSaveData saveData)
    {
        Logging.Log("Packing save data before saving.");

        saveData.MazeSize = MazeSize;
        saveData.MazeCells.Clear();

        for (int y = 0; y < MazeSize.y; y++)
            for (int x = 0; x < MazeSize.x; x++)
            {
                saveData.MazeCells.Add(WallManager.GetWall(new Vector2Int(x, y)));
            }
    }

    /// <summary>
    /// Debug tool that prints a visual representation of the maze to the debug log.
    /// </summary>
    internal static void PrintMaze() => Me.walls.PrintGrid();

    /// <summary>
    /// Sets a flag indicating that the maze has been modified in some way since the last time
    /// the flag was checked. The MazeHasChanged flag will reset when it is viewed.
    /// </summary>
    internal static void RegisterMazeChange() => Me.hasMazeChanged = true;

    /// <summary>
    /// Checks if the maze exists and removes all walls if it does.
    /// </summary>
    internal void DestroyMaze() => this.walls.DestroyGrid();

    internal void OnDestroy()
    {
        // Remove my static reference to me so I can be created again later.
        if (Me == this)
        {
            Me = null;
        }

        // Make sure we don't keep references to any destroyed assets.
        WallFactory.Reset();
    }
}
