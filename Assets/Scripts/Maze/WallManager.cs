﻿using System;
using UnityEngine;

internal class WallManager : MonoBehaviour
{
    private static WallManager Me;

    private class Cell
    {
        internal Wall wall = null;

        internal WallDescriptor descriptor = new WallDescriptor();
    }

    [SerializeField] private Cell[,] cells = null;

    /// <summary>
    /// Contains the current grid size of the maze.
    /// </summary>
    internal Vector2Int MazeSize { get; set; }

    /// <summary>
    /// Returns true if the wall position is within the maze size, false otherwise.
    /// </summary>
    /// <param name="position"></param>
    /// <returns></returns>
    public static bool IsWallPositionValid(Vector2Int position) => (position.x < Me.MazeSize.x) && (position.y < Me.MazeSize.y) && (position.x >= 0) && (position.y >= 0);

    /// <summary>
    /// Returns the descriptor for a wall segment at the given coordinate.
    /// </summary>
    /// <param name="gridPosition"></param>
    /// <returns></returns>
    internal static WallDescriptor GetWall(Vector2Int gridPosition) => Me.cells[gridPosition.x, gridPosition.y].descriptor;

    /// <summary>
    /// Returns the wall at the grid point or null if no wall exists there.
    /// </summary>
    /// <param name="gridPosition"></param>
    /// <returns></returns>
    internal static Wall GetWallObject(Vector2Int gridPosition) => Me.cells[gridPosition.x, gridPosition.y].wall;

    /// <summary>
    /// Returns true if there is a wall at the grid point. Returns false otherwise.
    /// </summary>
    /// <param name="gridPosition"></param>
    /// <returns></returns>
    internal static bool WallExists(Vector2Int gridPosition) => WallExists(gridPosition.x, gridPosition.y);
    internal static bool WallExists(int x, int y) => Me.cells[x, y].wall != null;

    /// <summary>
    /// Create a wall segment at the specified grid coordinates.
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="position"></param>
    /// <returns>True if successful, false if wall already exists</returns>
    internal static void CreateWall(Vector2Int gridPosition, WallPosition position = WallPosition.RISING)
    {
        if (WallExists(gridPosition))
        {
            if (Me.cells[gridPosition.x, gridPosition.y].descriptor.WallPosition < WallPosition.RISING)
            {
                Me.cells[gridPosition.x, gridPosition.y].wall.SetWallPosition(WallPosition.RISING);
                MazeManager.RegisterMazeChange();
            }
        }
        else
        {
            Me.cells[gridPosition.x, gridPosition.y].descriptor.WallPosition = position;

            Wall wall = WallFactory.GetWall(gridPosition, Me.cells[gridPosition.x, gridPosition.y].descriptor);
            wall.transform.parent = Me.transform;
            Me.cells[gridPosition.x, gridPosition.y].wall = wall;
            MazeManager.RegisterMazeChange();
        }
    }

    /// <summary>
    /// Remove the wall segment at the specified grid coordinates.
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <returns>True if successful, false if no wall exists</returns>
    internal static void DestroyWall(Vector2Int gridPosition)
    {
        Me.CheckWallRange(gridPosition);

        if (WallExists(gridPosition))
        {
            Me.cells[gridPosition.x, gridPosition.y].descriptor.WallType = WallType.UNDEFINED;

            WallFactory.Return(Me.cells[gridPosition.x, gridPosition.y].wall);
            Me.cells[gridPosition.x, gridPosition.y].wall = null;
            MazeManager.RegisterMazeChange();
        }
    }

    /// <summary>
    /// Attempts to add a wall if none exists or destroy the existing wall at the coordinates.
    /// </summary>
    /// <param name="gridPosition"></param>
    internal static void ToggleWall(Vector2Int gridPosition)
    {
        Me.CheckWallRange(gridPosition);

        if (WallExists(gridPosition))
        {
            // Start removing the wall.
            GetWallObject(gridPosition).SetWallPosition(WallPosition.FALLING);
        }
        else
        {
            CreateWall(gridPosition);
        }
        MazeManager.RegisterMazeChange();
    }

    internal void Awake()
    {
        if (Me == null)
            Me = this;
        else
            throw new SingletonReInstantiatedException("There is already a WallManager in the scene!");
    }

    /// <summary>
    /// Creates a new grid of the specified size. Walls will be tracked within the grid.
    /// </summary>
    /// <param name="size"></param>
    internal void CreateNewGrid(Vector2Int size)
    {
        DestroyGrid();
        this.MazeSize = size;
        this.cells = new Cell[size.x, size.y];

        for (int x = 0; x < this.MazeSize.x; x++)
        {
            for (int y = 0; y < this.MazeSize.y; y++)
            {
                this.cells[x, y] = new Cell 
                { 
                    descriptor = new WallDescriptor 
                    {
                        WallPosition = WallPosition.DOWN, WallType = WallType.MOBILE 
                    } 
                };
            }
        }
     }

    /// <summary>
    /// Prints the maze to the debug console.
    /// </summary>
    internal void PrintGrid()
    {
        string mapString = "";

        for (int i = 0; i < this.MazeSize.x; i++)
        {
            for (int j = 0; j < this.MazeSize.y; j++)
                mapString += WallExists(i, j) ? "X" : "_";

            mapString += "\n";
        }
        Logging.Log(mapString, LoggingLevels.Important);
    }

    /// <summary>
    /// Returns all walls to the factory and resets all cells to default values.
    /// Does not change the maze size.
    /// </summary>
    internal void DestroyGrid()
    {
        MazeManager.RegisterMazeChange();

        for (int x = 0; x < this.MazeSize.x; x++)
        {
            for (int y = 0; y < this.MazeSize.y; y++)
            {
                if (this.cells[x, y].wall != null)
                {
                    WallFactory.Return(this.cells[x, y].wall);
                    this.cells[x, y].wall = null;
                }
            }
        }
    }

    /// <summary>
    /// Throws an ArgumentOutOfRangeException if the received wall coordinates are invalid.
    /// </summary>
    /// <param name="position"></param>
    private void CheckWallRange(Vector2Int position)
    {
        if (!IsWallPositionValid(position))
            throw new ArgumentOutOfRangeException("Attempted to access a wall segment outside of the maze bounds. Maze size: " +
                this.MazeSize.x + ", " + this.MazeSize.y + ". Location: " + position.x + ", " + position.y + ".");
    }
}
