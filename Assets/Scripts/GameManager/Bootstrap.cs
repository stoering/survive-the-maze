﻿using CommandTerminal;
using System.Collections;
using UnityEngine;

/// <summary>
/// Simple class to start a game in Play mode or a build, but not unnecessarily in unit tests.
/// </summary>
public static class Bootstrap
{
    /// <summary>
    /// Bootstrap is enabled by default. Turn off via DisableBootstrap() before loading a scene.
    /// </summary>
    public static bool BootstrapEnabled { get; private set; } = true;

    /// <summary>
    /// Prevents a scene from automatically starting a new game upon load.
    /// </summary>
    public static void DisableBootstrap() => BootstrapEnabled = false;

    /// <summary>
    /// Runs the provided string as a terminal command immediately upon starting a new game.
    /// Use it to quickly get to a desired testing state.
    /// </summary>
    /// <param name="command"></param>
    /// <returns></returns>
    public static IEnumerator StartNewGame(string command)
    {
        // Give the Terminal Manager time to register all commands.
        yield return new WaitForEndOfFrame();

        if (BootstrapEnabled)
        {
            Terminal.Shell.RunCommand(command);
        }
    }
}
