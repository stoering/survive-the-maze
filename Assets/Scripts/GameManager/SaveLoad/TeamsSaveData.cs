﻿using System;
using System.Collections.Generic;

[Serializable]
internal class TeamsSaveData
{
    // TODO: Figure out how to save/load, and test games. Maybe we don't need to save if matches are quick enough.
    public List<TeamConfigurationData> ActiveTeams;

    public List<CharacterDescriptor> NavAgents;
}
