﻿using Interface;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

internal delegate void PackUnpackSaveData();

internal class SaveManager : MonoBehaviour
{
    // Dumb singleton check.
    private static SaveManager Me;

    [SerializeField] private string saveFileLocation = "StreamingAssets\\Temp";
    [SerializeField] private string settingsSaveFile = "Preferences.json";
    [SerializeField] private GameSettings gameSettings;

    internal SaveData SaveGameData { get; private set; }
    internal GameSettings GameSettings { get => this.gameSettings; private set => this.gameSettings = value; }

    internal string SaveFileLocation { get => this.saveFileLocation; set => this.saveFileLocation = value; }
    internal string DefaultSaveFile { get => this.GameSettings.LatestSaveFile; set => this.GameSettings.LatestSaveFile = value; }

    // Make sure our default values are available before other scripts need them.
    internal void Awake()
    {
        if (Me == null)
            Me = this;
        else
            throw new SingletonReInstantiatedException("There is already a SaveManager in the scene!");

        // Create the empty data structure.
        CreateSaveStructures();
    }

    /// <summary>
    /// Sets the default values for the save data structure.
    /// </summary>
    private void CreateSaveStructures()
    {
        // Initialize the Game Settings.
        this.GameSettings = new GameSettings
        {
            LatestSaveFile = "SaveGame.txt",

            // Create the save game data.
            ModInterface = new ModInterfaceSaveData
            {
                // Default option that cannot be set on object creation.
                DebuggerEnabled = Debug.isDebugBuild || Application.isEditor
            }
        };

        // Initialize the Save Game Data.
        this.SaveGameData = new SaveData
        {
            MazeSaveData = new MazeSaveData
            {
                MazeSize = Vector2Int.zero,
                MazeCells = new List<WallDescriptor>()
            }
        };
    }

    /// <summary>
    /// Calls save functions on our data structures to convert them to a serializable format.
    /// </summary>
    internal void CopyToSaveStructure() => MazeManager.SaveMaze(this.SaveGameData.MazeSaveData);

    /// <summary>
    /// Calls load functions on our data structures to recreate them from the save data.
    /// </summary>
    internal void CopyFromSaveStructure()
    {
        // Load the maze if one exists.
        if (this.SaveGameData.MazeSaveData != null)
        {
            MazeManager.LoadMaze(this.SaveGameData.MazeSaveData);
        }
    }

    /// <summary>
    /// Quickly saves the game to the default file.
    /// </summary>
    internal void SaveToLatest() => SaveToFile(this.DefaultSaveFile);

    /// <summary>
    /// Saves the game to the specified file.
    /// </summary>
    /// <param name="saveFile"></param>
    internal void SaveToFile(string saveFile)
    {
        if (saveFile.Contains(".txt") == false)
        {
            saveFile += ".txt";
        }

        CopyToSaveStructure();

        string fullPathSave = Path.Combine(this.saveFileLocation, saveFile);
        Logging.Log("Saved to " + fullPathSave, LoggingLevels.Information);
        FileManager.SaveObject(fullPathSave, this.SaveGameData);
    }

    /// <summary>
    /// Saves the game preferences to the default file.
    /// </summary>
    internal void SavePreferences()
    {
        string fullPathSave = Path.Combine(this.saveFileLocation, this.settingsSaveFile);
        Logging.Log("Saved settings to " + fullPathSave, LoggingLevels.Information);
        FileManager.SaveObject(fullPathSave, this.GameSettings);
    }

    /// <summary>
    /// Loads the game from the most recent file.
    /// </summary>
    /// <returns></returns>
    internal bool LoadGame() => LoadFromFile(this.GameSettings.LatestSaveFile);

    /// <summary>
    /// Loads the game from the last used file.
    /// </summary>
    /// <param name="saveFile"></param>
    /// <returns></returns>
    internal bool LoadFromFile(string saveFile)
    {
        if (saveFile.Contains(".txt") == false)
        {
            saveFile += ".txt";
        }
        string fullPathSave = Path.Combine(this.saveFileLocation, saveFile);
        bool loadSuccessful = true;
        // If the file doesn't exist, just catch and print.
        try
        {
            FileManager.LoadObject(fullPathSave, this.SaveGameData);
            CopyFromSaveStructure();
            Logging.Log("Successfully loaded game from " + fullPathSave, LoggingLevels.Information);
        }
        catch (FileNotFoundException)
        {
            loadSuccessful = false;
            Logging.Log("No file exists at " + fullPathSave, LoggingLevels.Warning);
        }

        return loadSuccessful;
    }

    /// <summary>
    /// Loads the game preferences from the default file.
    /// </summary>
    /// <returns></returns>
    internal bool LoadPreferences()
    {
        string fullPathSave = Path.Combine(this.saveFileLocation, this.settingsSaveFile);
        bool loadSuccessful = true;
        // If the file doesn't exist, just catch and print.
        try
        {
            FileManager.LoadObject(fullPathSave, this.GameSettings);
            Logging.Log("Successfully loaded game settings from " + fullPathSave, LoggingLevels.Information);
        }
        catch (FileNotFoundException)
        {
            loadSuccessful = false;
            Logging.Log("No file exists at " + fullPathSave, LoggingLevels.Warning);
        }

        return loadSuccessful;
    }

    internal void OnDestroy()
    {
        // Remove my static reference to me so I can be created again later.
        if (Me == this)
        {
            Me = null;
        }
    }
}
