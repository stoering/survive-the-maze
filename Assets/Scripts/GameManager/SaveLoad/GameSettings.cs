﻿using System;
using Interface;

[Serializable]
internal class GameSettings
{
    // Save Manager Settings
    public string LatestSaveFile;

    public ModInterfaceSaveData ModInterface;
}
