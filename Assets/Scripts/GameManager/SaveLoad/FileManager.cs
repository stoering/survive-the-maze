﻿using System.IO;
using UnityEngine;

/// <summary>
/// Manages all Json and disk operations. Generic class knows nothing of the data it is using.
/// </summary>
internal static class FileManager
{
    /// <summary>
    /// Converts the object to a Json string then writes it to the specified file.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="fileName"></param>
    /// <param name="saveData"></param>
    internal static void SaveObject<T>(string fileName, T saveData) => SaveString(JsonUtility.ToJson(saveData, true), fileName);

    /// <summary>
    /// Loads a Json string from the specified file, then loads the string into the given object.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="fileName"></param>
    /// <param name="loadData"></param>
    internal static void LoadObject(string fileName, object loadData) => JsonUtility.FromJsonOverwrite(LoadString(fileName), loadData);
    internal static T LoadObject<T>(string fileName) where T : new() => JsonUtility.FromJson<T>(LoadString(fileName));

    /// <summary>
    /// Saves a string to a file on Application.dataPath.
    /// </summary>
    /// <param name="toSave"></param>
    /// <param name="fileName"></param>
    internal static void SaveString(string toSave, string fileName)
    {
        string dataPath = Path.Combine(Application.dataPath, fileName);

        // If the file is in an uncreated subdirectory, create the directory first.
        _ = Directory.CreateDirectory(Path.GetDirectoryName(dataPath));

        using (StreamWriter streamWriter = File.CreateText(dataPath))
        {
            streamWriter.Write(toSave);
        }
    }

    /// <summary>
    /// Loads a Json script from a specified file.
    /// </summary>
    /// <param name="fileName"></param>
    /// <returns>Json string</returns>
    internal static string LoadString(string fileName)
    {
        string dataPath = Path.Combine(Application.dataPath, fileName);

        using (StreamReader streamReader = File.OpenText(dataPath))
        {
            return streamReader.ReadToEnd();
        }
    }
}
