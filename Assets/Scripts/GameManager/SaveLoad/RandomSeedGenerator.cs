﻿using System;
using System.ComponentModel;
using System.IO;

/// <summary>
/// Provides options for running unit tests across batch calls. Doesn't really fit here, but 
/// the Editor folder doesn't reference the Test folder *sigh*, so I guess this works.
///     There are five seeding modes that this module supports:
///         RandomSeed              - A pseudorandom seed is generated for each new test.
///         SequentialSeed          - The seed increases by 1 for each test. The seed is saved and resumes where it left off on the next run.
///         RepeatSequence          - The seed increases by 1 for each test. The same starting seed is used for all runs. Will repeat a failing case (full suite) for SequentialSeed.
///         StepChangeSequential    - A fixed seed is used for all tests in the run. On the next run, seed + 1 is used for the new seed.
///         FixedSeed               - The same seed is used across all tests and runs. Will repeat a failing case (full suite) for StepChangeSequential. Use for repeating single failed tests.
/// </summary>
[Serializable]
public class RandomSeedGenerator
{
    private Random randomGeneratorForNextSeed;

    private int initialSeed = 0;
    [UnityEngine.SerializeField] private int seed = 0;
    [UnityEngine.SerializeField] private TestSeedType seedType = TestSeedType.RandomSeed;

    // Reference to Me that both the unit test and the editor assemblies can reference (even though they don't reference each other.)
    private static RandomSeedGenerator me;

    /// <summary>
    /// Changes the behaviour of the seed generator.
    /// </summary>
    public enum TestSeedType
    {
        FixedSeed,
        RandomSeed,
        SequentialSeed,
        RepeatSequence,
        StepChangeSequential
    }

    private static RandomSeedGenerator Me
    {
        get
        {
            if (me == null)
            {
                LoadSeed();
            }
            return me;
        }
    }

    /// <summary>
    /// Returns the current/last used seed. Does not change the seed.
    /// </summary>
    public static int Seed { get => Me.seed; set => Me.seed = value; }

    /// <summary>
    /// Describes the seed generation pattern. See class definition for an explination of patterns.
    /// </summary>
    public static TestSeedType SeedType { get => Me.seedType; set => Me.seedType = value; }

    // Save and load functions ensure seeds can remain across batch call cycles.
    private const string SeedSaveLocation = "Tests/Temp/RandomSeedSettings.txt";

    /// <summary>
    /// Moves on to the next test seed in the sequence. Returns the seed.
    /// The sequence is determined by the TestSeedType.
    /// </summary>
    public static int NextSeed
    {
        get
        {
            switch (SeedType)
            {
                case TestSeedType.FixedSeed:
                case TestSeedType.StepChangeSequential:
                    // Do nothing, the seed shouldn't change.
                    break;
                case TestSeedType.RandomSeed:
                    // This is based on System.Random and it guaranteed to be unique. Thanks, Internet!
                    Seed = Guid.NewGuid().GetHashCode();
                    break;
                case TestSeedType.SequentialSeed:
                case TestSeedType.RepeatSequence:
                    // We are increasing the seed in both cases. The difference is in saving/loading the seeds across tests.
                    Seed++;
                    break;
                default:
                    throw new InvalidEnumArgumentException("Invalid Seed type used!");
            }

            return Seed;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public static void LoadSeed()
    {
        try
        {
            me = FileManager.LoadObject<RandomSeedGenerator>(SeedSaveLocation);
            Logging.Log("Successfully loaded unit test seeds from " + SeedSaveLocation, LoggingLevels.Verbose);
            me.initialSeed = Seed;
        }
        catch (Exception e)
        {
            if (e is FileNotFoundException || e is DirectoryNotFoundException)
            {
                Logging.LogException(e);
                me = new RandomSeedGenerator();
                SaveSeed();
                Logging.Log("Created a new Unit Test Seed file at " + SeedSaveLocation, LoggingLevels.Verbose);
            }
            else throw e; // Rethrow any unexpected exceptions.
        }
    }

    /// <summary>
    /// Will try to write the seed to file. Checks the SeedType first and updates
    /// the seed accordingly.
    /// </summary>
    public static void SaveSeed()
    {
        // Hold this on the stack so we don't overwrite it.
        // We may not be done generating our sequence of seeds yet.
        int oldSeed = Seed;

        switch (SeedType)
        {
            case TestSeedType.FixedSeed:
                // Do nothing, the seed has not changed unless we changed it manually.
                break;
            case TestSeedType.RandomSeed:
                // We shouldn't save a seed value since it changes for each test.
                Seed = 0;
                break;
            case TestSeedType.SequentialSeed:
                // Do nothing, we want to record the last used seed so we can pick up where we left off.
                break;
            case TestSeedType.RepeatSequence:
                // Revert to the initial value, the seed does not change during a run.
                Seed = me.initialSeed;
                break;
            case TestSeedType.StepChangeSequential:
                Seed = me.initialSeed + 1;
                break;
            default:
                throw new InvalidEnumArgumentException("Invalid Seed type used!");
        }
        FileManager.SaveObject(SeedSaveLocation, me);
        Logging.Log("Successfully Saved Random Seed State.", LoggingLevels.Verbose);

        Seed = oldSeed;
    }
}
