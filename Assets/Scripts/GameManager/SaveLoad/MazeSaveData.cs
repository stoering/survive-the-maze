﻿using MoonSharp.Interpreter;
using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
[MoonSharpUserData]
public class MazeSaveData
{
    public Vector2Int MazeSize;
    public List<WallDescriptor> MazeCells;
}
