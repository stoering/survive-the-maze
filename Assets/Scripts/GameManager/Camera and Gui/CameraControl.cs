﻿using UnityEngine;

internal class CameraControl : MonoBehaviour
{
    [SerializeField] internal float ZoomControl = 1.05f;
    [SerializeField] internal float GlideControl = 0.5f;
    [SerializeField] internal float MaxZoom = 1.5f;
    [SerializeField] internal float MinZoom = 0.5f;
    [SerializeField] internal float OverScroll = 0.0f;
    internal Vector3 Origin;
    internal Vector3 Difference;
    internal bool Drag = false;

    private void LateUpdate()
    {
        // Use the mouse scroll wheel to zoom in and out from the map.
        if (Input.GetAxis("Mouse ScrollWheel") > 0f) // forward
        {
            this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y / this.ZoomControl, this.transform.position.z);
        }
        else if (Input.GetAxis("Mouse ScrollWheel") < 0f) // backwards
        {
            this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y * this.ZoomControl, this.transform.position.z);
        }

        // Use the arrow keys to move the viewport.
        if (Input.GetKey(KeyCode.UpArrow))
        {
            this.transform.position += new Vector3(0, 0, this.GlideControl * this.transform.position.y * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            this.transform.position += new Vector3(0, 0, -this.GlideControl * this.transform.position.y * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            this.transform.position += new Vector3(-this.GlideControl * this.transform.position.y * Time.deltaTime, 0, 0);
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            this.transform.position += new Vector3(this.GlideControl * this.transform.position.y * Time.deltaTime, 0, 0);
        }

        // Use the middle mouse button to scroll.
        if (Input.GetMouseButton(2))
        {
            this.Difference = MouseToWorldPoint() - this.transform.position;
            if (this.Drag == false)
            {
                this.Drag = true;
                this.Origin = MouseToWorldPoint();
            }
            else
            {
                this.transform.position = this.Origin - this.Difference;
            }
        }
        else
        {
            this.Drag = false;
        }

        // Make sure we are not out of bounds.
        Clamp();
    }

    // Used like ScreenToWorldPoint, but works for perspective camera.
    private Vector3 MouseToWorldPoint()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        Plane xz = new Plane(Vector3.up, new Vector3(0, 0, 0));
        _ = xz.Raycast(ray, out float distance);
        return ray.GetPoint(distance);
    }

    private void Clamp()
    {
        float smallerMazeDimension = Mathf.Min(MazeManager.MazeSize.x, MazeManager.MazeSize.y);
        float largerMazeDimension = Mathf.Max(MazeManager.MazeSize.x, MazeManager.MazeSize.y);

        float xMove = Mathf.Clamp(this.transform.position.x, -(MazeManager.MazeSize.x / 2f) - this.OverScroll, (MazeManager.MazeSize.x / 2) + this.OverScroll );
        float zoom = Mathf.Clamp(this.transform.position.y, smallerMazeDimension * this.MinZoom, largerMazeDimension * this.MaxZoom);
        float zMove = Mathf.Clamp(this.transform.position.z, -(MazeManager.MazeSize.y / 2f) - this.OverScroll, (MazeManager.MazeSize.y / 2) + this.OverScroll );

        this.transform.position = new Vector3(xMove, zoom, zMove);
    }
}
