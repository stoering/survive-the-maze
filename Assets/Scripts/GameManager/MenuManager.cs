﻿using CommandTerminal;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour
{
    [SerializeField] public Button MenuButtonPrefab;

    [SerializeField] public MenuData MainMenu;
    [SerializeField] public MenuData PauseMenu;
    [SerializeField] public List<MenuData> Menus;

    [SerializeField] public Canvas Menu;

    private static MenuManager Me;

    internal void Awake()
    {
        if (Me == null)
            Me = this;
        else
            throw new SingletonReInstantiatedException("There is already a GameManager in the scene!");

        LoadMenus();
    }

    /// <summary>
    /// Loads the specified menu, if it exists.
    /// If it does not exist, logs an error.
    /// </summary>
    /// <param name="name"></param>
    internal static void LoadMenu(string name)
    {
        // Find the menu matching our string and load it.
        foreach (MenuData menu in Me.Menus)
        {
            if (menu.Name.ToLower() == name.ToLower())
            {
                Me.LoadMenu(menu);
                return;
            }
        }
        Logging.LogError("Menu " + name + " does not exist! Check your spelling and try again.");
    }

    /// <summary>
    /// Toggles the quick menu, if applicable.
    /// </summary>
    internal static void QuickMenu() => LoadMenu("Pause");

    /// <summary>
    /// Closes the open menu and performs the click action.
    /// </summary>
    /// <param name="action"></param>
    public void DoAction(string action)
    {
        if (this.Menu != null)
            Destroy(this.Menu.gameObject);

        Terminal.Shell.RunCommand(action);
    }

    /// <summary>
    /// Loads the game menus from the master menufile.
    /// </summary>
    private void LoadMenus()
    {
        foreach (string menuName in Directory.GetFiles(Path.Combine(Application.dataPath, "StreamingAssets/Menus"), "*.menu"))
        {
            this.Menus.Add(FileManager.LoadObject<MenuData>(menuName));
        }
    }

    /// <summary>
    /// Build and activate the selected menu.
    /// </summary>
    /// <param name="menuData"></param>
    private void LoadMenu(MenuData menuData)
    {
        // Remove any old menu and create a new one.
        if (this.Menu != null)
            Destroy(this.Menu.gameObject);

        // Create a new empty menu.
        this.Menu = new GameObject("Menu").AddComponent<Canvas>();
        _ = this.Menu.gameObject.AddComponent<GraphicRaycaster>();

        this.Menu.renderMode = RenderMode.WorldSpace;
        this.Menu.worldCamera = Camera.main;

        RectTransform menuObject = this.Menu.GetComponent<RectTransform>();
        menuObject.position = Vector3.zero;
        menuObject.rotation = Quaternion.Euler(90, 0, 0);
        menuObject.sizeDelta = menuData.MenuSize;

        // Add each menu button in a loop.
        foreach (MenuButtonData button in menuData.menuButtons)
        {
            AddMenuButton(button);
        }

        // If there is a specific action to do on menu load, do that action now.
        if (menuData.ActionOnLoad != "")
        {
            Terminal.Shell.RunCommand(menuData.ActionOnLoad);
        }
    }

    /// <summary>
    /// Add a button to the physical menu (built from file).
    /// </summary>
    /// <param name="buttonData"></param>
    private void AddMenuButton(MenuButtonData buttonData)
    {
        Button button = Instantiate(this.MenuButtonPrefab, this.Menu.transform);
        button.name = buttonData.ButtonText;
        button.onClick.AddListener(() => DoAction(buttonData.Action));

        RectTransform rect = button.GetComponent<RectTransform>();
        rect.position = new Vector3(buttonData.buttonPosition.x, 0.5f, buttonData.buttonPosition.y);
        rect.sizeDelta = buttonData.buttonSize;

        Text text = button.GetComponentInChildren<Text>();
        text.text = buttonData.ButtonText;
    }

    internal void OnDestroy()
    {
        // Remove my static reference to me so I can be created again later.
        if (Me == this)
        {
            Me = null;
        }
    }
}
