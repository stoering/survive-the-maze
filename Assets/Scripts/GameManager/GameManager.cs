﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Assertions;
using Interface;
using System.IO;

public class GameManager : MonoBehaviour
{
    [Tooltip("This command will be run immediately when the game begins. Leave empty to do nothing. Default is LoadGame")]
    [SerializeField] internal string BootstrapCommand = "LoadGame";
    [SerializeField] internal InputManager InputManager;
    [SerializeField] internal MazeManager MazeManager;
    [SerializeField] internal CharacterManager NavAgentManager;
    [SerializeField] internal SaveManager SaveManager;
    [SerializeField] internal TeamManager TeamManager;
    [SerializeField] internal MenuManager MenuManager;
    [SerializeField] internal VisibilityManager VisibilityManager = new VisibilityManager();
    [SerializeField] internal List<InputInterface> AiInterfaces = new List<InputInterface>();
    // Lights.
    [SerializeField] internal GameObject MinotaurLight;
    [SerializeField] internal GameObject FloodLight;

    private static GameManager Me;

    /// <summary>
    /// Returns the preferences for the game. Settings are loaded from a file when the game is first
    /// started.
    /// </summary>
    internal static GameSettings Preferences => Me.SaveManager.GameSettings;

    /// <summary>
    /// Returns the save game data for the entire game. Modules can modify their own data, which will be saved
    /// during a SaveGame event.
    /// </summary>
    internal static SaveData SaveData => Me.SaveManager.SaveGameData;

    internal void Awake()
    {
        if (Me == null)
            Me = this;
        else
        {
            // For the compiler. Maybe change later...
            this.InputManager = FindObjectOfType<InputManager>();
            this.MazeManager = FindObjectOfType<MazeManager>();
            this.NavAgentManager = FindObjectOfType<CharacterManager>();
            this.SaveManager = FindObjectOfType<SaveManager>();
            this.TeamManager = FindObjectOfType<TeamManager>();
            this.MenuManager = FindObjectOfType<MenuManager>();

            throw new SingletonReInstantiatedException("There is already a GameManager in the scene!");
        }

        Assert.IsNotNull(this.InputManager);
        Assert.IsNotNull(this.MazeManager);
        Assert.IsNotNull(this.NavAgentManager);
        Assert.IsNotNull(this.SaveManager);
        Assert.IsNotNull(this.TeamManager);
        Assert.IsNotNull(this.MenuManager);
    }

    internal void Start()
    {
        // Start the Mod Scripts before we need them.
        ModScriptRunner.LoadMoonSharp(this.SaveManager.GameSettings.ModInterface, false);

        // Call out to Bootstrap class to see if bootstrap was disabled by an external source.
        // Coroutine ensures that all Start methods have been called before beginning setup.
        _ = StartCoroutine(Bootstrap.StartNewGame(this.BootstrapCommand));
    }

    internal void LateUpdate()
    {
        // Update the maps for the Ais.
        this.VisibilityManager.UpdateVisibility();

        // Let the AIs do their thinking.
        ModScriptRunner.AiUpdate();
    }

    internal void OnDestroy()
    {
        // Remove my static reference to me so I can be created again later.
        if (Me == this)
        {
            Me = null;
        }
    }

    // --------------------------------------------------------------------------------------------------------------------------------------------------
    // This section defines Terminal call-in commands. Some commands are also mapped to menu functions.

    /// <summary>
    /// Saves the game data to the default or specified file.
    /// </summary>
    internal static void SaveGame()
    {
        Logging.Log("Saving Game");
        Me.SaveManager.SaveToLatest();
    }
    internal static void SaveGame(string saveFile)
    {
        Logging.Log("Saving Game to " + saveFile);
        Me.SaveManager.SaveToFile(saveFile);
    }

    /// <summary>
    /// Saves the game settings to file.
    /// </summary>
    internal static void SavePreferences()
    {
        Logging.Log("Saving Settings");
        Me.SaveManager.SavePreferences();
    }

    /// <summary>
    /// Loads the game data from the default or specified file.
    /// </summary>
    internal static void LoadGame()
    {
        Logging.Log("Loading Game");
        _ = Me.SaveManager.LoadGame();
    }
    internal static void LoadGame(string saveFile)
    {
        Logging.Log("Loading Game from " + saveFile);
        _ = Me.SaveManager.LoadFromFile(saveFile);
    }

    /// <summary>
    /// Loads the game settings from file.
    /// </summary>
    internal static void LoadPreferences()
    {
        Logging.Log("Loading Settings");
        _ = Me.SaveManager.LoadPreferences();
    }

    /// <summary>
    /// Starts a new game using the default settings.
    /// Pass in the names of the interface(s) you want to use.
    /// </summary>
    internal static void NewGame(string playerTeam = "Minotaur", string aiTeam = "Spartan")
    {
        GenerateMaze();
        ResetAis(playerTeam, aiTeam, "2_Medium");
    }

    /// <summary>
    /// Clear all maze segments and mortals from the game area.
    /// </summary>
    internal static void ClearGame()
    {
        ClearTeams();
        ClearMaze();
    }

    /// <summary>
    /// Creates a maze of the specified size. 
    /// </summary>
    internal static void GenerateMaze() => Me.MazeManager.CreateMaze();
    internal static void GenerateMaze(int size) => Me.MazeManager.CreateMaze(size);
    internal static void GenerateMaze(Vector2Int mazeSize) => Me.MazeManager.CreateMaze(mazeSize);

    /// <summary>
    /// Removes all maze segments. Does not affect mortals.
    /// </summary>
    internal static void ClearMaze() => Me.MazeManager.DestroyMaze();

    // Ai Management Functions
    internal static void ResetAis(string playerTeam, string aiTeam, string aiName)
    {
        ClearTeams();
        CreatePlayerTeam(playerTeam);
        CreateAiTeam(aiTeam, aiName);
    }

    /// <summary>
    /// Creates a player controlled team.
    /// Must pass in a name matching an existing team config file.
    /// </summary>
    /// <param name="teamName"></param>
    internal static void CreatePlayerTeam(string teamName)
    {
        if (Me.InputManager.PlayerInterface != null)
        {
            ActivateTeamAi(Me.InputManager.PlayerInterface, "2_Medium");
            Me.AiInterfaces.Add(Me.InputManager.PlayerInterface);
        }

        ActivatePlayerControl(Me.TeamManager.CreateTeam(teamName));
    }

    /// <summary>
    /// Creates an AI controlled team.
    /// Must pass in a name matching an existing team config file.
    /// </summary>
    /// <param name="teamName"></param>
    internal static void CreateAiTeam(string teamName, string aiName)
    {
        InputInterface inputInterface = Me.TeamManager.CreateTeam(teamName);

        ActivateTeamAi(inputInterface, aiName);
        Me.AiInterfaces.Add(inputInterface);
    }

    /// <summary>
    /// Removes all teams and mortals. Does not affect the maze itself.
    /// </summary>
    internal static void ClearTeams()
    {
        // Get all of the mortals in the maze.
        List<NavAgent> allMortals = new List<NavAgent>();
        foreach (InputInterface inputInterface in Me.AiInterfaces)
        {
            allMortals.AddRange(inputInterface.AllAgentsOnTeam);
        }
        if (Me.InputManager.PlayerInterface != null)
        {
            allMortals.AddRange(Me.InputManager.PlayerInterface.AllAgentsOnTeam);
        }

        // Remove them one by one.
        for (int i = 0; i < allMortals.Count; i++)
        {
            if (allMortals[i] != null)
            {
                Destroy(allMortals[i].gameObject);
            }
        }

        // Clear all of the AI scripts and interfaces as well.
        ModScriptRunner.ClearAiScripts();
        Me.AiInterfaces.Clear();
        Me.InputManager.PlayerInterface = null;
    }

    /// <summary>
    /// Turns on either the Minotaur lights (1) or the debugging floodlights (2).
    /// Will toggle if no argument is given.
    /// </summary>
    /// <param name="lightMode"></param>
    internal static void ActivateLights(int lightMode = -1)
    {
        switch (lightMode)
        {
            case 0: // All off.
                Me.MinotaurLight.SetActive(false);
                Me.FloodLight.SetActive(false);
                break;
            case 1: // Minotaur on.
                Me.MinotaurLight.SetActive(true);
                Me.FloodLight.SetActive(false);
                break;
            case 2: // Flood on.
                Me.MinotaurLight.SetActive(false);
                Me.FloodLight.SetActive(true);
                break;
            default:
                // Just toggle the Minotaur light.
                if (Me.MinotaurLight.activeInHierarchy || Me.FloodLight.activeInHierarchy)
                {
                    Me.MinotaurLight.SetActive(false);
                    Me.FloodLight.SetActive(false);
                }
                else
                    Me.MinotaurLight.SetActive(true);
                break;
        }
    }

    /// <summary>
    /// Register a team interface for use with the player Input Manager.
    /// </summary>
    /// <param name="inputInterface"></param>
    private static void ActivatePlayerControl(InputInterface inputInterface) => Me.InputManager.PlayerInterface = inputInterface;

    /// <summary>
    /// Create an AI for a team, and enable the AI to connect via an Input Buffer.
    /// </summary>
    /// <param name="inputInterface"></param>
    /// <param name="AiName"></param>
    private static void ActivateTeamAi(InputInterface inputInterface, string AiName)
    {
        InputBuffer inputBuffer = new InputBuffer(inputInterface);

        // Register the maze with the Visibility manager.
        Me.VisibilityManager.mazes.Add(inputBuffer.VisibleMaze);

        // Register the script with the mod interface.
        AiScriptRunner scriptRunner = new AiScriptRunner(Path.Combine("AiScripts", AiName));
        scriptRunner.RegisterObject("Interface", inputBuffer);
        scriptRunner.StartScript();
        ModScriptRunner.AddAiScript(scriptRunner);
    }
}
