﻿using System;
using System.Collections.Generic;
using UnityEngine;

internal class TeamManager : MonoBehaviour
{
    [SerializeField] internal GlobalActions GlobalActions = new GlobalActions();
    [SerializeField] internal Transform AiParent;
    [SerializeField] internal List<NavAgent> CharacterPrefabs = new List<NavAgent>();

    const string TeamConfigFileDirectory = "StreamingAssets/TeamDefinitions/";
    const string TeamConfigExtension = ".json";
    private int uniqueId = 0;

    internal int UniqueId => this.uniqueId++;

    internal InputInterface CreateTeam(string teamConfigFile)
    {
        this.GlobalActions.RegisterAllActions();
        string fullConfigFileName = TeamConfigFileDirectory + teamConfigFile + TeamConfigExtension;

        TeamConfigurationData teamConfigurationData = FileManager.LoadObject<TeamConfigurationData>(fullConfigFileName);

        InputInterface team = new InputInterface
        {
            TeamId = this.UniqueId
        };

        // Register all actions.
        foreach (string actionName in teamConfigurationData.TeamActionSet)
        {
            GenericAction action = this.GlobalActions.GetAction(actionName);

            if (action is KeyAction)
            {
                team.AddKeyAction(action as KeyAction);
            }
            else if (action is MouseAction)
            {
                team.AddMouseAction(action as MouseAction);
            }
            else if (action is KeyActivator)
            {
                team.AddKeyActivator(action as KeyActivator);
            }
            else throw new NotSupportedException();
        }

        // Add all NavAgents to the team.
        foreach (BaseAgent agentId in teamConfigurationData.TeamAgents)
        {
            foreach (NavAgent prefab in this.CharacterPrefabs)
            {
                if (prefab.name == agentId.AgentType)
                {
                    NavAgent agent = Instantiate(prefab, PositionManager.GridToWorldSpace(agentId.position), Quaternion.identity, this.AiParent );
                    team.SetActiveAgent(agent);
                }
            }
        }

        return team;
    }

    /// <summary>
    /// Destroys every NavAgent that has been created by the TeamManager.
    /// </summary>
    internal void ResetTeams()
    {
        Destroy(this.AiParent.gameObject);
        this.AiParent = new GameObject("Ais").transform;
    }
}
