﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
internal struct BaseAgent
{
    [SerializeField] internal string AgentType;
    [SerializeField] internal Vector2Int position;
}

[Serializable]
internal class TeamConfigurationData
{
    [SerializeField] internal List<BaseAgent> TeamAgents = new List<BaseAgent>();

    [SerializeField] internal List<string> TeamActionSet = new List<string>();
}
