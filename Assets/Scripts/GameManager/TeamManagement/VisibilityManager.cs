﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
internal class VisibilityManager
{
    [SerializeField] internal Dictionary<int, CharacterDescriptor> allAgents = new Dictionary<int, CharacterDescriptor>();
    [SerializeField] public WallPosition[,] MasterMaze = new WallPosition[0,0];
    [SerializeField] internal List<VisibleMaze> mazes = new List<VisibleMaze>();
    private bool MazeUpdateNeeded;

    internal void UpdateVisibility()
    {
        // Make sure that the maze is updated first.
        UpdateMasterMaze();

        foreach (VisibleMaze maze in this.mazes)
        {
            UpdateMazeVisibility(maze);
        }
    }

    private void UpdateMazeVisibility(VisibleMaze maze)
    {
        if (this.MazeUpdateNeeded) // Only update if the MazeManager reports a change.
        {
            if (this.MasterMaze.GetLength(0) != maze.MazeCells.GetLength(0) || this.MasterMaze.GetLength(1) != maze.MazeCells.GetLength(1))
            {
                // The maze size has changed! Re-size our array to match.
                maze.MazeCells = new WallDescriptor[this.MasterMaze.GetLength(0), this.MasterMaze.GetLength(1)];
                for (int x = 0; x < maze.MazeCells.GetLength(0); x++)
                    for (int y = 0; y < maze.MazeCells.GetLength(1); y++)
                        maze.MazeCells[x, y] = new WallDescriptor();
            }

            // Update each wall piece in the array with the latest position info.
            for (int x = 0; x < maze.MazeCells.GetLength(0); x++)
            {
                for (int y = 0; y < maze.MazeCells.GetLength(1); y++)
                {
                    // Update our wall position
                    maze.MazeCells[x, y].WallPosition = this.MasterMaze[x, y];
                    // TODO: maze.MazeCells[x, y].IsVisible = false;
                }
            }
        }

        // For now, just update the references to my NavAgents. In the future, will want independent copies.
        maze.AgentsNotOnMyTeam.Clear();
        maze.AgentsOnMyTeam.Clear();

        foreach (CharacterDescriptor agent in this.allAgents.Values)
        {
            if (agent.TeamId == maze.TeamId)
            {
                maze.AgentsOnMyTeam.Add(agent);
            }
            else
            {
                maze.AgentsNotOnMyTeam.Add(agent);
            }
        }
    }

    private void UpdateMasterMaze()
    {
        // Update all NavAgents.
        foreach (CharacterDescriptor navAgentDescriptor in this.allAgents.Values)
        {
            navAgentDescriptor.HasDied = true;
            navAgentDescriptor.Idle = true;
            // TODO: navAgentDescriptor.IsVisible = false;
        }

        foreach (NavAgent agent in CharacterManager.GetNavAgents())
        {
            Vector2 pos = CharacterManager.NavAgentToGridSpace(agent.Id);

            if (this.allAgents.ContainsKey(agent.Id))
            {
                CharacterDescriptor descriptor = this.allAgents[agent.Id];

                descriptor.Position_X = pos.x;
                descriptor.Position_Y = pos.y;
                descriptor.HasDied = false;
                descriptor.Idle = agent.Idle;
                descriptor.TeamId = agent.TeamId;
            }
            else
            {
                this.allAgents[agent.Id] = new CharacterDescriptor()
                {
                    HasDied = false,
                    Idle = agent.Idle,
                    // TODO: IsVisible = false,
                    TeamId = agent.TeamId,
                    Position_X = pos.x,
                    Position_Y = pos.y
                };
            }
        }

        // Update all maze segments.
        this.MazeUpdateNeeded = MazeManager.HasMazeChanged;
        if (this.MazeUpdateNeeded) // Only update if the MazeManager reports a change.
        {
            if (this.MasterMaze.GetLength(0) != MazeManager.MazeSize.x || this.MasterMaze.GetLength(1) != MazeManager.MazeSize.y)
            {
                // The maze size has changed! Re-size our array to match.
                this.MasterMaze = new WallPosition[MazeManager.MazeSize.x, MazeManager.MazeSize.y];
            }

            // Update each wall piece in the array with the latest position info.
            for (int x = 0; x < this.MasterMaze.GetLength(0); x++)
            {
                for (int y = 0; y < this.MasterMaze.GetLength(1); y++)
                {
                    WallDescriptor mazeWall = WallManager.GetWall(new Vector2Int(x, y));
                    this.MasterMaze[x, y] = (mazeWall == null) ? WallPosition.DOWN : mazeWall.WallPosition;
                }
            }
        }
    }
}