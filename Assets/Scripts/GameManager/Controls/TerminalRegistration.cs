﻿#pragma warning disable IDE0060 // Remove unused parameter using CommandTerminal;
using CommandTerminal;
using Interface;
using UnityEngine;

internal static class TerminalRegistration
{
    // --------------------------------------------------------------------------------------------------------------------------------------------------
    // Settings and navigation.
    // --------------------------------------------------------------------------------------------------------------------------------------------------
    [RegisterCommand(Name = "LoadMenu", Help = "Open the specified game menu.", MinArgCount = 1, MaxArgCount = 1)]
    private static void LoadMenu(CommandArg[] args) => MenuManager.LoadMenu(args[0].String);

    // --------------------------------------------------------------------------------------------------------------------------------------------------
    // Save manager functions.
    // --------------------------------------------------------------------------------------------------------------------------------------------------
    [RegisterCommand(Name = "SaveGame", Help = "Saves the game data to the default or specified file.", MinArgCount = 0, MaxArgCount = 1)]
    private static void SaveGame(CommandArg[] args)
    {
        if (args.Length == 0)
        {
            GameManager.SaveGame();
        }
        else
        {
            string saveFile = args[0].String;
            GameManager.SaveGame(saveFile);
        }
    }

    [RegisterCommand(Name = "LoadGame", Help = "Loads the game data from the default or specified file.", MinArgCount = 0, MaxArgCount = 1)]
    private static void LoadGame(CommandArg[] args)
    {
        if (args.Length == 0)
        {
            GameManager.LoadGame();
        }
        else
        {
            string saveFile = args[0].String;
            GameManager.LoadGame(saveFile);
        }
    }

    [RegisterCommand(Name = "SavePreferences", Help = "Saves the game settings to the default file.", MinArgCount = 0, MaxArgCount = 0)]
    private static void SaveSettings(CommandArg[] args) => GameManager.SavePreferences();

    [RegisterCommand(Name = "LoadPreferences", Help = "Loads the game settings from the default file.", MinArgCount = 0, MaxArgCount = 0)]
    private static void LoadSettings(CommandArg[] args) => GameManager.LoadPreferences();

    // --------------------------------------------------------------------------------------------------------------------------------------------------
    // Game Management functions.
    // --------------------------------------------------------------------------------------------------------------------------------------------------
    [RegisterCommand(Name = "NewGame", Help = "Starts a new game. If you want to change team, use 'Spartan' or 'Minotaur' to change.", MinArgCount = 0, MaxArgCount = 1)]
    private static void NewGame(CommandArg[] args)
    {
        // Change teams if it is relevant.
        if (args.Length == 1)
        {
            if (args[0].String.ToLower().Contains("m"))
                GameManager.NewGame("Minotaur", "Spartan");
            if (args[0].String.ToLower().Contains("s"))
                GameManager.NewGame("Spartan", "Minotaur");
        }
        else
        {
            GameManager.NewGame();
        }
    }

    [RegisterCommand(Name = "ClearGame", Help = "Removes all maze segments and mortals on the map.", MinArgCount = 0, MaxArgCount = 0)]
    private static void ClearGame(CommandArg[] args) => GameManager.ClearGame();

    // --------------------------------------------------------------------------------------------------------------------------------------------------
    // Maze Management functions.
    // --------------------------------------------------------------------------------------------------------------------------------------------------
    [RegisterCommand(Name = "NewMaze", Help = "Create a maze of the specified size", MinArgCount = 0, MaxArgCount = 2)]
    private static void NewMaze(CommandArg[] args)
    {
        if (args.Length == 0)
            GameManager.GenerateMaze();
        else if (args.Length == 1)
            GameManager.GenerateMaze(args[0].Int);
        else // args.Length == 2
            GameManager.GenerateMaze(new Vector2Int(args[0].Int, args[1].Int));
    }

    [RegisterCommand(Name = "ClearMaze", Help = "Removes all maze segments. Does not affect mortals.", MinArgCount = 0, MaxArgCount = 0)]
    private static void ClearMaze(CommandArg[] args) => GameManager.ClearMaze();


    // --------------------------------------------------------------------------------------------------------------------------------------------------
    // Team and AI management functions.
    // --------------------------------------------------------------------------------------------------------------------------------------------------
    [RegisterCommand(Name = "NewPlayerTeam", Help = "Creates a player controlled team. Use 'Spartan' or 'Minotaur' to specify the team you want.", MinArgCount = 1, MaxArgCount = 1)]
    private static void NewPlayerTeam(CommandArg[] args)
    {
        if (args[0].String.ToLower().Contains("m"))
            GameManager.CreatePlayerTeam("Minotaur");
        if (args[0].String.ToLower().Contains("s"))
            GameManager.CreatePlayerTeam("Spartan");
    }

    [RegisterCommand(Name = "NewAiTeam", Help = "Creates an AI controlled team. Use 'Spartan' or 'Minotaur' to specify the team you want.", MinArgCount = 1, MaxArgCount = 2)]
    private static void NewAiTeam(CommandArg[] args)
    {
        string aiName = "2_Medium";
        if (args.Length == 2)
            aiName = args[1].String;

        if (args[0].String.ToLower().Contains("m"))
            GameManager.CreateAiTeam("Minotaur", aiName);
        if (args[0].String.ToLower().Contains("s"))
            GameManager.CreateAiTeam("Spartan", aiName);
    }

    [RegisterCommand(Name = "ClearTeams", Help = "Removes all characters on the map.", MinArgCount = 0, MaxArgCount = 0)]
    private static void ClearTeams(CommandArg[] args) => GameManager.ClearTeams();

    // --------------------------------------------------------------------------------------------------------------------------------------------------
    // Ease of use and testing tools.
    // --------------------------------------------------------------------------------------------------------------------------------------------------

    [RegisterCommand(Name = "GameSpeed", Help = "Sets the timescale of the game. Higher is faster.", MinArgCount = 1, MaxArgCount = 1)]
    private static void GameSpeed(CommandArg[] args)
    {
        // Make sure we don't try to set a negative timescale.
        float timescale = args[0].Float;
        Time.timeScale = (timescale > 0) ? timescale : 0.1f;
    }

    [RegisterCommand(Name = "Lights", Help = "Toggles the \"heat vision\" lights or floodlights. 0 for no lights, 1 for Minotaur lights, 2 for floodlights.", MinArgCount = 0, MaxArgCount = 1)]
    private static void Lights(CommandArg[] args)
    {
        if (args.Length == 0)
            GameManager.ActivateLights();
        else
            GameManager.ActivateLights(args[0].Int);
    }

    // --------------------------------------------------------------------------------------------------------------------------------------------------
    // Script and unit test functions.
    // --------------------------------------------------------------------------------------------------------------------------------------------------
    [RegisterCommand(Name = "DebuggerOn", Help = "Turns on the debugger for Lua Scripts and AIs. See https://www.moonsharp.org/debugger.html for help attaching.", MinArgCount = 0, MaxArgCount = 0)]
    private static void EnableScriptDebugger(CommandArg[] args)
    {
        GameManager.Preferences.ModInterface.DebuggerEnabled = true;
        ModScriptRunner.LoadMoonSharp(GameManager.Preferences.ModInterface, false);
    }

    [RegisterCommand(Name = "DebuggerOff", Help = "Turns off the debugger for Lua Scripts and AIs. It will not take effect until the game is restarted.", MinArgCount = 0, MaxArgCount = 0)]
    private static void DisableScriptDebugger(CommandArg[] args) => GameManager.Preferences.ModInterface.DebuggerEnabled = false;

    [RegisterCommand(Name = "RunUnitTests", Help = "Runs the Lua file unit test suite. Can provide a seed, otherwise random is used", MinArgCount = 0, MaxArgCount = 1)]
    private static void RunUnitTests(CommandArg[] args)
    {
        int seed = args.Length == 0 ? (int)Random.value : args[0].Int;
        ModScriptRunner.LoadMoonSharp(GameManager.Preferences.ModInterface, true);
        _ = ModScriptRunner.Call("runUnitTestSuite", seed);
        ModScriptRunner.LoadMoonSharp(GameManager.Preferences.ModInterface, false);
    }

    [RegisterCommand(Name = "AiOnlyTestMatch", Help = "Runs a match between the two specified AIs." +
        " First AI is for the Minotaur, second is for the Spartans. Third paramater is the match count, empty for just one.", MinArgCount = 2, MaxArgCount = 3)]
    private static void RunAiMatch(CommandArg[] args)
    {
        GameManager.GenerateMaze();
        GameManager.CreateAiTeam("Minotaur", args[0].String);
        GameManager.CreateAiTeam("Spartan", args[1].String);
    }

    [RegisterCommand(Name = "ReloadModFiles", Help = "Reloads the Lua mod scripts. Does not affect AIs.", MinArgCount = 0, MaxArgCount = 0)]
    private static void ReloadScripts(CommandArg[] args) => ModScriptRunner.LoadMoonSharp(GameManager.Preferences.ModInterface, false);

    [RegisterCommand(Name = "Lua", Help = "Attempts to run the entered text as a Lua command", MinArgCount = 1)]
    private static void RunLuaCommand(CommandArg[] args) => ModScriptRunner.Call(string.Join(" ", args));

    // --------------------------------------------------------------------------------------------------------------------------------------------------
    // Hello.
    // --------------------------------------------------------------------------------------------------------------------------------------------------
    [RegisterCommand(Name = "HelloWorld", Help = "Echos :\"Hello, World!\" to the terminal output.", MinArgCount = 0, MaxArgCount = 0)]
    private static void HelloWorld(CommandArg[] args) => Logging.Log("Hello, World!", LoggingLevels.Important);
}

#pragma warning restore IDE0060 // Remove unused parameterusing CommandTerminal;