﻿using System;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Do something when a key is pressed. Each function will process a keystroke differently.
/// </summary>
internal delegate void ProcessKey();

/// <summary>
/// Maps a KeyCode to an Action. Each action can call up to 3 functions:
/// - ProcessKeyDown - call when Input.GetKeyDown() is true.
/// - ProcessKeyHold - call when Input.GetKey() is true
/// - ProcessKeyRelease - call when Input.GetKeyUp() is true.
/// Call OnKeyDown when the key is pressed (or a press is simulated by an AI.)
/// Call OnKeyDown when the key is released (or a release is simulated by an AI.)
/// Call Update once per frame to let a held key be processed properly.
/// </summary>
[System.Serializable]
internal class KeyAction : GenericAction, IKeyPress, IEquatable<KeyAction>
{
    internal KeyCode Key;
    internal ProcessKey ProcessKeyDown;
    internal ProcessKey ProcessKeyHold;
    internal ProcessKey ProcessKeyUp;
    private bool isKeyDown;

    /// <summary>
    /// Call from an AI script that wants to simulate a quick key press.
    /// Calls only the ProcessKeyDown delegate. To call other delegates, use OnKeyDown, Update, OnKeyUp.
    /// </summary>
    public void OnKeyTap()
    {
        // Process KeyDown.
        this.ProcessKeyDown?.Invoke();
    }

    /// <summary>
    /// Processes a "down and hold" key event. Each subsequent update will process a key event
    /// until a OnKeyUp event is sent.
    /// </summary>
    public void OnKeyDown()
    {
        // Process KeyDown.
        this.isKeyDown = true;
        this.ProcessKeyDown?.Invoke();
    }

    /// <summary>
    /// Processes a key release. Will trigger an OnKeyUp event, and will stop Update from invoking KeyHeld events.
    /// </summary>
    public void OnKeyUp()
    {
        // Process KeyUp.
        this.isKeyDown = false;
        this.ProcessKeyUp?.Invoke();
    }

    /// <summary>
    /// Processes any held keys. Held and released keys are defined by OnKeyDown and OnKeyUp events.
    /// This class does not check or interact with Unity's Input class directly.
    /// </summary>
    internal void Update()
    {
        if (this.isKeyDown)
        {
            // Process KeyHold.
            this.ProcessKeyHold?.Invoke();
        }
    }

    public override bool Equals(object obj) => Equals(obj as KeyAction);

    public bool Equals(KeyAction other)
    {
        return other != null &&
               this.Action == other.Action &&
               this.Key == other.Key &&
               EqualityComparer<ProcessKey>.Default.Equals(this.ProcessKeyDown, other.ProcessKeyDown) &&
               EqualityComparer<ProcessKey>.Default.Equals(this.ProcessKeyHold, other.ProcessKeyHold) &&
               EqualityComparer<ProcessKey>.Default.Equals(this.ProcessKeyUp, other.ProcessKeyUp);
    }

    public override int GetHashCode()
    {
        int hashCode = 1382501964;
        hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(this.Action);
        hashCode = hashCode * -1521134295 + this.Key.GetHashCode();
        hashCode = hashCode * -1521134295 + EqualityComparer<ProcessKey>.Default.GetHashCode(this.ProcessKeyDown);
        hashCode = hashCode * -1521134295 + EqualityComparer<ProcessKey>.Default.GetHashCode(this.ProcessKeyHold);
        hashCode = hashCode * -1521134295 + EqualityComparer<ProcessKey>.Default.GetHashCode(this.ProcessKeyUp);
        return hashCode;
    }

    public static bool operator ==(KeyAction left, KeyAction right) => EqualityComparer<KeyAction>.Default.Equals(left, right);

    public static bool operator !=(KeyAction left, KeyAction right) => !(left == right);
}
