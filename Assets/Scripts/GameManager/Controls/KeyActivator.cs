﻿using System;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Maps a KeyCode to a mouse action. Each KeyCode can enable one or two
/// MouseActions. The DefaultMouseAction is used when the key is not pressed.
/// The AlternativeMouseAction is used when the key is pressed.
/// </summary>
[System.Serializable]
internal class KeyActivator : GenericAction, IKeyPress, IMouseClick, IEquatable<KeyActivator>
{
    /// <summary>
    /// Distinguishes between activators that should be held (like Shift) and
    /// activators that should be toggled on and off (like Caps Lock).
    /// </summary>
    internal enum KeyActivationType
    {
        ToggleKey,
        HoldKey
    }

    internal KeyCode Key;
    internal KeyActivationType ActivationType;

    internal MouseAction DefaultMouseAction;
    internal MouseAction AlternativeMouseAction;
    private bool isKeyDown;

    private MouseAction ActiveMouseAction => this.isKeyDown ? this.AlternativeMouseAction : this.DefaultMouseAction;

    /// <summary>
    /// Call from an AI script that wants to simulate a quick key press.
    /// Calls only the ProcessKeyDown delegate. To call other delegates, use OnKeyDown, Update, OnKeyUp.
    /// </summary>
    public void OnKeyTap()
    {
        OnKeyDown();
        OnKeyUp();
    }

    /// <summary>
    /// Processes a "down and hold" key event. Each subsequent update will process a key event
    /// until a OnKeyUp event is sent.
    /// </summary>
    public void OnKeyDown() => this.isKeyDown = !this.isKeyDown;

    /// <summary>
    /// Processes a key release. Will trigger an OnKeyUp event, and will stop Update from invoking KeyHeld events.
    /// </summary>
    public void OnKeyUp()
    {
        if (this.ActivationType == KeyActivationType.HoldKey)
            this.isKeyDown = !this.isKeyDown;
    }

    /// <summary>
    /// Press and release the mouse in short succession. Only uses one coordinate location.
    /// </summary>
    public void OnMouseClick(Vector2 mouseDownLocation) => this.ActiveMouseAction?.OnMouseClick(mouseDownLocation);

    /// <summary>
    /// Process an ongoing mouse click and drag. The mouseDownLocation should not change during the drag. 
    /// </summary>
    public void OnMouseDrag(Vector2 mouseDownLocation, Vector2 mouseUpLocation) => this.ActiveMouseAction?.OnMouseDrag(mouseDownLocation, mouseUpLocation);

    /// <summary>
    /// Finishes processing a mouse click and drag. The mouseDownLocation should not change during the drag. 
    /// </summary>
    public void OnMouseDragRelease(Vector2 mouseDownLocation, Vector2 mouseUpLocation) => this.ActiveMouseAction?.OnMouseDragRelease(mouseDownLocation, mouseUpLocation);

    public bool Equals(KeyActivator other) => Equals((object) other);

    public override bool Equals(object obj) => obj is KeyActivator activator
                                               && this.Action == activator.Action
                                               && this.ActivationType == activator.ActivationType
                                               && this.Key == activator.Key
                                               && EqualityComparer<MouseAction>.Default.Equals(this.DefaultMouseAction, activator.DefaultMouseAction)
                                               && EqualityComparer<MouseAction>.Default.Equals(this.AlternativeMouseAction, activator.AlternativeMouseAction);

    public override int GetHashCode()
    {
        int hashCode = 605309945;
        hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(this.Action);
        hashCode = hashCode * -1521134295 + this.ActivationType.GetHashCode();
        hashCode = hashCode * -1521134295 + this.Key.GetHashCode();
        hashCode = hashCode * -1521134295 + EqualityComparer<MouseAction>.Default.GetHashCode(this.DefaultMouseAction);
        hashCode = hashCode * -1521134295 + EqualityComparer<MouseAction>.Default.GetHashCode(this.AlternativeMouseAction);
        return hashCode;
    }

    public static bool operator ==(KeyActivator left, KeyActivator right) => EqualityComparer<KeyActivator>.Default.Equals(left, right);
    public static bool operator !=(KeyActivator left, KeyActivator right) => !(left == right);
}
