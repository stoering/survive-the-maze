﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
internal class InputInterface
{
    internal InputInterface() => RegisterDefaultKeyActions();

    [SerializeField] private List<NavAgent> teamAgents = new List<NavAgent>();
    [SerializeField] private List<NavAgent> activeNavAgents = new List<NavAgent>();
    [SerializeField] private Dictionary<KeyCode, IKeyPress> keyActions = new Dictionary<KeyCode, IKeyPress>();
    [SerializeField] private List<IMouseClick> mouseActions = new List<IMouseClick>();
    [SerializeField] private float selectionRadius = 2;
    [SerializeField] private int teamId;

    /// <summary>
    /// Returns the list of all NavAgents currently ready to accept inputs through this InputInterface.
    /// All Agents in this list will receive MouseInput and KeyInput commands from the interface.
    /// </summary>
    internal List<NavAgent> ActiveAgents => this.activeNavAgents;

    /// <summary>
    /// Returns the list of all agents on the team, whether they are ready to accept inputs or not.
    /// All Agents in this list will receive MouseInput and KeyInput commands from the interface if selected first.
    /// </summary>
    internal List<NavAgent> AllAgentsOnTeam => this.teamAgents;

    /// <summary>
    /// Indicates if the "Modify" key is currently held (typically Shift).
    /// </summary>
    internal bool ModifySelect { get; private set; } = false;

    /// <summary>
    /// Returns the TeamId value for the interface and NavAgent.
    /// Sets the TeamId value for the InputInterface as well as its NavAgent.
    /// </summary>
    internal virtual int TeamId
    {
        get => this.teamId;
        set
        {
            foreach (NavAgent agent in this.teamAgents)
                agent.TeamId = value;
            this.teamId = value;
        }
    }

    protected float SelectionRadius => this.selectionRadius;

    /// <summary>
    /// Returns a readonly list of all keyactions defined by the interface.
    /// CAN NOT BE USED TO ADD KEYACTIONS TO THE INTERFACE. Use AddKeyAction instead.
    /// </summary>
    internal List<IKeyPress> KeyAction => new List<IKeyPress>(this.keyActions.Values);

    /// <summary>
    /// Checks if the interface has defined an action for a particular keycode.
    /// Returns the action if it exists. Throws a KeyNotFoundException if it does not.
    /// </summary>
    /// <param name="keyCode"></param>
    /// <returns></returns>
    internal IKeyPress GetKeyAction(KeyCode keyCode) => this.keyActions[keyCode];
    /// <summary>
    /// Checks if the interface has defined an action for a particular keycode.
    /// Returns the action if it exists. Throws a KeyNotFoundException if it does not.
    /// </summary>
    /// <param name="actionName"></param>
    /// <returns></returns>
    internal IKeyPress GetKeyAction(string actionName)
    {
        KeyAction keyAction = null;

        foreach (KeyAction action in this.keyActions.Values)
        {
            if (action.Action == actionName)
            {
                keyAction = action;
                break;
            }
        }
        if (keyAction == null)
        {
            // No key was found.
            throw new KeyNotFoundException(actionName + " is not a registered key action!");
        }

        return keyAction;
    }

    /// <summary>
    /// Registers the default input keyActions for this interface. For the base class, just registers the shift
    /// keys to "modify" clicks via the ModifySelect bool.
    /// </summary>
    internal virtual void RegisterDefaultKeyActions()
    {
        ClearKeyActions();

        // Create keyActions for "shift," used for multiple-select
        KeyAction modify = new KeyAction
        {
            Action = "Shift",
            Key = KeyCode.LeftShift,
            ProcessKeyDown = EnableShift,
            ProcessKeyUp = DisableShift
        };
        AddKeyAction(modify);
        KeyAction modify_alt = new KeyAction
        {
            Action = "Shift_Alt",
            Key = KeyCode.RightShift,
            ProcessKeyDown = EnableShift,
            ProcessKeyUp = DisableShift
        };
        AddKeyAction(modify_alt);

        // Create key-actions to select an agent via a keystroke.
        AddKeyAction(new KeyAction { Action = "Agent0", Key = KeyCode.Alpha0, ProcessKeyDown = SelectAgent_0, });
        AddKeyAction(new KeyAction { Action = "Agent1", Key = KeyCode.Alpha1, ProcessKeyDown = SelectAgent_1, });
        AddKeyAction(new KeyAction { Action = "Agent2", Key = KeyCode.Alpha2, ProcessKeyDown = SelectAgent_2, });
        AddKeyAction(new KeyAction { Action = "Agent3", Key = KeyCode.Alpha3, ProcessKeyDown = SelectAgent_3, });
        AddKeyAction(new KeyAction { Action = "Agent4", Key = KeyCode.Alpha4, ProcessKeyDown = SelectAgent_4, });
        AddKeyAction(new KeyAction { Action = "Agent5", Key = KeyCode.Alpha5, ProcessKeyDown = SelectAgent_5, });
        AddKeyAction(new KeyAction { Action = "Agent6", Key = KeyCode.Alpha6, ProcessKeyDown = SelectAgent_6, });
        AddKeyAction(new KeyAction { Action = "Agent7", Key = KeyCode.Alpha7, ProcessKeyDown = SelectAgent_7, });
        AddKeyAction(new KeyAction { Action = "Agent8", Key = KeyCode.Alpha8, ProcessKeyDown = SelectAgent_8, });
        AddKeyAction(new KeyAction { Action = "Agent9", Key = KeyCode.Alpha9, ProcessKeyDown = SelectAgent_9, });

        MouseAction tryActivate = new MouseAction
        {
            Action = "TryActivateAgent",
            ProcessMouseClick = TryActivateAgent,
            ProcessMouseDragRelease = TryActivateAgents
        };
        AddMouseAction(tryActivate);
    }

    /// <summary>
    /// Adds the new KeyAction to the interface. Multiple actions cannot share the same key.
    /// Will throw an ArgumentException if the key already exists.
    /// </summary>
    /// <param name="keyAction"></param>
    internal void AddKeyAction(KeyAction keyAction) => this.keyActions.Add(keyAction.Key, keyAction);

    internal void AddKeyActivator(KeyActivator keyActivator)
    {
        this.keyActions.Add(keyActivator.Key, keyActivator);
        this.mouseActions.Add(keyActivator);
    }

    /// <summary>
    /// Adds the new MouseAction to the interface.
    /// </summary>
    /// <param name="mouseAction"></param>
    internal void AddMouseAction(IMouseClick mouseAction) => this.mouseActions.Add(mouseAction);

    /// <summary>
    /// Removes all KeyActions tracked by this interface.
    /// </summary>
    internal void ClearKeyActions() => this.keyActions.Clear();

    /// <summary>
    /// Call from an AI script that wants to simulate a quick key press.
    /// Calls only the ProcessKeyDown delegate. To call other delegates, use OnKeyDown, Update, OnKeyUp.
    /// </summary>
    /// <param name="Key"></param>
    internal void OnKeyTap(KeyCode Key) => this.keyActions[Key].OnKeyTap();
    /// <summary>
    /// Call from an AI script that wants to simulate a quick key press.
    /// Calls only the ProcessKeyDown delegate. To call other delegates, use OnKeyDown, Update, OnKeyUp.
    /// </summary>
    /// <param name="actionName"></param>
    internal void OnKeyTap(string actionName) => GetKeyAction(actionName).OnKeyTap();

    /// <summary>
    /// Processes a "down and hold" key event. Each subsequent update will process a key event
    /// until a OnKeyUp event is sent.
    /// </summary>
    /// <param name="Key"></param>
    internal void OnKeyDown(KeyCode Key) => this.keyActions[Key].OnKeyDown();
    /// <summary>
    /// Processes a "down and hold" key event. Each subsequent update will process a key event
    /// until a OnKeyUp event is sent.
    /// </summary>
    /// <param name="actionName"></param>
    internal void OnKeyDown(string actionName) => GetKeyAction(actionName).OnKeyDown();

    /// <summary>
    /// Processes a key release. Will trigger an OnKeyUp event, and will stop Update from invoking KeyHeld events.
    /// </summary>
    /// <param name="Key"></param>
    internal void OnKeyUp(KeyCode Key) => this.keyActions[Key].OnKeyUp();
    /// <summary>
    /// Processes a key release. Will trigger an OnKeyUp event, and will stop Update from invoking KeyHeld events.
    /// </summary>
    /// <param name="actionName"></param>
    internal void OnKeyUp(string actionName) => GetKeyAction(actionName).OnKeyUp();

    /// <summary>
    /// Turns a left-click from the player or an emulated left-click from a computer player
    /// into a command. Interact commands are "select" or "do thing" style commands.
    /// </summary>
    /// <param name="gridLocation"></param>
    internal virtual void Interact(Vector2 mouseDownLocation)
    {
        foreach (IMouseClick mouseAction in this.mouseActions)
        {
            mouseAction.OnMouseClick(mouseDownLocation);
        }
    }

    /// <summary>
    /// Turns a two point click (mouse down, mouse up) from a player or computer player
    /// into a command. Interact commands are "select" or "do thing" style commands.
    /// </summary>
    /// <param name="mouseDownLocation"></param>
    /// <param name="mouseUpLocation"></param>
    internal virtual void Interact(Vector2 mouseDownLocation, Vector2 mouseUpLocation)
    {
        foreach (IMouseClick mouseAction in this.mouseActions)
        {
            mouseAction.OnMouseDragRelease(mouseDownLocation, mouseUpLocation);
        }
    }

    /// <summary>
    /// Send a command to the Nav Agent to move to the specified location.
    /// Overridable, but contains basic move and follow functionality.
    /// </summary>
    /// <param name="gridLocation"></param>
    /// <param name="isPlayerInput"></param>
    internal virtual void Move(Vector2 mouseDownLocation)
    {
        Vector3 destination = PositionManager.GridToWorldSpace(mouseDownLocation);

        if (CharacterManager.DistanceToNearestEnemyNavAgent(mouseDownLocation, this.TeamId) < this.SelectionRadius)
        {
            foreach (NavAgent agent in this.activeNavAgents)
                if (agent != null)
                    agent.SetTarget(CharacterManager.GetNavAgent(CharacterManager.GetNearestEnemyNavAgentId(mouseDownLocation, this.TeamId)).transform);
        }
        else
        {
            if (this.ModifySelect)
            {
                foreach (NavAgent agent in this.activeNavAgents)
                    if (agent != null)
                        agent.QueueDestination(destination);
            }
            else
            {
                foreach (NavAgent agent in this.activeNavAgents)
                    if (agent != null)
                        agent.SetDestination(destination);
            }
        }
    }

    /// <summary>
    /// Processes any held keys. Held and released keys are defined by OnKeyDown and OnKeyUp events.
    /// This class does not check or interact with Unity's Input class directly.
    /// </summary>
    internal void Update()
    {
        foreach (KeyAction action in this.KeyAction)
        {
            action.Update();
        }
    }

    /// <summary>
    /// Method set the enable or disable the alternative click action.
    /// </summary>
    private void EnableShift() => this.ModifySelect = true;
    private void DisableShift() => this.ModifySelect = false;

    /// <summary>
    /// Adds a NavAgent to the Input Interface and changes the agent's TeamId to match the
    /// interface's TeamId.
    /// </summary>
    /// <param name="navAgent"></param>
    internal void AddAgentToTeam(NavAgent navAgent)
    {
        navAgent.TeamId = this.TeamId;
        this.teamAgents.Add(navAgent);
    }

    /// <summary>
    /// Removes all NavAgents from this team Interface.
    /// Sets each NavAgent's TeamId back to zero.
    /// </summary>
    internal void ClearTeam()
    {
        foreach (NavAgent agent in this.teamAgents)
        {
            if (agent != null)
                agent.TeamId = 0;
        }
        this.teamAgents.Clear();
        this.ActiveAgents.Clear();
    }

    /// <summary>
    /// Turns a left-click from the player or an emulated left-click from a computer player
    /// into a command. Interact commands are "select" or "do thing" style commands.
    /// </summary>
    /// <param name="gridLocation"></param>
    internal void TryActivateAgent(Vector2 gridLocation)
    {
        // If we are clicking on a NavAgent on my team, we should change to that agent.
        int nearestAgent = CharacterManager.GetNearestNavAgentId(gridLocation, this.teamAgents.Select(i => i.Id).ToList());

        if (nearestAgent != CharacterDescriptor.InvalidCharacterId && CharacterManager.DistanceToNavAgent(gridLocation, nearestAgent) < this.SelectionRadius)
        {
            if (this.ModifySelect)
            {
                if (!this.ActiveAgents.Contains((NavAgent)nearestAgent))
                    this.ActiveAgents.Add((NavAgent)nearestAgent);
            }
            else
            {
                this.activeNavAgents.Clear();
                this.activeNavAgents.Add((NavAgent)nearestAgent);
            }
        }
    }

    /// <summary>
    /// Turns a two point click (mouse down, mouse up) from a player or computer player
    /// into a command. Interact commands are "select" or "do thing" style commands.
    /// </summary>
    /// <param name="mouseDownLocation"></param>
    /// <param name="mouseUpLocation"></param>
    internal void TryActivateAgents(Vector2 mouseDownLocation, Vector2 mouseUpLocation)
    {
        // Convert the mouse locations into a rectangle representing the drag.
        Rect mouseDrag = new Rect(
            Mathf.Min(mouseDownLocation.x, mouseUpLocation.x) - 0.001f,
            Mathf.Min(mouseDownLocation.y, mouseUpLocation.y) - 0.001f,
            Mathf.Abs(mouseDownLocation.x - mouseUpLocation.x) + 0.002f,
            Mathf.Abs(mouseDownLocation.y - mouseUpLocation.y) + 0.002f);

        List<NavAgent> selectedAgents = new List<NavAgent>();

        // Find all agents on the team that are inside the rectangle.
        foreach (NavAgent agent in this.teamAgents)
        {
            Vector2 agentPosition = CharacterManager.NavAgentToGridSpace(agent.Id);

            if (mouseDrag.Contains(agentPosition))
            {
                selectedAgents.Add(CharacterManager.GetNavAgent(agent.Id));
            }
        }

        // If we are not holding "Shift," then we should select only the agents in the square.
        if (!this.ModifySelect)
        {
            this.ActiveAgents.Clear();
        }

        foreach (NavAgent agent in selectedAgents)
        {
            if (!this.ActiveAgents.Contains(agent))
            {
                this.ActiveAgents.Add(agent);
            }
        }
    }

    /// <summary>
    /// Sets the active agent by AgentId.
    /// </summary>
    /// <param name="navAgent"></param>
    internal void SetActiveAgent(NavAgent navAgent)
    {
        if (navAgent != null)
        {
            this.ActiveAgents.Clear();
            this.ActiveAgents.Add(navAgent);

            if (!this.teamAgents.Contains(navAgent))
            {
                AddAgentToTeam(navAgent);
            }
        }
    }

    /// <summary>
    /// Private function to select an agent via a keyaction press.
    /// </summary>
    /// <param name="agentIndex"></param>
    private void SelectAgent(int agentIndex)
    {
        if (this.teamAgents.Count > agentIndex)
        {
            NavAgent agent = (NavAgent)this.teamAgents[agentIndex];
            if (agent != null)
            {
                if (this.ModifySelect)
                {
                    if (!this.ActiveAgents.Contains(agent))
                        this.ActiveAgents.Add(agent);
                }
                else
                {
                    this.activeNavAgents.Clear();
                    this.activeNavAgents.Add(agent);
                }
            }
        }
    }
    private void SelectAgent_1() => SelectAgent(0); private void SelectAgent_2() => SelectAgent(1); private void SelectAgent_3() => SelectAgent(2);
    private void SelectAgent_4() => SelectAgent(3); private void SelectAgent_5() => SelectAgent(4); private void SelectAgent_6() => SelectAgent(5);
    private void SelectAgent_7() => SelectAgent(6); private void SelectAgent_8() => SelectAgent(7); private void SelectAgent_9() => SelectAgent(8);
    private void SelectAgent_0() => SelectAgent(9); // A collection of void delegates all call into SelectAgent. These are passed to KeyAction scripts.
}
