﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

internal class GenericAction
{
    internal string Action;
}

[Serializable]
internal class GlobalActions
{
    [SerializeField] private GameObject light;
    [SerializeField] private List<GenericAction> availableActions;

    internal GameObject Light { get => this.light; set => this.light = value; }

    internal void RegisterAllActions()
    {
        this.availableActions = new List<GenericAction>
        {
            new KeyAction
            {
                Action = "Lights",
                Key = KeyCode.L,
                ProcessKeyDown = Lights
            },
            new MouseAction
            {
                Action = "ToggleWall",
                ProcessMouseClick = ToggleWall
            }
        };
    }

    internal GenericAction GetAction(string actionName) => this.availableActions.First(a => a.Action == actionName);

    // -----------------------------------------------------------------------------------------------------
    // Global Key or Mouse actions defined below this point.
    // -----------------------------------------------------------------------------------------------------

    /// <summary>
    /// Raise or lower a wall segment in the maze.
    /// </summary>
    internal void ToggleWall(Vector2 gridLocation)
    {
        // Send the grid coordinates to the map in order to move the walls.
        Vector2Int gridCoordinate = Vector2Int.RoundToInt(gridLocation);

        if (WallManager.IsWallPositionValid(gridCoordinate))
        {
            WallManager.ToggleWall(gridCoordinate);
        }
    }

    /// <summary>
    /// Activates the "Lights" gameobject if it's inactive, and deactivates it if it is active.
    /// </summary>
    internal void Lights()
    {
        if (this.Light != null)
        {
            this.Light.SetActive(!this.Light.activeSelf);
        }
    }
}
