using UnityEngine;

public interface IMouseClick
{
    /// <summary>
    /// Press and release the mouse in short succession. Only uses one coordinate location.
    /// </summary>
    void OnMouseClick(Vector2 mouseDownLocation);

    /// <summary>
    /// Process an ongoing mouse click and drag. The mouseDownLocation should not change during the drag. 
    /// </summary>
    void OnMouseDrag(Vector2 mouseDownLocation, Vector2 mouseUpLocation);

    /// <summary>
    /// Finishes processing a mouse click and drag. The mouseDownLocation should not change during the drag. 
    /// </summary>
    void OnMouseDragRelease(Vector2 mouseDownLocation, Vector2 mouseUpLocation);
}
