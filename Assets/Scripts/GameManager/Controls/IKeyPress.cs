﻿
public interface IKeyPress
{
    /// <summary>
    /// Call from an AI script that wants to simulate a quick key press.
    /// Calls only the ProcessKeyDown delegate. To call other delegates, use OnKeyDown, Update, OnKeyUp.
    /// </summary>
    void OnKeyTap();

    /// <summary>
    /// Processes a "down and hold" key event. Each subsequent update will process a key event
    /// until a OnKeyUp event is sent.
    /// </summary>
    void OnKeyDown();

    /// <summary>
    /// Processes a key release. Will trigger an OnKeyUp event, and will stop Update from invoking KeyHeld events.
    /// </summary>
    void OnKeyUp();
}
