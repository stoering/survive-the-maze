﻿using System;
using UnityEngine;

[Serializable]
public class MenuButtonData
{
    public string ButtonText;
    public string Action;

    public Vector2 buttonPosition;
    public Vector2 buttonSize;

    public float TextScale;
}
