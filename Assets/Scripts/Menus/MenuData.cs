﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class MenuData
{
    public string Name;
    public string ActionOnLoad;

    public Vector2 MenuSize = Vector2.zero;
    public Vector2 BorderSizes = Vector2.zero;

    public List<MenuButtonData> menuButtons = new List<MenuButtonData>();
}
