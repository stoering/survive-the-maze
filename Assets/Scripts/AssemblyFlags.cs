﻿using System.Diagnostics.CodeAnalysis;
using System.Runtime.CompilerServices;

// Used to share Start, Update, etc. with unit tests without allowing classes to call each other's Unity funcitons.
[assembly: InternalsVisibleTo("Tests_Core")]
[assembly: InternalsVisibleTo("Tests_Integration")]
[assembly: SuppressMessage("Style", "IDE0044:Add readonly modifier", Justification = "Unity accesses private variables during serialization, so they are not truly readonly.")] 
