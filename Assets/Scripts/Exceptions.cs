﻿using System;

internal class SaveFileException : Exception
{
    internal SaveFileException(string s) : base(s) { }
}

internal class LoadFileException : Exception
{
    internal LoadFileException(string s) : base(s) { }
}

internal class ObjectNotFoundException : Exception
{
    internal ObjectNotFoundException(string s) : base(s) { }
}

internal class SingletonReInstantiatedException : Exception
{
    internal SingletonReInstantiatedException(string s) : base(s) { }
}

internal class InvalidIndexException : Exception
{
    internal InvalidIndexException(string s) : base(s) { }
}

internal class NullPrefabException : NullReferenceException
{
    internal NullPrefabException(string s) : base(s) { }
}
