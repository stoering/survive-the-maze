﻿using MoonSharp.Interpreter;
using System;

[Serializable]
[MoonSharpUserData]
public class CharacterDescriptor
{
    internal const int InvalidCharacterId = -1;

    public bool Idle;
    public bool HasDied;

    public int TeamId;
    public int UniqueCharacterId = InvalidCharacterId;

    public float Position_X;
    public float Position_Y;
}
