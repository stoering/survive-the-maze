﻿using MoonSharp.Interpreter;
using System;

/// <summary>
/// Describes the position and velocity of a wall segment.
/// </summary>
public enum WallPosition
{
    DOWN = 0,
    RISING = 1,
    UP = 2,
    FALLING = 3,
}

/// <summary>
/// Describes the wall segment's current state (existance and permanance)
/// </summary>
public enum WallType
{
    UNDEFINED = 0,
    MOBILE = 1,
    FIXED_DOWN = 2,
    FIXED_UP = 3,
}

/// <summary>
/// Contains all of the unique data a wall segment can contain.
/// Can be shared with the AIs and saved to file.
/// </summary>
[Serializable]
[MoonSharpUserData]
public class WallDescriptor
{
    public WallType WallType;

    public WallPosition WallPosition;
}
