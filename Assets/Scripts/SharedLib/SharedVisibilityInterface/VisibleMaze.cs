﻿using MoonSharp.Interpreter;
using System;
using System.Collections.Generic;

[Serializable]
[MoonSharpUserData]
public class VisibleMaze
{
    internal VisibleMaze()
    {
        this.AgentsOnMyTeam = new List<CharacterDescriptor>();
        this.AgentsNotOnMyTeam = new List<CharacterDescriptor>();
        this.MazeCells = new WallDescriptor[0, 0];
    }

    public int TeamId;
    public WallDescriptor[,] MazeCells;
    public List<CharacterDescriptor> AgentsOnMyTeam;
    public List<CharacterDescriptor> AgentsNotOnMyTeam;
}
