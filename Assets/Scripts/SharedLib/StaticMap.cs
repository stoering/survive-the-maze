﻿using MoonSharpRegistration;
using UnityEngine;

namespace SharedLib
{
    internal class StaticMap : MonoBehaviour
    {
        /// <summary>
        /// Prints "Hello, World!" Used to validate scripts
        /// </summary>
        [PassToModScriptRunner] private static void HelloWorld() => Logging.Log("Hello, World!", LoggingLevels.Important);

        // Maze Segment Management Functions

        /// <summary>
        /// Creates a maze wall at the specified coordinates.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns>False if segment already exists</returns>
        [PassToModScriptRunner] internal static void CreateMazeSegment(int x, int y) => WallManager.CreateWall(new Vector2Int(x, y), WallPosition.UP);

        /// <summary>
        /// Removes a maze wall at the specified coordinates.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        [PassToModScriptRunner] internal static void RemoveMazeSegment(int x, int y) => WallManager.DestroyWall(new Vector2Int(x, y));

        /// <summary>
        /// Removes a maze wall at the specified coordinates.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        [PassToModScriptRunner] internal static void ToggleMazeSegment(int x, int y) => WallManager.ToggleWall(new Vector2Int(x, y));

        // Input Interface Callback Functions

        /// <summary>
        /// Moves the specified agent to the specified location.
        /// </summary>
        /// <param name="agentId"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        [PassToModScriptRunner]
        internal static void MoveAgentTo(int agentId, int x, int y) =>
            CharacterManager.GetNavAgent(agentId).SetDestination(
            new Vector3(x - (MazeManager.MazeSize.x / 2),
                        0,
                        y - (MazeManager.MazeSize.y / 2)));

    }
}
