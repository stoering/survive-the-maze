﻿using MoonSharp.Interpreter;
using UnityEngine;

[MoonSharpUserData]
internal class InputBuffer
{
    internal InputBuffer(InputInterface iInterface)
    {
        this.inputInterface = iInterface;
        this.VisibleMaze = new VisibleMaze()
        {
            TeamId = iInterface.TeamId,
        };
    }

    [MoonSharpHidden]
    private InputInterface inputInterface;

    /// <summary>
    /// Maze is used to share game data with the particular AI. No other AI can see this VisibleMaze.
    /// </summary>
    public VisibleMaze VisibleMaze;

    /// <summary>
    /// Call from an AI script that wants to simulate a quick key press.
    /// Calls only the ProcessKeyDown delegate. To call other delegates, use OnKeyDown, Update, OnKeyUp.
    /// </summary>
    /// <param name="Key"></param>
    public void OnKeyTap(KeyCode Key) => this.inputInterface.OnKeyTap(Key);
    /// <summary>
    /// Call from an AI script that wants to simulate a quick key press.
    /// Calls only the ProcessKeyDown delegate. To call other delegates, use OnKeyDown, Update, OnKeyUp.
    /// </summary>
    /// <param name="actionName"></param>
    public void OnKeyTap(string actionName) => this.inputInterface.OnKeyTap(actionName);

    /// <summary>
    /// Processes a "down and hold" key event. Each subsequent update will process a key event
    /// until a OnKeyUp event is sent.
    /// </summary>
    /// <param name="Key"></param>
    public void OnKeyDown(KeyCode Key) => this.inputInterface.OnKeyDown(Key);
    /// <summary>
    /// Processes a "down and hold" key event. Each subsequent update will process a key event
    /// until a OnKeyUp event is sent.
    /// </summary>
    /// <param name="actionName"></param>
    public void OnKeyDown(string actionName) => this.inputInterface.OnKeyDown(actionName);

    /// <summary>
    /// Processes a key release. Will trigger an OnKeyUp event, and will stop Update from invoking KeyHeld events.
    /// </summary>
    /// <param name="Key"></param>
    public void OnKeyUp(KeyCode Key) => this.inputInterface.OnKeyUp(Key);
    /// <summary>
    /// Processes a key release. Will trigger an OnKeyUp event, and will stop Update from invoking KeyHeld events.
    /// </summary>
    /// <param name="actionName"></param>
    public void OnKeyUp(string actionName) => this.inputInterface.OnKeyUp(actionName);

    /// <summary>
    /// Turns a left-click from the player or an emulated left-click from a computer player
    /// into a command. Interact commands are "select" or "do thing" style commands.
    /// </summary>
    /// <param name="gridLocation"></param>
    public void Interact(Vector2 mouseDownLocation) => this.inputInterface.Interact(mouseDownLocation);
    public void Interact(int x, int y) => Interact(new Vector2(x, y));

    /// <summary>
    /// Turns a two point click (mouse down, mouse up) from a player or computer player
    /// into a command. Interact commands are "select" or "do thing" style commands.
    /// </summary>
    /// <param name="mouseDownLocation"></param>
    /// <param name="mouseUpLocation"></param>
    public void Interact(Vector2 mouseDownLocation, Vector2 mouseUpLocation) => this.inputInterface.Interact(mouseDownLocation, mouseUpLocation);

    /// <summary>
    /// Send a command to the Nav Agent to move to the specified location.
    /// Overridable, but contains basic move and follow functionality.
    /// </summary>
    /// <param name="gridLocation"></param>
    /// <param name="isPlayerInput"></param>
    public void Move(Vector2 mouseDownLocation) => this.inputInterface.Move(mouseDownLocation);
    public void Move(int x, int y) => Move(new Vector2(x, y));
}
