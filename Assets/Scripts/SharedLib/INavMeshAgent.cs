﻿using UnityEngine;

namespace SharedLib
{
    internal interface INavMeshAgent
    {
        bool PathPending { get; }
        float RemainingDistance { get; }
        bool AutoBraking { get; set; }
        bool AutoRepath { get; set; }

        bool SetDestination(Vector3 target);
    }

    internal class NavMeshAgent : INavMeshAgent
    {
        private readonly UnityEngine.AI.NavMeshAgent agent;
        public NavMeshAgent(GameObject parent)
        {
            this.agent = parent.GetComponent<UnityEngine.AI.NavMeshAgent>();
            if (this.agent == null)
            {
                throw new ObjectNotFoundException(parent.name + " does not contain a NavMeshAgent!");
            }
        }

        public bool PathPending => this.agent.pathPending;
        public float RemainingDistance => this.agent.remainingDistance;
        public bool AutoBraking { get => this.agent.autoBraking; set => this.agent.autoBraking = value; }
        public bool AutoRepath { get => this.agent.autoRepath; set => this.agent.autoRepath = value; }
        public bool SetDestination(Vector3 target) => this.agent.SetDestination(target);
    }
}