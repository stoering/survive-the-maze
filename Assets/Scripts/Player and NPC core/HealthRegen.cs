﻿using UnityEngine;

internal class HealthRegen : MonoBehaviour
{
    [SerializeField] private float HealthRegenMultiplier = 0.01f;
    [SerializeField] private float HealthLostPerCollision = 55;
    [SerializeField] private float currentHealth = 100;
    [SerializeField] private Light pointlight = null;

    private float lightStartIntensity = 0;
    internal static bool MonitorHealth = true;

    internal void Start()
    {
        if (this.pointlight != null)
            this.lightStartIntensity = this.pointlight.intensity;
    }

    internal void Update()
    {
        this.currentHealth += (Time.deltaTime * this.currentHealth * this.HealthRegenMultiplier);
        if (this.currentHealth > 100)
        {
            this.currentHealth = 100;
        }

        if (this.pointlight != null)
            this.pointlight.intensity = this.lightStartIntensity / 100 * this.currentHealth;
    }

    internal void OnCollisionEnter(Collision collision)
    {
        if (MonitorHealth)
        {
            NavAgent me = GetComponent<NavAgent>();
            NavAgent other = collision.transform.GetComponent<NavAgent>();
            if (other != null && me.TeamId != other.TeamId)
            {
                this.currentHealth -= this.HealthLostPerCollision;
                if (this.currentHealth <= 0)
                {
                    Die();
                }
            }
        }
    }

    internal void Die() => Destroy(this.gameObject);
}
