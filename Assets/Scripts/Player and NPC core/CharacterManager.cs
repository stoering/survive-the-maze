﻿using System.Collections.Generic;
using UnityEngine;

public class CharacterManager : MonoBehaviour
{
    private readonly List<NavAgent> registeredAgents = new List<NavAgent>();

    // Dumb singleton check.
    private static CharacterManager Me;

    internal void Awake()
    {
        if (Me == null)
            Me = this;
        else if (Me != this)
            throw new SingletonReInstantiatedException("There is already a NavAgentManager in the scene!");
    }

    /// <summary>
    /// Returns the 2D floating location of a NavAgent in the maze.
    /// </summary>
    /// <param name="agentId"></param>
    /// <returns></returns>
    internal static Vector2 NavAgentToGridSpace(int agentId) => PositionManager.WorldToGridSpace(GetNavAgentPosition(agentId));

    /// <summary>
    /// Returns the ID of the grid square where the given NavAgent resides.
    /// </summary>
    /// <param name="agentId"></param>
    /// <returns></returns>
    internal static Vector2Int NavAgentToGridPoint(int agentId) => PositionManager.WorldToGridPoint(GetNavAgentPosition(agentId));

    /// <summary>
    /// Returns the Id of the nearest NavAgent. The default list of all NavAgents are
    /// used in the calculation if no Agent Id list is given.
    /// </summary>
    /// <param name="gridPosition"></param>
    /// <param name="navAgentIds"></param>
    /// <returns></returns>
    internal static int GetNearestNavAgentId(Vector2 gridPosition, List<int> navAgentIds = null)
    {
        if (navAgentIds == null)
            navAgentIds = GetNavAgents();

        int nearestAgentId = CharacterDescriptor.InvalidCharacterId;
        float nearestDistance = float.MaxValue;

        foreach (int agentId in navAgentIds)
        {
            float distance = Vector2.Distance(gridPosition, NavAgentToGridSpace(agentId));
            if (distance < nearestDistance)
            {
                nearestDistance = distance;
                nearestAgentId = agentId;
            }
        }
        return nearestAgentId;
    }
    internal static int GetNearestEnemyNavAgentId(Vector2 gridPosition, int myTeamId) => GetNearestNavAgentId(gridPosition, GetAllAgentsNotOnTeam(myTeamId));
    internal static int GetNearestFriendlyNavAgentId(Vector2 gridPosition, int myTeamId) => GetNearestNavAgentId(gridPosition, GetAllAgentsOnTeam(myTeamId));

    /// <summary>
    /// Returns the distance to the nearest NavAgent. The default list of all NavAgents are
    /// used in the calculation if no Agent Id list or team is specified.
    /// </summary>
    /// <param name="gridPosition"></param>
    /// <param name="navAgentIds"></param>
    /// <returns></returns>
    internal static float DistanceToNearestNavAgent(Vector2 gridPosition, List<int> navAgentIds = null)
    {
        if (navAgentIds == null || navAgentIds.Count > 0)
        {
            return DistanceToNavAgent(gridPosition, GetNearestNavAgentId(gridPosition, navAgentIds));
        }
        else return float.PositiveInfinity;
    }
    internal static float DistanceToNearestEnemyNavAgent(Vector2 gridPosition, int myTeamId) => DistanceToNearestNavAgent(gridPosition, GetAllAgentsNotOnTeam(myTeamId));
    internal static float DistanceToNearestFriendlyNavAgent(Vector2 gridPosition, int myTeamId) => DistanceToNearestNavAgent(gridPosition, GetAllAgentsOnTeam(myTeamId));

    /// <summary>
    /// Returns the distance from the gridLocation to the specified NavAgent.
    /// Throws an InvalidIndexException if the agent does not exist.
    /// </summary>
    /// <param name="gridPosition"></param>
    /// <param name="navAgentId"></param>
    /// <returns></returns>
    internal static float DistanceToNavAgent(Vector2 gridPosition, int navAgentId) => Vector2.Distance(gridPosition, NavAgentToGridSpace(navAgentId));

    /// <summary>
    /// Returns a list of all NavAgent IDs for agents on team teamId.
    /// </summary>
    /// <param name="teamId"></param>
    /// <returns></returns>
    internal static List<int> GetAllAgentsOnTeam(int teamId)
    {
        List<int> allAgents = GetNavAgents();
        List<int> agentsOnTeam = new List<int>();

        foreach (int agentId in allAgents)
        {
            if (GetNavAgent(agentId).TeamId == teamId)
            {
                agentsOnTeam.Add(agentId);
            }
        }

        return agentsOnTeam;
    }

    /// <summary>
    /// Returns a list of all NavAgent IDs for agents NOT on team teamId.
    /// </summary>
    /// <param name="teamId"></param>
    /// <returns></returns>
    internal static List<int> GetAllAgentsNotOnTeam(int teamId)
    {
        List<int> allAgents = GetNavAgents();
        List<int> agentsNotOnTeam = new List<int>();

        foreach (int agentId in allAgents)
        {
            if (GetNavAgent(agentId).TeamId != teamId)
            {
                agentsNotOnTeam.Add(agentId);
            }
        }

        return agentsNotOnTeam;
    }

    /// <summary>
    /// Returns the NavAgent object that matches the given Id.
    /// If no agents match the Id, throws an InvalidIndexException
    /// </summary>
    /// <param name="agentId"></param>
    /// <returns></returns>
    internal static NavAgent GetNavAgent(int agentId)
    {
        if (Me.registeredAgents.Count <= agentId)
            throw new InvalidIndexException(agentId.ToString() + " is not a valid AgentId number.");

        return Me.registeredAgents[agentId];
    }

    /// <summary>
    /// Safely attempts to get the transform.position of the NavAgent. If the agentId is invalid,
    /// returns Vector3.positiveInfinity instead. (This is used because (0,0,0) is a valid position for a NavAgent)
    /// </summary>
    /// <param name="agentId"></param>
    /// <returns></returns>
    internal static Vector3 GetNavAgentPosition(int agentId)
    {
        NavAgent agent = GetNavAgent(agentId);
        return (agent == null) ? Vector3.positiveInfinity : agent.transform.position;
    }

    /// <summary>
    /// Returns a list of all NavAgents 
    /// </summary>
    /// <returns></returns>
    internal static List<int> GetNavAgents() => Me.GetAgents();
    private List<int> GetAgents()
    {
        // Non-static call to reduce "Me" calls.
        List<int> navAgentIds = new List<int>();
        foreach (NavAgent agent in Me.registeredAgents)
        {
            if (agent != null)
            {
                navAgentIds.Add(agent.Id);
            }
        }
        return navAgentIds;
    }

    /// <summary>
    /// Returns the Id for a given NavAgent. If the agent is not registered, assign it a new Id.
    /// Also registers the agent with the GridManager for automatic position updates.
    /// *Possible race:* *I* am registered at Awake time. If this is called before *I* exist, will return InvalidAgentId. 
    /// </summary>
    /// <param name="agent"></param>
    /// <returns></returns>
    internal static int RegisterNavAgent(NavAgent agent)
    {
        if (Me == null)
            return CharacterDescriptor.InvalidCharacterId;

        return Me.RegisterAgent(agent);
    }

    private int RegisterAgent(NavAgent agent)
    {
        // Non-static call to reduce "Me" calls.
        // Check that we are truly registered.
        for (int i = 0; i < this.registeredAgents.Count; i++)
        {
            if (agent == this.registeredAgents[i])
            {
                throw new InvalidIndexException("Nav agent " + i.ToString() + " has already been registered!");
            }
        }

        this.registeredAgents.Add(agent);
        return this.registeredAgents.Count - 1;
    }

    /// <summary>
    /// Removes all NavAgents from the tracking arrays.
    /// </summary>
    internal static void ClearAllRegisteredAgents() => Me.registeredAgents.Clear();

    internal void OnDestroy()
    {
        // Remove my static reference to me so I can be created again later.
        if (Me == this)
        {
            Me = null;
        }
    }
}
