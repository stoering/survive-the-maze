﻿using System.Collections.Generic;
using UnityEngine;
using SharedLib;

internal class NavAgent : MonoBehaviour
{
    [SerializeField] private CharacterDescriptor descriptor = new CharacterDescriptor();
    [SerializeField] private static float retargetFrequency = 5;

    internal bool Idle => this.descriptor.Idle;
    private Queue<Vector3> Waypoints;
    internal INavMeshAgent MeshAgent;
    private Transform Target;

    /// <summary>
    /// The unique identifier assigned to this NavAgent. Will be used to get this agent from the pool.
    /// </summary>
    internal int Id
    {
        get
        {
            if (this.descriptor.UniqueCharacterId == CharacterDescriptor.InvalidCharacterId)
            {
                this.descriptor.UniqueCharacterId = CharacterManager.RegisterNavAgent(this);
            }
            return this.descriptor.UniqueCharacterId;
        }
    }

    /// <summary>
    /// Sets the team Id for the particular agent. Agents that share an ID will appear on the same team and will share visibility.
    /// </summary>
    internal int TeamId { get => this.descriptor.TeamId; set => this.descriptor.TeamId = value; }

    internal void Awake()
    {
        // Get the ID. If it is not set, it will be registered now.
        _ = this.Id;

        this.MeshAgent = new NavMeshAgent(this.gameObject);
        this.Waypoints = new Queue<Vector3>();
    }

    internal void Start()
    {
        if (this.MeshAgent == null)
            Logging.LogError("The nav mesh agent component is not attached to " + this.gameObject.name);
    }

    /// <summary>
    /// Monobehaviour update called once per frame.
    /// </summary>
    internal void Update()
    {
        // Check if we've reached the destination
        if ((this.MeshAgent != null) && !this.MeshAgent.PathPending && this.MeshAgent.RemainingDistance < 0.5f)
        {
            this.descriptor.Idle = true; // Set idle.
            if (this.Waypoints.Count > 0)
            {
                _ = this.MeshAgent.SetDestination(this.Waypoints.Dequeue());
                this.descriptor.Idle = false; // Start moving again, no longer idle.
                if (this.Waypoints.Count == 0)
                {
                    // If we are arriving at our last waypoint, then we should stop when we arrive.
                    this.MeshAgent.AutoBraking = true;
                }
            }
        }
    }

    /// <summary>
    /// Adds the target vector to a list of waypoints. Waypoints will be visited in order.
    /// </summary>
    /// <param name="targetVector"></param>
    internal void QueueDestination(Vector3 targetVector)
    {
        CancelInvoke();
        this.Target = null;

        // Add our destination to the list.
        this.Waypoints.Enqueue(targetVector);
        // Keep moving from this waypoint to the next one.
        this.MeshAgent.AutoBraking = false;
        this.descriptor.Idle = false; // Start moving again, no longer idle.
    }

    /// <summary>
    /// Clears any pending waypoints and begins moving the agent to the targetVector.
    /// </summary>
    /// <param name="targetVector"></param>
    internal void SetDestination(Vector3 targetVector)
    {

        CancelInvoke();
        this.Target = null;

        // We are going someplace new, get rid of the old waypoints.
        this.Waypoints.Clear();

        _ = this.MeshAgent.SetDestination(targetVector);
        // We want to stop right where the player indicates.
        this.MeshAgent.AutoBraking = true;
        this.descriptor.Idle = false; // Start moving again, no longer idle.
    }

    /// <summary>
    /// Clears any pending waypoints and begins moving the agent towards the target object.
    /// Will re-target periodically so the agent continues to follow the target.
    /// </summary>
    /// <param name="target"></param>
    internal void SetTarget(Transform target)
    {
        CancelInvoke();

        // We are not using the waypoints this time, we're going for one target until we reach it.
        this.Waypoints.Clear();

        this.Target = target;
        this.MeshAgent.AutoBraking = false;
        this.descriptor.Idle = false; // Start moving again, no longer idle.
        Retarget();
    }

    internal void Retarget()
    {
        if (this.Target == null)
        {
            this.MeshAgent.AutoBraking = true;
            this.Waypoints.Clear();
        }
        else
        {
            // Set my new destination.
            _ = this.MeshAgent.SetDestination(this.Target.position);

            // Do some math and invoke so we don't spend too much time recalculating a path.
            float timeUntilRepeat = Vector3.Distance(this.Target.transform.position, this.transform.position) / retargetFrequency;
            if (timeUntilRepeat < 0.5f)
                timeUntilRepeat = 0.5f;

            Invoke("Retarget", timeUntilRepeat);
        }
    }

    /// <summary>
    /// Convert an AgentId (int) to a NavAgent.
    /// If there is no agent with that ID, throws an InvalidIndexException.
    /// </summary>
    /// <param name="v"></param>
    public static explicit operator NavAgent(int v) => CharacterManager.GetNavAgent(v);

    private void OnDestroy() => CancelInvoke();
}
