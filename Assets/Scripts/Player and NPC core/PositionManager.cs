﻿using UnityEngine;

internal static class PositionManager
{
    /// <summary>
    /// Converts a point in 3D space into a floating point location on the 2D grid.
    /// </summary>
    /// <param name="worldPoint"></param>
    /// <returns></returns>
    internal static Vector2 WorldToGridSpace(Vector3 worldPoint) => new Vector2(worldPoint.x + (MazeManager.MazeSize.x / 2f), worldPoint.z + (MazeManager.MazeSize.y / 2f));

    /// <summary>
    /// Converts a point in 3D space to a 2D grid coordinate.
    /// </summary>
    /// <param name="worldPoint"></param>
    /// <returns>grid point</returns>
    internal static Vector2Int WorldToGridPoint(Vector3 worldPoint) => Vector2Int.RoundToInt(WorldToGridSpace(worldPoint));

    /// <summary>
    /// Converts a 2D grid point into a location in 3D world space.
    /// </summary>
    /// <param name="gridPoint"></param>
    /// <returns>world point</returns>
    internal static Vector3 GridToWorldSpace(Vector2 gridLocation) => new Vector3(gridLocation.x - (MazeManager.MazeSize.x / 2f), 0, gridLocation.y - (MazeManager.MazeSize.y / 2f));
}
