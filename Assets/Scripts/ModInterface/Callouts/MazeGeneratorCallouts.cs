﻿using Interface;
using UnityEngine;

namespace Callouts
{
    public delegate void SetMazeHeights(Vector2Int mazeSize);

    public class MazeGeneratorCallouts
    {
        public static void SetMazeHeights(Vector2Int mazeSize) => ModScriptRunner.Call("setMazeHeights", mazeSize.x, mazeSize.y);
    }
}
