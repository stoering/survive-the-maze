﻿using System;

namespace Interface
{
    [Serializable]
    public struct ModInterfaceSaveData
    {
        public bool DebuggerEnabled;
    }
}