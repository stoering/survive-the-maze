﻿using MoonSharp.Interpreter;
using MoonSharp.Interpreter.Loaders;
using MoonSharp.VsCodeDebugger;
using System;
using System.IO;
using UnityEngine;

public class ScriptRunner
{
    public ScriptRunner() => LoadScript();

    public ScriptRunner(string ScriptPath) => LoadScript(ScriptPath);

    protected Script LuaScripts { get; set; }

    /// <summary>
    /// Load (or reload) the Moon# script. Will not start the script yet.
    /// </summary>
    /// <param name="ScriptPath"></param>
    /// <param name="LoadUnitTests"></param>
    public void LoadScript(string ScriptPath, bool LoadUnitTests = false)
    {
        // Create a script to run all of our mod code.
        this.LuaScripts = new Script();

        // Set up the session options we need for autoregistration.
        string MyPath = Path.Combine(Application.dataPath, "StreamingAssets", ScriptPath);
        this.LuaScripts.Options.ScriptLoader = new FileSystemScriptLoader();
        ((ScriptLoaderBase)this.LuaScripts.Options.ScriptLoader).ModulePaths = new string[] { MyPath + "/?", MyPath + "/?.lua" };
        this.LuaScripts.Options.DebugPrint = Debug.Log;

        _ = this.LuaScripts.DoString(@"loadUnitTests = " + (LoadUnitTests ? "true" : "false"));
    }
    public void LoadScript(bool LoadUnitTests = false) => LoadScript("ModScripts", LoadUnitTests);

    /// <summary>
    /// Start the current Moon# Script. Will throw an ArgumentNullException if no script has been loaded.
    /// </summary>
    public void StartScript()
    {
        if (this.LuaScripts == null)
        {
            throw new ArgumentNullException("StartScript called before LoadScript for script runner!");
        }
        _ = this.LuaScripts.DoString(@"require 'LoadScripts'");
    }

    internal void AttachDebugger(MoonSharpVsCodeDebugServer server, int AiNumber) => server.AttachToScript(this.LuaScripts, "AiScript_" + AiNumber.ToString());

    /// <summary>
    /// Calls the specified function.
    /// </summary>
    /// <param name="function">The Lua/MoonSharp function to be called</param>
    /// <returns>
    /// The return value(s) of the function call.
    /// </returns>
    /// <exception cref="System.ArgumentException">Thrown if function is not of DataType.Function</exception>
    protected DynValue CallMe(string function) => this.LuaScripts.Call(this.LuaScripts.Globals[function]);

    /// <summary>
    /// Calls the specified function.
    /// </summary>
    /// <param name="function">The Lua/MoonSharp function to be called</param>
    /// <param name="args">The arguments to pass to the function.</param>
    /// <returns>
    /// The return value(s) of the function call.
    /// </returns>
    /// <exception cref="System.ArgumentException">Thrown if function is not of DataType.Function</exception>
    protected DynValue CallMe(string function, params DynValue[] args) => this.LuaScripts.Call(this.LuaScripts.Globals[function], args);

    /// <summary>
    /// Calls the specified function.
    /// </summary>
    /// <param name="function">The Lua/MoonSharp function to be called</param>
    /// <param name="args">The arguments to pass to the function.</param>
    /// <returns>
    /// The return value(s) of the function call.
    /// </returns>
    /// <exception cref="System.ArgumentException">Thrown if function is not of DataType.Function</exception>
    protected DynValue CallMe(string function, params object[] args) => this.LuaScripts.Call(this.LuaScripts.Globals[function], args);
}
