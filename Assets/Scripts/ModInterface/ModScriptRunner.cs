﻿/*
// Here's a singleton class to use if I decide to make more of these.
internal sealed class ModInterface
{
    private static readonly Lazy<ModInterface> lazy = new Lazy<ModInterface>(() => new ModInterface());
    private ModInterface() {}

    internal static ModInterface Me { get { return lazy.Value; } }
}
*/
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reflection;
using MoonSharp.Interpreter;
using MoonSharp.Interpreter.Platforms;
using MoonSharp.VsCodeDebugger;
using MoonSharpRegistration;

namespace Interface
{
    public sealed class ModScriptRunner : ScriptRunner
    {
        private ModScriptRunner() : base() => Script.GlobalOptions.Platform = new LimitedPlatformAccessor();

        // Use lazy me to control the singleton.
        private static readonly Lazy<ModScriptRunner> lazy = new Lazy<ModScriptRunner>(() => new ModScriptRunner());
        private static ModScriptRunner Me => lazy.Value;
        private readonly List<AiScriptRunner> aiScripts = new List<AiScriptRunner>();

        private MoonSharpVsCodeDebugServer DebugServer { get; set; }

        public static ReadOnlyCollection<AiScriptRunner> AiScripts => Me.aiScripts.AsReadOnly();

        /// <summary>
        /// Load (or reload) the Moon# scripts. Enable the debugger and enable or
        /// disable the unit test scripts. Note: Debugger support cannot be disabled
        /// in a play session once it has been enabled. It will be disabled when the game
        /// is reloaded.
        /// </summary>
        /// <param name="saveData"></param>
        public static void LoadMoonSharp(ModInterfaceSaveData saveData, bool LoadUnitTests)
        {
            Me.LoadScript(LoadUnitTests);

            // Check if we need to activate the debugger.
            if (saveData.DebuggerEnabled)
            {
                Me.ActivateDebugger();
            }

            Me.RegisterCommands(LoadUnitTests);

            Me.StartScript();
        }

        public static void ClearAiScripts()
        {
            Me.aiScripts.Clear();
            Me.TryRefreshDebugger();
        }

        public static void AddAiScript(AiScriptRunner script)
        {
            Me.aiScripts.Add(script);
            Me.TryRefreshDebugger();
        }

        /// <summary>
        /// Triggers the step update for all Ai scripts.
        /// TODO: Convert this to its own thread eventually.
        ///     Maybe this will work:
        ///     1. Update AI visibility
        ///     2. Send thread.continue to each independent AI thread.
        ///     3. Check the command input queues and process a command if needed.
        ///     4. Update any mods that need update (can be on the main thread.)
        /// </summary>
        public static void AiUpdate()
        {
            foreach (AiScriptRunner aiScript in AiScripts)
            {
                aiScript?.Update();
            }
        }

        /// <summary>
        /// Calls the specified function.
        /// </summary>
        /// <param name="function">The Lua/MoonSharp function to be called</param>
        /// <returns>
        /// The return value(s) of the function call.
        /// </returns>
        /// <exception cref="System.ArgumentException">Thrown if function is not of DataType.Function</exception>
        public static DynValue Call(string function) => Me.CallMe(function);

        /// <summary>
        /// Calls the specified function.
        /// </summary>
        /// <param name="function">The Lua/MoonSharp function to be called</param>
        /// <param name="args">The arguments to pass to the function.</param>
        /// <returns>
        /// The return value(s) of the function call.
        /// </returns>
        /// <exception cref="System.ArgumentException">Thrown if function is not of DataType.Function</exception>
        public static DynValue Call(string function, params DynValue[] args) => Me.CallMe(function, args);

        /// <summary>
        /// Calls the specified function.
        /// </summary>
        /// <param name="function">The Lua/MoonSharp function to be called</param>
        /// <param name="args">The arguments to pass to the function.</param>
        /// <returns>
        /// The return value(s) of the function call.
        /// </returns>
        /// <exception cref="System.ArgumentException">Thrown if function is not of DataType.Function</exception>
        public static DynValue Call(string function, params object[] args) => Me.CallMe(function, args);

        // Private helper functions.
        /// <summary>
        /// Turn on Lua debugging so we can troubleshoot via VS Code.
        /// </summary>
        private void ActivateDebugger()
        {
            // Create the debugger server
            if (Me.DebugServer == null)
            {
                this.DebugServer = new MoonSharpVsCodeDebugServer();

                // Start the debugger server
                _ = this.DebugServer.Start();
            }
            else
            {
                // Detach from all running scripts.
                while (this.DebugServer.Current != null)
                {
                    // Restart the debugger server with the new script.
                    this.DebugServer.Detach(this.DebugServer.Current);
                }
            }

            this.DebugServer.AttachToScript(this.LuaScripts, "SurviveMaze");
            for (int i = 0; i < this.aiScripts.Count; i++)
            {
                this.aiScripts[i].AttachDebugger(this.DebugServer, i);
            }
        }

        /// <summary>
        /// Resets all scripts if the debugger is currently active, does not activate it if it is not.
        /// </summary>
        private void TryRefreshDebugger()
        {
            if (Me.DebugServer != null)
            {
                Me.ActivateDebugger();
            }
        }

        /// <summary>
        /// Automatically register all commands and data sets using the [MoonSharpUserData] attribute,
        /// then explicitly register every static method as a function if it has the [PassToModScriptRunner] attribute.
        /// </summary>
        private void RegisterCommands(bool LoadUnitTests)
        {
            BindingFlags method_flags = BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic;
            LinkedList<Assembly> assemblies = new LinkedList<Assembly>();

            // Loop through the assemblies, sorting into unit test files and core files.
            foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                // Add unit test assemblies to the end of the list. They will overwrite funciton callouts.
                if (assembly.FullName.StartsWith("Tests_"))
                {
                    if (LoadUnitTests)
                    {
                        _ = assemblies.AddLast(assembly);
                    }
                }
                else
                {
                    _ = assemblies.AddFirst(assembly);
                }
            }

            foreach (Assembly assembly in assemblies)
            {
                foreach (Type type in assembly.GetTypes())
                {
                    foreach (MethodInfo method in type.GetMethods(method_flags))
                    {
                        if (Attribute.GetCustomAttribute(method, typeof(PassToModScriptRunner)) is PassToModScriptRunner attribute)
                        {
                            if (attribute.Name == null)
                            {
                                this.LuaScripts.Globals[method.Name] = method;
                            }
                            else
                            {
                                this.LuaScripts.Globals[attribute.Name] = method;
                            }
                        }
                        else
                        {
                            continue;
                        }
                    }
                }

            // Register all shared objects automatically.
            UserData.RegisterAssembly(assembly);
            }
        }
    }
}
