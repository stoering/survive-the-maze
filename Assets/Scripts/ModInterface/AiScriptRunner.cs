﻿using MoonSharp.Interpreter;

namespace Interface
{
    public class AiScriptRunner : ScriptRunner
    {
        public AiScriptRunner() : base() { }

        public AiScriptRunner(string ScriptPath) : base(ScriptPath) { }

        public void RegisterObject<T>(string Name, T Object)
        {
            _ = UserData.RegisterType<T>();
            this.LuaScripts.Globals[Name] = Object;
        }

        public void Update() => _ = this.LuaScripts.Call(this.LuaScripts.Globals["Update"]);
    }
}