﻿using System;
using System.Collections.Generic;
using UnityEngine;

internal class GenericFactory<T> where T : MonoBehaviour
{
    protected GenericFactory() { }

    protected Transform FreeObjParent { private get; set; }

    // Our instantiated objects that can be used.
    protected Queue<T> FreeObjects { get; set; } = new Queue<T>();

    /// <summary>
    /// Creates and returns a new object with all components attached.
    /// This is protected to encourage you to encapsualate it with a more meaningful name.
    /// </summary>
    /// <returns>Object of factory type</returns>
    protected T GetFree
    {
        get
        {
            T freeObject;

            if (this.FreeObjects.Count > 0)
            {
                freeObject = this.FreeObjects.Dequeue();
                freeObject.transform.parent = null;
                freeObject.gameObject.SetActive(true);
            }
            else
            {
                freeObject = CreateNewObject();
            }
            return freeObject;
        }
    }

    /// <summary>
    /// Return an object once it is done being used. The object will be stored in a queue until it is needed again.
    /// </summary>
    /// <param name="obj"></param>
    internal void ReturnObj(T obj)
    {
        if (this.FreeObjParent != null)
            obj.transform.parent = this.FreeObjParent;

        obj.gameObject.SetActive(false);
        this.FreeObjects.Enqueue(obj);
    }

    /// <summary>
    /// Destroys all of the objects in the free queue. Also unlinks the parent object.
    /// </summary>
    protected void ResetFactory()
    {
        while (this.FreeObjects.Count > 0)
        {
            T obj = this.FreeObjects.Dequeue();

            if (Application.isEditor)
                UnityEngine.Object.DestroyImmediate(obj.gameObject);
            else
                UnityEngine.Object.Destroy(obj.gameObject);
        }

        this.FreeObjParent = null;
    }

    /// <summary>
    /// Creates and returns a new object with all components attached. Cannot be directly called,
    /// this function is internal to the parent factory class.
    /// </summary>
    /// <returns></returns>
    protected virtual T CreateNewObject() => throw new NotImplementedException();
}
