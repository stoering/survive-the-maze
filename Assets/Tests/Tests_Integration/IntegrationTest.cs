﻿using CommandTerminal;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.TestTools;

public class IntegrationTest
{
    protected CommandShell Shell { get; set; }
    protected GameManager Manager { get; set; }

    [UnitySetUp]
    public IEnumerator SetUp()
    {
        // Load a scene and wait for it to appear.
        Bootstrap.DisableBootstrap();
        HealthRegen.MonitorHealth = false;
        SceneManager.LoadScene("Main");
        yield return null;

        this.Shell = Terminal.Shell;
        this.Manager = Object.FindObjectOfType<GameManager>();
    }

    [UnityTearDown]
    public IEnumerator TearDown()
    {
        // We can't unload the scene, but we can at least clean it before moving on.
        yield return RunCommand("ClearGame");

        WallFactory.Reset(); // Clear the disabled walls.
    }

    /// <summary>
    /// Function that runs any number of commands one after another, skipping a single frame after each.
    /// </summary>
    /// <param name="commands"></param>
    /// <returns></returns>
    protected IEnumerator RunCommand(params string[] commands)
    {
        // Process each command in order. Skip one frame between each.
        foreach (string command in commands)
        {
            this.Shell.RunCommand(command);
            yield return null;
        }
        yield return null;
    }
}
