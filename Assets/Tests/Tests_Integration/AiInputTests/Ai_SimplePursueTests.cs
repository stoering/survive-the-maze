﻿using System.Collections;
using UnityEngine;
using UnityEngine.TestTools;

namespace AiInputTests
{
    public class SimpleAiTests : IntegrationTest
    {
        [UnityTest]
        public IEnumerator Minotaur_SimplePursue_None_NoWalls_Test()
        {
            HealthRegen.MonitorHealth = true;
            // Set up a game to pit the two selected AIs against each other.
            yield return RunCommand(
                "Gamespeed 10",
                "NewAiTeam Minotaur Test_SimplePursue", // Set to go to each Spartan team member in order.
                "NewAiTeam Spartan None"); // Player does not move in the test.

            do
            {
                yield return new WaitForSeconds(1);
            } while (Object.FindObjectsOfType<NavAgent>().Length > 1);
        }

        [UnityTest]
        public IEnumerator Minotaur_SimplePursue_None_Test()
        {
            HealthRegen.MonitorHealth = true;
            // Set up a game to pit the two selected AIs against each other.
            yield return RunCommand(
                "Gamespeed 10",
                "NewMaze",
                "NewAiTeam Minotaur Test_SimplePursue", // Set to go to each Spartan team member in order.
                "NewAiTeam Spartan None"); // Player does not move in the test.

            do
            {
                yield return new WaitForSeconds(1);
            } while (Object.FindObjectsOfType<NavAgent>().Length > 1);
        }

//        [UnityTest]
//        public IEnumerator Minotaur_SimplePursue_RandomMovement_Test()
//        {
//            HealthRegen.MonitorHealth = true;
//            // Set up a game to pit the two selected AIs against each other.
//            yield return RunCommand(
//                "Gamespeed 10",
//                "NewMaze",
//                "NewAiTeam Minotaur Test_SimplePursue", // Set to go to each Spartan team member in order.
//                "NewAiTeam Spartan Test_RandomMovement"); // Player does not move in the test.
//
//            do
//            {
//                yield return new WaitForSeconds(1);
//            } while (Object.FindObjectsOfType<NavAgent>().Length > 1);
//        }

        [UnityTearDown]
        public new IEnumerator TearDown()
        {
            yield return RunCommand("ClearGame");
            // Turn this up if future tests are failing due to lingering Moon# scripts.
            yield return new WaitForSeconds(1); 
        }
    }
}
