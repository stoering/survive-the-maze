﻿using System.Collections;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using UnityEngine.SceneManagement;

namespace GameManagerTests
{
    internal class SceneManagementTests
    {
        [UnityTest]
        public IEnumerator MainSceneSingletonCheck()
        {
            // Load a scene and wait for it to appear.
            SceneManager.LoadScene("Main");
            yield return null;

            // Check that we only have one of each of our singletons
            GameManager[] gameManagers = Object.FindObjectsOfType<GameManager>();
            Assert.AreEqual(1, gameManagers.Length);
            TeamManager[] teamManagers = Object.FindObjectsOfType<TeamManager>();
            Assert.AreEqual(1, teamManagers.Length);
            MazeManager[] mazeManagers = Object.FindObjectsOfType<MazeManager>();
            Assert.AreEqual(1, mazeManagers.Length);
            CharacterManager[] navAgentManagers = Object.FindObjectsOfType<CharacterManager>();
            Assert.AreEqual(1, navAgentManagers.Length);
            SaveManager[] saveManagers = Object.FindObjectsOfType<SaveManager>();
            Assert.AreEqual(1, saveManagers.Length);
            InputManager[] inputManagers = Object.FindObjectsOfType<InputManager>();
            Assert.AreEqual(1, inputManagers.Length);
        }
    }
}
