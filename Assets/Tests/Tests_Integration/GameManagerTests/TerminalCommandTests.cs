﻿using System.Collections;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace GameManagerTests
{
    public class TerminalCommandTests : IntegrationTest
    {
        [UnityTest]
        public IEnumerator SaveGame_LoadGame_Test()
        {
            // Individual modules should test their save data extraction on their own.
            GameManager.SaveData.TestBool = false;
            yield return RunCommand("SaveGame");

            // Saving should not change the values.
            Assert.False(GameManager.SaveData.TestBool);
            GameManager.SaveData.TestBool = true;

            // Save to a custom file, then load the default.
            yield return RunCommand("SaveGame Temp", "LoadGame");
            Assert.False(GameManager.SaveData.TestBool);

            yield return RunCommand("LoadGame Temp");
            Assert.True(GameManager.SaveData.TestBool);

            // File doesn't exist, shouldn't change anything.
            yield return RunCommand("LoadGame InvalidGameId");
            Assert.True(GameManager.SaveData.TestBool);
        }

        [UnityTest]
        public IEnumerator SavePreferences_LoadPreferences_Test()
        {
            // Get the original value here so the test doesn't interfere with playmode testing.
            yield return RunCommand("LoadPreferences");
            bool originalDebuggerSetting = GameManager.Preferences.ModInterface.DebuggerEnabled;

            // Use the DebuggerEnabled field to test the overall system.
            GameManager.Preferences.ModInterface.DebuggerEnabled = false;
            yield return RunCommand("SavePreferences");
            Assert.False(GameManager.Preferences.ModInterface.DebuggerEnabled);
            GameManager.Preferences.ModInterface.DebuggerEnabled = true;

            // Load the preferences.
            yield return RunCommand("LoadPreferences");
            Assert.False(GameManager.Preferences.ModInterface.DebuggerEnabled);

            // Do it again, just in reverse.
            GameManager.Preferences.ModInterface.DebuggerEnabled = true;
            yield return RunCommand("SavePreferences");
            Assert.True(GameManager.Preferences.ModInterface.DebuggerEnabled);
            GameManager.Preferences.ModInterface.DebuggerEnabled = false;

            // Load the preferences.
            yield return RunCommand("LoadPreferences");
            Assert.True(GameManager.Preferences.ModInterface.DebuggerEnabled);

            // Set the value to the original again.
            GameManager.Preferences.ModInterface.DebuggerEnabled = originalDebuggerSetting;
            yield return RunCommand("SavePreferences");
        }


        [UnityTest]
        public IEnumerator NewGame_ClearGame_Test()
        {
            // Make sure that we have no agents or maze segments.
            Assert.Zero(Object.FindObjectsOfType<NavAgent>().Length, "NavAgents should not exist at this time!");
            Assert.Zero(Object.FindObjectsOfType<Wall>().Length, "Walls should not exist at this time!");

            yield return RunCommand("NewGame");
            Assert.NotZero(Object.FindObjectsOfType<NavAgent>().Length, "No NavAgents were created!");
            Assert.NotZero(Object.FindObjectsOfType<Wall>().Length, "No Walls were created!");

            yield return RunCommand("ClearGame");
            Assert.Zero(Object.FindObjectsOfType<NavAgent>().Length, "NavAgents should not exist at this time!");
            Assert.Zero(Object.FindObjectsOfType<Wall>().Length, "Walls should not exist at this time!");

            // Test each of the new game types independently:
            yield return RunCommand("NewGame Minotaur");
            Assert.AreEqual(1, this.Manager.InputManager.PlayerInterface.AllAgentsOnTeam.Count, "There should only be one Minotaur");
            Assert.AreEqual(5, this.Manager.AiInterfaces[0].AllAgentsOnTeam.Count, 3, "There should be more than one Spartan");
            Assert.NotZero(Object.FindObjectsOfType<NavAgent>().Length, "No NavAgents were created!");
            Assert.NotZero(Object.FindObjectsOfType<Wall>().Length, "No Walls were created!");

            yield return RunCommand("ClearGame");
            Assert.Zero(Object.FindObjectsOfType<NavAgent>().Length, "NavAgents should not exist at this time!");
            Assert.Zero(Object.FindObjectsOfType<Wall>().Length, "Walls should not exist at this time!");

            yield return RunCommand("NewGame Spartan");
            Assert.AreEqual(1, this.Manager.AiInterfaces[0].AllAgentsOnTeam.Count, "There should only be one Minotaur");
            Assert.AreEqual(5, this.Manager.InputManager.PlayerInterface.AllAgentsOnTeam.Count, 3, "There should be more than one Spartan");
            Assert.NotZero(Object.FindObjectsOfType<NavAgent>().Length, "No NavAgents were created!");
            Assert.NotZero(Object.FindObjectsOfType<Wall>().Length, "No Walls were created!");

            yield return RunCommand("ClearGame");
            Assert.Zero(Object.FindObjectsOfType<NavAgent>().Length, "NavAgents should not exist at this time!");
            Assert.Zero(Object.FindObjectsOfType<Wall>().Length, "Walls should not exist at this time!");

            // These are equivalent to the commands above, test some more specific things while maintaining coverage.
            // A count of all NavAgents should be the same for multiple calls to the same thing.
            yield return RunCommand("NewGame minotaur");
            int navAgentCount = Object.FindObjectsOfType<NavAgent>().Length;
            yield return RunCommand("NewGame M");
            Assert.AreEqual(navAgentCount, Object.FindObjectsOfType<NavAgent>().Length, "There should be the same number of NavAgents in the scene across games.");
            yield return RunCommand("NewGame m");
            Assert.AreEqual(navAgentCount, Object.FindObjectsOfType<NavAgent>().Length, "There should be the same number of NavAgents in the scene across games.");

            // We can't test the exact number of walls in the scene, but there should be roughly the same number each time.
            yield return RunCommand("NewGame spartan");
            int wallCount = Object.FindObjectsOfType<Wall>().Length;
            yield return RunCommand("NewGame S");
            Assert.AreEqual(wallCount, Object.FindObjectsOfType<Wall>().Length, wallCount / 5, "There should be roughly the same number of Walls in the scene across games.");
            yield return RunCommand("NewGame s");
            Assert.AreEqual(wallCount, Object.FindObjectsOfType<Wall>().Length, wallCount / 5, "There should be roughly the same number of Walls in the scene across games.");
        }

        [UnityTest]
        public IEnumerator NewMaze_ClearMaze_Test()
        {
            // Start with active mortals in the scene. They should not be destroyed during this test.
            yield return RunCommand("NewGame");

            for (int i = 0; i < 10; i++)
            {
                int mazeSizeX = Random.Range(10, 50);
                int mazeSizeY = Random.Range(10, 50);

                yield return RunCommand("ClearMaze");
                Assert.NotZero(Object.FindObjectsOfType<NavAgent>().Length, "NavAgents were destroyed!");
                Assert.Zero(Object.FindObjectsOfType<Wall>().Length, "Walls should not exist at this time!");

                yield return RunCommand("NewMaze");
                Assert.NotZero(Object.FindObjectsOfType<NavAgent>().Length, "NavAgents were destroyed!");
                Assert.NotZero(Object.FindObjectsOfType<Wall>().Length, "No Walls were created!");

                yield return RunCommand("NewMaze " + mazeSizeX.ToString());
                Assert.AreEqual(mazeSizeX, MazeManager.MazeSize.x);
                Assert.AreEqual(mazeSizeX, MazeManager.MazeSize.y);

                yield return RunCommand("NewMaze " + mazeSizeX.ToString() + " " + mazeSizeY.ToString());
                Assert.AreEqual(mazeSizeX, MazeManager.MazeSize.x);
                Assert.AreEqual(mazeSizeY, MazeManager.MazeSize.y);
            }
        }

        [UnityTest]
        public IEnumerator NewPlayerTeam_Test()
        {
            // Create a maze with no AIs.
            yield return RunCommand("NewGame", "ClearTeams");
            Assert.IsNull(this.Manager.InputManager.PlayerInterface, "The player interface should have been destroyed.");
            Assert.Zero(this.Manager.AiInterfaces.Count, "There should be no AI interfaces");
            Assert.NotZero(Object.FindObjectsOfType<Wall>().Length, "Walls should not have been destroyed!");

            // Create a series of player Minotaur teams.
            yield return RunCommand("NewPlayerTeam Minotaur");
            Assert.AreEqual(1, Object.FindObjectsOfType<NavAgent>().Length, "There should be one Minotaur per team!");
            Assert.AreEqual(1, this.Manager.InputManager.PlayerInterface.AllAgentsOnTeam.Count, "There should only be one Minotaur");

            yield return RunCommand("NewPlayerTeam minotaur");
            Assert.AreEqual(2, Object.FindObjectsOfType<NavAgent>().Length, "There should be one Minotaur per team!");
            Assert.AreEqual(1, this.Manager.InputManager.PlayerInterface.AllAgentsOnTeam.Count, "There should only be one Minotaur");

            yield return RunCommand("NewPlayerTeam M");
            Assert.AreEqual(3, Object.FindObjectsOfType<NavAgent>().Length, "There should be one Minotaur per team!");
            Assert.AreEqual(1, this.Manager.InputManager.PlayerInterface.AllAgentsOnTeam.Count, "There should only be one Minotaur");

            yield return RunCommand("NewPlayerTeam m");
            Assert.AreEqual(4, Object.FindObjectsOfType<NavAgent>().Length, "There should be one Minotaur per team!");
            Assert.AreEqual(1, this.Manager.InputManager.PlayerInterface.AllAgentsOnTeam.Count, "There should only be one Minotaur");
            Assert.NotZero(Object.FindObjectsOfType<Wall>().Length, "Walls should not have been destroyed!");

            // Clear all teams, leaving just the maze.
            yield return RunCommand("ClearTeams");
            Assert.IsNull(this.Manager.InputManager.PlayerInterface, "The player interface should have been destroyed.");
            Assert.Zero(this.Manager.AiInterfaces.Count, "There should be no AI interfaces");
            Assert.NotZero(Object.FindObjectsOfType<Wall>().Length, "Walls should not have been destroyed!");

            // Create a series of player Spartan teams.
            yield return RunCommand("NewPlayerTeam Spartan");
            Assert.AreEqual(5, this.Manager.InputManager.PlayerInterface.AllAgentsOnTeam.Count, 3, "There should be more than one Spartan");
            Assert.AreEqual(0, this.Manager.AiInterfaces.Count, "There shoudn't be any AI controlled teams yet.");

            yield return RunCommand("NewPlayerTeam spartan");
            Assert.AreEqual(5, this.Manager.InputManager.PlayerInterface.AllAgentsOnTeam.Count, 3, "There should be more than one Spartan");
            Assert.AreEqual(1, this.Manager.AiInterfaces.Count, "The old player team should have been taken over by an AI.");

            yield return RunCommand("NewPlayerTeam S");
            Assert.AreEqual(5, this.Manager.InputManager.PlayerInterface.AllAgentsOnTeam.Count, 3, "There should be more than one Spartan");
            Assert.AreEqual(2, this.Manager.AiInterfaces.Count, "The old player teams should have been taken over by an AI.");

            yield return RunCommand("NewPlayerTeam s");
            Assert.AreEqual(5, this.Manager.InputManager.PlayerInterface.AllAgentsOnTeam.Count, 3, "There should be more than one Spartan");
            Assert.AreEqual(3, this.Manager.AiInterfaces.Count, "The old player teams should have been taken over by an AI.");
            Assert.NotZero(Object.FindObjectsOfType<Wall>().Length, "Walls should not have been destroyed!");

            // Clear all teams, leaving just the maze.
            yield return RunCommand("ClearTeams");
            Assert.IsNull(this.Manager.InputManager.PlayerInterface, "The player interface should have been destroyed.");
            Assert.Zero(this.Manager.AiInterfaces.Count, "There should be no AI interfaces");
            Assert.NotZero(Object.FindObjectsOfType<Wall>().Length, "Walls should not have been destroyed!");
        }

        [UnityTest]
        public IEnumerator NewAiTeam_Test()
        {
            // Create a maze with no AIs.
            yield return RunCommand("NewGame", "ClearTeams");
            Assert.IsNull(this.Manager.InputManager.PlayerInterface, "The player interface should have been destroyed.");
            Assert.Zero(this.Manager.AiInterfaces.Count, "There should be no AI interfaces");
            Assert.NotZero(Object.FindObjectsOfType<Wall>().Length, "Walls should not have been destroyed!");

            // Create a series of ai Minotaur teams.
            yield return RunCommand("NewAiTeam Minotaur");
            Assert.AreEqual(1, Object.FindObjectsOfType<NavAgent>().Length, "There should be one Minotaur per team!");
            Assert.AreEqual(1, this.Manager.AiInterfaces.Count, "We should create one team per command.");
            Assert.AreEqual(1, this.Manager.AiInterfaces[0].AllAgentsOnTeam.Count, "There should only be one Minotaur.");

            yield return RunCommand("NewAiTeam minotaur");
            Assert.AreEqual(2, Object.FindObjectsOfType<NavAgent>().Length, "There should be one Minotaur per team!");
            Assert.AreEqual(2, this.Manager.AiInterfaces.Count, "We should create one team per command.");

            yield return RunCommand("NewAiTeam M");
            Assert.AreEqual(3, Object.FindObjectsOfType<NavAgent>().Length, "There should be one Minotaur per team!");
            Assert.AreEqual(3, this.Manager.AiInterfaces.Count, "We should create one team per command.");

            yield return RunCommand("NewAiTeam m");
            Assert.AreEqual(4, Object.FindObjectsOfType<NavAgent>().Length, "There should be one Minotaur per team!");
            Assert.NotZero(Object.FindObjectsOfType<Wall>().Length, "Walls should not have been destroyed!");
            Assert.AreEqual(4, this.Manager.AiInterfaces.Count, "We should create one team per command.");

            // Clear all teams, leaving just the maze.
            yield return RunCommand("ClearTeams");
            Assert.IsNull(this.Manager.InputManager.PlayerInterface, "The player interface should have been destroyed.");
            Assert.Zero(this.Manager.AiInterfaces.Count, "There should be no AI interfaces");
            Assert.NotZero(Object.FindObjectsOfType<Wall>().Length, "Walls should not have been destroyed!");

            // Create a series of ai Spartan teams.
            yield return RunCommand("NewAiTeam Spartan");
            Assert.AreEqual(5, Object.FindObjectsOfType<NavAgent>().Length, 3);
            Assert.AreEqual(1, this.Manager.AiInterfaces.Count, "We should create one team per command.");
            Assert.AreEqual(5, this.Manager.AiInterfaces[0].AllAgentsOnTeam.Count, 3, "There should be more than one Spartan");

            yield return RunCommand("NewAiTeam spartan");
            Assert.AreEqual(10, Object.FindObjectsOfType<NavAgent>().Length, 6);
            Assert.AreEqual(2, this.Manager.AiInterfaces.Count, "We should create one team per command.");

            yield return RunCommand("NewAiTeam S");
            Assert.AreEqual(15, Object.FindObjectsOfType<NavAgent>().Length, 9);
            Assert.AreEqual(3, this.Manager.AiInterfaces.Count, "We should create one team per command.");

            yield return RunCommand("NewAiTeam s");
            Assert.AreEqual(20, Object.FindObjectsOfType<NavAgent>().Length, 12);
            Assert.AreEqual(4, this.Manager.AiInterfaces.Count, "We should create one team per command.");
            Assert.NotZero(Object.FindObjectsOfType<Wall>().Length, "Walls should not have been destroyed!");

            // Clear all teams, leaving just the maze.
            yield return RunCommand("ClearTeams");
            Assert.IsNull(this.Manager.InputManager.PlayerInterface, "The player interface should have been destroyed.");
            Assert.Zero(this.Manager.AiInterfaces.Count, "There should be no AI interfaces");
            Assert.NotZero(Object.FindObjectsOfType<Wall>().Length, "Walls should not have been destroyed!");
        }

        [UnityTest]
        public IEnumerator NewAiTeamSelectAi_Test()
        {
            HealthRegen.MonitorHealth = true;
            // Battle Royale! 10 teams, 0 bugs, no time for a victory! :-P
            yield return RunCommand(
                "ClearGame",
                "GameSpeed 20",
                "NewAiTeam Minotaur 0_Easiest",
                "NewAiTeam Minotaur 1_Easy",
                "NewAiTeam Minotaur 2_Medium",
                "NewAiTeam Minotaur 3_Hard",
                "NewAiTeam Minotaur 4_Extreme",
                "NewAiTeam Minotaur 5_Impossible",
                "NewAiTeam Spartan 0_Easiest",
                "NewAiTeam Spartan 1_Easy",
                "NewAiTeam Spartan 2_Medium",
                "NewAiTeam Spartan 3_Hard",
                "NewAiTeam Spartan 4_Extreme",
                "NewAiTeam Spartan 5_Impossible");

            // Give us a little bit, make sure that no scripts throw an error.
            yield return new WaitForSecondsRealtime(10);
        }

        [UnityTest]
        public IEnumerator Lights_Test()
        {
            // Start with everything off.
            yield return RunCommand("Lights 0");
            Assert.IsFalse(this.Manager.MinotaurLight.activeInHierarchy);
            Assert.IsFalse(this.Manager.FloodLight.activeInHierarchy);

            // Toggle Minotaur light.
            yield return RunCommand("Lights");
            Assert.IsTrue(this.Manager.MinotaurLight.activeInHierarchy);
            Assert.IsFalse(this.Manager.FloodLight.activeInHierarchy);

            yield return RunCommand("Lights");
            Assert.IsFalse(this.Manager.MinotaurLight.activeInHierarchy);
            Assert.IsFalse(this.Manager.FloodLight.activeInHierarchy);

            // Check each set state.
            yield return RunCommand("Lights 0");
            Assert.IsFalse(this.Manager.MinotaurLight.activeInHierarchy);
            Assert.IsFalse(this.Manager.FloodLight.activeInHierarchy);
            // Shouldn't toggle anything.
            yield return RunCommand("Lights 0");
            Assert.IsFalse(this.Manager.MinotaurLight.activeInHierarchy);
            Assert.IsFalse(this.Manager.FloodLight.activeInHierarchy);

            yield return RunCommand("Lights 1");
            Assert.IsTrue(this.Manager.MinotaurLight.activeInHierarchy);
            Assert.IsFalse(this.Manager.FloodLight.activeInHierarchy);
            // Shouldn't toggle anything.
            yield return RunCommand("Lights 1");
            Assert.IsTrue(this.Manager.MinotaurLight.activeInHierarchy);
            Assert.IsFalse(this.Manager.FloodLight.activeInHierarchy);

            yield return RunCommand("Lights 2");
            Assert.IsFalse(this.Manager.MinotaurLight.activeInHierarchy);
            Assert.IsTrue(this.Manager.FloodLight.activeInHierarchy);
            // Shouldn't toggle anything.
            yield return RunCommand("Lights 2");
            Assert.IsFalse(this.Manager.MinotaurLight.activeInHierarchy);
            Assert.IsTrue(this.Manager.FloodLight.activeInHierarchy);

            // Should deactivate the other light.
            yield return RunCommand("Lights 1");
            Assert.IsTrue(this.Manager.MinotaurLight.activeInHierarchy);
            Assert.IsFalse(this.Manager.FloodLight.activeInHierarchy);

            // Should deactivate either light.
            yield return RunCommand("Lights 2", "Lights 0");
            Assert.IsFalse(this.Manager.MinotaurLight.activeInHierarchy);
            Assert.IsFalse(this.Manager.FloodLight.activeInHierarchy);

            yield return RunCommand("Lights 1", "Lights 0");
            Assert.IsFalse(this.Manager.MinotaurLight.activeInHierarchy);
            Assert.IsFalse(this.Manager.FloodLight.activeInHierarchy);
        }


        [UnityTest]
        public IEnumerator OtherTerminalCommandTest()
        {
            // There is no practical way to test these commands. They are way out of the way,
            // so I just need to test them by hand if I ever work in the mod manager code.
            yield return RunCommand(
                "GameSpeed 10",
                "GameSpeed 1",
                "EnableDebugger",
                "DisableDebugger",
                "ReloadModFiles",
                "RunUnitTests",
                "RunUnitTests 10",
                "Lua HelloWorld",
                "HelloWorld");
        }
    }
}
