﻿using Interface;
using UnityEngine.Assertions;

namespace Tests
{
    internal class ModTests
    {
        /// <summary>
        /// Initializes the mod interface for running unit tests.
        /// </summary>
        protected void InitTests()
        {
            ModInterfaceSaveData saveData = new ModInterfaceSaveData() { DebuggerEnabled = true };

            ModScriptRunner.LoadMoonSharp(saveData, true);
        }

        protected void CallTest(string testName)
        {
            InitTests();
            Assert.IsTrue(ModScriptRunner.Call(testName).Boolean);
        }

        protected void CallTest(string testName, float seed)
        {
            InitTests();
            Assert.IsTrue(ModScriptRunner.Call(testName, seed).Boolean);
        }
    }
}