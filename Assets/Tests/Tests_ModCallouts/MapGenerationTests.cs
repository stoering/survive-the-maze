﻿using NUnit.Framework;

namespace Tests
{
    [Category("Random Seed")]
    internal class MapGenerationTests : ModTests
    {
        [Test]
        public void MapGenerationTest() => CallTest("mapGenerationTest");
    }
}
