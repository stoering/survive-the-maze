﻿using Interface;
using NUnit.Framework;

namespace Tests
{
    [Category("Static Tests")]
    internal class ModLoadUnloadTests
    {
        [Test]
        public void MoonSharpLoad()
        {
            ModInterfaceSaveData saveData = new ModInterfaceSaveData() { DebuggerEnabled = false };

            ModScriptRunner.LoadMoonSharp(saveData, false);
        }

        [Test]
        public void MoonSharpLoadWithDebugging()
        {
            ModInterfaceSaveData saveData = new ModInterfaceSaveData() { DebuggerEnabled = true };

            ModScriptRunner.LoadMoonSharp(saveData, false);
        }

        [Test]
        public void MoonSharpLoadUnitTests()
        {
            ModInterfaceSaveData saveData = new ModInterfaceSaveData() { DebuggerEnabled = false };

            ModScriptRunner.LoadMoonSharp(saveData, true);
        }

        [Test]
        public void MoonSharpLoadUnitTestWithDebugging()
        {
            ModInterfaceSaveData saveData = new ModInterfaceSaveData() { DebuggerEnabled = true };

            ModScriptRunner.LoadMoonSharp(saveData, true);
        }

        [Test]
        public void MoonSharpLoadReloadTest()
        {
            ModInterfaceSaveData saveData = new ModInterfaceSaveData() { DebuggerEnabled = false };

            // Sequence one: Load, reload w/o debugger
            ModScriptRunner.LoadMoonSharp(saveData, false);
            ModScriptRunner.LoadMoonSharp(saveData, false);

            // Sequence two: Load unit test w/o debugger
            ModScriptRunner.LoadMoonSharp(saveData, false);
            ModScriptRunner.LoadMoonSharp(saveData, true);

            // Sequence three: Load unit test with debugger.
            ModScriptRunner.LoadMoonSharp(saveData, false);
            // Debugger cannot be turned off while program is running.
            saveData.DebuggerEnabled = true;
            ModScriptRunner.LoadMoonSharp(saveData, true);

            // Sequence four: Load, reload with debugger.
            ModScriptRunner.LoadMoonSharp(saveData, false);
            ModScriptRunner.LoadMoonSharp(saveData, false);

            // Sequence five: Load unit test, debugger on.
            ModScriptRunner.LoadMoonSharp(saveData, true);
            ModScriptRunner.LoadMoonSharp(saveData, false);
        }
    }
}
