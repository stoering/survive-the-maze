﻿using NUnit.Framework;
using UnityEngine;

namespace Tests
{
    [Category("Superset Tests")]
    internal class RunAllTests : ModTests
    {
        [Test]
        public void RunUnitTestSuite() => CallTest("runUnitTestSuite", Random.value);
    }
}
