﻿using NUnit.Framework;

namespace Tests
{
    internal class ManagedTest : SeededTest
    {
        internal Test_ManagerManager Manager { get; set; }

        [SetUp]
        public virtual void SetUp() => this.Manager = new Test_ManagerManager();

        [TearDown]
        public virtual void TearDown() => this.Manager.Destroy();
    }
}
