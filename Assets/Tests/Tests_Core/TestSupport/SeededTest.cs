﻿using NUnit.Framework;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Tests
{
    public class SeededTest
    {
        private Scene testScene;

        [OneTimeSetUp]
        public void LoadSeedForTestSuite() => RandomSeedGenerator.LoadSeed();

        [SetUp]
        public void GenerateNextSeededTest()
        {
            int seed = RandomSeedGenerator.NextSeed;
            Random.InitState(seed);
            Debug.Log("Random Seed: " + seed.ToString());

            if (!Application.isBatchMode)
                this.testScene = EditorSceneManager.NewScene(NewSceneSetup.EmptyScene, NewSceneMode.Single);
        }

        [TearDown]
        public void ObjectDeletionCheck()
        {
            if (!Application.isBatchMode)
                foreach (GameObject o in this.testScene.GetRootGameObjects())
                    Debug.LogError("The test left objects in the scene. " + o.name);
        }

        /// <summary>
        /// Checks if the UT is being run in batch mode (Command Prompt). If so, attempts
        /// to save the random seed for persistance across runs.
        /// </summary>
        [OneTimeTearDown]
        public void SaveSeededTestSuiteResults() => RandomSeedGenerator.SaveSeed();
    }
}
