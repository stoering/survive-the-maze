﻿using UnityEngine;
using SharedLib;

namespace Tests
{
    internal class Test_MeshAgent : INavMeshAgent
    {
        public Test_MeshAgent()
        {
        }

        public Vector3 Destination { get; private set; }
        private Vector3 Position { get; set; }
        public bool PathPending { get; set; }
        public float RemainingDistance { get; set; }
        public bool AutoBraking { get; set; }
        public bool AutoRepath { get; set; }
        public bool IsStopped { get; set; }
        public Vector2 GridDestination => new Vector2(this.Destination.x + (MazeManager.MazeSize.x / 2f), this.Destination.z + (MazeManager.MazeSize.y / 2f));
        public bool SetDestination(Vector3 target)
        {
            this.Destination = target;
            this.PathPending = true;
            this.IsStopped = true;
            return true;
        }
        public void Update()
        {
            if (!this.PathPending && !this.IsStopped)
            {
                // Baby steps.
                this.Position = Vector3.MoveTowards(this.Position, this.Destination, 0.5f);
                if (Vector3.Distance(this.Position, this.Destination) < 1)
                {
                    this.IsStopped = true;
                }
            }
        }
    }
}