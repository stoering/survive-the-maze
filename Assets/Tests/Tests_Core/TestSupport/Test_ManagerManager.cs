﻿using UnityEngine;

namespace Tests
{
    internal class Test_ManagerManager
    {
        internal Test_ManagerManager()
        {
            this.Manager = new GameObject("Game Manager Scripts");

            // Create one gamemanager object with every manager type we test. Call start and awake on each manager as needed.
            this.GameManager = this.Manager.AddComponent<GameManager>();
            this.Save = this.Manager.AddComponent<SaveManager>();
            this.Maze = this.Manager.AddComponent<MazeManager>();
            this.Walls = this.Manager.AddComponent<WallManager>();
            this.NavAgent = this.Manager.AddComponent<CharacterManager>();
            this.Input = this.Manager.AddComponent<InputManager>();
            this.TeamManager = this.Manager.AddComponent<TeamManager>();
            this.Menu = this.Manager.AddComponent<MenuManager>();

            this.GameManager.SaveManager = this.Save;
            this.GameManager.InputManager = this.Input;
            this.GameManager.MazeManager = this.Maze;
            this.GameManager.NavAgentManager = this.NavAgent;
            this.GameManager.TeamManager = this.TeamManager;
            this.GameManager.MenuManager = this.Menu;

            this.Save.Awake();
            this.Maze.Awake();
            this.Walls.Awake();            
            this.NavAgent.Awake();
            this.Input.Awake();
            //this.Menu.Awake();
            this.GameManager.Awake();

            this.Maze.SetMazeHeights_X = SetMazeHeights;
            this.WallFactoryPrefab = new GameObject("Wall");
            WallFactory.WallPrefab = this.WallFactoryPrefab.AddComponent<Wall>();
        }

        // Create a gameobject to house all manager scripts.
        private GameObject Manager { get; set; }

        private GameObject WallFactoryPrefab { get; set; }

        /// <summary>
        /// Pointer to MazeManager instance
        /// </summary>
        internal GameManager GameManager { get; set; }

        /// <summary>
        /// Pointer to MazeManager instance
        /// </summary>
        internal SaveManager Save { get; set; }

        /// <summary>
        /// Pointer to MazeManager instance
        /// </summary>
        internal MazeManager Maze { get; set; }

        /// <summary>
        /// Pointer to WallManager instance
        /// </summary>
        internal WallManager Walls { get; set; }

        /// <summary>
        /// Pointer to NavAgentManager instance
        /// </summary>
        internal CharacterManager NavAgent { get; set; }

        /// <summary>
        /// Pointer to InputManager instance
        /// </summary>
        internal InputManager Input { get; set; }

        /// <summary>
        /// Pointer to TeamManager instance
        /// </summary>
        internal TeamManager TeamManager { get; set; }

        /// <summary>
        /// Pointer to TeamManager instance
        /// </summary>
        internal MenuManager Menu { get; set; }

        /// <summary>
        /// Destroys the manager gameobject and clears all pointers so singletons can be reused.
        /// </summary>
        internal void Destroy()
        {
            this.Maze.OnDestroy();
            this.NavAgent.OnDestroy();
            this.Input.OnDestroy();

            Object.DestroyImmediate(this.WallFactoryPrefab);
            Object.DestroyImmediate(this.Manager);
        }

        /// <summary>
        /// Callout to set the maze heights through a lua script.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        private void SetMazeHeights(Vector2Int mazeSize)
        {
        }

        ~Test_ManagerManager()
        {
            if (this.Manager != null)
            {
                Destroy();
            }
        }
    }
}
