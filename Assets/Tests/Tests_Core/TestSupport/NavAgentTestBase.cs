﻿using UnityEngine;

namespace Tests
{
    internal class NavAgentBaseTests : ManagedTest
    {
        internal const int mazeSize = 100;
        internal Vector3 RandomVector3 => new Vector3(Random.Range(-mazeSize / 2, mazeSize / 2), 0, Random.Range(-mazeSize / 2, mazeSize / 2));
        internal Vector2 RandomVector2 => new Vector2(Random.Range(0, mazeSize), Random.Range(0, mazeSize));
        internal Vector2Int RandomVector2i => new Vector2Int(Random.Range(0, mazeSize), Random.Range(0, mazeSize));

        internal NavAgent InitNavAgent()
        {
            // Create a mesh agent and add all components.
            GameObject navAgentObject = new GameObject("NavAgent");
            _ = navAgentObject.AddComponent<UnityEngine.AI.NavMeshAgent>();
            NavAgent agent = navAgentObject.AddComponent<NavAgent>();
            agent.Awake();
            agent.Start();
            return agent;
        }

        internal Test_MeshAgent AddMeshAgentToNavAgent(NavAgent agent)
        {
            // Create and link in a test meshAgent to intercept Unity lib commands.
            Test_MeshAgent meshAgent = new Test_MeshAgent();
            agent.MeshAgent = meshAgent;
            return meshAgent;
        }

        internal void PathfindingUpdateLoop(NavAgent agent, Test_MeshAgent mesh)
        {
            int pathCalcTime = Random.Range(1, 5);
            mesh.IsStopped = false;
            mesh.PathPending = true;
            for (int i = 0; i < pathCalcTime; i++)
            {
                mesh.Update();
                if (agent != null)
                    agent.Update();
            }
            mesh.PathPending = false;
            while (!mesh.IsStopped)
            {
                mesh.Update();
                if (agent != null)
                    agent.Update();
            }
        }
    }
}
