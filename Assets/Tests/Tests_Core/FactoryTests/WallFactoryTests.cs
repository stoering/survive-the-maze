﻿using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using Tests;

namespace FactoryTests
{
    [Category("Static Tests")]
    internal class WallFactoryTests : ManagedTest
    {
        internal Transform parentTransform;
        private Wall wallTemplate;

        [SetUp]
        public override void SetUp()
        {
            base.SetUp();

            // Set our parent transform.
            this.parentTransform = new GameObject("ParentTransform").transform;
            WallFactory.SetFreeObjectParent(this.parentTransform);
            SetFactoryTemplate();
        }

        [Test]
        public void WallFactoryPrefabPassFail()
        {
            WallFactory.Reset();
            try
            {
                _ = WallFactory.GetWall();
                Assert.Fail("We should not have been able to get this wall segment.");
            }
            catch (NullPrefabException)
            {
                // We want this one.
            }
            SetFactoryTemplate();
            Wall wall = WallFactory.GetWall(); // Should not throw an exception

            WallFactory.Return(wall);
            WallFactory.Reset();
            try
            {
                _ = WallFactory.GetWall();
                Assert.Fail("We should not have been able to get this wall segment.");
            }
            catch (NullPrefabException)
            {
                // We want this one.
            }
        }

        [Test]
        public void WallFactoryCreateAtLocation()
        {
            // Get a Wall from the factory.
            for (int i = 0; i < 1000; i++)
            {
                Vector2Int wallLocation = new Vector2Int(Random.Range(-100, 100), Random.Range(-100, 100));
                Wall wall1 = WallFactory.GetWall(wallLocation);
                Assert.AreEqual(wallLocation, PositionManager.WorldToGridPoint(wall1.transform.position));

                Wall wall2 = WallFactory.GetWall(PositionManager.GridToWorldSpace(wallLocation));
                Assert.AreEqual(wall1.transform.position, wall2.transform.position);

                // Cleanup.
                WallFactory.Return(wall1);
                WallFactory.Return(wall2);
            }
            WallFactory.Reset();
        }

        [Test]
        public void WallFactoryCreateMovingWalls()
        {
            for (int i = 0; i < 1000; i++)
            {
                Vector2Int wallLocation = new Vector2Int(Random.Range(-100, 100), Random.Range(-100, 100));
                Vector3 wallPosition = PositionManager.GridToWorldSpace(wallLocation);

                // Get walls in each of the 4 positions from the factory.
                Wall wall1 = WallFactory.GetWall(wallPosition, new WallDescriptor { WallPosition = WallPosition.DOWN, WallType = WallType.MOBILE });
                Wall wall2 = WallFactory.GetWall(wallPosition, new WallDescriptor { WallPosition = WallPosition.RISING, WallType = WallType.MOBILE });
                Wall wall3 = WallFactory.GetWall(wallPosition, new WallDescriptor { WallPosition = WallPosition.UP, WallType = WallType.MOBILE });
                Wall wall4 = WallFactory.GetWall(wallPosition, new WallDescriptor { WallPosition = WallPosition.FALLING, WallType = WallType.MOBILE });

                // Make sure that all walls are in the correct grid point.
                Assert.AreEqual(PositionManager.WorldToGridPoint(wall1.transform.position), wallLocation);
                Assert.AreEqual(PositionManager.WorldToGridPoint(wall2.transform.position), wallLocation);
                Assert.AreEqual(PositionManager.WorldToGridPoint(wall3.transform.position), wallLocation);
                Assert.AreEqual(PositionManager.WorldToGridPoint(wall4.transform.position), wallLocation);

                // Make sure that rising walls start down and falling walls start up.
                Assert.AreEqual(wall1.transform.position, wall2.transform.position);
                Assert.AreEqual(wall3.transform.position, wall4.transform.position);

                // Make sure that down walls are down and up walls are up.
                Assert.Less(wall1.transform.position.y, 0.001f);
                Assert.Less(wall2.transform.position.y, 0.001f);
                Assert.Greater(wall3.transform.position.y, 0.001f);
                Assert.Greater(wall4.transform.position.y, 0.001f);

                WallFactory.Return(wall1);
                WallFactory.Return(wall2);
                WallFactory.Return(wall3);
                WallFactory.Return(wall4);
            }
            WallFactory.Reset();
        }

        [Test]
        public void WallFactoryGetReturn()
        {
            Assert.AreEqual(this.parentTransform.childCount, 0);

            // Get a Wall from the factory.
            Wall wall = WallFactory.GetWall();
            Assert.AreEqual(this.parentTransform.childCount, 0);

            // Return the Wall.
            WallFactory.Return(wall);
            Assert.AreEqual(this.parentTransform.childCount, 1);
            Assert.AreSame(this.parentTransform.GetComponentInChildren<Wall>(true), wall);

            // Get the same object.
            Wall newWall = WallFactory.GetWall();
            Assert.AreSame(wall, newWall);
            Assert.AreEqual(this.parentTransform.childCount, 0);

            // Return the Wall.
            WallFactory.Return(newWall);
            Assert.AreEqual(this.parentTransform.childCount, 1);

            // Clear out the WallFactory before the next test.
            WallFactory.Reset();
            Assert.AreEqual(this.parentTransform.childCount, 0);
        }

        [Test]
        public void WallFactoryGetMultiple()
        {
            List<Wall> Walls = new List<Wall>();
            SetFactoryTemplate();

            // Get 1000 new objects, make sure that there are no memory leaks, etc.
            for (int i = 0; i < 1000; i++)
            {
                Walls.Add(WallFactory.GetWall());
            }
            foreach (Wall wall in Walls)
            {
                WallFactory.Return(wall);
            }

            // Clear out the WallFactory before the next test.
            WallFactory.Reset();
        }

        [Test]
        public void WallFactoryEnableDisableObject()
        {
            // Get a Wall from the factory.
            Wall wall = WallFactory.GetWall();
            Assert.IsTrue(wall.isActiveAndEnabled);

            // Return the Wall.
            WallFactory.Return(wall);
            Assert.IsFalse(wall.isActiveAndEnabled);

            // Get the same object.
            _ = WallFactory.GetWall();
            Assert.IsTrue(wall.isActiveAndEnabled);

            // Clear out the WallFactory before the next test.
            WallFactory.Return(wall);
            WallFactory.Reset();
        }

        [TearDown]
        public override void TearDown()
        {
            Object.DestroyImmediate(this.wallTemplate.gameObject);
            Object.DestroyImmediate(this.parentTransform.gameObject);

            base.TearDown();
        }

        private void SetFactoryTemplate()
        {
            if (this.wallTemplate != null)
            {
                Object.DestroyImmediate(this.wallTemplate.gameObject);
            }
            GameObject gameObject = new GameObject("Wall Template");
            this.wallTemplate = gameObject.AddComponent<Wall>();
            WallFactory.WallPrefab = this.wallTemplate;
        }
    }
}
