﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using UnityEngine;
using Tests;

namespace FactoryTests
{
    [Category("Static Tests")]
    internal class GenericFactoryTests : SeededTest
    {
        internal class SpecialObject : MonoBehaviour { }

        internal sealed class TestFactory : GenericFactory<SpecialObject>
        {
            // This class is a singleton. Use Me to get "this"
            private static readonly Lazy<TestFactory> lazy = new Lazy<TestFactory>(() => new TestFactory());
            private static TestFactory Me => lazy.Value;
            private TestFactory() { }

            internal static SpecialObject GetSpecialObject => Me.GetFree;
            internal static void Reset() => Me.ResetFactory();
            internal static void Return(SpecialObject specialObject) => Me.ReturnObj(specialObject);
            internal static void SetFreeObjectParent(Transform parent) => Me.FreeObjParent = parent;

            protected override SpecialObject CreateNewObject()
            {
                GameObject gameObject = new GameObject();
                return gameObject.AddComponent<SpecialObject>();
            }
        }

        internal Transform parentTransform;

        [SetUp]
        public void SetUp()
        {
            // Set our parent transform.
            this.parentTransform = new GameObject().transform;
            TestFactory.SetFreeObjectParent(this.parentTransform);
        }

        [Test]
        public void TestFactoryGetReturn()
        {
            // Set our parent transform.
            Assert.AreEqual(this.parentTransform.childCount, 0);

            // Get a SpecialObject from the factory.
            SpecialObject special = TestFactory.GetSpecialObject;
            Assert.AreEqual(this.parentTransform.childCount, 0);

            // Return the SpecialObject.
            TestFactory.Return(special);
            Assert.AreEqual(this.parentTransform.childCount, 1);
            Assert.AreSame(this.parentTransform.GetComponentInChildren<SpecialObject>(true), special);

            // Get the same object.
            SpecialObject newSpecial = TestFactory.GetSpecialObject;
            Assert.AreSame(special, newSpecial);
            Assert.AreEqual(this.parentTransform.childCount, 0);

            // Return the SpecialObject.
            TestFactory.Return(newSpecial);
            Assert.AreEqual(this.parentTransform.childCount, 1);

            // Clear out the TestFactory before the next test.
            TestFactory.Reset();
            Assert.AreEqual(this.parentTransform.childCount, 0);
        }

        [Test]
        public void TestFactoryGetMultiple()
        {
            List<SpecialObject> specialObjects = new List<SpecialObject>();

            // Get 1000 new objects, make sure that there are no memory leaks, etc.
            for (int i = 0; i < 1000; i++)
            {
                specialObjects.Add(TestFactory.GetSpecialObject);
            }
            foreach (SpecialObject obj in specialObjects)
            {
                TestFactory.Return(obj);
            }

            // Clear out the TestFactory before the next test.
            TestFactory.Reset();
        }

        [Test]
        public void TestFactoryEnableDisableObject()
        {
            // Get a SpecialObject from the factory.
            SpecialObject special = TestFactory.GetSpecialObject;
            Assert.IsTrue(special.isActiveAndEnabled);

            // Return the SpecialObject.
            TestFactory.Return(special);
            Assert.IsFalse(special.isActiveAndEnabled);

            // Get the same object.
            _ = TestFactory.GetSpecialObject;
            Assert.IsTrue(special.isActiveAndEnabled);

            // Return the object so it will be cleaned up.
            TestFactory.Return(special);
        }

        [TearDown]
        public void TearDown()
        {
            // Clear out the TestFactory before the next test.
            TestFactory.Reset();
            UnityEngine.Object.DestroyImmediate(this.parentTransform.gameObject);
        }
    }
}