﻿using NUnit.Framework;
using Tests;
using UnityEngine;

namespace MazeTests
{
    [Category("Random Seed")]
    internal class MazeManagerTests : ManagedTest
    {
        private struct MazeTracker
        {
            public bool wallExists;
            public WallPosition wallPosition;
        }

        private Vector2Int mazeSize;
        private MazeTracker[,] testMazeTracker;

        [Test]
        public void CreatePopulatedMazeTest()
        {
            int size = Random.Range(25, 100);
            this.mazeSize = new Vector2Int(size, size);
            this.Manager.Maze.SetMazeHeights_X = SetMazeHeights;
            this.testMazeTracker = new MazeTracker[this.mazeSize.x, this.mazeSize.y];

            // Create a maze. This should be empty since the callout is not mapped to anything.
            this.Manager.Maze.CreateMaze(size);
            ValidateEmptyMaze();

            for (int i = 0; i < 500; i++)
            {
                Vector2Int position = new Vector2Int(Random.Range(0, size - 1), Random.Range(0, size - 1));
                WallManager.CreateWall(position);
                WallDescriptor wall = WallManager.GetWall(position);
                Assert.True(WallManager.WallExists(position));
                this.testMazeTracker[position.x, position.y].wallExists = true;
                this.testMazeTracker[position.x, position.y].wallPosition = WallPosition.RISING;
            }
            MazeManager.PrintMaze();
            ValidatePopulatedMaze();

            // Clear out the maze we created.
            this.Manager.Maze.DestroyMaze();
            Assert.AreEqual(this.mazeSize, MazeManager.MazeSize);

            WallFactory.Reset();
        }

        [Test]
        public void CreatePopulatedMazeRandomWallStateTest()
        {
            int size = Random.Range(25, 100);
            this.mazeSize = new Vector2Int(size, size);
            this.Manager.Maze.SetMazeHeights_X = SetMazeHeights;
            this.testMazeTracker = new MazeTracker[this.mazeSize.x, this.mazeSize.y];

            // Create a maze. This should be empty since the callout is not mapped to anything.
            this.Manager.Maze.CreateMaze(size);
            ValidateEmptyMaze();

            for (int i = 0; i < 500; i++)
            {
                Vector2Int position = new Vector2Int(Random.Range(0, size - 1), Random.Range(0, size - 1));
                if (!this.testMazeTracker[position.x, position.y].wallExists)
                {
                    WallPosition wallPosition = (WallPosition)Random.Range(0, 3);
                    WallManager.CreateWall(position, wallPosition);
                    WallDescriptor wall = WallManager.GetWall(position);
                    Assert.True(WallManager.WallExists(position));
                    this.testMazeTracker[position.x, position.y].wallExists = true;
                    this.testMazeTracker[position.x, position.y].wallPosition = wallPosition;
                }
            }
            MazeManager.PrintMaze();
            ValidatePopulatedMaze();

            // Clear out the maze we created.
            this.Manager.Maze.DestroyMaze();
            Assert.AreEqual(this.mazeSize, MazeManager.MazeSize);

            WallFactory.Reset();
        }

        [Test]
        public void AddAndRemoveWallTest()
        {
            int size = Random.Range(25, 100);
            this.mazeSize = new Vector2Int(size, size);
            this.Manager.Maze.SetMazeHeights_X = SetMazeHeights;
            this.testMazeTracker = new MazeTracker[this.mazeSize.x, this.mazeSize.y];

            // Create a maze. This should be empty since the callout is not mapped to anything.
            this.Manager.Maze.CreateMaze(size);
            ValidateEmptyMaze();

            // Populate the maze.
            for (int i = 0; i < 500; i++)
            {
                Vector2Int position = new Vector2Int(Random.Range(0, size - 1), Random.Range(0, size - 1));
                if (!this.testMazeTracker[position.x, position.y].wallExists)
                {
                    WallPosition wallPosition = (WallPosition)Random.Range(0, 3);
                    WallManager.CreateWall(position, wallPosition);
                    WallDescriptor wall = WallManager.GetWall(position);
                    Assert.True(WallManager.WallExists(position));
                    this.testMazeTracker[position.x, position.y].wallExists = true;
                    this.testMazeTracker[position.x, position.y].wallPosition = wallPosition;
                }
            }
            MazeManager.PrintMaze();
            ValidatePopulatedMaze();

            // Now that the maze it populated, go ahead and move the walls around.
            for (int i = 0; i < 500; i++)
            {
                Vector2Int position = new Vector2Int(Random.Range(0, size - 1), Random.Range(0, size - 1));
                if (!this.testMazeTracker[position.x, position.y].wallExists)
                {
                    if (Random.value < 0.5f)
                    {
                        // Simple create, then destory.
                        WallPosition wallPosition = (WallPosition)Random.Range(0, 3);
                        Assert.False(WallManager.WallExists(position), "Wall should start down.");
                        WallManager.CreateWall(position, wallPosition);
                        Assert.True(WallManager.WallExists(position), "CreateWall failed to create a wall segment at the expected position.");
                        WallManager.DestroyWall(position);
                        Assert.False(WallManager.WallExists(position), "DestroyWall failed to remove the wall segment at the expected position.");
                    }
                    else
                    {
                        // Toggle wall (starting down)
                        Assert.False(WallManager.WallExists(position), "Wall should start down.");
                        WallManager.ToggleWall(position);
                        Assert.True(WallManager.WallExists(position), "ToggleWall failed to create a wall segment at the expected position.");
                        WallManager.ToggleWall(position);
                        if (WallManager.GetWall(position).WallPosition == WallPosition.FALLING)
                        {
                            Wall wall = WallManager.GetWallObject(position);
                            wall.Update();
                            wall.SetWallPosition(WallPosition.DOWN); // Shortcut the wait since we're not in playmode.
                            wall.Update();
                        }
                        Assert.False(WallManager.WallExists(position), "ToggleWall failed to remove the wall segment at the expected position.");
                    }
                }
                else
                {
                    if (Random.value < 0.5f)
                    {
                        // Simple destory, then create.
                        Assert.True(WallManager.WallExists(position), "Wall should start up.");
                        WallManager.DestroyWall(position);
                        Assert.False(WallManager.WallExists(position), "DestroyWall failed to remove the wall segment at the expected position.");
                        WallPosition wallPosition = (WallPosition)Random.Range(0, 3);
                        WallManager.CreateWall(position, wallPosition);
                        Assert.True(WallManager.WallExists(position), "CreateWall failed to create a wall segment at the expected position.");
                    }
                    else
                    {
                        // Toggle wall (starting up)
                        Assert.True(WallManager.WallExists(position), "Wall should start up.");
                        WallManager.ToggleWall(position);
                        if (WallManager.GetWall(position).WallPosition == WallPosition.FALLING)
                        {
                            Wall wall = WallManager.GetWallObject(position);
                            wall.Update();
                            wall.SetWallPosition(WallPosition.DOWN); // Shortcut the wait since we're not in playmode.
                            wall.Update();
                        }
                        Assert.False(WallManager.WallExists(position), "ToggleWall failed to remove the wall segment at the expected position.");
                        WallManager.ToggleWall(position);
                        Assert.True(WallManager.WallExists(position), "ToggleWall failed to create a wall segment at the expected position.");
                    }
                }
            }
            
            // Clear out the maze we created.
            this.Manager.Maze.DestroyMaze();
            Assert.AreEqual(this.mazeSize, MazeManager.MazeSize);

            WallFactory.Reset();
        }

        // Create an empty maze and verify that it is empty.
        [Test]
        public void CreateDefaultEmptyMaze()
        {
            // Create an empty gameobject with a grid manager.
            this.Manager.Maze.SetMazeHeights_X = SetMazeHeights;
            this.mazeSize = new Vector2Int(50, 50);

            // Create a maze. This should be empty since the callout is not mapped to anything.
            this.Manager.Maze.CreateMaze();

            ValidateEmptyMaze();

            // Show the maze for easy debugging.
            MazeManager.PrintMaze();
        }

        // Create an empty maze and verify that it is empty.
        [Test]
        public void CreateEmptySquareMaze()
        {
            int size = Random.Range(1, 100);
            this.mazeSize = new Vector2Int(size, size);

            // Create an empty gameobject with a grid manager.
            this.Manager.Maze.SetMazeHeights_X = SetMazeHeights;

            // Create a maze. This should be empty since the callout is not mapped to anything.
            this.Manager.Maze.CreateMaze(size);

            ValidateEmptyMaze();

            // Show the maze for easy debugging.
            MazeManager.PrintMaze();
        }

        // Create an empty maze and verify that it is empty.
        [Test]
        public void CreateCustomEmptyMaze()
        {
            this.mazeSize = new Vector2Int(Random.Range(1, 100), Random.Range(1, 100));

            // Create an empty gameobject with a grid manager.
            this.Manager.Maze.SetMazeHeights_X = SetMazeHeights;

            // Create a maze. This should be empty since the callout is not mapped to anything.
            this.Manager.Maze.CreateMaze(this.mazeSize);

            ValidateEmptyMaze();

            // Show the maze for easy debugging.
            MazeManager.PrintMaze();
        }

        /// <summary>
        /// Callout to set the maze heights through a lua script.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        private void SetMazeHeights(Vector2Int mazeSize)
        {
        }

        private void ValidateEmptyMaze()
        {
            // Check that everything is working as intended.
            Assert.AreEqual(this.mazeSize, MazeManager.MazeSize);

            Vector2Int wallLocation;
            for (int x = 0; x < this.mazeSize.x; x++)
            {
                for (int y = 0; y < this.mazeSize.y; y++)
                {
                    wallLocation = new Vector2Int(x, y);
                    Assert.False(WallManager.WallExists(wallLocation));
                }
            }
        }

        private void ValidatePopulatedMaze()
        {
            // Check that everything is working as intended.
            Assert.AreEqual(this.mazeSize, MazeManager.MazeSize);

            Vector2Int wallLocation;
            for (int x = 0; x < this.mazeSize.x; x++)
            {
                for (int y = 0; y < this.mazeSize.y; y++)
                {
                    wallLocation = new Vector2Int(x, y);
                    Assert.AreEqual(this.testMazeTracker[x, y].wallExists, WallManager.WallExists(wallLocation));
                    if (this.testMazeTracker[x, y].wallExists)
                    {
                        Assert.AreEqual(PositionManager.WorldToGridPoint(WallManager.GetWallObject(wallLocation).transform.position), wallLocation);
                        Assert.AreEqual(this.testMazeTracker[x, y].wallPosition, WallManager.GetWall(wallLocation).WallPosition);
                    }
                }
            }
        }

        ~MazeManagerTests()
        {
            Object.DestroyImmediate(this.Manager.Maze);
        }
    }
}
