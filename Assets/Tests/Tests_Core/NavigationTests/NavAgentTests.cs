﻿using NUnit.Framework;
using System.Collections.Generic;
using Tests;
using UnityEngine;

namespace NavigationTests
{
    internal class NavAgentTests : NavAgentBaseTests
    {
        [Category("Static Tests")]
        [Test]
        public void RegisterAndGetAgent()
        {
            // Create a mesh agent and add all components.
            GameObject navAgentObject = new GameObject();
            NavAgent agent = navAgentObject.AddComponent<NavAgent>();

            // Register the agent and ensure that its ID does not change.
            int navAgentId = agent.Id;
            Assert.AreEqual(navAgentId, agent.Id);

            // Get the agent by ID and make sure that it matches the current agent.
            Assert.AreSame(agent, CharacterManager.GetNavAgent(navAgentId));
            NavAgent doubleAgent = (NavAgent)navAgentId;
            Assert.AreSame(agent, doubleAgent);

            Object.DestroyImmediate(agent.gameObject);
        }

        [Category("Static Tests")]
        [Test]
        public void RegisterAndGetAgents()
        {
            int agentCount = 100;
            int iterationCount = 500;

            // Create a list of mesh agents and add all components.
            List<GameObject> navAgentObjects = new List<GameObject>();
            List<NavAgent> agents = new List<NavAgent>();
            List<int> navAgentIds = new List<int>();

            for (int i = 0; i < agentCount; i++)
            {
                // Create a mesh agent and add all components.
                navAgentObjects.Add(new GameObject());
                agents.Add(navAgentObjects[i].AddComponent<NavAgent>());
                navAgentIds.Add(agents[i].Id);
            }
            for (int i = 0; i < iterationCount; i++)
            {
                int j = Random.Range(0, agentCount);

                // Make sure that the agent Id hasn't changed.
                Assert.AreEqual(navAgentIds[j], agents[j].Id);

                // Get the agent by ID and make sure that it matches the current agent.
                Assert.AreSame(agents[j], CharacterManager.GetNavAgent(navAgentIds[j]));
            }
            // Clear all agents.
            CharacterManager.ClearAllRegisteredAgents();
            for (int i = 0; i < iterationCount; i++)
            {
                int j = Random.Range(0, agentCount);
                try
                {
                    _ = CharacterManager.GetNavAgent(navAgentIds[j]);
                    Assert.Fail();
                }
                catch (InvalidIndexException)
                {
                    // Exception expected.
                }
            }

            // Cleanup.
            for (int i = 0; i < navAgentObjects.Count; i++)
            {
                Object.DestroyImmediate(navAgentObjects[i]);
            }
        }

        [Category("Random Seed")]
        [Test]
        public void UpdatePlayerNavAgentSingle()
        {
            NavAgent agent = InitNavAgent();
            Test_MeshAgent meshAgent = AddMeshAgentToNavAgent(agent);

            // We should be retargetting each time we complete a NavMeshAgent move.
            for (int i = 0; i < 100; i++)
            {
                agent.SetDestination(this.RandomVector3);
                Vector3 temp = this.RandomVector3;
                agent.SetDestination(temp);
                Assert.AreEqual(temp, meshAgent.Destination);
                PathfindingUpdateLoop(agent, meshAgent);
            }

            // Cleanup.
            Object.DestroyImmediate(agent.gameObject);
        }

        [Category("Random Seed")]
        [Test]
        public void UpdatePlayerNavAgentQueued()
        {
            NavAgent agent = InitNavAgent();
            Test_MeshAgent meshAgent = AddMeshAgentToNavAgent(agent);

            // Create a list of waypoints.
            List<Vector3> waypoints = new List<Vector3>();
            for (int i = 0; i < 50; i++)
            {
                Vector3 temp = this.RandomVector3;
                while ((i > 0) && (Vector3.Distance(temp, waypoints[i - 1]) < 1))
                    temp = this.RandomVector3;

                waypoints.Add(temp);
                agent.QueueDestination(temp);
            }

            // Test that we go to each waypoint in order.
            for (int i = 0; i < 50; i++)
            {
                PathfindingUpdateLoop(agent, meshAgent);
                Assert.AreEqual(waypoints[i], meshAgent.Destination);
            }

            // Re-queue the waypoints.
            for (int i = 0; i < 50; i++)
                agent.QueueDestination(waypoints[i]);

            // We should discard all of them when we get a direct waypoint.
            agent.SetDestination(new Vector3(101, 101, 101)); // Out of the random range.
            for (int i = 0; i < 50; i++)
            {
                PathfindingUpdateLoop(agent, meshAgent);
                Assert.AreNotEqual(waypoints[i], meshAgent.Destination);
            }

            // Cleanup.
            Object.DestroyImmediate(agent.gameObject);
        }

        [Category("Random Seed")]
        [Test]
        public void UpdatePlayerNavAgentFollowTarget()
        {
            NavAgent agent = InitNavAgent();
            Test_MeshAgent meshAgent = AddMeshAgentToNavAgent(agent);

            // Create a gameobject to go to.
            GameObject targetObject = new GameObject();
            agent.SetTarget(targetObject.transform);

            for (int i = 0; i < 100; i++)
            {
                Vector3 newpos = this.RandomVector3;
                targetObject.transform.position = newpos;
                agent.Retarget();
                PathfindingUpdateLoop(agent, meshAgent);
                Assert.AreEqual(newpos, meshAgent.Destination);
            }

            // Check that we can end targetting if the target is destroyed.
            Object.DestroyImmediate(targetObject);
            for (int i = 0; i < 10; i++)
            {
                agent.Retarget();
                PathfindingUpdateLoop(agent, meshAgent);
            }

            // Cleanup.
            Object.DestroyImmediate(agent.gameObject);
        }
    }
}
