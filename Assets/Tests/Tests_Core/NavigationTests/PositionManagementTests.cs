using System.Collections.Generic;
using NUnit.Framework;
using Tests;
using UnityEngine;

namespace NavigationTests
{
    [Category("Static Tests")]
    internal class PositionManagementTests : NavAgentBaseTests
    {
        private NavAgent agent;
        private const int maxAgents = 10;
        private const int testLoops = 100;

        [SetUp]
        public override void SetUp()
        {
            base.SetUp();
            this.agent = InitNavAgent();
            this.Manager.Maze.CreateMaze(new Vector2Int(mazeSize, mazeSize));
        }

        [Test]
        public void WorldAndGridPointConversionTest()
        {
            for (int i = 0; i < 1000; i++)
            {
                // Get a grid point and the associated world point.
                Vector2Int gridPoint = new Vector2Int(Random.Range(0, 1000), Random.Range(0, 1000));
                Vector3 worldPoint = PositionManager.GridToWorldSpace(gridPoint);

                // Validate basic conversion.
                Assert.AreEqual(gridPoint, PositionManager.WorldToGridPoint(worldPoint));

                // Select any locaiton within the grid square. The square should be centered at the gridpoint,
                //      so (0, 0) should go to (-0.499~, -0,499~) and (0.5, 0.5)
                worldPoint += new Vector3(Random.Range(-0.4999f, 0.5f), Random.Range(-1000, 1000), Random.Range(-0.4999f, 0.5f));
                Assert.AreEqual(gridPoint, PositionManager.WorldToGridPoint(worldPoint));
            }
            for (int i = 0; i < 1000; i++)
            {
                // Get a world point and the associated grid location.
                Vector3 worldPoint = new Vector3(Random.Range(-1000, 1000), 0, Random.Range(-1000, 1000));
                Assert.AreEqual(worldPoint, PositionManager.GridToWorldSpace(PositionManager.WorldToGridSpace(worldPoint)));
            }
        }

        [Test]
        public void PlaceAndFindNavAgentById()
        {
            for (int i = 0; i < testLoops; i++)
            {
                Vector3 position = this.RandomVector3;
                this.agent.transform.position = position;

                Assert.AreEqual(CharacterManager.NavAgentToGridPoint(this.agent.Id), PositionManager.WorldToGridPoint(position));
                Assert.AreEqual(CharacterManager.NavAgentToGridSpace(this.agent.Id), PositionManager.WorldToGridSpace(position));
            }
        }

        [Test]
        public void PlaceAndFindNavAgentsById()
        {
            List<NavAgent> Agents = new List<NavAgent>();

            for (int i = 0; i < maxAgents; i++)
            {
                Agents.Add(InitNavAgent());
            }

            for (int i = 0; i < testLoops; i++)
            {
                Vector3 position = this.RandomVector3;
                int index = Random.Range(0, maxAgents);
                Agents[index].transform.position = position;

                Assert.AreEqual(CharacterManager.NavAgentToGridPoint(Agents[index].Id), PositionManager.WorldToGridPoint(position));
                Assert.AreEqual(CharacterManager.NavAgentToGridSpace(Agents[index].Id), PositionManager.WorldToGridSpace(position));
            }

            for (int i = 0; i < maxAgents; i++)
            {
                Object.DestroyImmediate(Agents[i].gameObject);
            }
        }

        [Test]
        public void PlaceAndFindNavAgentByLocation()
        {
            for (int i = 0; i < testLoops; i++)
            {
                Vector3 position = this.RandomVector3;
                this.agent.transform.position = position;

                Assert.AreEqual(this.agent.Id, CharacterManager.GetNearestNavAgentId(PositionManager.WorldToGridSpace(position)));
            }
        }

        [Test]
        public void PlaceAndFindNavAgentsByLocation()
        {
            List<NavAgent> Agents = new List<NavAgent>();

            for (int i = 0; i < maxAgents; i++)
            {
                Agents.Add(InitNavAgent());
            }

            for (int i = 0; i < testLoops; i++)
            {
                Vector3 position = this.RandomVector3;
                int index = Random.Range(0, maxAgents);
                Agents[index].transform.position = position;

                Assert.Zero(CharacterManager.DistanceToNearestNavAgent(PositionManager.WorldToGridSpace(position)));
                Assert.AreEqual(Agents[index].Id, CharacterManager.GetNearestNavAgentId(PositionManager.WorldToGridSpace(position)));
            }

            for (int i = 0; i < maxAgents; i++)
            {
                Object.DestroyImmediate(Agents[i].gameObject);
            }
        }

        [Test]
        public void PlaceAndFindNavAgentsByTeam()
        {
            CharacterManager.ClearAllRegisteredAgents();
            List<NavAgent> navAgents = new List<NavAgent>();
            List<int> Team1Agents = new List<int>();
            List<int> Team2Agents = new List<int>();

            // Create agents and randomly assign teams.
            do
            {
                for (int i = 0; i < (maxAgents * 2); i++)
                {
                    NavAgent agent = InitNavAgent();
                    if (Random.value > 0.5)
                    {
                        agent.TeamId = 1;
                        Team1Agents.Add(agent.Id);
                    }
                    else
                    {
                        agent.TeamId = 2;
                        Team2Agents.Add(agent.Id);
                    }
                    navAgents.Add(agent);
                }
            } while (Team1Agents.Count == 0 || Team2Agents.Count == 0);

            // Get the autogenerated teams, make sure that they are the same.
            List<int> Team1 = CharacterManager.GetAllAgentsOnTeam(1);
            List<int> Team2 = CharacterManager.GetAllAgentsOnTeam(2);

            List<int> NotTeam1 = CharacterManager.GetAllAgentsNotOnTeam(1);
            List<int> NotTeam2 = CharacterManager.GetAllAgentsNotOnTeam(2);

            Assert.AreEqual(Team1Agents, Team1);
            Assert.AreEqual(Team2Agents, Team2);
            Assert.AreEqual(Team1Agents, NotTeam2);
            Assert.AreEqual(Team2Agents, NotTeam1);

            // Cleanup.
            for (int i = 0; i < navAgents.Count; i++)
                Object.DestroyImmediate(navAgents[i].gameObject);
        }

        [TearDown]
        public override void TearDown()
        {
            Object.DestroyImmediate(this.agent.gameObject);
            this.Manager.Maze.DestroyMaze();
            this.agent = null;
            CharacterManager.ClearAllRegisteredAgents();
            base.TearDown();
        }
    }
}
