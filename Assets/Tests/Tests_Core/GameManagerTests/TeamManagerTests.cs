﻿using NUnit.Framework;
using Tests;
using UnityEngine;

namespace GameManagerTests
{
    internal class TeamManagerTests : ManagedTest
    {
        private GameObject aiParent;
        private NavAgent minotaurPrefab;
        private NavAgent spartanPrefab;

        [SetUp]
        public override void SetUp()
        {
            base.SetUp();

            this.aiParent = new GameObject("AiParent");

            GameObject minotaurObject = new GameObject("Minotaur");
            this.minotaurPrefab = minotaurObject.AddComponent<NavAgent>();

            GameObject spartanObject = new GameObject("Spartan");
            this.spartanPrefab = spartanObject.AddComponent<NavAgent>();

            this.Manager.TeamManager.AiParent = this.aiParent.transform;
            this.Manager.TeamManager.CharacterPrefabs.Add(this.minotaurPrefab);
            this.Manager.TeamManager.CharacterPrefabs.Add(this.spartanPrefab);
        }

        [Test]
        public void TeamManagerValidateMinotaurTeam()
        {
            // This is all we need to do in order to have a fully functional team.
            InputInterface minotaurInterface = this.Manager.TeamManager.CreateTeam("Minotaur");

            // Ensure that the unique actions for this team have been registered.
            minotaurInterface.OnKeyDown("Lights");

            // Ensure that the correct number of NavAgents have been registered.
            Assert.AreEqual(1, this.aiParent.transform.childCount);

            minotaurInterface.TryActivateAgents(new Vector2(-1000, -1000), new Vector2(1000, 1000));
            Assert.AreEqual(1, minotaurInterface.ActiveAgents.Count);
        }

        [Test]
        public void TeamManagerValidateSpartanTeam()
        {
            // This is all we need to do in order to have a fully functional team.
            InputInterface spartanInterface = this.Manager.TeamManager.CreateTeam("Spartan");

            // Ensure that the correct number of NavAgents have been registered.
            Assert.AreEqual(5, this.aiParent.transform.childCount);

            spartanInterface.TryActivateAgents(new Vector2(-1000, -1000), new Vector2(1000, 1000));
            Assert.AreEqual(5, spartanInterface.ActiveAgents.Count);

        }

        [TearDown]
        public override void TearDown()
        {
            Object.DestroyImmediate(this.aiParent);
            Object.DestroyImmediate(this.minotaurPrefab.gameObject);
            Object.DestroyImmediate(this.spartanPrefab.gameObject);

            base.TearDown();
        }
    }
}
