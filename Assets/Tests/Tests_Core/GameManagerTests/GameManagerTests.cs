﻿using NUnit.Framework;

namespace GameManagerTests
{
    public class GameManagerTests
    {
        // A Test behaves as an ordinary method
        [Test]
        public void TeamManagerTestsSimplePasses() => Logging.LogWarning("There are currently no GameManager tests defined for EditMode! We should have some here.");
    }
}
