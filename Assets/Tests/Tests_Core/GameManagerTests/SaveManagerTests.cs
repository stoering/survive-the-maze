using NUnit.Framework;
using Tests;
using UnityEngine;

namespace GameManagerTests
{
    internal class SaveManagerTests : ManagedTest
    {
        [Test]
        public void SaveLoadSettingsTest()
        {
            GameManager.LoadPreferences();
            bool originalValue = GameManager.Preferences.ModInterface.DebuggerEnabled;

            // Use the DebuggerEnabled field to test the overall system.
            // Individual modules should test their save data extraction on their own.
            GameManager.Preferences.ModInterface.DebuggerEnabled = false;
            GameManager.SavePreferences();

            // Saving should not change the values.
            Assert.False(GameManager.Preferences.ModInterface.DebuggerEnabled);
            GameManager.Preferences.ModInterface.DebuggerEnabled = true;

            // Load the values.
            GameManager.LoadPreferences();
            Assert.False(GameManager.Preferences.ModInterface.DebuggerEnabled);

            // Restore the original value.
            GameManager.Preferences.ModInterface.DebuggerEnabled = originalValue;
            GameManager.SaveGame();
        }

        [Test]
        public void SaveLoadGameTest()
        {
            // Use the DebuggerEnabled field to test the overall system.
            // Individual modules should test their save data extraction on their own.
            GameManager.SaveData.TestBool = false;
            GameManager.SaveGame();

            // Saving should not change the values.
            Assert.False(GameManager.SaveData.TestBool);
            GameManager.SaveData.TestBool = true;

            // Save to a custom file, then load the default.
            //yield return RunCommand("SaveGame Temp", "LoadGame");
            GameManager.SaveGame("Temp");
            GameManager.LoadGame();
            Assert.False(GameManager.SaveData.TestBool);

            GameManager.LoadGame("Temp");
            Assert.True(GameManager.SaveData.TestBool);

            // File doesn't exist, shouldn't change anything.
            GameManager.LoadGame("InvalidGameId");
            Assert.True(GameManager.SaveData.TestBool);
            GameManager.SaveGame();
        }

        [Test]
        // Simple functionality test, more advanced MazeSaveLoad tests should be done by the Maze Manager module unit tests.
        public void SaveLoadMazeTest()
        {
            // Setup a new maze
            this.Manager.Maze.CreateMaze(10);
            WallManager.CreateWall(new Vector2Int(5, 5), WallPosition.UP);

            // Save the maze to file
            GameManager.SaveGame();

            // Clear the maze
            this.Manager.Maze.CreateMaze(10);

            // Save to a different file.
            GameManager.SaveGame("Temp");
            Assert.IsNull(WallManager.GetWallObject(new Vector2Int(5, 5)), "Wall should not exist here!");

            // Load the maze, validate that it matches the saved maze.
            GameManager.LoadGame();
            Assert.IsNotNull(WallManager.GetWallObject(new Vector2Int(5, 5)), "Wall should exist here!");
        }
    }
}
