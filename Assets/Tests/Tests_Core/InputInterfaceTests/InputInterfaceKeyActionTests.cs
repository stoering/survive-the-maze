﻿using System.Collections.Generic;
using NUnit.Framework;
using Tests;
using UnityEngine;

namespace InputInterfaceTests
{
    [Category("Static Tests")]
    internal class InputInterfaceKeyActionTests : NavAgentBaseTests
    {
        private bool KeyPressed;
        private bool KeyHeld;
        private bool KeyReleased;
        private InputInterface Interface;

        [SetUp]
        public override void SetUp()
        {
            base.SetUp();
            ResetKeys();
            this.Interface = new InputInterface();
            this.Interface.ClearKeyActions();
        }

        [Test]
        public void KeyActionAddAndRetrieve()
        {
            // Add new keyActions to the interface.
            for (int i = 0; i < 10; i++)
                this.Interface.AddKeyAction(new KeyAction { Key = (KeyCode)i });

            // Add the keyAction we care about.
            KeyAction testKey = new KeyAction
            {
                Action = "test",
                Key = KeyCode.A,
            };
            this.Interface.AddKeyAction(testKey);

            for (int i = 0; i < 10; i++)
                this.Interface.AddKeyAction(new KeyAction { Key = (KeyCode)(i + 10) });

            // Get the KeyAction by key and name, make sure that it is the same one we added.
            Assert.AreSame(testKey, this.Interface.GetKeyAction(testKey.Key));
            Assert.AreSame(testKey, this.Interface.GetKeyAction(testKey.Action));

            // Make sure that our list contains our test keyAction. Also check the length.
            Assert.IsTrue(this.Interface.KeyAction.Contains(testKey));
            Assert.AreEqual(21, this.Interface.KeyAction.Count);

            // Clear out all KeyActions.
            this.Interface.ClearKeyActions();

            // Make sure that our list does not contain our test keyAction. Also check the length.
            Assert.IsFalse(this.Interface.KeyAction.Contains(testKey));
            Assert.AreEqual(0, this.Interface.KeyAction.Count);

            // Make sure that we are getting a new KeyAction if ours isn't defined.
            try
            {
                _ = this.Interface.GetKeyAction(testKey.Key);
                Assert.Fail();
            }
            catch (KeyNotFoundException)
            {
                // We happy
            }
            try
            {
                _ = this.Interface.GetKeyAction(testKey.Action);
                Assert.Fail();
            }
            catch (KeyNotFoundException)
            {
                // We happy
            }

            try
            {
                // Try to add a KeyAction twice. Should NOT work.
                this.Interface.AddKeyAction(testKey);
                this.Interface.AddKeyAction(testKey);
                Assert.Fail("Should have thrown an exception here.");
            }
            catch (System.ArgumentException)
            {
                // We happy
            }
        }

        [Test]
        public void KeyActionCallByKeyTest()
        {
            KeyAction keyAction = new KeyAction
            {
                Action = "Test",
                Key = KeyCode.A,
                ProcessKeyDown = OnKeyDown,
                ProcessKeyHold = OnKeyHold,
                ProcessKeyUp = OnKeyUp
            };

            this.Interface.AddKeyAction(keyAction);

            // Tap and hold, update should call "KeyHeld"
            Assert.IsFalse(this.KeyPressed);
            Assert.IsFalse(this.KeyHeld);
            Assert.IsFalse(this.KeyReleased);
            ResetKeys();

            this.Interface.OnKeyDown(KeyCode.A);

            Assert.IsTrue(this.KeyPressed);
            Assert.IsFalse(this.KeyHeld);
            Assert.IsFalse(this.KeyReleased);
            ResetKeys();

            // Update should call OnKeyHold once per frame.
            for (int i = 0; i < 10; i++)
            {
                this.Interface.Update();

                Assert.IsFalse(this.KeyPressed);
                Assert.IsTrue(this.KeyHeld);
                Assert.IsFalse(this.KeyReleased);
                ResetKeys();
            }

            this.Interface.OnKeyUp(KeyCode.A);

            Assert.IsFalse(this.KeyPressed);
            Assert.IsFalse(this.KeyHeld);
            Assert.IsTrue(this.KeyReleased);
            ResetKeys();

            this.Interface.Update();

            Assert.IsFalse(this.KeyPressed);
            Assert.IsFalse(this.KeyHeld);
            Assert.IsFalse(this.KeyReleased);
            ResetKeys();

            this.Interface.OnKeyTap(KeyCode.A);

            Assert.IsTrue(this.KeyPressed);
            Assert.IsFalse(this.KeyHeld);
            Assert.IsFalse(this.KeyReleased);
            ResetKeys();

            this.Interface.Update();

            Assert.IsFalse(this.KeyPressed);
            Assert.IsFalse(this.KeyHeld);
            Assert.IsFalse(this.KeyReleased);
        }

        [Test]
        public void KeyActionCallByNameTest()
        {
            string actionName = "Test";

            KeyAction keyAction = new KeyAction
            {
                Action = actionName,
                Key = KeyCode.A,
                ProcessKeyDown = OnKeyDown,
                ProcessKeyHold = OnKeyHold,
                ProcessKeyUp = OnKeyUp
            };

            this.Interface.AddKeyAction(keyAction);

            // Tap and hold, update should call "KeyHeld"
            Assert.IsFalse(this.KeyPressed);
            Assert.IsFalse(this.KeyHeld);
            Assert.IsFalse(this.KeyReleased);
            ResetKeys();

            this.Interface.OnKeyDown(actionName);

            Assert.IsTrue(this.KeyPressed);
            Assert.IsFalse(this.KeyHeld);
            Assert.IsFalse(this.KeyReleased);
            ResetKeys();

            // Update should call OnKeyHold once per frame.
            for (int i = 0; i < 10; i++)
            {
                this.Interface.Update();

                Assert.IsFalse(this.KeyPressed);
                Assert.IsTrue(this.KeyHeld);
                Assert.IsFalse(this.KeyReleased);
                ResetKeys();
            }

            this.Interface.OnKeyUp(actionName);

            Assert.IsFalse(this.KeyPressed);
            Assert.IsFalse(this.KeyHeld);
            Assert.IsTrue(this.KeyReleased);
            ResetKeys();

            this.Interface.Update();

            Assert.IsFalse(this.KeyPressed);
            Assert.IsFalse(this.KeyHeld);
            Assert.IsFalse(this.KeyReleased);
            ResetKeys();

            this.Interface.OnKeyTap(actionName);

            Assert.IsTrue(this.KeyPressed);
            Assert.IsFalse(this.KeyHeld);
            Assert.IsFalse(this.KeyReleased);
            ResetKeys();

            this.Interface.Update();

            Assert.IsFalse(this.KeyPressed);
            Assert.IsFalse(this.KeyHeld);
            Assert.IsFalse(this.KeyReleased);
        }

        [Test]
        public void InputInterfaceShiftModifyTest()
        {
            try
            {
                this.Interface.OnKeyTap("Shift");
                Assert.Fail();
            }
            catch (KeyNotFoundException)
            {
                // We happy
            }

            // Create the shift key actions.
            this.Interface.RegisterDefaultKeyActions();

            // Shift test - make sure the default "shift" key works as expected.
            Assert.IsFalse(this.Interface.ModifySelect);
            this.Interface.OnKeyDown("Shift");
            Assert.IsTrue(this.Interface.ModifySelect);
            this.Interface.Update();
            Assert.IsTrue(this.Interface.ModifySelect);
            this.Interface.OnKeyUp("Shift");
            Assert.IsFalse(this.Interface.ModifySelect);
            this.Interface.OnKeyTap("Shift");
            Assert.IsTrue(this.Interface.ModifySelect);
            this.Interface.OnKeyTap("Shift");
            Assert.IsTrue(this.Interface.ModifySelect);
            this.Interface.OnKeyUp("Shift");
            Assert.IsFalse(this.Interface.ModifySelect);

            // Same thing, but with the left shift key.
            Assert.IsFalse(this.Interface.ModifySelect);
            this.Interface.OnKeyDown(KeyCode.LeftShift);
            Assert.IsTrue(this.Interface.ModifySelect);
            this.Interface.Update();
            Assert.IsTrue(this.Interface.ModifySelect);
            this.Interface.OnKeyUp(KeyCode.LeftShift);
            Assert.IsFalse(this.Interface.ModifySelect);
            this.Interface.OnKeyTap(KeyCode.LeftShift);
            Assert.IsTrue(this.Interface.ModifySelect);
            this.Interface.OnKeyTap(KeyCode.LeftShift);
            Assert.IsTrue(this.Interface.ModifySelect);
            this.Interface.OnKeyUp(KeyCode.LeftShift);
            Assert.IsFalse(this.Interface.ModifySelect);

            // Same thing, but with the right shift key.
            Assert.IsFalse(this.Interface.ModifySelect);
            this.Interface.OnKeyDown(KeyCode.RightShift);
            Assert.IsTrue(this.Interface.ModifySelect);
            this.Interface.Update();
            Assert.IsTrue(this.Interface.ModifySelect);
            this.Interface.OnKeyUp(KeyCode.RightShift);
            Assert.IsFalse(this.Interface.ModifySelect);
            this.Interface.OnKeyTap(KeyCode.RightShift);
            Assert.IsTrue(this.Interface.ModifySelect);
            this.Interface.OnKeyTap(KeyCode.RightShift);
            Assert.IsTrue(this.Interface.ModifySelect);
            this.Interface.OnKeyUp(KeyCode.RightShift);
            Assert.IsFalse(this.Interface.ModifySelect);

            // Make sure using both doesn't mess anything up.
            Assert.IsFalse(this.Interface.ModifySelect);
            this.Interface.OnKeyDown(KeyCode.RightShift);
            Assert.IsTrue(this.Interface.ModifySelect);
            this.Interface.OnKeyDown(KeyCode.LeftShift);
            Assert.IsTrue(this.Interface.ModifySelect);
            this.Interface.OnKeyUp(KeyCode.RightShift);
            Assert.IsFalse(this.Interface.ModifySelect);
            this.Interface.OnKeyUp(KeyCode.LeftShift);
            Assert.IsFalse(this.Interface.ModifySelect);
        }

        private void ResetKeys()
        {
            this.KeyPressed = false;
            this.KeyHeld = false;
            this.KeyReleased = false;
        }

        private void OnKeyDown() => this.KeyPressed = true;
        private void OnKeyHold() => this.KeyHeld = true;
        private void OnKeyUp() => this.KeyReleased = true;
    }
}
