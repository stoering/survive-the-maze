﻿using NUnit.Framework;
using Tests;
using UnityEngine;

namespace InputInterfaceTests
{
    internal class MinotaurTeamInterfaceTests : NavAgentBaseTests
    {
        [Category("Static Tests")]
        [Test]
        public void LightsWallsAction()
        {
            // Make sure we set up the correct keyactions for the Minotaur.
            GameObject lightObject = new GameObject("Light");

            this.Manager.TeamManager.GlobalActions.Light = lightObject;
            
            InputInterface MinotaurInterface = this.Manager.TeamManager.CreateTeam("Minotaur");

            // Each call should toggle the light. Name should be "Lights," default key should be "L"
            Assert.True(lightObject.activeSelf);
            MinotaurInterface.OnKeyDown("Lights");
            Assert.False(lightObject.activeSelf);
            MinotaurInterface.Update(); // Should do nothing for this command.
            MinotaurInterface.OnKeyUp("Lights");
            Assert.False(lightObject.activeSelf);
            MinotaurInterface.OnKeyTap(KeyCode.L);
            Assert.True(lightObject.activeSelf);
            MinotaurInterface.OnKeyTap(KeyCode.L);
            Assert.False(lightObject.activeSelf);
            // Destroy the light, make sure we don't barf.
            Object.DestroyImmediate(lightObject);
            MinotaurInterface.OnKeyTap(KeyCode.L);

            // Sending a left click should create or destory a wall segment.
            this.Manager.Maze.CreateMaze(mazeSize);
            Vector2Int wallPosition = this.RandomVector2i;
            Assert.False(WallManager.WallExists(wallPosition));
            MinotaurInterface.Interact(wallPosition);
            Assert.True(WallManager.WallExists(wallPosition)); // Wall was created.
            MinotaurInterface.Interact(wallPosition);
            Assert.True(WallManager.GetWall(wallPosition).WallPosition == WallPosition.FALLING,
                "Wall position should be falling after click, instead is " + WallManager.GetWall(wallPosition).WallPosition.ToString()); // Wall is being removed.

            // Send clicks outside of the maze. Should not raise any exceptions, but should not create a wall either.
            for (int i = 0; i < 10; i++)
            {
                wallPosition = MazeManager.MazeSize + this.RandomVector2i;
                MinotaurInterface.Interact(wallPosition);
                MinotaurInterface.Interact(wallPosition);
            }

            // Cleanup.
            this.Manager.Maze.DestroyMaze();
            Object.DestroyImmediate(lightObject);
            WallFactory.Reset(); // So we don't keep a persistent list.
        }

        [Category("Random Seed")]
        [Test]
        public void DragSelectTest()
        {
            // Make sure we set up the correct keyactions for the Minotaur.
            InputInterface MinotaurInterface = this.Manager.TeamManager.CreateTeam("Minotaur");
            // If we ever define this function, we will need to test it. For now, ensure that it does not throw an exception.
            MinotaurInterface.Interact(this.RandomVector2, this.RandomVector2);
        }
    }
}
