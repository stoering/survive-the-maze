﻿using System.Collections.Generic;
using NUnit.Framework;
using Tests;
using UnityEngine;

namespace InputInterfaceTests
{
    [Category("Random Seed")]
    internal class InputInterfaceNavAgentTests : NavAgentBaseTests
    {
        private Vector2 AgentGridDestination;
        private NavAgent agent;
        private Test_MeshAgent meshAgent;
        private InputInterface Interface;

        [SetUp]
        public override void SetUp()
        {
            base.SetUp();

            this.Interface = this.Manager.TeamManager.CreateTeam("Generic");
            this.agent = InitNavAgent();
            this.meshAgent = AddMeshAgentToNavAgent(this.agent);
            this.Manager.Maze.CreateMaze(new Vector2Int(mazeSize, mazeSize));
            this.Interface.SetActiveAgent(this.agent);
        }

        [Test]
        public void NavAgentGoToLocation()
        {
            // We should be retargetting each time we complete a NavMeshAgent move.
            for (int i = 0; i < 100; i++)
            {
                this.AgentGridDestination = this.RandomVector2;
                this.Interface.Move(this.AgentGridDestination);
                PathfindingUpdateLoop(this.agent, this.meshAgent);
                Assert.AreEqual(this.AgentGridDestination, this.meshAgent.GridDestination);
            }
        }

        [Test]
        public void NavAgentQueueLocations()
        {
            // Make sure we still have "Shift" as a default action.
            this.Interface.RegisterDefaultKeyActions();
            
            // Hold the "shift" key down.
            this.Interface.GetKeyAction("Shift").OnKeyDown();

            // Create a list of waypoints.
            List<Vector2> waypoints = new List<Vector2>();
            for (int i = 0; i < 50; i++)
            {
                Vector2 temp = this.RandomVector2;
                while ((i > 0) && (Vector2.Distance(temp, waypoints[i - 1]) < 1))
                    temp = this.RandomVector2;

                waypoints.Add(temp);
                this.Interface.Move(temp);
            }

            // Test that we go to each waypoint in order.
            for (int i = 0; i < 50; i++)
            {
                PathfindingUpdateLoop(this.agent, this.meshAgent);
                Assert.AreEqual(waypoints[i], this.meshAgent.GridDestination);
            }

            // Re-queue the waypoints.
            for (int i = 0; i < 50; i++)
                this.Interface.Move(waypoints[i]);

            // Release the "shift" key.
            this.Interface.GetKeyAction("Shift").OnKeyUp();

            // We should discard all of them when we get a direct waypoint.
            this.Interface.Move(new Vector2(101, 101)); // Out of the random range.
            for (int i = 0; i < 50; i++)
            {
                PathfindingUpdateLoop(this.agent, this.meshAgent);
                Assert.AreNotEqual(waypoints[i], this.meshAgent.GridDestination);
            }
        }

        [Test]
        public void NavAgentFollowTarget()
        {
            // Create a gameobject to go to. Make sure it has a unique teamId.
            NavAgent targetObject = InitNavAgent();
            targetObject.TeamId = this.agent.TeamId + 1;
            // Set the agent on top of our agent to make sure we select the right one.
            targetObject.transform.position = this.agent.transform.position;
            this.Interface.Move(PositionManager.WorldToGridSpace(targetObject.transform.position));

            for (int i = 0; i < 100; i++)
            {
                Vector3 temp = this.RandomVector3;
                targetObject.transform.position = temp;
                this.agent.Retarget();
                this.agent.CancelInvoke();
                PathfindingUpdateLoop(this.agent, this.meshAgent);
                Assert.AreEqual(temp, this.meshAgent.Destination, "Failed on loop " + i.ToString());
            }

            // Change the team of the target object to out own team. We should not follow them any more.
            targetObject.TeamId = this.agent.TeamId;
            targetObject.transform.position = this.RandomVector3;
            this.Interface.Move(PositionManager.WorldToGridSpace(targetObject.transform.position));

            for (int i = 0; i < 100; i++)
            {
                Vector3 temp = this.RandomVector3;
                targetObject.transform.position = temp;
                this.agent.Retarget();
                PathfindingUpdateLoop(this.agent, this.meshAgent);
                Assert.AreNotEqual(temp, this.meshAgent.Destination);
            }

            // Kill our agent. (Check for null checking).
            targetObject.TeamId = this.agent.TeamId + 1;
            this.Interface.ClearTeam();
            this.Interface.Move(PositionManager.WorldToGridSpace(targetObject.transform.position));

            // Cleanup.
            Object.DestroyImmediate(targetObject.gameObject);
        }

        [Test]
        public void NavAgentDestroyedTest()
        {
            // Call all of the things to make sure they don't barf.
            this.Interface.ClearTeam();
            this.Interface.RegisterDefaultKeyActions();

            this.Interface.Move(this.RandomVector2);
            this.Interface.Interact(this.RandomVector2);
            this.Interface.Interact(this.RandomVector2, this.RandomVector2);
            this.Interface.Update();
            this.Interface.OnKeyDown(KeyCode.LeftShift);

            this.Interface.Move(this.RandomVector2);
            this.Interface.Interact(this.RandomVector2);
            this.Interface.Interact(this.RandomVector2, this.RandomVector2);
            this.Interface.Update();
            this.Interface.OnKeyUp(KeyCode.LeftShift);

        }

        [Test]
        public void NavAgentManageTeamIdTest()
        {
            int teamId = Random.Range(0, int.MaxValue);
            this.Interface.ClearTeam();
            this.agent.TeamId = Random.Range(0, int.MaxValue);

            // Add the agent to the team, then make sure they are a team.
            this.Interface.TeamId = teamId;
            Assert.AreEqual(teamId, this.Interface.TeamId, "InputInterface.TeamId = teamId");

            Assert.AreNotEqual(teamId, this.agent.TeamId);
            this.Interface.AddAgentToTeam(this.agent);
            Assert.AreEqual(teamId, this.agent.TeamId, "InputInterface.NavAgent = agent)");

            // Change the team. Agent should update to match.
            teamId = Random.Range(0, int.MaxValue);
            this.Interface.TeamId = teamId;
            Assert.AreEqual(teamId, this.agent.TeamId, "InputInterface.TeamId = teamId");

            // Unregister the team. They should change their IDs to be zero so they are not mistaken for a part of the team.
            this.Interface.ClearTeam();
            teamId = Random.Range(0, int.MaxValue);
            this.Interface.TeamId = teamId;
            Assert.AreEqual(0, this.agent.TeamId, "InputInterface.NavAgent = null should set ID to 0");
            Assert.AreNotEqual(teamId, this.agent.TeamId, "InputInterface.NavAgent = null");
        }

        [Test]
        public void InputInterfaceMemberTests()
        {
            // Assorted quick, tiny tests.
            this.Interface.AddAgentToTeam(this.agent);
            this.agent.TeamId = Random.Range(0, int.MaxValue);

            // Team test - Interface.Team should match agent team.
            Assert.AreNotEqual(this.agent.TeamId, this.Interface.TeamId);
            this.Interface.TeamId = Random.Range(0, 100);
            Assert.AreEqual(this.agent.TeamId, this.Interface.TeamId);

            // Team test part II - setting the agent after the team should provide the same result.
            this.agent.TeamId = Random.Range(0, int.MaxValue);
            Assert.AreNotEqual(this.agent.TeamId, this.Interface.TeamId);
            this.Interface.AddAgentToTeam(this.agent);
            Assert.AreEqual(this.Interface.TeamId, this.agent.TeamId);

            // We are testing the teams functionality in the Spartan team tests.
        }

        [TearDown]
        public override void TearDown()
        {
            if (this.agent != null)
                Object.DestroyImmediate(this.agent.gameObject);

            this.agent = null;
            this.meshAgent = null;
            CharacterManager.ClearAllRegisteredAgents();
            base.TearDown();
        }
    }
}
