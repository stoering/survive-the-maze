﻿using System.Collections.Generic;
using NUnit.Framework;
using Tests;
using UnityEngine;

namespace InputInterfaceTests
{
    [Category("Static Tests")]
    internal class SpartanTeamInterfaceTests : NavAgentBaseTests
    {
        private const int NavAgentCount = 10;
        private const int SmallLoop = 5;
        private List<NavAgent> navAgents;
        private InputInterface SpartanInterface;

        [SetUp]
        public void CreateInterface() => this.SpartanInterface = this.Manager.TeamManager.CreateTeam("Spartan");

        [Test]
        public void SelectNavAgentWithKeyboard()
        {
            // Test selects one NavAgent at a time with the keyboard.
            CreateNavAgentList();
            
            // Use the keys 1 - 0 as the default select agent keycodes.
            KeyCode[] keys =
            {
                KeyCode.Alpha1,
                KeyCode.Alpha2,
                KeyCode.Alpha3,
                KeyCode.Alpha4,
                KeyCode.Alpha5,
                KeyCode.Alpha6,
                KeyCode.Alpha7,
                KeyCode.Alpha8,
                KeyCode.Alpha9,
                KeyCode.Alpha0,
            };
            
            // Select the NavAgent with OnKeyTap (what scripts may use.)
            for (int i = 0; i < NavAgentCount; i++)
            {
                this.SpartanInterface.OnKeyTap(keys[i]);
                Assert.AreSame(this.navAgents[i], this.SpartanInterface.ActiveAgents[0], "NavAgent Select Agent " + i.ToString() +
                    " via key " + keys[i].ToString() + ". (OnKeyTap)");
            }
            // Select the NavAgent with OnKeyDown, OnKeyUp (what the player does.)
            for (int i = 0; i < NavAgentCount; i++)
            {
                this.SpartanInterface.OnKeyDown(keys[i]);
                this.SpartanInterface.OnKeyUp(keys[i]);
                Assert.AreSame(this.navAgents[i], this.SpartanInterface.ActiveAgents[0], "NavAgent Select Agent " + i.ToString() +
                    " via key " + keys[i].ToString() + ". (OnKeyDown)");
            }
            // Clear the team, make sure we are clearing our active agents as well.
            this.SpartanInterface.ClearTeam();
            for (int i = 0; i < NavAgentCount; i++)
            {
                this.SpartanInterface.OnKeyTap(keys[i]);
                Assert.Zero(this.SpartanInterface.ActiveAgents.Count, "NavAgent Select Agent " + i.ToString() +
                    " via key " + keys[i].ToString() + ". (Should fail to find agent)");
                this.SpartanInterface.AddAgentToTeam(this.navAgents[i]); // for the next test.
            }

            // Remove agents through Destroy. Null references should not be passed to the active NavAgent.
            NavAgent rogueAgent = InitNavAgent();
            this.SpartanInterface.AddAgentToTeam(rogueAgent);
            this.SpartanInterface.ActiveAgents.Add(rogueAgent);
            for (int i = 0; i < NavAgentCount; i++)
            {
                Object.DestroyImmediate(this.navAgents[i].gameObject);
                this.SpartanInterface.OnKeyTap(keys[i]);
                Assert.NotZero(this.SpartanInterface.ActiveAgents.Count);
            }
            Object.DestroyImmediate(rogueAgent.gameObject); // Cleanup :-)
        }

        [Test]
        public void SelectNavAgentsWithKeyboard()
        {
            // Test selects multiple NavAgents at once with the keyboard.
            CreateNavAgentList();

            // Use the keys 1 - 0 as the default select agent keycodes.
            KeyCode[] keys =
            {
                KeyCode.Alpha1,
                KeyCode.Alpha2,
                KeyCode.Alpha3,
                KeyCode.Alpha4,
                KeyCode.Alpha5,
                KeyCode.Alpha6,
                KeyCode.Alpha7,
                KeyCode.Alpha8,
                KeyCode.Alpha9,
                KeyCode.Alpha0,
            };

            // Select each NavAgent in turn, previous agents should still be selected.
            this.SpartanInterface.OnKeyDown(KeyCode.LeftShift);
            for (int i = 0; i < NavAgentCount; i++)
            {
                this.SpartanInterface.OnKeyTap(keys[i]);

                Assert.AreEqual(i + 1, this.SpartanInterface.ActiveAgents.Count, "Interface did not add as many agents as expected.");
                for (int j = i; j >= 0; j--)
                {
                    Assert.AreSame(this.navAgents[j], this.SpartanInterface.ActiveAgents[j], "NavAgent Select Agents " + i.ToString() +
                    " via key " + keys[i].ToString() + ". (OnKeyTap)");
                }
                // Make sure we cannot double-add the same agent.
                this.SpartanInterface.OnKeyTap(keys[i]);
                Assert.AreEqual(i + 1, this.SpartanInterface.ActiveAgents.Count, "Interface added an agent twice.");
            }
            this.SpartanInterface.OnKeyUp(KeyCode.LeftShift);

            this.SpartanInterface.ActiveAgents.Clear();

            // Select each NavAgent in turn, previous agents should still be selected.
            for (int i = 0; i < NavAgentCount; i++)
            {
                this.SpartanInterface.OnKeyDown(KeyCode.LeftShift);
                this.SpartanInterface.OnKeyTap(keys[i]);
                this.SpartanInterface.OnKeyUp(KeyCode.LeftShift);
             
                Assert.AreEqual(i + 1, this.SpartanInterface.ActiveAgents.Count, "Interface did not add as many agents as expected.");
                for (int j = i; j >= 0; j--)
                {
                    Assert.AreSame(this.navAgents[j], this.SpartanInterface.ActiveAgents[j], "NavAgent Select Agents " + i.ToString() +
                    " via key " + keys[i].ToString() + ". (OnKeyTap)");
                }

                // Make sure we cannot double-add the same agent.
                this.SpartanInterface.OnKeyDown(KeyCode.LeftShift);
                this.SpartanInterface.OnKeyTap(keys[i]);
                this.SpartanInterface.OnKeyUp(KeyCode.LeftShift);
                Assert.AreEqual(i + 1, this.SpartanInterface.ActiveAgents.Count, "Interface added an agent twice.");
            }

            // Make sure that we are not moving, then send a single click to move all agents at the same time.
            for (int i = 0; i < NavAgentCount; i++)
                Assert.False(this.navAgents[i].MeshAgent.PathPending, "No NavAgents should be moving at this time");

            this.SpartanInterface.Move(this.RandomVector2i);
            for (int i = 0; i < NavAgentCount; i++)
                Assert.True(this.navAgents[i].MeshAgent.PathPending, "All of our NavAgents should have received a command to begin moving.");

            // Cleanup.
            DestroyNavAgentList();
        }

        [Test]
        public void SelectNavAgentWithMouse()
        {
            // Test selects one NavAgent at a time with the mouse.
            CreateNavAgentList();
            this.SpartanInterface.TeamId = Random.Range(0, int.MaxValue);

            // Select a random agent from the team to activate. Make sure we actually selected it.
            for (int i = 0; i < SmallLoop; i++)
            {
                NavAgent specialAgent = this.navAgents[Random.Range(0, this.navAgents.Count)];
                this.SpartanInterface.Interact(CharacterManager.NavAgentToGridSpace(specialAgent.Id));
                Assert.AreSame(specialAgent, this.SpartanInterface.ActiveAgents[0], "SpartanTeamInterface.Interact " +
                    "failed to find NavAgent" + specialAgent.Id.ToString() + ".");
            }
            DestroyNavAgentList();
        }

        [Test]
        public void SelectNavAgentsWithMouse()
        {
            // Test selects multiple NavAgents at once with the mouse.
            CreateNavAgentList();
            this.SpartanInterface.TeamId = Random.Range(0, int.MaxValue);

            // Select each NavAgent in turn, previous agents should still be selected.
            this.SpartanInterface.OnKeyDown(KeyCode.LeftShift);
            for (int i = 0; i < NavAgentCount; i++)
            {
                this.SpartanInterface.Interact(CharacterManager.NavAgentToGridSpace(this.navAgents[i].Id));

                Assert.AreEqual(i + 1, this.SpartanInterface.ActiveAgents.Count, "Interface did not add as many agents as expected.");
                for (int j = i; j >= 0; j--)
                {
                    Assert.AreSame(this.navAgents[j], this.SpartanInterface.ActiveAgents[j], "NavAgent Select Agents " + i.ToString() +
                    " with mouse.");
                }
                // Make sure we cannot double-add the same agent.
                this.SpartanInterface.Interact(CharacterManager.NavAgentToGridSpace(this.navAgents[i].Id));
                Assert.AreEqual(i + 1, this.SpartanInterface.ActiveAgents.Count, "Interface added an agent twice.");
            }
            this.SpartanInterface.OnKeyUp(KeyCode.LeftShift);
            this.SpartanInterface.ActiveAgents.Clear();

            // Select each NavAgent in turn, previous agents should still be selected.
            for (int i = 0; i < NavAgentCount; i++)
            {
                this.SpartanInterface.OnKeyDown(KeyCode.LeftShift);
                this.SpartanInterface.Interact(CharacterManager.NavAgentToGridSpace(this.navAgents[i].Id));
                this.SpartanInterface.OnKeyUp(KeyCode.LeftShift);

                Assert.AreEqual(i + 1, this.SpartanInterface.ActiveAgents.Count, "Interface did not add as many agents as expected.");
                for (int j = i; j >= 0; j--)
                {
                    Assert.AreSame(this.navAgents[j], this.SpartanInterface.ActiveAgents[j], "NavAgent Select Agents " + i.ToString() +
                    " with mouse.");
                }
                // Make sure we cannot double-add the same agent.
                this.SpartanInterface.OnKeyDown(KeyCode.LeftShift);
                this.SpartanInterface.Interact(CharacterManager.NavAgentToGridSpace(this.navAgents[i].Id));
                this.SpartanInterface.OnKeyUp(KeyCode.LeftShift);
                Assert.AreEqual(i + 1, this.SpartanInterface.ActiveAgents.Count, "Interface added an agent twice.");
            }

            // Make sure that we are not moving, then send a single click to move all agents at the same time.
            for (int i = 0; i < NavAgentCount; i++)
                Assert.False(this.navAgents[i].MeshAgent.PathPending, "No NavAgents should be moving at this time");

            this.SpartanInterface.Move(this.RandomVector2i);
            for (int i = 0; i < NavAgentCount; i++)
                Assert.True(this.navAgents[i].MeshAgent.PathPending, "All of our NavAgents should have received a command to begin moving.");

            // Cleanup.
            DestroyNavAgentList();
        }

        [Test]
        public void DragToSelectAgents()
        {
            // Test selects multiple NavAgents at once with the mouse.
            CreateNavAgentList();
            this.SpartanInterface.TeamId = Random.Range(0, int.MaxValue);
            bool[] isInSquare = new bool[NavAgentCount];
            Vector2 lowerBounds = this.RandomVector2;
            Vector2 upperBounds = lowerBounds + this.RandomVector2;

            // Place some agents within our square, and some without.
            isInSquare[0] = true; // Exactly on the lower bounds.
            this.navAgents[0].transform.position = PositionManager.GridToWorldSpace(lowerBounds);
            this.navAgents[0].name = "lowerBounds";

            isInSquare[1] = true; // Exactly on the upper bounds.
            this.navAgents[1].transform.position = PositionManager.GridToWorldSpace(upperBounds);
            this.navAgents[1].name = "upperBounds";

            isInSquare[2] = true; // Right in the middle.
            this.navAgents[2].transform.position = PositionManager.GridToWorldSpace((lowerBounds + upperBounds ) / 2f);
            this.navAgents[2].name = "Center";

            isInSquare[3] = false; // Outside - above.
            this.navAgents[3].transform.position = PositionManager.GridToWorldSpace(upperBounds + Vector2.up);
            this.navAgents[3].name = "Above";

            isInSquare[4] = false; // Outside - below.
            this.navAgents[4].transform.position = PositionManager.GridToWorldSpace(lowerBounds + Vector2.down);
            this.navAgents[4].name = "Below";

            isInSquare[5] = false; // Outside - to the left.
            this.navAgents[5].transform.position = PositionManager.GridToWorldSpace(lowerBounds + Vector2.left);
            this.navAgents[5].name = "Left";

            isInSquare[6] = false; // Outside - to the right.
            this.navAgents[6].transform.position = PositionManager.GridToWorldSpace(upperBounds + Vector2.right);
            this.navAgents[6].name = "Right";

            isInSquare[7] = true;
            this.navAgents[7].transform.position = PositionManager.GridToWorldSpace(
                new Vector2(Random.Range(lowerBounds.x, upperBounds.x), Random.Range(lowerBounds.y, upperBounds.y)));
            this.navAgents[7].name = "RandomInSquare";

            isInSquare[8] = true;
            this.navAgents[8].name = "LowerX,UpperY";
            this.navAgents[8].transform.position = PositionManager.GridToWorldSpace(new Vector2(lowerBounds.x, upperBounds.y));

            isInSquare[9] = true;
            this.navAgents[9].transform.position = PositionManager.GridToWorldSpace(new Vector2(upperBounds.x, lowerBounds.y));
            this.navAgents[9].name = "UpperX,LowerY";

            // Draw a square between "lowerBounds" and "upperBounds"
            this.SpartanInterface.Interact(lowerBounds, upperBounds);
            Assert.AreEqual(6, this.SpartanInterface.ActiveAgents.Count, "Interface added the wrong number of agents.");
            
            // Make sure that we are not moving, then send a single click to move all agents at the same time.
            for (int i = 0; i < NavAgentCount; i++)
                Assert.False(this.navAgents[i].MeshAgent.PathPending, "No NavAgents should be moving at this time");

            this.SpartanInterface.Move(this.RandomVector2i);
            for (int i = 0; i < NavAgentCount; i++)
            {
                if (isInSquare[i])
                {
                    Assert.True(this.navAgents[i].MeshAgent.PathPending, "Nav Agent " + i.ToString() + " is inside the selection box and should have started moving.");
                }
                else
                {
                    Assert.False(this.navAgents[i].MeshAgent.PathPending, "Nav Agent " + i.ToString() + " is outside the selection box and should not have started moving.");
                }
            }

            // Cleanup.
            DestroyNavAgentList();
        }

        [Test]
        public void AdvancedDragToSelectAgents()
        {
            // Create 2 rectangles with a shared top and bottom, but different widths.
            Rect rect1 = new Rect(this.RandomVector2, this.RandomVector2);
            Rect rect2 = new Rect(rect1.xMax + 1, rect1.yMin, this.RandomVector2.x, rect1.height);
            int rect1Count = 0, rect2Count = 0;
            CreateNavAgentList();

            for (int i = 0; i < NavAgentCount; i++)
            {
                if (Random.value > 0.5f)
                {
                    rect1Count++;
                    this.navAgents[i].name = "Rect1";
                    this.navAgents[i].transform.position = PositionManager.GridToWorldSpace(RandomVectorInSquare(rect1));
                }
                else
                {
                    rect2Count++;
                    this.navAgents[i].name = "Rect2";
                    this.navAgents[i].transform.position = PositionManager.GridToWorldSpace(RandomVectorInSquare(rect2));
                }
            }

            // Select only the agents in square 1.
            this.SpartanInterface.Interact(rect1.position, rect1.position + rect1.size);
            foreach (NavAgent selectedAgent in this.SpartanInterface.ActiveAgents)
            {
                Assert.AreEqual("Rect1", selectedAgent.name, "An agent from Rect2 was seleced when only agents from Rect1 should have been");
            }
            Assert.AreEqual(rect1Count, this.SpartanInterface.ActiveAgents.Count, "Not all Navagents within Rect1 have been seleted.");

            // Select only the agents in square 2 ('invert' the square this time, shouldn't matter).
            this.SpartanInterface.Interact(rect2.position + rect2.size, rect2.position);
            foreach (NavAgent selectedAgent in this.SpartanInterface.ActiveAgents)
            {
                Assert.AreEqual("Rect2", selectedAgent.name, "An agent from Rect1 was seleced when only agents from Rect2 should have been");
            }
            Assert.AreEqual(rect2Count, this.SpartanInterface.ActiveAgents.Count, "Not all Navagents within Rect2 have been seleted.");

            // Select no agents by drawing a square containing no agents.
            this.SpartanInterface.Interact(Vector2.zero - this.RandomVector2, Vector2.zero - this.RandomVector2);
            Assert.AreEqual(0, this.SpartanInterface.ActiveAgents.Count, "No agents should have been selected this time.");

            // Use Shift to select the agents in square 1, then square 2.
            this.SpartanInterface.OnKeyDown(KeyCode.LeftShift);
            this.SpartanInterface.Interact(rect1.position, rect1.position + rect1.size);
            this.SpartanInterface.Interact(rect2.position, rect2.position + rect2.size);
            this.SpartanInterface.OnKeyUp(KeyCode.LeftShift);
            Assert.AreEqual(rect1Count + rect2Count, this.SpartanInterface.ActiveAgents.Count, "Agents from both rects should have been selected.");

            // Select no agents by drawing a square containing no agents.
            this.SpartanInterface.Interact(Vector2.zero - this.RandomVector2, Vector2.zero - this.RandomVector2);
            Assert.AreEqual(0, this.SpartanInterface.ActiveAgents.Count, "No agents should have been selected this time.");

            // Draw one final square containing all agents.
            this.SpartanInterface.OnKeyDown(KeyCode.LeftShift);
            this.SpartanInterface.Interact(rect1.position, rect2.position + rect2.size);
            Assert.AreEqual(rect1Count + rect2Count, this.SpartanInterface.ActiveAgents.Count, "Agents from both rects should have been selected.");

            // Draw square 1 again, ensure that agents are not added twice.
            this.SpartanInterface.Interact(rect1.position, rect2.position + rect2.size);
            Assert.AreEqual(rect1Count + rect2Count, this.SpartanInterface.ActiveAgents.Count, "We should not have added any more agents than we actually have.");
            this.SpartanInterface.OnKeyUp(KeyCode.LeftShift);

            // Cleanup.
            DestroyNavAgentList();
        }

        [Test]
        public void ManageTeamIdTest()
        {
            this.navAgents = new List<NavAgent>();
            int teamId = Random.Range(0, int.MaxValue);

            for (int i = 0; i < NavAgentCount; i++)
            {
                this.navAgents.Add(InitNavAgent());
                do { this.navAgents[i].TeamId = Random.Range(0, int.MaxValue); } while (this.navAgents[i].TeamId == teamId); // In case we get real unlucky.
            }

            // Add the agents to the team, then make sure they are a team.
            this.SpartanInterface.TeamId = teamId;
            Assert.AreEqual(teamId, this.SpartanInterface.TeamId, "SpartanTeamInterface.TeamId = teamId");

            foreach (NavAgent agent in this.navAgents)
            {
                Assert.AreNotEqual(teamId, agent.TeamId);
                this.SpartanInterface.AddAgentToTeam(agent);
                Assert.AreEqual(teamId, agent.TeamId, "SpartanTeamInterface.AddAgentToTeam(agent)");
            }

            // Change the team. Agents should update to match.
            teamId = Random.Range(0, int.MaxValue);
            this.SpartanInterface.TeamId = teamId;
            foreach (NavAgent agent in this.navAgents)
            {
                Assert.AreEqual(teamId, agent.TeamId, "SpartanTeamInterface.TeamId = teamId");
            }

            // Unregister the team. They should change their IDs to be zero so they are not mistaken for a part of the team.
            this.SpartanInterface.ClearTeam();
            teamId = Random.Range(0, int.MaxValue);
            this.SpartanInterface.TeamId = teamId;
            foreach (NavAgent agent in this.navAgents)
            {
                Assert.AreEqual(0, agent.TeamId, "SpartanTeamInterface.ClearTeam() should set ID to 0");
                Assert.AreNotEqual(teamId, agent.TeamId, "SpartanTeamInterface.ClearTeam()");
            }

            DestroyNavAgentList();
        }

        private void CreateNavAgentList()
        {
            this.navAgents = new List<NavAgent>();

            for (int i = 0; i < NavAgentCount; i++)
            {
                this.navAgents.Add(InitNavAgent());
                _ = AddMeshAgentToNavAgent(this.navAgents[i]);
                this.navAgents[i].transform.position = this.RandomVector3;
                this.SpartanInterface.AddAgentToTeam(this.navAgents[i]);
            }
        }

        private Vector2 RandomVectorInSquare(Rect rect) => new Vector2(Random.Range(rect.xMin, rect.xMax), Random.Range(rect.yMin, rect.yMax));

        public void DestroyNavAgentList()
        {
            // Remove agents through Destroy.
            for (int i = 0; i < this.navAgents.Count; i++)
            {
                if (this.navAgents[i] != null)
                    Object.DestroyImmediate(this.navAgents[i].gameObject);
            }
            this.navAgents.Clear();

            this.SpartanInterface.ActiveAgents.Clear();
            this.SpartanInterface.ClearTeam();
            this.SpartanInterface.OnKeyUp(KeyCode.LeftShift); // To ensure future tests do not fail if current test fails.
        }
    }
}
