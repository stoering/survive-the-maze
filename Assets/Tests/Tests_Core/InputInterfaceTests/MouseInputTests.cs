﻿using NUnit.Framework;
using Tests;
using UnityEngine;

namespace InputInterfaceTests
{
    [Category("Static Tests")]
    internal class MouseInputTests : SeededTest
    {
        private Vector2 MouseDownLocation;
        private Vector2 MouseUpLocation;
        private bool MouseTapped;
        private bool MouseDragActive;
        private bool MouseDragReleased;

        // Get a random Vector2 between (-1000, -1000) and (1000, 1000)
        private Vector2 RandomVector2 => new Vector2(Random.Range(-1000, 1000), Random.Range(-1000, 1000));

        [Test]
        public void MouseActionTest()
        {
            ResetClicks();
            MouseAction mouseAction = new MouseAction
            {
                Action = "Test",
            };

            // Make sure we can handle missing delegates.
            mouseAction.OnMouseClick(this.RandomVector2);
            mouseAction.OnMouseDrag(this.RandomVector2, this.RandomVector2);
            mouseAction.OnMouseDragRelease(this.RandomVector2, this.RandomVector2);
            Assert.AreEqual(Vector2.zero, this.MouseDownLocation);
            Assert.AreEqual(Vector2.zero, this.MouseUpLocation);
            Assert.IsFalse(this.MouseTapped);   
            Assert.IsFalse(this.MouseDragActive);
            Assert.IsFalse(this.MouseDragReleased);

            ResetClicks();

            mouseAction = new MouseAction
            {
                Action = "Test",
                ProcessMouseClick = ProcessMouseClick,
                ProcessMouseDrag = ProcessMouseDrag,
                ProcessMouseDragRelease = ProcessMouseDragRelease
            };

            // Send a click, then ensure that it was received.
            Vector2 clickDownPosition = this.RandomVector2;
            Vector2 clickUpPosition = this.RandomVector2;

            mouseAction.OnMouseClick(clickDownPosition);

            Assert.AreEqual(clickDownPosition, this.MouseDownLocation);
            Assert.AreEqual(clickDownPosition, this.MouseUpLocation);
            Assert.IsTrue(this.MouseTapped);
            Assert.IsFalse(this.MouseDragActive);
            Assert.IsFalse(this.MouseDragReleased);
            ResetClicks();

            // Test a 10 step drag event with a release at the end.
            for (int i = 0; i < 10; i++)
            {
                mouseAction.OnMouseDrag(clickDownPosition, clickUpPosition);

                Assert.AreEqual(clickDownPosition, this.MouseDownLocation);
                Assert.AreEqual(clickUpPosition, this.MouseUpLocation);
                Assert.IsFalse(this.MouseTapped);
                Assert.IsTrue(this.MouseDragActive);
                Assert.IsFalse(this.MouseDragReleased);

                clickDownPosition = this.RandomVector2;
                clickUpPosition = this.RandomVector2;
            }
            mouseAction.OnMouseDragRelease(clickDownPosition, clickUpPosition);

            Assert.AreEqual(clickDownPosition, this.MouseDownLocation);
            Assert.AreEqual(clickUpPosition, this.MouseUpLocation);
            Assert.IsFalse(this.MouseTapped);
            Assert.IsFalse(this.MouseDragActive);
            Assert.IsTrue(this.MouseDragReleased);

            ResetClicks();
        }

        [Test]
        public void MouseActionOperatorTests()
        {
            // Operators were auto-generated, but we should check them anyways.
            MouseAction mouseAction1 = new MouseAction
            {
                Action = "Action",
                ProcessMouseClick = ProcessMouseClick,
                ProcessMouseDrag = ProcessMouseDrag,
                ProcessMouseDragRelease = ProcessMouseDragRelease
            };

            // Operators were auto-generated, but we should check them anyways.
            MouseAction mouseAction2 = new MouseAction
            {
                Action = "Action",
                ProcessMouseClick = ProcessMouseClick,
                ProcessMouseDrag = ProcessMouseDrag,
                ProcessMouseDragRelease = ProcessMouseDragRelease
            };

            // Make sure that we aren't comparing the private state.
            mouseAction2.OnMouseClick(this.RandomVector2);

            // Check all operators at least once.
            Assert.AreEqual(mouseAction1, mouseAction2);
            Assert.True(mouseAction1 == mouseAction2);
            Assert.False(mouseAction1 != mouseAction2);
            Assert.AreEqual(mouseAction1.GetHashCode(), mouseAction2.GetHashCode());

            // Modify each value and check once.
            mouseAction2.Action = "Backtion";
            Assert.AreNotEqual(mouseAction1, mouseAction2);
            mouseAction2.Action = mouseAction1.Action;
            Assert.AreEqual(mouseAction1, mouseAction2);
            mouseAction2.ProcessMouseClick = null;
            Assert.AreNotEqual(mouseAction1, mouseAction2);
            mouseAction2.ProcessMouseClick = mouseAction1.ProcessMouseClick;
            Assert.AreEqual(mouseAction1, mouseAction2);
            mouseAction2.ProcessMouseDrag = ProcessMouseDragRelease;
            Assert.AreNotEqual(mouseAction1, mouseAction2);
            mouseAction2.ProcessMouseDrag = mouseAction1.ProcessMouseDrag;
            Assert.AreEqual(mouseAction1, mouseAction2);
            mouseAction2.ProcessMouseDragRelease = ProcessMouseDrag;
            Assert.AreNotEqual(mouseAction1, mouseAction2);
        }

        private void ProcessMouseClick(Vector2 mouseLocation)
        {
            this.MouseDownLocation = this.MouseUpLocation = mouseLocation;
            this.MouseTapped = true;
        }

        private void ProcessMouseDrag(Vector2 mouseDownLocation, Vector2 mouseUpLocation)
        {
            this.MouseDownLocation = mouseDownLocation;
            this.MouseUpLocation = mouseUpLocation;
            this.MouseDragActive = true;
        }

        private void ProcessMouseDragRelease(Vector2 mouseDownLocation, Vector2 mouseUpLocation)
        {
            this.MouseDownLocation = mouseDownLocation;
            this.MouseUpLocation = mouseUpLocation;
            this.MouseDragActive = false;
            this.MouseDragReleased = true;
        }

        private void ResetClicks()
        {
            this.MouseDownLocation = Vector2.zero;
            this.MouseUpLocation = Vector2.zero;
            this.MouseTapped = false;
            this.MouseDragActive = false;
            this.MouseDragReleased = false;
        }
    }
}
