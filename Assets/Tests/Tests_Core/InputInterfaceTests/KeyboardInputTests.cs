﻿using NUnit.Framework;
using Tests;
using UnityEngine;

namespace InputInterfaceTests
{
    [Category("Static Tests")]
    internal class KeyboardInputTests : SeededTest
    {
        private bool KeyPressed;
        private bool KeyHeld;
        private bool KeyReleased;

        [Test]
        public void KeyActionTest()
        {
            ResetKeys();
            KeyAction keyAction = new KeyAction
            {
                Action = "Test",
            };

            // Make sure we can handle missing delegates.
            keyAction.OnKeyTap();
            keyAction.OnKeyDown();
            keyAction.Update();
            keyAction.OnKeyUp();
            Assert.IsFalse(this.KeyPressed);
            Assert.IsFalse(this.KeyHeld);
            Assert.IsFalse(this.KeyReleased);

            ResetKeys();

            keyAction = new KeyAction
            {
                Action = "Test",
                ProcessKeyDown = OnKeyDown,
                ProcessKeyHold = OnKeyHold,
                ProcessKeyUp = OnKeyUp
            };

            // Tap and hold, update should call "KeyHeld"
            Assert.IsFalse(this.KeyPressed);
            Assert.IsFalse(this.KeyHeld);
            Assert.IsFalse(this.KeyReleased);
            ResetKeys();

            keyAction.OnKeyDown();

            Assert.IsTrue(this.KeyPressed);
            Assert.IsFalse(this.KeyHeld);
            Assert.IsFalse(this.KeyReleased);
            ResetKeys();

            // Update should call OnKeyHold once per frame.
            for (int i = 0; i < 10; i++)
            {
                keyAction.Update();

                Assert.IsFalse(this.KeyPressed);
                Assert.IsTrue(this.KeyHeld);
                Assert.IsFalse(this.KeyReleased);
                ResetKeys();
            }

            keyAction.OnKeyUp();

            Assert.IsFalse(this.KeyPressed);
            Assert.IsFalse(this.KeyHeld);
            Assert.IsTrue(this.KeyReleased);
            ResetKeys();

            keyAction.Update();

            Assert.IsFalse(this.KeyPressed);
            Assert.IsFalse(this.KeyHeld);
            Assert.IsFalse(this.KeyReleased);
            ResetKeys();

            keyAction.OnKeyTap();

            Assert.IsTrue(this.KeyPressed);
            Assert.IsFalse(this.KeyHeld);
            Assert.IsFalse(this.KeyReleased);
            ResetKeys();

            keyAction.Update();

            Assert.IsFalse(this.KeyPressed);
            Assert.IsFalse(this.KeyHeld);
            Assert.IsFalse(this.KeyReleased);
        }

        [Test]
        public void KeyActionOperatorTests()
        {
            // Operators were auto-generated, but we should check them anyways.
            KeyAction keyAction1 = new KeyAction
            {
                Action = "Action",
                Key = KeyCode.Alpha0,
                ProcessKeyDown = OnKeyDown,
                ProcessKeyHold = OnKeyHold,
                ProcessKeyUp = OnKeyUp,
            };

            // Operators were auto-generated, but we should check them anyways.
            KeyAction keyAction2 = new KeyAction
            {
                Action = "Action",
                Key = KeyCode.Alpha0,
                ProcessKeyDown = OnKeyDown,
                ProcessKeyHold = OnKeyHold,
                ProcessKeyUp = OnKeyUp,
            };

            // Make sure that we aren't comparing the private state.
            keyAction2.OnKeyDown();

            // Check all operators at least once.
            Assert.AreEqual(keyAction1, keyAction2);
            Assert.True(keyAction1 == keyAction2);
            Assert.False(keyAction1 != keyAction2);
            Assert.AreEqual(keyAction1.GetHashCode(), keyAction2.GetHashCode());

            // Modify each value and check once.
            keyAction2.Action = "Backtion";
            Assert.AreNotEqual(keyAction1, keyAction2);
            keyAction2.Action = keyAction1.Action;
            Assert.AreEqual(keyAction1, keyAction2);
            keyAction2.Key = KeyCode.Alpha1;
            Assert.AreNotEqual(keyAction1, keyAction2);
            keyAction2.Key = keyAction1.Key;
            Assert.AreEqual(keyAction1, keyAction2);
            keyAction2.ProcessKeyDown = OnKeyUp;
            Assert.AreNotEqual(keyAction1, keyAction2);
            keyAction2.ProcessKeyDown = keyAction1.ProcessKeyDown;
            Assert.AreEqual(keyAction1, keyAction2);
            keyAction2.ProcessKeyHold = OnKeyUp;
            Assert.AreNotEqual(keyAction1, keyAction2);
            keyAction2.ProcessKeyHold = keyAction1.ProcessKeyHold;
            Assert.AreEqual(keyAction1, keyAction2);
            keyAction2.ProcessKeyUp = OnKeyDown;
            Assert.AreNotEqual(keyAction1, keyAction2);
        }

        private void ResetKeys()
        {
            this.KeyPressed = false;
            this.KeyHeld = false;
            this.KeyReleased = false;
        }

        private void OnKeyDown() => this.KeyPressed = true;
        private void OnKeyHold() => this.KeyHeld = true;
        private void OnKeyUp() => this.KeyReleased = true;
    }
}
