﻿using NUnit.Framework;
using Tests;
using UnityEngine;

namespace InputInterfaceTests
{
    [Category("Static Tests")]
    internal class KeyboardActivatorMouseTests : SeededTest
    {
        private Vector2 MouseDownLocation;
        private Vector2 MouseUpLocation;
        private bool MouseTapped;
        private bool MouseDragActive;
        private bool MouseDragReleased;

        // Get a random Vector2 between (-1000, -1000) and (1000, 1000)
        private Vector2 RandomVector2 => new Vector2(Random.Range(-1000, 1000), Random.Range(-1000, 1000));

        [Test]
        public void HoldKeyDefaultMouseActionTest()
        {
            ResetClicks();
            KeyActivator keyActivator = new KeyActivator
            {
                Action = "Test",
                ActivationType = KeyActivator.KeyActivationType.HoldKey
            };
            // Ensure that we can process an empty activator.
            TestBadMouseClicksForActivator(keyActivator);

            ResetClicks();

            MouseAction mouseAction = new MouseAction
            {
                Action = "Test",
                ProcessMouseClick = ProcessMouseClick,
                ProcessMouseDrag = ProcessMouseDrag,
                ProcessMouseDragRelease = ProcessMouseDragRelease
            };
            keyActivator.DefaultMouseAction = mouseAction;

            // Make sure that all commands are received by the Default mouseAction.
            TestGoodMouseClicksForActivator(keyActivator);
            ResetClicks();

            keyActivator.OnKeyDown();
            // Make sure that no commands are received by the Default mouseAction.
            TestBadMouseClicksForActivator(keyActivator);
            keyActivator.OnKeyUp();
            ResetClicks();

            // Make sure that all commands are received by the Default mouseAction.
            TestGoodMouseClicksForActivator(keyActivator);
            ResetClicks();
        }

        [Test]
        public void HoldKeyAlternativeMouseActionTest()
        {
            ResetClicks();
            KeyActivator keyActivator = new KeyActivator
            {
                Action = "Test",
                ActivationType = KeyActivator.KeyActivationType.HoldKey
            };

            keyActivator.OnKeyDown();
            // Ensure that we can process an empty activator.
            TestBadMouseClicksForActivator(keyActivator);
            keyActivator.OnKeyUp();

            ResetClicks();

            MouseAction mouseAction = new MouseAction
            {
                Action = "Test",
                ProcessMouseClick = ProcessMouseClick,
                ProcessMouseDrag = ProcessMouseDrag,
                ProcessMouseDragRelease = ProcessMouseDragRelease
            };
            keyActivator.AlternativeMouseAction = mouseAction;
            // Make sure that no commands are received by the Alternative mouseAction.
            TestBadMouseClicksForActivator(keyActivator);
            ResetClicks();

            // Make sure that all commands are received by the Alternative mouseAction.
            keyActivator.OnKeyDown();
            TestGoodMouseClicksForActivator(keyActivator);
            keyActivator.OnKeyUp();
            ResetClicks();

            // Make sure that no commands are received by the Alternative mouseAction.
            TestBadMouseClicksForActivator(keyActivator);
            ResetClicks();
        }

        [Test]
        public void ToggleKeyDefaultMouseActionTest()
        {
            ResetClicks();
            KeyActivator keyActivator = new KeyActivator
            {
                Action = "Test",
                ActivationType = KeyActivator.KeyActivationType.ToggleKey
            };
            // Ensure that we can process an empty activator.
            TestBadMouseClicksForActivator(keyActivator);

            ResetClicks();

            MouseAction mouseAction = new MouseAction
            {
                Action = "Test",
                ProcessMouseClick = ProcessMouseClick,
                ProcessMouseDrag = ProcessMouseDrag,
                ProcessMouseDragRelease = ProcessMouseDragRelease
            };
            keyActivator.DefaultMouseAction = mouseAction;

            // Make sure that all commands are received by the Default mouseAction.
            TestGoodMouseClicksForActivator(keyActivator);
            ResetClicks();

            keyActivator.OnKeyTap();
            // Make sure that no commands are received by the Default mouseAction.
            TestBadMouseClicksForActivator(keyActivator);
            keyActivator.OnKeyTap();
            ResetClicks();

            // Make sure that all commands are received by the Default mouseAction.
            TestGoodMouseClicksForActivator(keyActivator);
            ResetClicks();
        }

        [Test]
        public void ToggleKeyAlternativeMouseActionTest()
        {
            ResetClicks();
            KeyActivator keyActivator = new KeyActivator
            {
                Action = "Test",
                ActivationType = KeyActivator.KeyActivationType.ToggleKey
            };

            keyActivator.OnKeyTap();
            // Ensure that we can process an empty activator.
            TestBadMouseClicksForActivator(keyActivator);
            keyActivator.OnKeyTap();

            ResetClicks();

            MouseAction mouseAction = new MouseAction
            {
                Action = "Test",
                ProcessMouseClick = ProcessMouseClick,
                ProcessMouseDrag = ProcessMouseDrag,
                ProcessMouseDragRelease = ProcessMouseDragRelease
            };
            keyActivator.AlternativeMouseAction = mouseAction;
            // Make sure that no commands are received by the Alternative mouseAction.
            TestBadMouseClicksForActivator(keyActivator);
            ResetClicks();

            // Make sure that all commands are received by the Alternative mouseAction.
            keyActivator.OnKeyTap();
            TestGoodMouseClicksForActivator(keyActivator);
            keyActivator.OnKeyTap();
            ResetClicks();

            // Make sure that no commands are received by the Alternative mouseAction.
            TestBadMouseClicksForActivator(keyActivator);
            ResetClicks();
        }

        [Test]
        public void MouseActionOperatorTests()
        {
            MouseAction mouseAction1 = new MouseAction()
            {
                Action = "Action1"
            };
            MouseAction mouseAction2 = new MouseAction()
            {
                Action = "Action2"
            };

            // Operators were auto-generated, but we should check them anyways.
            KeyActivator keyActivator1 = new KeyActivator
            {
                Action = "Action",
                Key = KeyCode.A,
                ActivationType = KeyActivator.KeyActivationType.HoldKey,
                DefaultMouseAction = mouseAction1,
                AlternativeMouseAction = mouseAction2
            };

            // Operators were auto-generated, but we should check them anyways.
            KeyActivator keyActivator2 = new KeyActivator
            {
                Action = "Action",
                Key = KeyCode.A,
                ActivationType = KeyActivator.KeyActivationType.HoldKey,
                DefaultMouseAction = mouseAction1,
                AlternativeMouseAction = mouseAction2
            };

            // Make sure that we aren't comparing the private state.
            keyActivator1.OnKeyDown();

            // Check all operators at least once.
            Assert.AreEqual(keyActivator1, keyActivator2);
            Assert.True(keyActivator1 == keyActivator2);
            Assert.False(keyActivator1 != keyActivator2);
            Assert.AreEqual(keyActivator1.GetHashCode(), keyActivator2.GetHashCode());

            // Modify each value and check once.
            keyActivator2.Action = "Backtion";
            Assert.AreNotEqual(keyActivator1, keyActivator2);
            keyActivator2.Action = keyActivator1.Action;
            Assert.AreEqual(keyActivator1, keyActivator2);

            keyActivator2.Key = KeyCode.B;
            Assert.AreNotEqual(keyActivator1, keyActivator2);
            keyActivator2.Key = keyActivator1.Key;
            Assert.AreEqual(keyActivator1, keyActivator2);

            keyActivator2.ActivationType = KeyActivator.KeyActivationType.ToggleKey;
            Assert.AreNotEqual(keyActivator1, keyActivator2);
            keyActivator2.ActivationType = keyActivator1.ActivationType;
            Assert.AreEqual(keyActivator1, keyActivator2);

            keyActivator2.DefaultMouseAction = mouseAction2;
            Assert.AreNotEqual(keyActivator1, keyActivator2);
            keyActivator2.DefaultMouseAction = keyActivator1.DefaultMouseAction;
            Assert.AreEqual(keyActivator1, keyActivator2);

            keyActivator2.AlternativeMouseAction = mouseAction1;
            Assert.AreNotEqual(keyActivator1, keyActivator2);
            keyActivator2.AlternativeMouseAction = keyActivator1.AlternativeMouseAction;
            Assert.AreEqual(keyActivator1, keyActivator2);
        }

        private void TestGoodMouseClicksForActivator(KeyActivator keyActivator)
        {
            // Send a click, then ensure that it was received.
            Vector2 clickDownPosition = this.RandomVector2;
            Vector2 clickUpPosition = this.RandomVector2;

            keyActivator.OnMouseClick(clickDownPosition);

            Assert.AreEqual(clickDownPosition, this.MouseDownLocation);
            Assert.AreEqual(clickDownPosition, this.MouseUpLocation);
            Assert.IsTrue(this.MouseTapped);
            Assert.IsFalse(this.MouseDragActive);
            Assert.IsFalse(this.MouseDragReleased);
            ResetClicks();

            // Test a 10 step drag event with a release at the end.
            for (int i = 0; i < 10; i++)
            {
                keyActivator.OnMouseDrag(clickDownPosition, clickUpPosition);

                Assert.AreEqual(clickDownPosition, this.MouseDownLocation);
                Assert.AreEqual(clickUpPosition, this.MouseUpLocation);
                Assert.IsFalse(this.MouseTapped);
                Assert.IsTrue(this.MouseDragActive);
                Assert.IsFalse(this.MouseDragReleased);

                clickDownPosition = this.RandomVector2;
                clickUpPosition = this.RandomVector2;
            }
            keyActivator.OnMouseDragRelease(clickDownPosition, clickUpPosition);

            Assert.AreEqual(clickDownPosition, this.MouseDownLocation);
            Assert.AreEqual(clickUpPosition, this.MouseUpLocation);
            Assert.IsFalse(this.MouseTapped);
            Assert.IsFalse(this.MouseDragActive);
            Assert.IsTrue(this.MouseDragReleased);
        }

        private void TestBadMouseClicksForActivator(KeyActivator keyActivator)
        {
            // Make sure we can handle missing MouseAction.
            keyActivator.OnMouseClick(this.RandomVector2);
            keyActivator.OnMouseDrag(this.RandomVector2, this.RandomVector2);
            keyActivator.OnMouseDragRelease(this.RandomVector2, this.RandomVector2);
            Assert.AreEqual(Vector2.zero, this.MouseDownLocation);
            Assert.AreEqual(Vector2.zero, this.MouseUpLocation);
            Assert.IsFalse(this.MouseTapped);
            Assert.IsFalse(this.MouseDragActive);
            Assert.IsFalse(this.MouseDragReleased);
        }

        private void ProcessMouseClick(Vector2 mouseLocation)
        {
            this.MouseDownLocation = this.MouseUpLocation = mouseLocation;
            this.MouseTapped = true;
        }

        private void ProcessMouseDrag(Vector2 mouseDownLocation, Vector2 mouseUpLocation)
        {
            this.MouseDownLocation = mouseDownLocation;
            this.MouseUpLocation = mouseUpLocation;
            this.MouseDragActive = true;
        }

        private void ProcessMouseDragRelease(Vector2 mouseDownLocation, Vector2 mouseUpLocation)
        {
            this.MouseDownLocation = mouseDownLocation;
            this.MouseUpLocation = mouseUpLocation;
            this.MouseDragActive = false;
            this.MouseDragReleased = true;
        }

        private void ResetClicks()
        {
            this.MouseDownLocation = Vector2.zero;
            this.MouseUpLocation = Vector2.zero;
            this.MouseTapped = false;
            this.MouseDragActive = false;
            this.MouseDragReleased = false;
        }
    }
}
