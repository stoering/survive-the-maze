﻿using NUnit.Framework;
using Tests;

namespace CollisionTests
{
    [Category("Random Seed")]
    internal class HealthTests : ManagedTest
    {
        [Test]
        public void TestHealth() => Logging.LogWarning("No Health tests are defined! There is little use testing the current bootstrap code, but don't forget to use some real TDD with the final version of the health manager");
    }
}
