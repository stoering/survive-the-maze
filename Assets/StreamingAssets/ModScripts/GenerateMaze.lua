-- Defines a function that raises the 0,0 corner of the maze.
function setMazeHeights (x, y)
	local i, j

	local imin = -1 -- 15
	local imax = -1 -- 35
	local jmin = -1 -- 10
	local jmax = -1 -- 40

	local placementThreshold = 25
	for i=1,x-2 do
		for j=1,y-2 do
			-- This is only needed for menu generation. Fix the saving problem first!
			if (((i == imin) or (i == imax)) and ((j >= jmin) and (j <= jmax))) or
			 (((j == jmin) or ( j == jmax )) and ((i >= imin) and (i <= imax))) then
				CreateMazeSegment(i,j)
			else
			if (i % 2 == 0 and j % 2 == 0) then
				local a = math.random( 0,1 ) < .5 and 0 or (math.random( 0,1 ) < .5 and - 1 or 1)
				local b = a ~= 0 and 0 or (math.random( 0,1 ) < .5 and - 1 or 1)
				if (math.random( 0,100 ) > placementThreshold) then
					if (((i < imin) or (i > imax)) or ((j < jmin) or (j > jmax))) then
					CreateMazeSegment(i,j)
					end
				end
				if (math.random( 0,100 ) > placementThreshold) then
					if (((i+a < imin) or (i+a > imax)) or ((j+b < jmin) or (j+b > jmax))) then
					CreateMazeSegment(i+a,j+b)
					end
				end
			end
			end
		end
	end
end
