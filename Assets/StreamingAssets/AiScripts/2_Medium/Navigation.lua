-- Using the code from TESTS_RANDOM_MOVEMENT for now. Will need to make this *WAY* smarter.

function Update()
    -- Minotaur doesn't need to be selected.
    if #Interface.VisibleMaze.AgentsOnMyTeam == 1 then
        if Interface.VisibleMaze.AgentsOnMyTeam[1].Idle then
            -- Find an NPC that is still alive.
            i=0
            j=0
            repeat
                i=math.random(1,#Interface.VisibleMaze.AgentsNotOnMyTeam)
                j=j+1
            until (Interface.VisibleMaze.AgentsNotOnMyTeam[i].HasDied == false) or (j==(#Interface.VisibleMaze.AgentsNotOnMyTeam*2))
            Interface.Move(Interface.VisibleMaze.AgentsNotOnMyTeam[i].Position_X, Interface.VisibleMaze.AgentsNotOnMyTeam[i].Position_Y)
        end
    else
        -- There are multiple mortals on the team, must select them one by one.
        for i=1,#Interface.VisibleMaze.AgentsOnMyTeam do
            if Interface.VisibleMaze.AgentsOnMyTeam[i].Idle then
                Interface.Interact(Interface.VisibleMaze.AgentsOnMyTeam[i].Position_X, Interface.VisibleMaze.AgentsOnMyTeam[i].Position_Y)
                Interface.Move(math.random( 0,50 ), math.random( 0,50 ))
            end
        end
    end
end
