-- No AI here, just an empty update loop. Use AI "None" to load up this one.

function Update()
    -- Wow, would you look at that code! What beauty, what efficiency!
    -- How is it possible for a programmer to say so much in so few lines?
    -- Truly, this file is a masterpiece that shall never be matched by any
    -- mortal effort. Try in vain, or rest in peace, it matters not to me.
    -- You have seen it with your own two eyes, all else will be but a
    -- shadow of a crude copy.
end
