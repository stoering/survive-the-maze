function Update()
    -- Minotaur doesn't need to be selected.
    if #Interface.VisibleMaze.AgentsOnMyTeam == 1 then
        if Interface.VisibleMaze.AgentsOnMyTeam[1].Idle then
            Interface.Move(math.random( 0,50 ), math.random( 0,50 ))
        end
    else
        -- There are multiple mortals on the team, must select them one by one.
        for i=1,#Interface.VisibleMaze.AgentsOnMyTeam do
            if Interface.VisibleMaze.AgentsOnMyTeam[i].Idle then
                Interface.Interact(Interface.VisibleMaze.AgentsOnMyTeam[i].Position_X, Interface.VisibleMaze.AgentsOnMyTeam[i].Position_Y)
                Interface.Move(math.random( 0,50 ), math.random( 0,50 ))
            end
        end
    end
end
