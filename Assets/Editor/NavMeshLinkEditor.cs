using UnityEngine;
using UnityEngine.AI;

namespace UnityEditor.AI
{
    [CanEditMultipleObjects]
    [CustomEditor(typeof(NavMeshLink))]
    internal class NavMeshLinkEditor : Editor
    {
        private SerializedProperty m_AgentTypeID;
        private SerializedProperty m_Area;
        private SerializedProperty m_CostModifier;
        private SerializedProperty m_AutoUpdatePosition;
        private SerializedProperty m_Bidirectional;
        private SerializedProperty m_EndPoint;
        private SerializedProperty m_StartPoint;
        private SerializedProperty m_Width;
        private static int s_SelectedID;
        private static int s_SelectedPoint = -1;
        private static Color s_HandleColor = new Color(255f, 167f, 39f, 210f) / 255;
        private static Color s_HandleColorDisabled = new Color(255f * 0.75f, 167f * 0.75f, 39f * 0.75f, 100f) / 255;

        private void OnEnable()
        {
            this.m_AgentTypeID = this.serializedObject.FindProperty("m_AgentTypeID");
            this.m_Area = this.serializedObject.FindProperty("m_Area");
            this.m_CostModifier = this.serializedObject.FindProperty("m_CostModifier");
            this.m_AutoUpdatePosition = this.serializedObject.FindProperty("m_AutoUpdatePosition");
            this.m_Bidirectional = this.serializedObject.FindProperty("m_Bidirectional");
            this.m_EndPoint = this.serializedObject.FindProperty("m_EndPoint");
            this.m_StartPoint = this.serializedObject.FindProperty("m_StartPoint");
            this.m_Width = this.serializedObject.FindProperty("m_Width");

            s_SelectedID = 0;
            s_SelectedPoint = -1;

            NavMeshVisualizationSettings.showNavigation++;
        }

        private void OnDisable() => NavMeshVisualizationSettings.showNavigation--;

        private static Matrix4x4 UnscaledLocalToWorldMatrix(Transform t) => Matrix4x4.TRS(t.position, t.rotation, Vector3.one);

        private void AlignTransformToEndPoints(NavMeshLink navLink)
        {
            Matrix4x4 mat = UnscaledLocalToWorldMatrix(navLink.transform);

            Vector3 worldStartPt = mat.MultiplyPoint(navLink.startPoint);
            Vector3 worldEndPt = mat.MultiplyPoint(navLink.endPoint);

            Vector3 forward = worldEndPt - worldStartPt;
            Vector3 up = navLink.transform.up;

            // Flatten
            forward -= Vector3.Dot(up, forward) * up;

            Transform transform = navLink.transform;
            transform.rotation = Quaternion.LookRotation(forward, up);
            transform.position = (worldEndPt + worldStartPt) * 0.5f;
            transform.localScale = Vector3.one;

            navLink.startPoint = transform.InverseTransformPoint(worldStartPt);
            navLink.endPoint = transform.InverseTransformPoint(worldEndPt);
        }

        public override void OnInspectorGUI()
        {
            this.serializedObject.Update();

            NavMeshComponentsGUIUtility.AgentTypePopup("Agent Type", this.m_AgentTypeID);
            EditorGUILayout.Space();

            _ = EditorGUILayout.PropertyField(this.m_StartPoint);
            _ = EditorGUILayout.PropertyField(this.m_EndPoint);

            GUILayout.BeginHorizontal();
            GUILayout.Space(EditorGUIUtility.labelWidth);
            if (GUILayout.Button("Swap"))
            {
                foreach (NavMeshLink navLink in this.targets)
                {
                    Vector3 tmp = navLink.startPoint;
                    navLink.startPoint = navLink.endPoint;
                    navLink.endPoint = tmp;
                }
                SceneView.RepaintAll();
            }
            if (GUILayout.Button("Align Transform"))
            {
                foreach (NavMeshLink navLink in this.targets)
                {
                    Undo.RecordObject(navLink.transform, "Align Transform to End Points");
                    Undo.RecordObject(navLink, "Align Transform to End Points");
                    AlignTransformToEndPoints(navLink);
                }
                SceneView.RepaintAll();
            }
            GUILayout.EndHorizontal();
            EditorGUILayout.Space();

            _ = EditorGUILayout.PropertyField(this.m_Width);
            _ = EditorGUILayout.PropertyField(this.m_CostModifier);
            _ = EditorGUILayout.PropertyField(this.m_AutoUpdatePosition);
            _ = EditorGUILayout.PropertyField(this.m_Bidirectional);

            NavMeshComponentsGUIUtility.AreaPopup("Area Type", this.m_Area);

            _ = this.serializedObject.ApplyModifiedProperties();

            EditorGUILayout.Space();
        }

        private static Vector3 CalcLinkRight(NavMeshLink navLink)
        {
            Vector3 dir = navLink.endPoint - navLink.startPoint;
            return new Vector3(-dir.z, 0.0f, dir.x).normalized;
        }

        private static void DrawLink(NavMeshLink navLink)
        {
            Vector3 right = CalcLinkRight(navLink);
            float rad = navLink.width * 0.5f;

            Gizmos.DrawLine(navLink.startPoint - right * rad, navLink.startPoint + right * rad);
            Gizmos.DrawLine(navLink.endPoint - right * rad, navLink.endPoint + right * rad);
            Gizmos.DrawLine(navLink.startPoint - right * rad, navLink.endPoint - right * rad);
            Gizmos.DrawLine(navLink.startPoint + right * rad, navLink.endPoint + right * rad);
        }

        [DrawGizmo(GizmoType.Selected | GizmoType.Active | GizmoType.Pickable)]
#pragma warning disable IDE0060 // Remove unused parameter
        private static void RenderBoxGizmo(NavMeshLink navLink, GizmoType gizmoType)
#pragma warning restore IDE0060 // Remove unused parameter
        {
            if (!EditorApplication.isPlaying)
                navLink.UpdateLink();

            Color color = s_HandleColor;
            if (!navLink.enabled)
                color = s_HandleColorDisabled;

            Color oldColor = Gizmos.color;
            Matrix4x4 oldMatrix = Gizmos.matrix;

            Gizmos.matrix = UnscaledLocalToWorldMatrix(navLink.transform);

            Gizmos.color = color;
            DrawLink(navLink);

            Gizmos.matrix = oldMatrix;
            Gizmos.color = oldColor;

            Gizmos.DrawIcon(navLink.transform.position, "NavMeshLink Icon", true);
        }

        [DrawGizmo(GizmoType.NotInSelectionHierarchy | GizmoType.Pickable)]
#pragma warning disable IDE0060 // Remove unused parameter
        private static void RenderBoxGizmoNotSelected(NavMeshLink navLink, GizmoType gizmoType)
#pragma warning restore IDE0060 // Remove unused parameter
        {
            if (NavMeshVisualizationSettings.showNavigation > 0)
            {
                Color color = s_HandleColor;
                if (!navLink.enabled)
                    color = s_HandleColorDisabled;

                Color oldColor = Gizmos.color;
                Matrix4x4 oldMatrix = Gizmos.matrix;

                Gizmos.matrix = UnscaledLocalToWorldMatrix(navLink.transform);

                Gizmos.color = color;
                DrawLink(navLink);

                Gizmos.matrix = oldMatrix;
                Gizmos.color = oldColor;
            }

            Gizmos.DrawIcon(navLink.transform.position, "NavMeshLink Icon", true);
        }

        internal void OnSceneGUI()
        {
            NavMeshLink navLink = (NavMeshLink)this.target;
            if (!navLink.enabled)
                return;

            Matrix4x4 mat = UnscaledLocalToWorldMatrix(navLink.transform);

            Vector3 startPt = mat.MultiplyPoint(navLink.startPoint);
            Vector3 endPt = mat.MultiplyPoint(navLink.endPoint);
            Vector3 midPt = Vector3.Lerp(startPt, endPt, 0.35f);
            float startSize = HandleUtility.GetHandleSize(startPt);
            float endSize = HandleUtility.GetHandleSize(endPt);
            float midSize = HandleUtility.GetHandleSize(midPt);

            Quaternion zup = Quaternion.FromToRotation(Vector3.forward, Vector3.up);
            Vector3 right = mat.MultiplyVector(CalcLinkRight(navLink));

            Color oldColor = Handles.color;
            Handles.color = s_HandleColor;

            Vector3 pos;

            if (navLink.GetInstanceID() == s_SelectedID && s_SelectedPoint == 0)
            {
                EditorGUI.BeginChangeCheck();
                Handles.CubeHandleCap(0, startPt, zup, 0.1f * startSize, Event.current.type);
                pos = Handles.PositionHandle(startPt, navLink.transform.rotation);
                if (EditorGUI.EndChangeCheck())
                {
                    Undo.RecordObject(navLink, "Move link point");
                    navLink.startPoint = mat.inverse.MultiplyPoint(pos);
                }
            }
            else
            {
                if (Handles.Button(startPt, zup, 0.1f * startSize, 0.1f * startSize, Handles.CubeHandleCap))
                {
                    s_SelectedPoint = 0;
                    s_SelectedID = navLink.GetInstanceID();
                }
            }

            if (navLink.GetInstanceID() == s_SelectedID && s_SelectedPoint == 1)
            {
                EditorGUI.BeginChangeCheck();
                Handles.CubeHandleCap(0, endPt, zup, 0.1f * startSize, Event.current.type);
                pos = Handles.PositionHandle(endPt, navLink.transform.rotation);
                if (EditorGUI.EndChangeCheck())
                {
                    Undo.RecordObject(navLink, "Move link point");
                    navLink.endPoint = mat.inverse.MultiplyPoint(pos);
                }
            }
            else
            {
                if (Handles.Button(endPt, zup, 0.1f * endSize, 0.1f * endSize, Handles.CubeHandleCap))
                {
                    s_SelectedPoint = 1;
                    s_SelectedID = navLink.GetInstanceID();
                }
            }

            EditorGUI.BeginChangeCheck();
            pos = Handles.Slider(midPt + right * navLink.width * 0.5f, right, midSize * 0.03f, Handles.DotHandleCap, 0);
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(navLink, "Adjust link width");
                navLink.width = Mathf.Max(0.0f, 2.0f * Vector3.Dot(right, pos - midPt));
            }

            EditorGUI.BeginChangeCheck();
            pos = Handles.Slider(midPt - right * navLink.width * 0.5f, -right, midSize * 0.03f, Handles.DotHandleCap, 0);
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(navLink, "Adjust link width");
                navLink.width = Mathf.Max(0.0f, 2.0f * Vector3.Dot(-right, pos - midPt));
            }

            Handles.color = oldColor;
        }

        [MenuItem("GameObject/AI/NavMesh Link", false, 2002)]
        static internal void CreateNavMeshLink(MenuCommand menuCommand)
        {
            GameObject parent = menuCommand.context as GameObject;
            GameObject go = NavMeshComponentsGUIUtility.CreateAndSelectGameObject("NavMesh Link", parent);
            _ = go.AddComponent<NavMeshLink>();
            SceneView view = SceneView.lastActiveSceneView;
            if (view != null)
                view.MoveToView(go.transform);
        }
    }
}
