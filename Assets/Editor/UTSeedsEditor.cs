﻿using UnityEngine;
using UnityEditor;

public class UnityTestSeedManager : EditorWindow
{
    // Add menu named "My Window" to the Window menu
    [MenuItem("Window/SeededTest")]
    static void Init()
    {
        // Get existing open window or if none, make a new one:
        UnityTestSeedManager window = GetWindow<UnityTestSeedManager>("Test Seeds");
        window.Show();
    }

    void OnGUI()
    {
        GUILayout.Label("Random Seed Settings", EditorStyles.boldLabel);
        RandomSeedGenerator.Seed = EditorGUILayout.IntField("Seed", RandomSeedGenerator.Seed);
        RandomSeedGenerator.SeedType = (RandomSeedGenerator.TestSeedType) EditorGUILayout.EnumPopup("Randomization Type", RandomSeedGenerator.SeedType);

        _ = GUILayout.HorizontalSlider(0, 0, 0);
        if (GUILayout.Button("Generate Next Seed"))
        {
            _ = RandomSeedGenerator.NextSeed;
        }
        _ = GUILayout.HorizontalSlider(0, 0, 0);
        if (GUILayout.Button("Load Seed From File"))
        {
            RandomSeedGenerator.LoadSeed();
        }
        _ = GUILayout.HorizontalSlider(0, 0, 0);
        if (GUILayout.Button("Save Seed Settings to File"))
        {
            RandomSeedGenerator.SaveSeed();
            RandomSeedGenerator.LoadSeed();
        }
    }
}