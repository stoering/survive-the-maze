using System;

namespace CommandTerminal
{
    [AttributeUsage(AttributeTargets.Method)]
    public class RegisterCommandAttribute : Attribute
    {
        public int MinArgCount { get; set; } = 0;

        public int MaxArgCount { get; set; } = -1;

        public string Name { get; set; }
        public string Help { get; set; }
        public string Hint { get; set; }

        public RegisterCommandAttribute(string command_name = null) => this.Name = command_name;
    }
}
