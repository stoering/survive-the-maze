using System;
using System.Reflection;
using System.Collections.Generic;

namespace CommandTerminal
{
    public struct CommandInfo
    {
        public Action<CommandArg[]> proc;
        public int max_arg_count;
        public int min_arg_count;
        public string help;
        public string hint;
    }

    public struct CommandArg
    {
        public string String { get; set; }

        public int Int
        {
            get
            {

                if (int.TryParse(this.String, out int int_value))
                {
                    return int_value;
                }

                TypeError("int");
                return 0;
            }
        }

        public float Float
        {
            get
            {

                if (float.TryParse(this.String, out float float_value))
                {
                    return float_value;
                }

                TypeError("float");
                return 0;
            }
        }

        public bool Bool
        {
            get
            {
                if (string.Compare(this.String, "TRUE", ignoreCase: true) == 0)
                {
                    return true;
                }

                if (string.Compare(this.String, "FALSE", ignoreCase: true) == 0)
                {
                    return false;
                }

                TypeError("bool");
                return false;
            }
        }

        public override string ToString() => this.String;

        private void TypeError(string expected_type)
        {
            Terminal.Shell.IssueErrorMessage(
                "Incorrect type for {0}, expected <{1}>",
                this.String, expected_type
            );
        }
    }

    public class CommandShell
    {
        private List<CommandArg> arguments = new List<CommandArg>(); // Cache for performance

        public string IssuedErrorMessage { get; private set; }

        public Dictionary<string, CommandInfo> Commands { get; } = new Dictionary<string, CommandInfo>();

        public Dictionary<string, CommandArg> Variables { get; } = new Dictionary<string, CommandArg>();

        /// <summary>
        /// Uses reflection to find all RegisterCommand attributes
        /// and adds them to the commands dictionary.
        /// </summary>
        public void RegisterCommands()
        {
            Dictionary<string, CommandInfo> rejected_commands = new Dictionary<string, CommandInfo>();
            BindingFlags method_flags = BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic;

            foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                foreach (Type type in assembly.GetTypes())
                {
                    foreach (MethodInfo method in type.GetMethods(method_flags))
                    {
                        RegisterCommandAttribute attribute = Attribute.GetCustomAttribute(
                            method, typeof(RegisterCommandAttribute)) as RegisterCommandAttribute;

                        if (attribute == null)
                        {
                            if (method.Name.StartsWith("FRONTCOMMAND", StringComparison.CurrentCultureIgnoreCase))
                            {
                                // Front-end Command methods don't implement RegisterCommand, use default attribute
                                attribute = new RegisterCommandAttribute();
                            }
                            else
                            {
                                continue;
                            }
                        }

                        ParameterInfo[] methods_params = method.GetParameters();

                        string command_name = InferFrontCommandName(method.Name);
                        Action<CommandArg[]> proc;

                        if (attribute.Name == null)
                        {
                            // Use the method's name as the command's name
                            command_name = InferCommandName(command_name == null ? method.Name : command_name);
                        }
                        else
                        {
                            command_name = attribute.Name;
                        }

                        if (methods_params.Length != 1 || methods_params[0].ParameterType != typeof(CommandArg[]))
                        {
                            // Method does not match expected Action signature,
                            // this could be a command that has a FrontCommand method to handle its arguments.
                            rejected_commands.Add(command_name.ToUpper(), CommandFromParamInfo(methods_params, attribute.Help));
                            continue;
                        }

                        // Convert MethodInfo to Action.
                        // This is essentially allows us to store a reference to the method,
                        // which makes calling the method significantly more performant than using MethodInfo.Invoke().
                        proc = (Action<CommandArg[]>)Delegate.CreateDelegate(typeof(Action<CommandArg[]>), method);
                        AddCommand(command_name, proc, attribute.MinArgCount, attribute.MaxArgCount, attribute.Help, attribute.Hint);
                    }
                }
            }
            HandleRejectedCommands(rejected_commands);
        }

        /// <summary>
        /// Parses an input line into a command and runs that command.
        /// </summary>
        public void RunCommand(string line)
        {
            string remaining = line;
            this.IssuedErrorMessage = null;
            this.arguments.Clear();

            while (remaining != "")
            {
                CommandArg argument = EatArgument(ref remaining);

                if (argument.String != "")
                {
                    if (argument.String[0] == '$')
                    {
                        string variable_name = argument.String.Substring(1).ToUpper();

                        if (this.Variables.ContainsKey(variable_name))
                        {
                            // Replace variable argument if it's defined
                            argument = this.Variables[variable_name];
                        }
                    }
                    this.arguments.Add(argument);
                }
            }

            if (this.arguments.Count == 0)
            {
                // Nothing to run
                return;
            }

            string command_name = this.arguments[0].String.ToUpper();
            this.arguments.RemoveAt(0); // Remove command name from arguments

            if (!this.Commands.ContainsKey(command_name))
            {
                IssueErrorMessage("Command {0} could not be found", command_name);
                return;
            }

            RunCommand(command_name, this.arguments.ToArray());
        }

        public void RunCommand(string command_name, CommandArg[] arguments)
        {
            CommandInfo command = this.Commands[command_name];
            int arg_count = arguments.Length;
            string error_message = null;
            int required_arg = 0;

            if (arg_count < command.min_arg_count)
            {
                if (command.min_arg_count == command.max_arg_count)
                {
                    error_message = "exactly";
                }
                else
                {
                    error_message = "at least";
                }
                required_arg = command.min_arg_count;
            }
            else if (command.max_arg_count > -1 && arg_count > command.max_arg_count)
            {
                // Do not check max allowed number of arguments if it is -1
                if (command.min_arg_count == command.max_arg_count)
                {
                    error_message = "exactly";
                }
                else
                {
                    error_message = "at most";
                }
                required_arg = command.max_arg_count;
            }

            if (error_message != null)
            {
                string plural_fix = required_arg == 1 ? "" : "s";

                IssueErrorMessage(
                    "{0} requires {1} {2} argument{3}",
                    command_name,
                    error_message,
                    required_arg,
                    plural_fix
                );

                if (command.hint != null)
                {
                    this.IssuedErrorMessage += string.Format("\n    -> Usage: {0}", command.hint);
                }

                return;
            }

            command.proc(arguments);
        }

        public void AddCommand(string name, CommandInfo info)
        {
            name = name.ToUpper();

            if (this.Commands.ContainsKey(name))
            {
                IssueErrorMessage("Command {0} is already defined.", name);
                return;
            }

            this.Commands.Add(name, info);
        }

        public void AddCommand(string name, Action<CommandArg[]> proc, int min_args = 0, int max_args = -1, string help = "", string hint = null)
        {
            CommandInfo info = new CommandInfo()
            {
                proc = proc,
                min_arg_count = min_args,
                max_arg_count = max_args,
                help = help,
                hint = hint
            };

            AddCommand(name, info);
        }

        public void SetVariable(string name, string value) => SetVariable(name, new CommandArg() { String = value });

        public void SetVariable(string name, CommandArg value)
        {
            name = name.ToUpper();

            if (this.Variables.ContainsKey(name))
            {
                this.Variables[name] = value;
            }
            else
            {
                this.Variables.Add(name, value);
            }
        }

        public CommandArg GetVariable(string name)
        {
            name = name.ToUpper();

            if (this.Variables.ContainsKey(name))
            {
                return this.Variables[name];
            }

            IssueErrorMessage("No variable named {0}", name);
            return new CommandArg();
        }

        public void IssueErrorMessage(string format, params object[] message) => this.IssuedErrorMessage = string.Format(format, message);

        private string InferCommandName(string method_name)
        {
            string command_name;
            int index = method_name.IndexOf("COMMAND", StringComparison.CurrentCultureIgnoreCase);

            if (index >= 0)
            {
                // Method is prefixed, suffixed with, or contains "COMMAND".
                command_name = method_name.Remove(index, 7);
            }
            else
            {
                command_name = method_name;
            }

            return command_name;
        }

        private string InferFrontCommandName(string method_name)
        {
            int index = method_name.IndexOf("FRONT", StringComparison.CurrentCultureIgnoreCase);
            return index >= 0 ? method_name.Remove(index, 5) : null;
        }

        private void HandleRejectedCommands(Dictionary<string, CommandInfo> rejected_commands)
        {
            foreach (KeyValuePair<string, CommandInfo> command in rejected_commands)
            {
                if (this.Commands.ContainsKey(command.Key))
                {
                    this.Commands[command.Key] = new CommandInfo()
                    {
                        proc = this.Commands[command.Key].proc,
                        min_arg_count = command.Value.min_arg_count,
                        max_arg_count = command.Value.max_arg_count,
                        help = command.Value.help
                    };
                }
                else
                {
                    IssueErrorMessage("{0} is missing a front command.", command);
                }
            }
        }

        private CommandInfo CommandFromParamInfo(ParameterInfo[] parameters, string help)
        {
            int optional_args = 0;

            foreach (ParameterInfo param in parameters)
            {
                if (param.IsOptional)
                {
                    optional_args += 1;
                }
            }

            return new CommandInfo()
            {
                proc = null,
                min_arg_count = parameters.Length - optional_args,
                max_arg_count = parameters.Length,
                help = help
            };
        }

        private CommandArg EatArgument(ref string s)
        {
            CommandArg arg = new CommandArg();
            int space_index = s.IndexOf(' ');

            if (space_index >= 0)
            {
                arg.String = s.Substring(0, space_index);
                s = s.Substring(space_index + 1); // Remaining
            }
            else
            {
                arg.String = s;
                s = "";
            }

            return arg;
        }
    }
}
