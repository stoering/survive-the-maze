using UnityEngine;
using System.Text;
using UnityEngine.Assertions;
#pragma warning disable IDE0044 // Add readonly modifier
#pragma warning disable 0649 // Field is never assigned to, and will always have its default value 'null'

namespace CommandTerminal
{
    public enum TerminalState
    {
        Close,
        OpenSmall,
        OpenFull
    }

    public class Terminal : MonoBehaviour
    {
        [Header("Window")]
        [Range(0, 1)]
        [SerializeField]
        private float MaxHeight = 0.7f;

        [SerializeField]
        [Range(0, 1)]
        private float SmallTerminalRatio = 0.33f;

        [Range(100, 1000)]
        [SerializeField]
        private float ToggleSpeed = 360;

        [SerializeField] private string ToggleHotkey = "`";
        [SerializeField] private string ToggleFullHotkey = "#`";
        [SerializeField] private int BufferSize = 512;

        [Header("Input")]
        [SerializeField] private Font ConsoleFont;
        [SerializeField] private string InputCaret = ">";
        [SerializeField] private bool ShowGUIButtons;
        [SerializeField] private bool RightAlignButtons;

        [Header("Theme")]
        [Range(0, 1)]
        [SerializeField] private float InputContrast;
        [Range(0, 1)]
        [SerializeField] private float InputAlpha = 0.5f;

        [SerializeField] private Color BackgroundColor = Color.black;
        [SerializeField] private Color ForegroundColor = Color.white;
        [SerializeField] private Color ShellColor = Color.white;
        [SerializeField] private Color InputColor = Color.cyan;
        [SerializeField] private Color WarningColor = Color.yellow;
        [SerializeField] private Color ErrorColor = Color.red;

        private TerminalState state;
        private TextEditor editor_state;
        private bool input_fix;
        private bool move_cursor;
        private bool initial_open; // Used to focus on TextField when console opens
        private Rect window;
        private float current_open_t;
        private float open_target;
        private float real_window_size;
        private string command_text;
        private string cached_command_text;
        private Vector2 scroll_position;
        private GUIStyle window_style;
        private GUIStyle label_style;
        private GUIStyle input_style;
        private Texture2D background_texture;
        private Texture2D input_background_texture;

        public static CommandLog Buffer { get; private set; }
        public static CommandShell Shell { get; private set; }
        public static CommandHistory History { get; private set; }
        public static CommandAutocomplete Autocomplete { get; private set; }

        public static bool IssuedError => Shell.IssuedErrorMessage != null;

        public bool IsClosed => this.state == TerminalState.Close && Mathf.Approximately(this.current_open_t, this.open_target);

        public static void Log(string format, params object[] message) => Log(TerminalLogType.ShellMessage, format, message);

        public static void Log(TerminalLogType type, string format, params object[] message) => Buffer.HandleLog(string.Format(format, message), type);

        public void SetState(TerminalState new_state)
        {
            this.input_fix = true;
            this.cached_command_text = this.command_text;
            this.command_text = "";

            switch (new_state)
            {
                case TerminalState.Close:
                    {
                        this.open_target = 0;
                        break;
                    }
                case TerminalState.OpenSmall:
                    {
                        this.open_target = Screen.height * this.MaxHeight * this.SmallTerminalRatio;
                        if (this.current_open_t > this.open_target)
                        {
                            // Prevent resizing from OpenFull to OpenSmall if window y position
                            // is greater than OpenSmall's target
                            this.open_target = 0;
                            this.state = TerminalState.Close;
                            return;
                        }
                        this.real_window_size = this.open_target;
                        this.scroll_position.y = int.MaxValue;
                        break;
                    }
                case TerminalState.OpenFull:
                default:
                    {
                        this.real_window_size = Screen.height * this.MaxHeight;
                        this.open_target = this.real_window_size;
                        break;
                    }
            }

            this.state = new_state;
        }

        public void ToggleState(TerminalState new_state)
        {
            if (this.state == new_state)
            {
                SetState(TerminalState.Close);
            }
            else
            {
                SetState(new_state);
            }
        }

        private void OnEnable()
        {
            Buffer = new CommandLog(this.BufferSize);
            Shell = new CommandShell();
            History = new CommandHistory();
            Autocomplete = new CommandAutocomplete();

            // Hook Unity log events
            Application.logMessageReceived += HandleUnityLog;
        }

        private void OnDisable() => Application.logMessageReceived -= HandleUnityLog;

        private void Start()
        {
            if (this.ConsoleFont == null)
            {
                this.ConsoleFont = Font.CreateDynamicFontFromOSFont("Courier New", 16);
                Logging.LogWarning("Command Console Warning: Please assign a font.");
            }

            this.command_text = "";
            this.cached_command_text = this.command_text;
            Assert.AreNotEqual(this.ToggleHotkey.ToLower(), "return", "Return is not a valid ToggleHotkey");

            SetupWindow();
            SetupInput();
            SetupLabels();

            Shell.RegisterCommands();

            if (IssuedError)
            {
                Log(TerminalLogType.Error, "Error: {0}", Shell.IssuedErrorMessage);
            }

            foreach (System.Collections.Generic.KeyValuePair<string, CommandInfo> command in Shell.Commands)
            {
                Autocomplete.Register(command.Key);
            }
        }

        private void OnGUI()
        {
            if (Event.current.Equals(Event.KeyboardEvent(this.ToggleHotkey)))
            {
                SetState(TerminalState.OpenSmall);
                this.initial_open = true;
            }
            else if (Event.current.Equals(Event.KeyboardEvent(this.ToggleFullHotkey)))
            {
                SetState(TerminalState.OpenFull);
                this.initial_open = true;
            }

            if (this.ShowGUIButtons)
            {
                DrawGUIButtons();
            }

            if (this.IsClosed)
            {
                return;
            }

            HandleOpenness();
            this.window = GUILayout.Window(88, this.window, DrawConsole, "", this.window_style);
        }

        private void SetupWindow()
        {
            this.real_window_size = Screen.height * this.MaxHeight / 3;
            this.window = new Rect(0, this.current_open_t - this.real_window_size, Screen.width, this.real_window_size);

            // Set background color
            this.background_texture = new Texture2D(1, 1);
            this.background_texture.SetPixel(0, 0, this.BackgroundColor);
            this.background_texture.Apply();

            this.window_style = new GUIStyle();
            this.window_style.normal.background = this.background_texture;
            this.window_style.padding = new RectOffset(4, 4, 4, 4);
            this.window_style.normal.textColor = this.ForegroundColor;
            this.window_style.font = this.ConsoleFont;
        }

        private void SetupLabels()
        {
            this.label_style = new GUIStyle
            {
                font = ConsoleFont
            };
            this.label_style.normal.textColor = this.ForegroundColor;
            this.label_style.wordWrap = true;
        }

        private void SetupInput()
        {
            this.input_style = new GUIStyle
            {
                padding = new RectOffset(4, 4, 4, 4),
                font = ConsoleFont,
                fixedHeight = this.ConsoleFont.fontSize * 1.6f
            };
            this.input_style.normal.textColor = this.InputColor;

            Color dark_background = new Color
            {
                r = this.BackgroundColor.r - this.InputContrast,
                g = this.BackgroundColor.g - this.InputContrast,
                b = this.BackgroundColor.b - this.InputContrast,
                a = this.InputAlpha
            };

            this.input_background_texture = new Texture2D(1, 1);
            this.input_background_texture.SetPixel(0, 0, dark_background);
            this.input_background_texture.Apply();
            this.input_style.normal.background = this.input_background_texture;
        }

        private void DrawConsole(int Window2D)
        {
            GUILayout.BeginVertical();

            this.scroll_position = GUILayout.BeginScrollView(this.scroll_position, false, false, GUIStyle.none, GUIStyle.none);
            GUILayout.FlexibleSpace();
            DrawLogs();
            GUILayout.EndScrollView();

            if (this.move_cursor)
            {
                CursorToEnd();
                this.move_cursor = false;
            }

            if (Event.current.Equals(Event.KeyboardEvent("escape")))
            {
                SetState(TerminalState.Close);
            }
            else if (Event.current.Equals(Event.KeyboardEvent("return"))
              || Event.current.Equals(Event.KeyboardEvent("[enter]")))
            {
                EnterCommand();
            }
            else if (Event.current.Equals(Event.KeyboardEvent("up")))
            {
                this.command_text = History.Previous();
                this.move_cursor = true;
            }
            else if (Event.current.Equals(Event.KeyboardEvent("down")))
            {
                this.command_text = History.Next();
            }
            else if (Event.current.Equals(Event.KeyboardEvent(this.ToggleHotkey)))
            {
                ToggleState(TerminalState.OpenSmall);
            }
            else if (Event.current.Equals(Event.KeyboardEvent(this.ToggleFullHotkey)))
            {
                ToggleState(TerminalState.OpenFull);
            }
            else if (Event.current.Equals(Event.KeyboardEvent("tab")))
            {
                CompleteCommand();
                this.move_cursor = true; // Wait till next draw call
            }

            GUILayout.BeginHorizontal();

            if (this.InputCaret != "")
            {
                GUILayout.Label(this.InputCaret, this.input_style, GUILayout.Width(this.ConsoleFont.fontSize));
            }

            GUI.SetNextControlName("command_text_field");
            this.command_text = GUILayout.TextField(this.command_text, this.input_style);

            if (this.input_fix && this.command_text.Length > 0)
            {
                this.command_text = this.cached_command_text; // Otherwise the TextField picks up the ToggleHotkey character event
                this.input_fix = false;                  // Prevents checking string Length every draw call
            }

            if (this.initial_open)
            {
                GUI.FocusControl("command_text_field");
                this.initial_open = false;
            }

            if (this.ShowGUIButtons && GUILayout.Button("| run", this.input_style, GUILayout.Width(Screen.width / 10)))
            {
                EnterCommand();
            }

            GUILayout.EndHorizontal();
            GUILayout.EndVertical();
        }

        private void DrawLogs()
        {
            foreach (LogItem log in Buffer.Logs)
            {
                this.label_style.normal.textColor = GetLogColor(log.type);
                GUILayout.Label(log.message, this.label_style);
            }
        }

        private void DrawGUIButtons()
        {
            int size = this.ConsoleFont.fontSize;
            float x_position = this.RightAlignButtons ? Screen.width - 7 * size : 0;

            // 7 is the number of chars in the button plus some padding, 2 is the line height.
            // The layout will resize according to the font size.
            GUILayout.BeginArea(new Rect(x_position, this.current_open_t, 7 * size, size * 2));
            GUILayout.BeginHorizontal();

            if (GUILayout.Button("Small", this.window_style))
            {
                ToggleState(TerminalState.OpenSmall);
            }
            else if (GUILayout.Button("Full", this.window_style))
            {
                ToggleState(TerminalState.OpenFull);
            }

            GUILayout.EndHorizontal();
            GUILayout.EndArea();
        }

        private void HandleOpenness()
        {
            float dt = this.ToggleSpeed * Time.unscaledDeltaTime;

            if (this.current_open_t < this.open_target)
            {
                this.current_open_t += dt;
                if (this.current_open_t > this.open_target) this.current_open_t = this.open_target;
            }
            else if (this.current_open_t > this.open_target)
            {
                this.current_open_t -= dt;
                if (this.current_open_t < this.open_target) this.current_open_t = this.open_target;
            }
            else
            {
                if (this.input_fix)
                {
                    this.input_fix = false;
                }
                return; // Already at target
            }

            this.window = new Rect(0, this.current_open_t - this.real_window_size, Screen.width, this.real_window_size);
        }

        private void EnterCommand()
        {
            Log(TerminalLogType.Input, "{0}", this.command_text);
            Shell.RunCommand(this.command_text);
            History.Push(this.command_text);

            if (IssuedError)
            {
                Log(TerminalLogType.Error, "Error: {0}", Shell.IssuedErrorMessage);
            }

            this.command_text = "";
            this.scroll_position.y = int.MaxValue;
        }

        private void CompleteCommand()
        {
            string head_text = this.command_text;
            int format_width = 0;

            string[] completion_buffer = Autocomplete.Complete(ref head_text, ref format_width);
            int completion_length = completion_buffer.Length;

            if (completion_length != 0)
            {
                this.command_text = head_text;
            }

            if (completion_length > 1)
            {
                // Print possible completions
                StringBuilder log_buffer = new StringBuilder();

                foreach (string completion in completion_buffer)
                {
                    _ = log_buffer.Append(completion.PadRight(format_width + 4));
                }

                Log("{0}", log_buffer);
                this.scroll_position.y = int.MaxValue;
            }
        }

        private void CursorToEnd()
        {
            if (this.editor_state == null)
            {
                this.editor_state = (TextEditor)GUIUtility.GetStateObject(typeof(TextEditor), GUIUtility.keyboardControl);
            }

            this.editor_state.MoveCursorToPosition(new Vector2(999, 999));
        }

        private void HandleUnityLog(string message, string stack_trace, LogType type)
        {
            Buffer.HandleLog(message, stack_trace, (TerminalLogType)type);
            this.scroll_position.y = int.MaxValue;
        }

        private Color GetLogColor(TerminalLogType type)
        {
            switch (type)
            {
                case TerminalLogType.Message: return this.ForegroundColor;
                case TerminalLogType.Warning: return this.WarningColor;
                case TerminalLogType.Input: return this.InputColor;
                case TerminalLogType.ShellMessage: return this.ShellColor;
                default: return this.ErrorColor;
            }
        }
    }
}
