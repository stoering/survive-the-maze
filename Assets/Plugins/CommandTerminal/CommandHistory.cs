using System.Collections.Generic;

namespace CommandTerminal
{
    public class CommandHistory
    {
        private List<string> history = new List<string>();
        private int position;

        public void Push(string command_string)
        {
            if (command_string == "")
            {
                return;
            }

            this.history.Add(command_string);
            this.position = this.history.Count;
        }

        public string Next()
        {
            this.position++;

            if (this.position >= this.history.Count)
            {
                this.position = this.history.Count;
                return "";
            }

            return this.history[this.position];
        }

        public string Previous()
        {
            if (this.history.Count == 0)
            {
                return "";
            }

            this.position--;

            if (this.position < 0)
            {
                this.position = 0;
            }

            return this.history[this.position];
        }

        public void Clear()
        {
            this.history.Clear();
            this.position = 0;
        }
    }
}
