﻿using System.Collections.Generic;
using System.Linq;
using MoonSharp.Interpreter.Debugging;
using MoonSharp.Interpreter.Execution;
using MoonSharp.Interpreter.Execution.VM;

using MoonSharp.Interpreter.Tree.Expressions;

namespace MoonSharp.Interpreter.Tree.Statements
{
    internal class ForEachLoopStatement : Statement
    {
        private RuntimeScopeBlock m_StackFrame;
        private SymbolRef[] m_Names;
        private IVariable[] m_NameExps;
        private Expression m_RValues;
        private Statement m_Block;
        private SourceRef m_RefFor, m_RefEnd;

        public ForEachLoopStatement(ScriptLoadingContext lcontext, Token firstNameToken, Token forToken)
            : base(lcontext)
        {
            //	for namelist in explist do block end | 		

            List<string> names = new List<string>
            {
                firstNameToken.Text
            };

            while (lcontext.Lexer.Current.Type == TokenType.Comma)
            {
                lcontext.Lexer.Next();
                Token name = CheckTokenType(lcontext, TokenType.Name);
                names.Add(name.Text);
            }

            _ = CheckTokenType(lcontext, TokenType.In);

            this.m_RValues = new ExprListExpression(Expression.ExprList(lcontext), lcontext);

            lcontext.Scope.PushBlock();

            this.m_Names = names
                .Select(n => lcontext.Scope.TryDefineLocal(n))
                .ToArray();

            this.m_NameExps = this.m_Names
                .Select(s => new SymbolRefExpression(lcontext, s))
                .Cast<IVariable>()
                .ToArray();

            this.m_RefFor = forToken.GetSourceRef(CheckTokenType(lcontext, TokenType.Do));

            this.m_Block = new CompositeStatement(lcontext);

            this.m_RefEnd = CheckTokenType(lcontext, TokenType.End).GetSourceRef();

            this.m_StackFrame = lcontext.Scope.PopBlock();

            lcontext.Source.Refs.Add(this.m_RefFor);
            lcontext.Source.Refs.Add(this.m_RefEnd);
        }


        public override void Compile(ByteCode bc)
        {
            //for var_1, ···, var_n in explist do block end

            bc.PushSourceRef(this.m_RefFor);

            Loop L = new Loop()
            {
                Scope = m_StackFrame
            };
            _ = bc.LoopTracker.Loops.Push(L);

            // get iterator tuple
            this.m_RValues.Compile(bc);

            // prepares iterator tuple - stack : iterator-tuple
            _ = bc.Emit_IterPrep();

            // loop start - stack : iterator-tuple
            int start = bc.GetJumpPointForNextInstruction();
            _ = bc.Emit_Enter(this.m_StackFrame);

            // expand the tuple - stack : iterator-tuple, f, var, s
            _ = bc.Emit_ExpTuple(0);

            // calls f(s, var) - stack : iterator-tuple, iteration result
            bc.Emit_Call(2, "for..in");

            // perform assignment of iteration result- stack : iterator-tuple, iteration result
            for (int i = 0; i < this.m_NameExps.Length; i++)
                this.m_NameExps[i].CompileAssignment(bc, 0, i);

            // pops  - stack : iterator-tuple
            _ = bc.Emit_Pop();

            // repushes the main iterator var - stack : iterator-tuple, main-iterator-var
            _ = bc.Emit_Load(this.m_Names[0]);

            // updates the iterator tuple - stack : iterator-tuple, main-iterator-var
            _ = bc.Emit_IterUpd();

            // checks head, jumps if nil - stack : iterator-tuple, main-iterator-var
            Instruction endjump = bc.Emit_Jump(OpCode.JNil, -1);

            // executes the stuff - stack : iterator-tuple
            this.m_Block.Compile(bc);

            bc.PopSourceRef();
            bc.PushSourceRef(this.m_RefEnd);

            // loop back again - stack : iterator-tuple
            _ = bc.Emit_Leave(this.m_StackFrame);
            _ = bc.Emit_Jump(OpCode.Jump, start);

            _ = bc.LoopTracker.Loops.Pop();

            int exitpointLoopExit = bc.GetJumpPointForNextInstruction();
            _ = bc.Emit_Leave(this.m_StackFrame);

            int exitpointBreaks = bc.GetJumpPointForNextInstruction();

            _ = bc.Emit_Pop();

            foreach (Instruction i in L.BreakJumps)
                i.NumVal = exitpointBreaks;

            endjump.NumVal = exitpointLoopExit;

            bc.PopSourceRef();
        }


    }
}
