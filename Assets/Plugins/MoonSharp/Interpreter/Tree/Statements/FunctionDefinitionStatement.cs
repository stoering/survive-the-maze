﻿using System.Collections.Generic;
using MoonSharp.Interpreter.Debugging;
using MoonSharp.Interpreter.Execution;

using MoonSharp.Interpreter.Tree.Expressions;

namespace MoonSharp.Interpreter.Tree.Statements
{
    internal class FunctionDefinitionStatement : Statement
    {
        private SymbolRef m_FuncSymbol;
        private SourceRef m_SourceRef;
        private bool m_Local = false;
        private bool m_IsMethodCallingConvention = false;
        private string m_MethodName = null;
        private string m_FriendlyName;
        private List<string> m_TableAccessors;
        private FunctionDefinitionExpression m_FuncDef;

        public FunctionDefinitionStatement(ScriptLoadingContext lcontext, bool local, Token localToken)
            : base(lcontext)
        {
            // here lexer must be at the 'function' keyword
            Token funcKeyword = CheckTokenType(lcontext, TokenType.Function);
            funcKeyword = localToken ?? funcKeyword; // for debugger purposes

            this.m_Local = local;

            if (this.m_Local)
            {
                Token name = CheckTokenType(lcontext, TokenType.Name);
                this.m_FuncSymbol = lcontext.Scope.TryDefineLocal(name.Text);
                this.m_FriendlyName = string.Format("{0} (local)", name.Text);
                this.m_SourceRef = funcKeyword.GetSourceRef(name);
            }
            else
            {
                Token name = CheckTokenType(lcontext, TokenType.Name);
                string firstName = name.Text;

                this.m_SourceRef = funcKeyword.GetSourceRef(name);

                this.m_FuncSymbol = lcontext.Scope.Find(firstName);
                this.m_FriendlyName = firstName;

                if (lcontext.Lexer.Current.Type != TokenType.Brk_Open_Round)
                {
                    this.m_TableAccessors = new List<string>();

                    while (lcontext.Lexer.Current.Type != TokenType.Brk_Open_Round)
                    {
                        Token separator = lcontext.Lexer.Current;

                        if (separator.Type != TokenType.Colon && separator.Type != TokenType.Dot)
                            _ = UnexpectedTokenType(separator);

                        lcontext.Lexer.Next();

                        Token field = CheckTokenType(lcontext, TokenType.Name);

                        this.m_FriendlyName += separator.Text + field.Text;
                        this.m_SourceRef = funcKeyword.GetSourceRef(field);

                        if (separator.Type == TokenType.Colon)
                        {
                            this.m_MethodName = field.Text;
                            this.m_IsMethodCallingConvention = true;
                            break;
                        }
                        else
                        {
                            this.m_TableAccessors.Add(field.Text);
                        }
                    }

                    if (this.m_MethodName == null && this.m_TableAccessors.Count > 0)
                    {
                        this.m_MethodName = this.m_TableAccessors[this.m_TableAccessors.Count - 1];
                        this.m_TableAccessors.RemoveAt(this.m_TableAccessors.Count - 1);
                    }
                }
            }

            this.m_FuncDef = new FunctionDefinitionExpression(lcontext, this.m_IsMethodCallingConvention, false);
            lcontext.Source.Refs.Add(this.m_SourceRef);
        }

        public override void Compile(Execution.VM.ByteCode bc)
        {
            using (bc.EnterSource(this.m_SourceRef))
            {
                if (this.m_Local)
                {
                    _ = bc.Emit_Literal(DynValue.Nil);
                    _ = bc.Emit_Store(this.m_FuncSymbol, 0, 0);
                    _ = this.m_FuncDef.Compile(bc, () => SetFunction(bc, 2), this.m_FriendlyName);
                }
                else if (this.m_MethodName == null)
                {
                    _ = this.m_FuncDef.Compile(bc, () => SetFunction(bc, 1), this.m_FriendlyName);
                }
                else
                {
                    _ = this.m_FuncDef.Compile(bc, () => SetMethod(bc), this.m_FriendlyName);
                }
            }
        }

        private int SetMethod(Execution.VM.ByteCode bc)
        {
            int cnt = 0;

            cnt += bc.Emit_Load(this.m_FuncSymbol);

            foreach (string str in this.m_TableAccessors)
            {
                _ = bc.Emit_Index(DynValue.NewString(str), true);
                cnt += 1;
            }

            _ = bc.Emit_IndexSet(0, 0, DynValue.NewString(this.m_MethodName), true);

            return 1 + cnt;
        }

        private int SetFunction(Execution.VM.ByteCode bc, int numPop)
        {
            int num = bc.Emit_Store(this.m_FuncSymbol, 0, 0);
            _ = bc.Emit_Pop(numPop);
            return num + 1;
        }

    }
}
