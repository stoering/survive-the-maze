﻿using System.Collections.Generic;

using MoonSharp.Interpreter.Execution;


namespace MoonSharp.Interpreter.Tree.Statements
{
    internal class CompositeStatement : Statement
    {
        private List<Statement> m_Statements = new List<Statement>();

        public CompositeStatement(ScriptLoadingContext lcontext)
            : base(lcontext)
        {
            while (true)
            {
                Token t = lcontext.Lexer.Current;
                if (t.IsEndOfBlock()) break;


                Statement s = CreateStatement(lcontext, out bool forceLast);
                this.m_Statements.Add(s);

                if (forceLast) break;
            }

            // eat away all superfluos ';'s
            while (lcontext.Lexer.Current.Type == TokenType.SemiColon)
                lcontext.Lexer.Next();
        }


        public override void Compile(Execution.VM.ByteCode bc)
        {
            if (this.m_Statements != null)
            {
                foreach (Statement s in this.m_Statements)
                {
                    s.Compile(bc);
                }
            }
        }
    }
}
