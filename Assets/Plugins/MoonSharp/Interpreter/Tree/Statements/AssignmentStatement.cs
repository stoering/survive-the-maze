﻿using System;
using System.Collections.Generic;
using MoonSharp.Interpreter.Debugging;
using MoonSharp.Interpreter.Execution;

using MoonSharp.Interpreter.Tree.Expressions;

namespace MoonSharp.Interpreter.Tree.Statements
{
    internal class AssignmentStatement : Statement
    {
        private List<IVariable> m_LValues = new List<IVariable>();
        private List<Expression> m_RValues;
        private SourceRef m_Ref;


        public AssignmentStatement(ScriptLoadingContext lcontext, Token startToken)
            : base(lcontext)
        {
            List<string> names = new List<string>();

            Token first = startToken;

            while (true)
            {
                Token name = CheckTokenType(lcontext, TokenType.Name);
                names.Add(name.Text);

                if (lcontext.Lexer.Current.Type != TokenType.Comma)
                    break;

                lcontext.Lexer.Next();
            }

            if (lcontext.Lexer.Current.Type == TokenType.Op_Assignment)
            {
                _ = CheckTokenType(lcontext, TokenType.Op_Assignment);
                this.m_RValues = Expression.ExprList(lcontext);
            }
            else
            {
                this.m_RValues = new List<Expression>();
            }

            foreach (string name in names)
            {
                SymbolRef localVar = lcontext.Scope.TryDefineLocal(name);
                SymbolRefExpression symbol = new SymbolRefExpression(lcontext, localVar);
                this.m_LValues.Add(symbol);
            }

            Token last = lcontext.Lexer.Current;
            this.m_Ref = first.GetSourceRefUpTo(last);
            lcontext.Source.Refs.Add(this.m_Ref);

        }


        public AssignmentStatement(ScriptLoadingContext lcontext, Expression firstExpression, Token first)
            : base(lcontext)
        {
            this.m_LValues.Add(CheckVar(lcontext, firstExpression));

            while (lcontext.Lexer.Current.Type == TokenType.Comma)
            {
                lcontext.Lexer.Next();
                Expression e = Expression.PrimaryExp(lcontext);
                this.m_LValues.Add(CheckVar(lcontext, e));
            }

            _ = CheckTokenType(lcontext, TokenType.Op_Assignment);

            this.m_RValues = Expression.ExprList(lcontext);

            Token last = lcontext.Lexer.Current;
            this.m_Ref = first.GetSourceRefUpTo(last);
            lcontext.Source.Refs.Add(this.m_Ref);

        }

        private IVariable CheckVar(ScriptLoadingContext lcontext, Expression firstExpression)
        {
            if (!(firstExpression is IVariable v))
                throw new SyntaxErrorException(lcontext.Lexer.Current, "unexpected symbol near '{0}' - not a l-value", lcontext.Lexer.Current);

            return v;
        }


        public override void Compile(Execution.VM.ByteCode bc)
        {
            using (bc.EnterSource(this.m_Ref))
            {
                foreach (Expression exp in this.m_RValues)
                {
                    exp.Compile(bc);
                }

                for (int i = 0; i < this.m_LValues.Count; i++)
                    this.m_LValues[i].CompileAssignment(bc,
                            Math.Max(this.m_RValues.Count - 1 - i, 0), // index of r-value
                            i - Math.Min(i, this.m_RValues.Count - 1)); // index in last tuple

                _ = bc.Emit_Pop(this.m_RValues.Count);
            }
        }

    }
}
