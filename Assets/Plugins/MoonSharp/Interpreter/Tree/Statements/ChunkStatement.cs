﻿using MoonSharp.Interpreter.Execution;
using MoonSharp.Interpreter.Execution.VM;

namespace MoonSharp.Interpreter.Tree.Statements
{
    internal class ChunkStatement : Statement, IClosureBuilder
    {
        private Statement m_Block;
        private RuntimeScopeFrame m_StackFrame;
        private SymbolRef m_Env;
        private SymbolRef m_VarArgs;

        public ChunkStatement(ScriptLoadingContext lcontext)
            : base(lcontext)
        {
            lcontext.Scope.PushFunction(this, true);
            this.m_Env = lcontext.Scope.DefineLocal(WellKnownSymbols.ENV);
            this.m_VarArgs = lcontext.Scope.DefineLocal(WellKnownSymbols.VARARGS);

            this.m_Block = new CompositeStatement(lcontext);

            if (lcontext.Lexer.Current.Type != TokenType.Eof)
                throw new SyntaxErrorException(lcontext.Lexer.Current, "<eof> expected near '{0}'", lcontext.Lexer.Current.Text);

            this.m_StackFrame = lcontext.Scope.PopFunction();
        }


        public override void Compile(Execution.VM.ByteCode bc)
        {
            Instruction meta = bc.Emit_Meta("<chunk-root>", OpCodeMetadataType.ChunkEntrypoint);
            int metaip = bc.GetJumpPointForLastInstruction();

            _ = bc.Emit_BeginFn(this.m_StackFrame);
            _ = bc.Emit_Args(this.m_VarArgs);

            _ = bc.Emit_Load(SymbolRef.Upvalue(WellKnownSymbols.ENV, 0));
            _ = bc.Emit_Store(this.m_Env, 0, 0);
            _ = bc.Emit_Pop();

            this.m_Block.Compile(bc);
            _ = bc.Emit_Ret(0);

            meta.NumVal = bc.GetJumpPointForLastInstruction() - metaip;
        }

        public SymbolRef CreateUpvalue(BuildTimeScope scope, SymbolRef symbol) => null;
    }
}
