﻿using MoonSharp.Interpreter.Debugging;
using MoonSharp.Interpreter.Execution;
using MoonSharp.Interpreter.Execution.VM;

using MoonSharp.Interpreter.Tree.Expressions;

namespace MoonSharp.Interpreter.Tree.Statements
{
    internal class ForLoopStatement : Statement
    {
        //for' NAME '=' exp ',' exp (',' exp)? 'do' block 'end'
        private RuntimeScopeBlock m_StackFrame;
        private Statement m_InnerBlock;
        private SymbolRef m_VarName;
        private Expression m_Start, m_End, m_Step;
        private SourceRef m_RefFor, m_RefEnd;

        public ForLoopStatement(ScriptLoadingContext lcontext, Token nameToken, Token forToken)
            : base(lcontext)
        {
            //	for Name ‘=’ exp ‘,’ exp [‘,’ exp] do block end | 

            // lexer already at the '=' ! [due to dispatching vs for-each]
            _ = CheckTokenType(lcontext, TokenType.Op_Assignment);

            this.m_Start = Expression.Expr(lcontext);
            _ = CheckTokenType(lcontext, TokenType.Comma);
            this.m_End = Expression.Expr(lcontext);

            if (lcontext.Lexer.Current.Type == TokenType.Comma)
            {
                lcontext.Lexer.Next();
                this.m_Step = Expression.Expr(lcontext);
            }
            else
            {
                this.m_Step = new LiteralExpression(lcontext, DynValue.NewNumber(1));
            }

            lcontext.Scope.PushBlock();
            this.m_VarName = lcontext.Scope.DefineLocal(nameToken.Text);
            this.m_RefFor = forToken.GetSourceRef(CheckTokenType(lcontext, TokenType.Do));
            this.m_InnerBlock = new CompositeStatement(lcontext);
            this.m_RefEnd = CheckTokenType(lcontext, TokenType.End).GetSourceRef();
            this.m_StackFrame = lcontext.Scope.PopBlock();

            lcontext.Source.Refs.Add(this.m_RefFor);
            lcontext.Source.Refs.Add(this.m_RefEnd);
        }


        public override void Compile(ByteCode bc)
        {
            bc.PushSourceRef(this.m_RefFor);

            Loop L = new Loop()
            {
                Scope = m_StackFrame
            };

            _ = bc.LoopTracker.Loops.Push(L);

            this.m_End.Compile(bc);
            _ = bc.Emit_ToNum(3);
            this.m_Step.Compile(bc);
            _ = bc.Emit_ToNum(2);
            this.m_Start.Compile(bc);
            _ = bc.Emit_ToNum(1);

            int start = bc.GetJumpPointForNextInstruction();
            Instruction jumpend = bc.Emit_Jump(OpCode.JFor, -1);
            _ = bc.Emit_Enter(this.m_StackFrame);
            //bc.Emit_SymStorN(m_VarName);

            _ = bc.Emit_Store(this.m_VarName, 0, 0);

            this.m_InnerBlock.Compile(bc);

            bc.PopSourceRef();
            bc.PushSourceRef(this.m_RefEnd);

            bc.Emit_Debug("..end");
            _ = bc.Emit_Leave(this.m_StackFrame);
            _ = bc.Emit_Incr(1);
            _ = bc.Emit_Jump(OpCode.Jump, start);

            _ = bc.LoopTracker.Loops.Pop();

            int exitpoint = bc.GetJumpPointForNextInstruction();

            foreach (Instruction i in L.BreakJumps)
                i.NumVal = exitpoint;

            jumpend.NumVal = exitpoint;
            _ = bc.Emit_Pop(3);

            bc.PopSourceRef();
        }

    }
}
