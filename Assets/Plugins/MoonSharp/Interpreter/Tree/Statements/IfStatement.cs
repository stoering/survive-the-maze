﻿using System.Collections.Generic;
using MoonSharp.Interpreter.Debugging;
using MoonSharp.Interpreter.Execution;
using MoonSharp.Interpreter.Execution.VM;


namespace MoonSharp.Interpreter.Tree.Statements
{
    internal class IfStatement : Statement
    {
        private class IfBlock
        {
            public Expression Exp;
            public Statement Block;
            public RuntimeScopeBlock StackFrame;
            public SourceRef Source;
        }

        private List<IfBlock> m_Ifs = new List<IfBlock>();
        private IfBlock m_Else = null;
        private SourceRef m_End;

        public IfStatement(ScriptLoadingContext lcontext)
            : base(lcontext)
        {
            while (lcontext.Lexer.Current.Type != TokenType.Else && lcontext.Lexer.Current.Type != TokenType.End)
            {
                this.m_Ifs.Add(CreateIfBlock(lcontext));
            }

            if (lcontext.Lexer.Current.Type == TokenType.Else)
            {
                this.m_Else = CreateElseBlock(lcontext);
            }

            this.m_End = CheckTokenType(lcontext, TokenType.End).GetSourceRef();
            lcontext.Source.Refs.Add(this.m_End);
        }

        private IfBlock CreateIfBlock(ScriptLoadingContext lcontext)
        {
            Token type = CheckTokenType(lcontext, TokenType.If, TokenType.ElseIf);

            lcontext.Scope.PushBlock();

            IfBlock ifblock = new IfBlock
            {
                Exp = Expression.Expr(lcontext),
                Source = type.GetSourceRef(CheckTokenType(lcontext, TokenType.Then)),
                Block = new CompositeStatement(lcontext),
                StackFrame = lcontext.Scope.PopBlock()
            };
            lcontext.Source.Refs.Add(ifblock.Source);


            return ifblock;
        }

        private IfBlock CreateElseBlock(ScriptLoadingContext lcontext)
        {
            Token type = CheckTokenType(lcontext, TokenType.Else);

            lcontext.Scope.PushBlock();

            IfBlock ifblock = new IfBlock
            {
                Block = new CompositeStatement(lcontext),
                StackFrame = lcontext.Scope.PopBlock(),
                Source = type.GetSourceRef()
            };
            lcontext.Source.Refs.Add(ifblock.Source);
            return ifblock;
        }


        public override void Compile(Execution.VM.ByteCode bc)
        {
            List<Instruction> endJumps = new List<Instruction>();

            Instruction lastIfJmp = null;

            foreach (IfBlock ifblock in this.m_Ifs)
            {
                using (bc.EnterSource(ifblock.Source))
                {
                    if (lastIfJmp != null)
                        lastIfJmp.NumVal = bc.GetJumpPointForNextInstruction();

                    ifblock.Exp.Compile(bc);
                    lastIfJmp = bc.Emit_Jump(OpCode.Jf, -1);
                    _ = bc.Emit_Enter(ifblock.StackFrame);
                    ifblock.Block.Compile(bc);
                }

                using (bc.EnterSource(this.m_End))
                    _ = bc.Emit_Leave(ifblock.StackFrame);

                endJumps.Add(bc.Emit_Jump(OpCode.Jump, -1));
            }

            lastIfJmp.NumVal = bc.GetJumpPointForNextInstruction();

            if (this.m_Else != null)
            {
                using (bc.EnterSource(this.m_Else.Source))
                {
                    _ = bc.Emit_Enter(this.m_Else.StackFrame);
                    this.m_Else.Block.Compile(bc);
                }

                using (bc.EnterSource(this.m_End))
                    _ = bc.Emit_Leave(this.m_Else.StackFrame);
            }

            foreach (Instruction endjmp in endJumps)
                endjmp.NumVal = bc.GetJumpPointForNextInstruction();
        }



    }
}
