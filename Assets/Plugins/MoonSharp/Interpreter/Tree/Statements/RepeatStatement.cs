﻿using MoonSharp.Interpreter.Debugging;
using MoonSharp.Interpreter.Execution;
using MoonSharp.Interpreter.Execution.VM;


namespace MoonSharp.Interpreter.Tree.Statements
{
    internal class RepeatStatement : Statement
    {
        private Expression m_Condition;
        private Statement m_Block;
        private RuntimeScopeBlock m_StackFrame;
        private SourceRef m_Repeat, m_Until;

        public RepeatStatement(ScriptLoadingContext lcontext)
            : base(lcontext)
        {
            this.m_Repeat = CheckTokenType(lcontext, TokenType.Repeat).GetSourceRef();

            lcontext.Scope.PushBlock();
            this.m_Block = new CompositeStatement(lcontext);

            Token until = CheckTokenType(lcontext, TokenType.Until);

            this.m_Condition = Expression.Expr(lcontext);

            this.m_Until = until.GetSourceRefUpTo(lcontext.Lexer.Current);

            this.m_StackFrame = lcontext.Scope.PopBlock();
            lcontext.Source.Refs.Add(this.m_Repeat);
            lcontext.Source.Refs.Add(this.m_Until);
        }

        public override void Compile(ByteCode bc)
        {
            Loop L = new Loop()
            {
                Scope = m_StackFrame
            };

            bc.PushSourceRef(this.m_Repeat);

            _ = bc.LoopTracker.Loops.Push(L);

            int start = bc.GetJumpPointForNextInstruction();

            _ = bc.Emit_Enter(this.m_StackFrame);
            this.m_Block.Compile(bc);

            bc.PopSourceRef();
            bc.PushSourceRef(this.m_Until);
            bc.Emit_Debug("..end");

            this.m_Condition.Compile(bc);
            _ = bc.Emit_Leave(this.m_StackFrame);
            _ = bc.Emit_Jump(OpCode.Jf, start);

            _ = bc.LoopTracker.Loops.Pop();

            int exitpoint = bc.GetJumpPointForNextInstruction();

            foreach (Instruction i in L.BreakJumps)
                i.NumVal = exitpoint;

            bc.PopSourceRef();
        }


    }
}
