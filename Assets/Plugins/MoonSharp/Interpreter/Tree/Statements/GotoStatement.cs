﻿using MoonSharp.Interpreter.Debugging;
using MoonSharp.Interpreter.Execution;
using MoonSharp.Interpreter.Execution.VM;

namespace MoonSharp.Interpreter.Tree.Statements
{
    internal class GotoStatement : Statement
    {
        internal SourceRef SourceRef { get; private set; }
        internal Token GotoToken { get; private set; }
        public string Label { get; private set; }

        internal int DefinedVarsCount { get; private set; }
        internal string LastDefinedVarName { get; private set; }

        private Instruction m_Jump;
        private int m_LabelAddress = -1;

        public GotoStatement(ScriptLoadingContext lcontext)
            : base(lcontext)
        {
            this.GotoToken = CheckTokenType(lcontext, TokenType.Goto);
            Token name = CheckTokenType(lcontext, TokenType.Name);

            this.SourceRef = this.GotoToken.GetSourceRef(name);

            this.Label = name.Text;

            lcontext.Scope.RegisterGoto(this);
        }

        public override void Compile(ByteCode bc) => this.m_Jump = bc.Emit_Jump(OpCode.Jump, this.m_LabelAddress);

        internal void SetDefinedVars(int definedVarsCount, string lastDefinedVarsName)
        {
            this.DefinedVarsCount = definedVarsCount;
            this.LastDefinedVarName = lastDefinedVarsName;
        }


        internal void SetAddress(int labelAddress)
        {
            this.m_LabelAddress = labelAddress;

            if (this.m_Jump != null)
                this.m_Jump.NumVal = labelAddress;
        }

    }
}
