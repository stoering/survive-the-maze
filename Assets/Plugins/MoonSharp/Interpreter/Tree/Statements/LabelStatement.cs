﻿using System.Collections.Generic;
using MoonSharp.Interpreter.Debugging;
using MoonSharp.Interpreter.Execution;

namespace MoonSharp.Interpreter.Tree.Statements
{
    internal class LabelStatement : Statement
    {
        public string Label { get; private set; }
        public int Address { get; private set; }
        public SourceRef SourceRef { get; private set; }
        public Token NameToken { get; private set; }

        internal int DefinedVarsCount { get; private set; }
        internal string LastDefinedVarName { get; private set; }

        private List<GotoStatement> m_Gotos = new List<GotoStatement>();
        private RuntimeScopeBlock m_StackFrame;


        public LabelStatement(ScriptLoadingContext lcontext)
            : base(lcontext)
        {
            _ = CheckTokenType(lcontext, TokenType.DoubleColon);
            this.NameToken = CheckTokenType(lcontext, TokenType.Name);
            _ = CheckTokenType(lcontext, TokenType.DoubleColon);

            this.SourceRef = this.NameToken.GetSourceRef();
            this.Label = this.NameToken.Text;

            lcontext.Scope.DefineLabel(this);
        }

        internal void SetDefinedVars(int definedVarsCount, string lastDefinedVarsName)
        {
            this.DefinedVarsCount = definedVarsCount;
            this.LastDefinedVarName = lastDefinedVarsName;
        }

        internal void RegisterGoto(GotoStatement gotostat) => this.m_Gotos.Add(gotostat);


        public override void Compile(Execution.VM.ByteCode bc)
        {
            _ = bc.Emit_Clean(this.m_StackFrame);

            this.Address = bc.GetJumpPointForLastInstruction();

            foreach (GotoStatement gotostat in this.m_Gotos)
                gotostat.SetAddress(this.Address);
        }

        internal void SetScope(RuntimeScopeBlock runtimeScopeBlock) => this.m_StackFrame = runtimeScopeBlock;
    }
}

