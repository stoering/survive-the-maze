﻿using MoonSharp.Interpreter.Execution;
using MoonSharp.Interpreter.Execution.VM;
using MoonSharp.Interpreter.Tree.Expressions;

namespace MoonSharp.Interpreter.Tree.Statements
{
    internal class FunctionCallStatement : Statement
    {
        private FunctionCallExpression m_FunctionCallExpression;

        public FunctionCallStatement(ScriptLoadingContext lcontext, FunctionCallExpression functionCallExpression)
            : base(lcontext)
        {
            this.m_FunctionCallExpression = functionCallExpression;
            lcontext.Source.Refs.Add(this.m_FunctionCallExpression.SourceRef);
        }


        public override void Compile(ByteCode bc)
        {
            using (bc.EnterSource(this.m_FunctionCallExpression.SourceRef))
            {
                this.m_FunctionCallExpression.Compile(bc);
                RemoveBreakpointStop(bc.Emit_Pop());
            }
        }

        private void RemoveBreakpointStop(Instruction instruction) => instruction.SourceCodeRef = null;
    }
}
