﻿using MoonSharp.Interpreter.Debugging;
using MoonSharp.Interpreter.Execution;
using MoonSharp.Interpreter.Execution.VM;


namespace MoonSharp.Interpreter.Tree.Statements
{
    internal class WhileStatement : Statement
    {
        private Expression m_Condition;
        private Statement m_Block;
        private RuntimeScopeBlock m_StackFrame;
        private SourceRef m_Start, m_End;

        public WhileStatement(ScriptLoadingContext lcontext)
            : base(lcontext)
        {
            Token whileTk = CheckTokenType(lcontext, TokenType.While);

            this.m_Condition = Expression.Expr(lcontext);

            this.m_Start = whileTk.GetSourceRefUpTo(lcontext.Lexer.Current);

            //m_Start = BuildSourceRef(context.Start, exp.Stop);
            //m_End = BuildSourceRef(context.Stop, context.END());

            lcontext.Scope.PushBlock();
            _ = CheckTokenType(lcontext, TokenType.Do);
            this.m_Block = new CompositeStatement(lcontext);
            this.m_End = CheckTokenType(lcontext, TokenType.End).GetSourceRef();
            this.m_StackFrame = lcontext.Scope.PopBlock();

            lcontext.Source.Refs.Add(this.m_Start);
            lcontext.Source.Refs.Add(this.m_End);
        }


        public override void Compile(ByteCode bc)
        {
            Loop L = new Loop()
            {
                Scope = m_StackFrame
            };


            _ = bc.LoopTracker.Loops.Push(L);

            bc.PushSourceRef(this.m_Start);

            int start = bc.GetJumpPointForNextInstruction();

            this.m_Condition.Compile(bc);
            Instruction jumpend = bc.Emit_Jump(OpCode.Jf, -1);

            _ = bc.Emit_Enter(this.m_StackFrame);

            this.m_Block.Compile(bc);

            bc.PopSourceRef();
            bc.Emit_Debug("..end");
            bc.PushSourceRef(this.m_End);

            _ = bc.Emit_Leave(this.m_StackFrame);
            _ = bc.Emit_Jump(OpCode.Jump, start);

            _ = bc.LoopTracker.Loops.Pop();

            int exitpoint = bc.GetJumpPointForNextInstruction();

            foreach (Instruction i in L.BreakJumps)
                i.NumVal = exitpoint;

            jumpend.NumVal = exitpoint;

            bc.PopSourceRef();
        }

    }
}
