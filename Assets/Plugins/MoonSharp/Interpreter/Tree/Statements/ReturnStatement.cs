﻿using MoonSharp.Interpreter.Debugging;
using MoonSharp.Interpreter.Execution;

using MoonSharp.Interpreter.Tree.Expressions;

namespace MoonSharp.Interpreter.Tree.Statements
{
    internal class ReturnStatement : Statement
    {
        private Expression m_Expression = null;
        private SourceRef m_Ref;

        public ReturnStatement(ScriptLoadingContext lcontext, Expression e, SourceRef sref)
            : base(lcontext)
        {
            this.m_Expression = e;
            this.m_Ref = sref;
            lcontext.Source.Refs.Add(sref);
        }




        public ReturnStatement(ScriptLoadingContext lcontext)
            : base(lcontext)
        {
            Token ret = lcontext.Lexer.Current;

            lcontext.Lexer.Next();

            Token cur = lcontext.Lexer.Current;

            if (cur.IsEndOfBlock() || cur.Type == TokenType.SemiColon)
            {
                this.m_Expression = null;
                this.m_Ref = ret.GetSourceRef();
            }
            else
            {
                this.m_Expression = new ExprListExpression(Expression.ExprList(lcontext), lcontext);
                this.m_Ref = ret.GetSourceRefUpTo(lcontext.Lexer.Current);
            }
            lcontext.Source.Refs.Add(this.m_Ref);
        }



        public override void Compile(Execution.VM.ByteCode bc)
        {
            using (bc.EnterSource(this.m_Ref))
            {
                if (this.m_Expression != null)
                {
                    this.m_Expression.Compile(bc);
                    _ = bc.Emit_Ret(1);
                }
                else
                {
                    _ = bc.Emit_Ret(0);
                }
            }
        }
    }
}
