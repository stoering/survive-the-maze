﻿using System.Collections.Generic;
using MoonSharp.Interpreter.Execution;
using MoonSharp.Interpreter.Execution.VM;

namespace MoonSharp.Interpreter.Tree
{
    internal class Loop : ILoop
    {
        public RuntimeScopeBlock Scope;
        public List<Instruction> BreakJumps = new List<Instruction>();

        public void CompileBreak(ByteCode bc)
        {
            _ = bc.Emit_Exit(this.Scope);
            this.BreakJumps.Add(bc.Emit_Jump(OpCode.Jump, -1));
        }

        public bool IsBoundary() => false;
    }

    internal class LoopBoundary : ILoop
    {
        public void CompileBreak(ByteCode bc) => throw new InternalErrorException("CompileBreak called on LoopBoundary");

        public bool IsBoundary() => true;
    }

}
