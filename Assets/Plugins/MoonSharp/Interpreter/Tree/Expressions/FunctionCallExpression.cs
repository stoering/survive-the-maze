﻿using System.Collections.Generic;
using MoonSharp.Interpreter.Debugging;
using MoonSharp.Interpreter.Execution;

namespace MoonSharp.Interpreter.Tree.Expressions
{
    internal class FunctionCallExpression : Expression
    {
        private List<Expression> m_Arguments;
        private Expression m_Function;
        private string m_Name;
        private string m_DebugErr;

        internal SourceRef SourceRef { get; private set; }


        public FunctionCallExpression(ScriptLoadingContext lcontext, Expression function, Token thisCallName)
            : base(lcontext)
        {
            Token callToken = thisCallName ?? lcontext.Lexer.Current;

            this.m_Name = thisCallName != null ? thisCallName.Text : null;
            this.m_DebugErr = function.GetFriendlyDebugName();
            this.m_Function = function;

            switch (lcontext.Lexer.Current.Type)
            {
                case TokenType.Brk_Open_Round:
                    Token openBrk = lcontext.Lexer.Current;
                    lcontext.Lexer.Next();
                    Token t = lcontext.Lexer.Current;
                    if (t.Type == TokenType.Brk_Close_Round)
                    {
                        this.m_Arguments = new List<Expression>();
                        this.SourceRef = callToken.GetSourceRef(t);
                        lcontext.Lexer.Next();
                    }
                    else
                    {
                        this.m_Arguments = ExprList(lcontext);
                        this.SourceRef = callToken.GetSourceRef(CheckMatch(lcontext, openBrk, TokenType.Brk_Close_Round, ")"));
                    }
                    break;
                case TokenType.String:
                case TokenType.String_Long:
                    {
                        this.m_Arguments = new List<Expression>();
                        Expression le = new LiteralExpression(lcontext, lcontext.Lexer.Current);
                        this.m_Arguments.Add(le);
                        this.SourceRef = callToken.GetSourceRef(lcontext.Lexer.Current);
                    }
                    break;
                case TokenType.Brk_Open_Curly:
                case TokenType.Brk_Open_Curly_Shared:
                    {
                        this.m_Arguments = new List<Expression>
                        {
                            new TableConstructor(lcontext, lcontext.Lexer.Current.Type == TokenType.Brk_Open_Curly_Shared)
                        };
                        this.SourceRef = callToken.GetSourceRefUpTo(lcontext.Lexer.Current);
                    }
                    break;
                default:
                    throw new SyntaxErrorException(lcontext.Lexer.Current, "function arguments expected")
                    {
                        IsPrematureStreamTermination = (lcontext.Lexer.Current.Type == TokenType.Eof)
                    };
            }
        }

        public override void Compile(Execution.VM.ByteCode bc)
        {
            this.m_Function.Compile(bc);

            int argslen = this.m_Arguments.Count;

            if (!string.IsNullOrEmpty(this.m_Name))
            {
                _ = bc.Emit_Copy(0);
                _ = bc.Emit_Index(DynValue.NewString(this.m_Name), true);
                _ = bc.Emit_Swap(0, 1);
                ++argslen;
            }

            for (int i = 0; i < this.m_Arguments.Count; i++)
                this.m_Arguments[i].Compile(bc);

            if (!string.IsNullOrEmpty(this.m_Name))
            {
                bc.Emit_ThisCall(argslen, this.m_DebugErr);
            }
            else
            {
                bc.Emit_Call(argslen, this.m_DebugErr);
            }
        }

        public override DynValue Eval(ScriptExecutionContext context) => throw new DynamicExpressionException("Dynamic Expressions cannot call functions.");

    }
}
