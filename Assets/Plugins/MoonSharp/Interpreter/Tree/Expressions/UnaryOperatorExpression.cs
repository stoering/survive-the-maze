﻿using MoonSharp.Interpreter.Execution;
using MoonSharp.Interpreter.Execution.VM;

namespace MoonSharp.Interpreter.Tree.Expressions
{
    internal class UnaryOperatorExpression : Expression
    {
        private Expression m_Exp;
        private string m_OpText;

        public UnaryOperatorExpression(ScriptLoadingContext lcontext, Expression subExpression, Token unaryOpToken)
            : base(lcontext)
        {
            this.m_OpText = unaryOpToken.Text;
            this.m_Exp = subExpression;
        }



        public override void Compile(ByteCode bc)
        {
            this.m_Exp.Compile(bc);

            switch (this.m_OpText)
            {
                case "not":
                    _ = bc.Emit_Operator(OpCode.Not);
                    break;
                case "#":
                    _ = bc.Emit_Operator(OpCode.Len);
                    break;
                case "-":
                    _ = bc.Emit_Operator(OpCode.Neg);
                    break;
                default:
                    throw new InternalErrorException("Unexpected unary operator '{0}'", this.m_OpText);
            }


        }

        public override DynValue Eval(ScriptExecutionContext context)
        {
            DynValue v = this.m_Exp.Eval(context).ToScalar();

            switch (this.m_OpText)
            {
                case "not":
                    return DynValue.NewBoolean(!v.CastToBool());
                case "#":
                    return v.GetLength();
                case "-":
                    {
                        double? d = v.CastToNumber();

                        if (d.HasValue)
                            return DynValue.NewNumber(-d.Value);

                        throw new DynamicExpressionException("Attempt to perform arithmetic on non-numbers.");
                    }
                default:
                    throw new DynamicExpressionException("Unexpected unary operator '{0}'", this.m_OpText);
            }
        }
    }
}
