﻿using System;
using MoonSharp.Interpreter.Execution;

namespace MoonSharp.Interpreter.Tree.Expressions
{
    internal class DynamicExprExpression : Expression
    {
        private Expression m_Exp;

        public DynamicExprExpression(Expression exp, ScriptLoadingContext lcontext)
            : base(lcontext)
        {
            lcontext.Anonymous = true;
            this.m_Exp = exp;
        }


        public override DynValue Eval(ScriptExecutionContext context) => this.m_Exp.Eval(context);

        public override void Compile(Execution.VM.ByteCode bc) => throw new InvalidOperationException();

        public override SymbolRef FindDynamic(ScriptExecutionContext context) => this.m_Exp.FindDynamic(context);
    }
}
