﻿using MoonSharp.Interpreter.Execution;
using MoonSharp.Interpreter.Execution.VM;

namespace MoonSharp.Interpreter.Tree.Expressions
{
    internal class IndexExpression : Expression, IVariable
    {
        private Expression m_BaseExp;
        private Expression m_IndexExp;
        private string m_Name;


        public IndexExpression(Expression baseExp, Expression indexExp, ScriptLoadingContext lcontext)
            : base(lcontext)
        {
            this.m_BaseExp = baseExp;
            this.m_IndexExp = indexExp;
        }

        public IndexExpression(Expression baseExp, string name, ScriptLoadingContext lcontext)
            : base(lcontext)
        {
            this.m_BaseExp = baseExp;
            this.m_Name = name;
        }


        public override void Compile(ByteCode bc)
        {
            this.m_BaseExp.Compile(bc);

            if (this.m_Name != null)
            {
                _ = bc.Emit_Index(DynValue.NewString(this.m_Name), true);
            }
            else if (this.m_IndexExp is LiteralExpression lit)
            {
                _ = bc.Emit_Index(lit.Value);
            }
            else
            {
                this.m_IndexExp.Compile(bc);
                _ = bc.Emit_Index(isExpList: (this.m_IndexExp is ExprListExpression));
            }
        }

        public void CompileAssignment(ByteCode bc, int stackofs, int tupleidx)
        {
            this.m_BaseExp.Compile(bc);

            if (this.m_Name != null)
            {
                _ = bc.Emit_IndexSet(stackofs, tupleidx, DynValue.NewString(this.m_Name), isNameIndex: true);
            }
            else if (this.m_IndexExp is LiteralExpression lit)
            {
                _ = bc.Emit_IndexSet(stackofs, tupleidx, lit.Value);
            }
            else
            {
                this.m_IndexExp.Compile(bc);
                _ = bc.Emit_IndexSet(stackofs, tupleidx, isExpList: (this.m_IndexExp is ExprListExpression));
            }
        }

        public override DynValue Eval(ScriptExecutionContext context)
        {
            DynValue b = this.m_BaseExp.Eval(context).ToScalar();
            DynValue i = this.m_IndexExp != null ? this.m_IndexExp.Eval(context).ToScalar() : DynValue.NewString(this.m_Name);

            if (b.Type != DataType.Table) throw new DynamicExpressionException("Attempt to index non-table.");
            else if (i.IsNilOrNan()) throw new DynamicExpressionException("Attempt to index with nil or nan key.");
            return b.Table.Get(i) ?? DynValue.Nil;
        }
    }
}
