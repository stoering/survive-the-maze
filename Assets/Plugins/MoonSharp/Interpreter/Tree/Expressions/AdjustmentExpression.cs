﻿using MoonSharp.Interpreter.Execution;


namespace MoonSharp.Interpreter.Tree.Expressions
{
    internal class AdjustmentExpression : Expression
    {
        private Expression expression;

        public AdjustmentExpression(ScriptLoadingContext lcontext, Expression exp)
            : base(lcontext) => this.expression = exp;

        public override void Compile(Execution.VM.ByteCode bc)
        {
            this.expression.Compile(bc);
            _ = bc.Emit_Scalar();
        }

        public override DynValue Eval(ScriptExecutionContext context) => this.expression.Eval(context).ToScalar();
    }
}
