﻿using System.Collections.Generic;
using MoonSharp.Interpreter.Execution;

namespace MoonSharp.Interpreter.Tree.Expressions
{
    internal class ExprListExpression : Expression
    {
        private List<Expression> expressions;

        public ExprListExpression(List<Expression> exps, ScriptLoadingContext lcontext)
            : base(lcontext) => this.expressions = exps;


        public Expression[] GetExpressions() => this.expressions.ToArray();

        public override void Compile(Execution.VM.ByteCode bc)
        {
            foreach (Expression exp in this.expressions)
                exp.Compile(bc);

            if (this.expressions.Count > 1)
                _ = bc.Emit_MkTuple(this.expressions.Count);
        }

        public override DynValue Eval(ScriptExecutionContext context)
        {
            if (this.expressions.Count >= 1)
                return this.expressions[0].Eval(context);

            return DynValue.Void;
        }
    }
}
