﻿#if !USE_DYNAMIC_STACKS

using System;
using System.Collections.Generic;

namespace MoonSharp.Interpreter.DataStructs
{
    /// <summary>
    /// A preallocated, non-resizable, stack
    /// </summary>
    /// <typeparam name="T"></typeparam>
    internal class FastStack<T> : IList<T>
    {
        private T[] m_Storage;

        public FastStack(int maxCapacity) => this.m_Storage = new T[maxCapacity];

        public T this[int index]
        {
            get => this.m_Storage[index];
            set => this.m_Storage[index] = value;
        }

        public T Push(T item)
        {
            this.m_Storage[this.Count++] = item;
            return item;
        }

        public void Expand(int size) => this.Count += size;

        private void Zero(int from, int to) => Array.Clear(this.m_Storage, from, to - from + 1);

        private void Zero(int index) => this.m_Storage[index] = default;

        public T Peek(int idxofs = 0)
        {
            T item = this.m_Storage[this.Count - 1 - idxofs];
            return item;
        }

        public void Set(int idxofs, T item) => this.m_Storage[this.Count - 1 - idxofs] = item;

        public void CropAtCount(int p) => RemoveLast(this.Count - p);

        public void RemoveLast(int cnt = 1)
        {
            if (cnt == 1)
            {
                --this.Count;
                this.m_Storage[this.Count] = default;
            }
            else
            {
                int oldhead = this.Count;
                this.Count -= cnt;
                Zero(this.Count, oldhead);
            }
        }

        public T Pop()
        {
            --this.Count;
            T retval = this.m_Storage[this.Count];
            this.m_Storage[this.Count] = default;
            return retval;
        }

        public void Clear()
        {
            Array.Clear(this.m_Storage, 0, this.m_Storage.Length);
            this.Count = 0;
        }

        public int Count { get; private set; } = 0;


        #region IList<T> Impl.

        int IList<T>.IndexOf(T item) => throw new NotImplementedException();

        void IList<T>.Insert(int index, T item) => throw new NotImplementedException();

        void IList<T>.RemoveAt(int index) => throw new NotImplementedException();

        T IList<T>.this[int index]
        {
            get => this[index];
            set => this[index] = value;
        }

        void ICollection<T>.Add(T item) => Push(item);

        void ICollection<T>.Clear() => Clear();

        bool ICollection<T>.Contains(T item) => throw new NotImplementedException();

        void ICollection<T>.CopyTo(T[] array, int arrayIndex) => throw new NotImplementedException();

        int ICollection<T>.Count => this.Count;

        bool ICollection<T>.IsReadOnly => false;

        bool ICollection<T>.Remove(T item) => throw new NotImplementedException();

        IEnumerator<T> IEnumerable<T>.GetEnumerator() => throw new NotImplementedException();

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator() => throw new NotImplementedException();

        #endregion

    }
}

#endif