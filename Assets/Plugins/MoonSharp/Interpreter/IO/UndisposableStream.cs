﻿using System;
using System.IO;

namespace MoonSharp.Interpreter.IO
{
    /// <summary>
    /// An adapter over Stream which bypasses the Dispose and Close methods.
    /// Used to work around the pesky wrappers .NET has over Stream (BinaryReader, StreamWriter, etc.) which think they
    /// own the Stream and close them when they shouldn't. Damn.
    /// </summary>
    public class UndisposableStream : Stream
    {
        private Stream m_Stream;

        public UndisposableStream(Stream stream) => this.m_Stream = stream;

        protected override void Dispose(bool disposing)
        {
        }

#if !(PCL || ENABLE_DOTNET || NETFX_CORE)
        public override void Close()
        {
        }
#endif


        public override bool CanRead => this.m_Stream.CanRead;

        public override bool CanSeek => this.m_Stream.CanSeek;

        public override bool CanWrite => this.m_Stream.CanWrite;

        public override void Flush() => this.m_Stream.Flush();

        public override long Length => this.m_Stream.Length;

        public override long Position
        {
            get => this.m_Stream.Position;
            set => this.m_Stream.Position = value;
        }

        public override int Read(byte[] buffer, int offset, int count) => this.m_Stream.Read(buffer, offset, count);

        public override long Seek(long offset, SeekOrigin origin) => this.m_Stream.Seek(offset, origin);

        public override void SetLength(long value) => this.m_Stream.SetLength(value);

        public override void Write(byte[] buffer, int offset, int count) => this.m_Stream.Write(buffer, offset, count);

#if (!(NETFX_CORE))
        public override IAsyncResult BeginRead(byte[] buffer, int offset, int count, AsyncCallback callback, object state) => this.m_Stream.BeginRead(buffer, offset, count, callback, state);

        public override IAsyncResult BeginWrite(byte[] buffer, int offset, int count, AsyncCallback callback, object state) => this.m_Stream.BeginWrite(buffer, offset, count, callback, state);

        public override void EndWrite(IAsyncResult asyncResult) => this.m_Stream.EndWrite(asyncResult);

        public override int EndRead(IAsyncResult asyncResult) => this.m_Stream.EndRead(asyncResult);
#endif
        public override bool CanTimeout => this.m_Stream.CanTimeout;


        public override bool Equals(object obj) => this.m_Stream.Equals(obj);

        public override int GetHashCode() => this.m_Stream.GetHashCode();


        public override int ReadByte() => this.m_Stream.ReadByte();

        public override int ReadTimeout
        {
            get => this.m_Stream.ReadTimeout;
            set => this.m_Stream.ReadTimeout = value;
        }

        public override string ToString() => this.m_Stream.ToString();

        public override void WriteByte(byte value) => this.m_Stream.WriteByte(value);

        public override int WriteTimeout
        {
            get => this.m_Stream.WriteTimeout;
            set => this.m_Stream.WriteTimeout = value;
        }


    }
}
