﻿using System;
using System.Diagnostics;

namespace MoonSharp.Interpreter.Diagnostics.PerformanceCounters
{
    /// <summary>
    /// This class is not *really* IDisposable.. it's just use to have a RAII like pattern.
    /// You are free to reuse this instance after calling Dispose.
    /// </summary>
    internal class GlobalPerformanceStopwatch : IPerformanceStopwatch
    {
        private class GlobalPerformanceStopwatch_StopwatchObject : IDisposable
        {
            private Stopwatch m_Stopwatch;
            private GlobalPerformanceStopwatch m_Parent;

            public GlobalPerformanceStopwatch_StopwatchObject(GlobalPerformanceStopwatch parent)
            {
                this.m_Parent = parent;
                this.m_Stopwatch = Stopwatch.StartNew();
            }

            public void Dispose()
            {
                this.m_Stopwatch.Stop();
                this.m_Parent.SignalStopwatchTerminated(this.m_Stopwatch);
            }
        }

        private int m_Count = 0;
        private long m_Elapsed = 0;
        private PerformanceCounter m_Counter;

        public GlobalPerformanceStopwatch(PerformanceCounter perfcounter) => this.m_Counter = perfcounter;

        private void SignalStopwatchTerminated(Stopwatch sw)
        {
            this.m_Elapsed += sw.ElapsedMilliseconds;
            this.m_Count += 1;
        }

        public IDisposable Start() => new GlobalPerformanceStopwatch_StopwatchObject(this);

        public PerformanceResult GetResult()
        {
            return new PerformanceResult()
            {
                Type = PerformanceCounterType.TimeMilliseconds,
                Global = false,
                Name = this.m_Counter.ToString(),
                Instances = m_Count,
                Counter = m_Elapsed
            };
        }
    }
}
