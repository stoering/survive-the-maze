﻿using System;

namespace MoonSharp.Interpreter.Diagnostics.PerformanceCounters
{
    internal class DummyPerformanceStopwatch : IPerformanceStopwatch, IDisposable
    {
        public static DummyPerformanceStopwatch Instance = new DummyPerformanceStopwatch();
        private PerformanceResult m_Result;

        private DummyPerformanceStopwatch()
        {
            this.m_Result = new PerformanceResult()
            {
                Counter = 0,
                Global = true,
                Instances = 0,
                Name = "::dummy::",
                Type = PerformanceCounterType.TimeMilliseconds
            };
        }


        public IDisposable Start() => this;

        public PerformanceResult GetResult() => this.m_Result;

        public void Dispose()
        {
        }
    }
}
