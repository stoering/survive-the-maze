﻿using System;
using System.Diagnostics;

namespace MoonSharp.Interpreter.Diagnostics.PerformanceCounters
{
    /// <summary>
    /// This class is not *really* IDisposable.. it's just use to have a RAII like pattern.
    /// You are free to reuse this instance after calling Dispose.
    /// </summary>
    internal class PerformanceStopwatch : IDisposable, IPerformanceStopwatch
    {
        private Stopwatch m_Stopwatch = new Stopwatch();
        private int m_Count = 0;
        private int m_Reentrant = 0;
        private PerformanceCounter m_Counter;

        public PerformanceStopwatch(PerformanceCounter perfcounter) => this.m_Counter = perfcounter;


        public IDisposable Start()
        {
            if (this.m_Reentrant == 0)
            {
                this.m_Count += 1;
                this.m_Stopwatch.Start();
            }

            this.m_Reentrant += 1;

            return this;
        }

        public void Dispose()
        {
            this.m_Reentrant -= 1;

            if (this.m_Reentrant == 0)
            {
                this.m_Stopwatch.Stop();
            }
        }

        public PerformanceResult GetResult()
        {
            return new PerformanceResult()
            {
                Type = PerformanceCounterType.TimeMilliseconds,
                Global = false,
                Name = this.m_Counter.ToString(),
                Instances = m_Count,
                Counter = this.m_Stopwatch.ElapsedMilliseconds
            };
        }
    }
}
