﻿using System;

namespace MoonSharp.Interpreter.REPL
{
    /// <summary>
    /// This class provides a simple REPL intepreter ready to be reused in a simple way.
    /// </summary>
    public class ReplInterpreter
    {
        private Script m_Script;
        private string m_CurrentCommand = string.Empty;

        /// <summary>
        /// Initializes a new instance of the <see cref="ReplInterpreter"/> class.
        /// </summary>
        /// <param name="script">The script.</param>
        public ReplInterpreter(Script script) => this.m_Script = script;

        /// <summary>
        /// Gets or sets a value indicating whether this instances handle inputs starting with a "?" as a 
        /// dynamic expression to evaluate instead of script code (likely invalid)
        /// </summary>
        public bool HandleDynamicExprs { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instances handle inputs starting with a "=" as a 
        /// non-dynamic expression to evaluate (just like the Lua interpreter does by default).
        /// </summary>
        public bool HandleClassicExprsSyntax { get; set; }

        /// <summary>
        /// Gets a value indicating whether this instance has a pending command 
        /// </summary>
        public virtual bool HasPendingCommand => this.m_CurrentCommand.Length > 0;

        /// <summary>
        /// Gets the current pending command.
        /// </summary>
        public virtual string CurrentPendingCommand => this.m_CurrentCommand;

        /// <summary>
        /// Gets the classic prompt (">" or ">>") given the current state of the interpreter
        /// </summary>
        public virtual string ClassicPrompt => this.HasPendingCommand ? ">>" : ">";

        /// <summary>
        /// Evaluate a REPL command.
        /// This method returns the result of the computation, or null if more input is needed for having valid code.
        /// In case of errors, exceptions are propagated to the caller.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns>This method returns the result of the computation, or null if more input is needed for a computation.</returns>
        public virtual DynValue Evaluate(string input)
        {
            bool isFirstLine = !this.HasPendingCommand;

            bool forced = (input == "");

            this.m_CurrentCommand += input;

            if (this.m_CurrentCommand.Length == 0)
                return DynValue.Void;

            this.m_CurrentCommand += "\n";

            try
            {
                DynValue result = null;

                if (isFirstLine && this.HandleClassicExprsSyntax && this.m_CurrentCommand.StartsWith("="))
                {
                    this.m_CurrentCommand = "return " + this.m_CurrentCommand.Substring(1);
                }

                if (isFirstLine && this.HandleDynamicExprs && this.m_CurrentCommand.StartsWith("?"))
                {
                    string code = this.m_CurrentCommand.Substring(1);
                    DynamicExpression exp = this.m_Script.CreateDynamicExpression(code);
                    result = exp.Evaluate();
                }
                else
                {
                    DynValue v = this.m_Script.LoadString(this.m_CurrentCommand, null, "stdin");
                    result = this.m_Script.Call(v);
                }

                this.m_CurrentCommand = "";
                return result;
            }
            catch (SyntaxErrorException ex)
            {
                if (forced || !ex.IsPrematureStreamTermination)
                {
                    this.m_CurrentCommand = "";
                    ex.Rethrow();
                    throw;
                }
                else
                {
                    return null;
                }
            }
            catch (ScriptRuntimeException sre)
            {
                this.m_CurrentCommand = "";
                sre.Rethrow();
                throw;
            }
            catch (Exception)
            {
                this.m_CurrentCommand = "";
                throw;
            }
        }
    }
}
