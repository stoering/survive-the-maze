﻿
namespace MoonSharp.Interpreter.REPL
{
    /// <summary>
    /// An implementation of <see cref="ReplInterpreter"/> which supports a very basic history of recent input lines.
    /// </summary>
    public class ReplHistoryInterpreter : ReplInterpreter
    {
        private string[] m_History;
        private int m_Last = -1;
        private int m_Navi = -1;

        /// <summary>
        /// Initializes a new instance of the <see cref="ReplHistoryInterpreter"/> class.
        /// </summary>
        /// <param name="script">The script.</param>
        /// <param name="historySize">Size of the history.</param>
        public ReplHistoryInterpreter(Script script, int historySize)
            : base(script) => this.m_History = new string[historySize];


        /// <summary>
        /// Evaluate a REPL command.
        /// This method returns the result of the computation, or null if more input is needed for having valid code.
        /// In case of errors, exceptions are propagated to the caller.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns>
        /// This method returns the result of the computation, or null if more input is needed for a computation.
        /// </returns>
        public override DynValue Evaluate(string input)
        {
            this.m_Navi = -1;
            this.m_Last = (this.m_Last + 1) % this.m_History.Length;
            this.m_History[this.m_Last] = input;
            return base.Evaluate(input);
        }

        /// <summary>
        /// Gets the previous item in history, or null
        /// </summary>
        public string HistoryPrev()
        {
            if (this.m_Navi == -1)
                this.m_Navi = this.m_Last;
            else
                this.m_Navi = ((this.m_Navi - 1) + this.m_History.Length) % this.m_History.Length;

            if (this.m_Navi >= 0) return this.m_History[this.m_Navi];
            return null;
        }

        /// <summary>
        /// Gets the next item in history, or null
        /// </summary>
        public string HistoryNext()
        {
            if (this.m_Navi == -1)
                return null;
            else
                this.m_Navi = (this.m_Navi + 1) % this.m_History.Length;

            if (this.m_Navi >= 0) return this.m_History[this.m_Navi];
            return null;
        }



    }
}
