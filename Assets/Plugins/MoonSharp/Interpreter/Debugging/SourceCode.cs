﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MoonSharp.Interpreter.Debugging
{
    /// <summary>
    /// Class representing the source code of a given script
    /// </summary>
    public class SourceCode : IScriptPrivateResource
    {
        /// <summary>
        /// Gets the name of the source code
        /// </summary>
        public string Name { get; private set; }
        /// <summary>
        /// Gets the source code as a string
        /// </summary>
        public string Code { get; private set; }
        /// <summary>
        /// Gets the source code lines.
        /// </summary>
        public string[] Lines { get; private set; }
        /// <summary>
        /// Gets the script owning this resource.
        /// </summary>
        public Script OwnerScript { get; private set; }
        /// <summary>
        /// Gets the source identifier inside a script
        /// </summary>
        public int SourceID { get; private set; }

        internal List<SourceRef> Refs { get; private set; }

        internal SourceCode(string name, string code, int sourceID, Script ownerScript)
        {
            this.Refs = new List<SourceRef>();

            List<string> lines = new List<string>();

            this.Name = name;
            this.Code = code;

            lines.Add(string.Format("-- Begin of chunk : {0} ", name));

            lines.AddRange(this.Code.Split('\n'));

            this.Lines = lines.ToArray();

            this.OwnerScript = ownerScript;
            this.SourceID = sourceID;
        }

        /// <summary>
        /// Gets the code snippet represented by a source ref
        /// </summary>
        /// <param name="sourceCodeRef">The source code reference.</param>
        /// <returns></returns>
        public string GetCodeSnippet(SourceRef sourceCodeRef)
        {
            if (sourceCodeRef.FromLine == sourceCodeRef.ToLine)
            {
                int from = AdjustStrIndex(this.Lines[sourceCodeRef.FromLine], sourceCodeRef.FromChar);
                int to = AdjustStrIndex(this.Lines[sourceCodeRef.FromLine], sourceCodeRef.ToChar);
                return this.Lines[sourceCodeRef.FromLine].Substring(from, to - from);
            }

            StringBuilder sb = new StringBuilder();

            for (int i = sourceCodeRef.FromLine; i <= sourceCodeRef.ToLine; i++)
            {
                if (i == sourceCodeRef.FromLine)
                {
                    int from = AdjustStrIndex(this.Lines[i], sourceCodeRef.FromChar);
                    _ = sb.Append(this.Lines[i].Substring(from));
                }
                else if (i == sourceCodeRef.ToLine)
                {
                    int to = AdjustStrIndex(this.Lines[i], sourceCodeRef.ToChar);
                    _ = sb.Append(this.Lines[i].Substring(0, to + 1));
                }
                else
                {
                    _ = sb.Append(this.Lines[i]);
                }
            }

            return sb.ToString();
        }

        private int AdjustStrIndex(string str, int loc) => Math.Max(Math.Min(str.Length, loc), 0);
    }
}
