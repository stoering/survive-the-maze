﻿#define EMIT_DEBUG_OPS

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using MoonSharp.Interpreter.Debugging;

namespace MoonSharp.Interpreter.Execution.VM
{
    internal class ByteCode : RefIdObject
    {
        public List<Instruction> Code = new List<Instruction>();
        public Script Script { get; private set; }
        private List<SourceRef> m_SourceRefStack = new List<SourceRef>();
        private SourceRef m_CurrentSourceRef = null;

        internal LoopTracker LoopTracker = new LoopTracker();

        public ByteCode(Script script) => this.Script = script;


        public IDisposable EnterSource(SourceRef sref) => new SourceCodeStackGuard(sref, this);


        private class SourceCodeStackGuard : IDisposable
        {
            private ByteCode m_Bc;

            public SourceCodeStackGuard(SourceRef sref, ByteCode bc)
            {
                this.m_Bc = bc;
                this.m_Bc.PushSourceRef(sref);
            }

            public void Dispose() => this.m_Bc.PopSourceRef();
        }


        public void PushSourceRef(SourceRef sref)
        {
            this.m_SourceRefStack.Add(sref);
            this.m_CurrentSourceRef = sref;
        }

        public void PopSourceRef()
        {
            this.m_SourceRefStack.RemoveAt(this.m_SourceRefStack.Count - 1);
            this.m_CurrentSourceRef = (this.m_SourceRefStack.Count > 0) ? this.m_SourceRefStack[this.m_SourceRefStack.Count - 1] : null;
        }

#if (!PCL) && ((!UNITY_5) || UNITY_STANDALONE) && (!(NETFX_CORE))
        public void Dump(string file)
        {
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < this.Code.Count; i++)
            {
                if (this.Code[i].OpCode == OpCode.Debug)
                    _ = sb.AppendFormat("    {0}\n", this.Code[i]);
                else
                    _ = sb.AppendFormat("{0:X8}  {1}\n", i, this.Code[i]);
            }

            File.WriteAllText(file, sb.ToString());
        }
#endif

        public int GetJumpPointForNextInstruction() => this.Code.Count;
        public int GetJumpPointForLastInstruction() => this.Code.Count - 1;

        public Instruction GetLastInstruction() => this.Code[this.Code.Count - 1];

        private Instruction AppendInstruction(Instruction c)
        {
            this.Code.Add(c);
            return c;
        }

        public Instruction Emit_Nop(string comment) => AppendInstruction(new Instruction(this.m_CurrentSourceRef) { OpCode = OpCode.Nop, Name = comment });

        public Instruction Emit_Invalid(string type) => AppendInstruction(new Instruction(this.m_CurrentSourceRef) { OpCode = OpCode.Invalid, Name = type });

        public Instruction Emit_Pop(int num = 1) => AppendInstruction(new Instruction(this.m_CurrentSourceRef) { OpCode = OpCode.Pop, NumVal = num });

        public void Emit_Call(int argCount, string debugName) => AppendInstruction(new Instruction(this.m_CurrentSourceRef) { OpCode = OpCode.Call, NumVal = argCount, Name = debugName });

        public void Emit_ThisCall(int argCount, string debugName) => AppendInstruction(new Instruction(this.m_CurrentSourceRef) { OpCode = OpCode.ThisCall, NumVal = argCount, Name = debugName });

        public Instruction Emit_Literal(DynValue value) => AppendInstruction(new Instruction(this.m_CurrentSourceRef) { OpCode = OpCode.Literal, Value = value });

        public Instruction Emit_Jump(OpCode jumpOpCode, int idx, int optPar = 0) => AppendInstruction(new Instruction(this.m_CurrentSourceRef) { OpCode = jumpOpCode, NumVal = idx, NumVal2 = optPar });

        public Instruction Emit_MkTuple(int cnt) => AppendInstruction(new Instruction(this.m_CurrentSourceRef) { OpCode = OpCode.MkTuple, NumVal = cnt });

        public Instruction Emit_Operator(OpCode opcode)
        {
            Instruction i = AppendInstruction(new Instruction(this.m_CurrentSourceRef) { OpCode = opcode });

            if (opcode == OpCode.LessEq)
                _ = AppendInstruction(new Instruction(this.m_CurrentSourceRef) { OpCode = OpCode.CNot });

            if (opcode == OpCode.Eq || opcode == OpCode.Less)
                _ = AppendInstruction(new Instruction(this.m_CurrentSourceRef) { OpCode = OpCode.ToBool });

            return i;
        }


        [Conditional("EMIT_DEBUG_OPS")]
        public void Emit_Debug(string str) => AppendInstruction(new Instruction(this.m_CurrentSourceRef) { OpCode = OpCode.Debug, Name = str.Substring(0, Math.Min(32, str.Length)) });

        public Instruction Emit_Enter(RuntimeScopeBlock runtimeScopeBlock) => AppendInstruction(new Instruction(this.m_CurrentSourceRef) { OpCode = OpCode.Clean, NumVal = runtimeScopeBlock.From, NumVal2 = runtimeScopeBlock.ToInclusive });

        public Instruction Emit_Leave(RuntimeScopeBlock runtimeScopeBlock) => AppendInstruction(new Instruction(this.m_CurrentSourceRef) { OpCode = OpCode.Clean, NumVal = runtimeScopeBlock.From, NumVal2 = runtimeScopeBlock.To });

        public Instruction Emit_Exit(RuntimeScopeBlock runtimeScopeBlock) => AppendInstruction(new Instruction(this.m_CurrentSourceRef) { OpCode = OpCode.Clean, NumVal = runtimeScopeBlock.From, NumVal2 = runtimeScopeBlock.ToInclusive });

        public Instruction Emit_Clean(RuntimeScopeBlock runtimeScopeBlock) => AppendInstruction(new Instruction(this.m_CurrentSourceRef) { OpCode = OpCode.Clean, NumVal = runtimeScopeBlock.To + 1, NumVal2 = runtimeScopeBlock.ToInclusive });

        public Instruction Emit_Closure(SymbolRef[] symbols, int jmpnum) => AppendInstruction(new Instruction(this.m_CurrentSourceRef) { OpCode = OpCode.Closure, SymbolList = symbols, NumVal = jmpnum });

        public Instruction Emit_Args(params SymbolRef[] symbols) => AppendInstruction(new Instruction(this.m_CurrentSourceRef) { OpCode = OpCode.Args, SymbolList = symbols });

        public Instruction Emit_Ret(int retvals) => AppendInstruction(new Instruction(this.m_CurrentSourceRef) { OpCode = OpCode.Ret, NumVal = retvals });

        public Instruction Emit_ToNum(int stage = 0) => AppendInstruction(new Instruction(this.m_CurrentSourceRef) { OpCode = OpCode.ToNum, NumVal = stage });

        public Instruction Emit_Incr(int i) => AppendInstruction(new Instruction(this.m_CurrentSourceRef) { OpCode = OpCode.Incr, NumVal = i });

        public Instruction Emit_NewTable(bool shared) => AppendInstruction(new Instruction(this.m_CurrentSourceRef) { OpCode = OpCode.NewTable, NumVal = shared ? 1 : 0 });

        public Instruction Emit_IterPrep() => AppendInstruction(new Instruction(this.m_CurrentSourceRef) { OpCode = OpCode.IterPrep });

        public Instruction Emit_ExpTuple(int stackOffset) => AppendInstruction(new Instruction(this.m_CurrentSourceRef) { OpCode = OpCode.ExpTuple, NumVal = stackOffset });

        public Instruction Emit_IterUpd() => AppendInstruction(new Instruction(this.m_CurrentSourceRef) { OpCode = OpCode.IterUpd });

        public Instruction Emit_Meta(string funcName, OpCodeMetadataType metaType, DynValue value = null)
        {
            return AppendInstruction(new Instruction(this.m_CurrentSourceRef)
            {
                OpCode = OpCode.Meta,
                Name = funcName,
                NumVal2 = (int)metaType,
                Value = value
            });
        }


        public Instruction Emit_BeginFn(RuntimeScopeFrame stackFrame)
        {
            return AppendInstruction(new Instruction(this.m_CurrentSourceRef)
            {
                OpCode = OpCode.BeginFn,
                SymbolList = stackFrame.DebugSymbols.ToArray(),
                NumVal = stackFrame.Count,
                NumVal2 = stackFrame.ToFirstBlock,
            });
        }

        public Instruction Emit_Scalar() => AppendInstruction(new Instruction(this.m_CurrentSourceRef) { OpCode = OpCode.Scalar });

        public int Emit_Load(SymbolRef sym)
        {
            switch (sym.Type)
            {
                case SymbolRefType.Global:
                    _ = Emit_Load(sym.i_Env);
                    _ = AppendInstruction(new Instruction(this.m_CurrentSourceRef) { OpCode = OpCode.Index, Value = DynValue.NewString(sym.i_Name) });
                    return 2;
                case SymbolRefType.Local:
                    _ = AppendInstruction(new Instruction(this.m_CurrentSourceRef) { OpCode = OpCode.Local, Symbol = sym });
                    return 1;
                case SymbolRefType.Upvalue:
                    _ = AppendInstruction(new Instruction(this.m_CurrentSourceRef) { OpCode = OpCode.Upvalue, Symbol = sym });
                    return 1;
                default:
                    throw new InternalErrorException("Unexpected symbol type : {0}", sym);
            }
        }

        public int Emit_Store(SymbolRef sym, int stackofs, int tupleidx)
        {
            switch (sym.Type)
            {
                case SymbolRefType.Global:
                    _ = Emit_Load(sym.i_Env);
                    _ = AppendInstruction(new Instruction(this.m_CurrentSourceRef) { OpCode = OpCode.IndexSet, Symbol = sym, NumVal = stackofs, NumVal2 = tupleidx, Value = DynValue.NewString(sym.i_Name) });
                    return 2;
                case SymbolRefType.Local:
                    _ = AppendInstruction(new Instruction(this.m_CurrentSourceRef) { OpCode = OpCode.StoreLcl, Symbol = sym, NumVal = stackofs, NumVal2 = tupleidx });
                    return 1;
                case SymbolRefType.Upvalue:
                    _ = AppendInstruction(new Instruction(this.m_CurrentSourceRef) { OpCode = OpCode.StoreUpv, Symbol = sym, NumVal = stackofs, NumVal2 = tupleidx });
                    return 1;
                default:
                    throw new InternalErrorException("Unexpected symbol type : {0}", sym);
            }
        }

        public Instruction Emit_TblInitN() => AppendInstruction(new Instruction(this.m_CurrentSourceRef) { OpCode = OpCode.TblInitN });

        public Instruction Emit_TblInitI(bool lastpos) => AppendInstruction(new Instruction(this.m_CurrentSourceRef) { OpCode = OpCode.TblInitI, NumVal = lastpos ? 1 : 0 });

        public Instruction Emit_Index(DynValue index = null, bool isNameIndex = false, bool isExpList = false)
        {
            OpCode o;
            if (isNameIndex) o = OpCode.IndexN;
            else if (isExpList) o = OpCode.IndexL;
            else o = OpCode.Index;

            return AppendInstruction(new Instruction(this.m_CurrentSourceRef) { OpCode = o, Value = index });
        }

        public Instruction Emit_IndexSet(int stackofs, int tupleidx, DynValue index = null, bool isNameIndex = false, bool isExpList = false)
        {
            OpCode o;
            if (isNameIndex) o = OpCode.IndexSetN;
            else if (isExpList) o = OpCode.IndexSetL;
            else o = OpCode.IndexSet;

            return AppendInstruction(new Instruction(this.m_CurrentSourceRef) { OpCode = o, NumVal = stackofs, NumVal2 = tupleidx, Value = index });
        }

        public Instruction Emit_Copy(int numval) => AppendInstruction(new Instruction(this.m_CurrentSourceRef) { OpCode = OpCode.Copy, NumVal = numval });

        public Instruction Emit_Swap(int p1, int p2) => AppendInstruction(new Instruction(this.m_CurrentSourceRef) { OpCode = OpCode.Swap, NumVal = p1, NumVal2 = p2 });

    }
}
