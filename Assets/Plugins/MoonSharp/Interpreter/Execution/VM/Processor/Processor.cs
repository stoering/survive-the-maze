﻿using System;
using System.Collections.Generic;
using System.Threading;
using MoonSharp.Interpreter.DataStructs;
using MoonSharp.Interpreter.Debugging;

namespace MoonSharp.Interpreter.Execution.VM
{
    internal sealed partial class Processor
    {
        private ByteCode m_RootChunk;
        private FastStack<DynValue> m_ValueStack = new FastStack<DynValue>(131072);
        private FastStack<CallStackItem> m_ExecutionStack = new FastStack<CallStackItem>(131072);
        private List<Processor> m_CoroutinesStack;
        private Table m_GlobalTable;
        private Script m_Script;
        private Processor m_Parent = null;
        private bool m_CanYield = true;
        private int m_SavedInstructionPtr = -1;
        private DebugContext m_Debug;

        public Processor(Script script, Table globalContext, ByteCode byteCode)
        {
            this.m_CoroutinesStack = new List<Processor>();

            this.m_Debug = new DebugContext();
            this.m_RootChunk = byteCode;
            this.m_GlobalTable = globalContext;
            this.m_Script = script;
            this.State = CoroutineState.Main;
            _ = DynValue.NewCoroutine(new Coroutine(this)); // creates an associated coroutine for the main processor
        }

        private Processor(Processor parentProcessor)
        {
            this.m_Debug = parentProcessor.m_Debug;
            this.m_RootChunk = parentProcessor.m_RootChunk;
            this.m_GlobalTable = parentProcessor.m_GlobalTable;
            this.m_Script = parentProcessor.m_Script;
            this.m_Parent = parentProcessor;
            this.State = CoroutineState.NotStarted;
        }



        public DynValue Call(DynValue function, DynValue[] args)
        {
            List<Processor> coroutinesStack = this.m_Parent != null ? this.m_Parent.m_CoroutinesStack : this.m_CoroutinesStack;

            if (coroutinesStack.Count > 0 && coroutinesStack[coroutinesStack.Count - 1] != this)
                return coroutinesStack[coroutinesStack.Count - 1].Call(function, args);

            EnterProcessor();

            try
            {
                IDisposable stopwatch = this.m_Script.PerformanceStats.StartStopwatch(Diagnostics.PerformanceCounter.Execution);

                this.m_CanYield = false;

                try
                {
                    int entrypoint = PushClrToScriptStackFrame(CallStackItemFlags.CallEntryPoint, function, args);
                    return Processing_Loop(entrypoint);
                }
                finally
                {
                    this.m_CanYield = true;

                    if (stopwatch != null)
                        stopwatch.Dispose();
                }
            }
            finally
            {
                LeaveProcessor();
            }
        }

        // pushes all what's required to perform a clr-to-script function call. function can be null if it's already
        // at vstack top.
        private int PushClrToScriptStackFrame(CallStackItemFlags flags, DynValue function, DynValue[] args)
        {
            if (function == null)
                function = this.m_ValueStack.Peek();
            else
                _ = this.m_ValueStack.Push(function);  // func val

            args = Internal_AdjustTuple(args);

            for (int i = 0; i < args.Length; i++)
                _ = this.m_ValueStack.Push(args[i]);

            _ = this.m_ValueStack.Push(DynValue.NewNumber(args.Length));  // func args count

            _ = this.m_ExecutionStack.Push(new CallStackItem()
            {
                BasePointer = this.m_ValueStack.Count,
                Debug_EntryPoint = function.Function.EntryPointByteCodeLocation,
                ReturnAddress = -1,
                ClosureScope = function.Function.ClosureContext,
                CallingSourceRef = SourceRef.GetClrLocation(),
                Flags = flags
            });

            return function.Function.EntryPointByteCodeLocation;
        }

        private int m_OwningThreadID = -1;
        private int m_ExecutionNesting = 0;

        private void LeaveProcessor()
        {
            this.m_ExecutionNesting -= 1;
            this.m_OwningThreadID = -1;

            if (this.m_Parent != null)
            {
                this.m_Parent.m_CoroutinesStack.RemoveAt(this.m_Parent.m_CoroutinesStack.Count - 1);
            }

            if (this.m_ExecutionNesting == 0 && this.m_Debug != null && this.m_Debug.DebuggerEnabled
                && this.m_Debug.DebuggerAttached != null)
            {
                this.m_Debug.DebuggerAttached.SignalExecutionEnded();
            }
        }

        private int GetThreadId()
        {
#if ENABLE_DOTNET || NETFX_CORE
				return 1;
#else
            return Thread.CurrentThread.ManagedThreadId;
#endif
        }

        private void EnterProcessor()
        {
            int threadID = GetThreadId();

            if (this.m_OwningThreadID >= 0 && this.m_OwningThreadID != threadID && this.m_Script.Options.CheckThreadAccess)
            {
                string msg = string.Format("Cannot enter the same MoonSharp processor from two different threads : {0} and {1}", this.m_OwningThreadID, threadID);
                throw new InvalidOperationException(msg);
            }

            this.m_OwningThreadID = threadID;

            this.m_ExecutionNesting += 1;

            if (this.m_Parent != null)
            {
                this.m_Parent.m_CoroutinesStack.Add(this);
            }
        }

        internal SourceRef GetCoroutineSuspendedLocation() => GetCurrentSourceRef(this.m_SavedInstructionPtr);
    }
}
