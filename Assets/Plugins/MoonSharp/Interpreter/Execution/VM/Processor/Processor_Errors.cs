﻿using MoonSharp.Interpreter.Debugging;

namespace MoonSharp.Interpreter.Execution.VM
{
    internal sealed partial class Processor
    {
        private SourceRef GetCurrentSourceRef(int instructionPtr)
        {
            if (instructionPtr >= 0 && instructionPtr < this.m_RootChunk.Code.Count)
            {
                return this.m_RootChunk.Code[instructionPtr].SourceCodeRef;
            }
            return null;
        }


        private void FillDebugData(InterpreterException ex, int ip)
        {
            // adjust IP
            if (ip == YIELD_SPECIAL_TRAP)
                ip = this.m_SavedInstructionPtr;
            else
                ip -= 1;

            ex.InstructionPtr = ip;

            SourceRef sref = GetCurrentSourceRef(ip);

            ex.DecorateMessage(this.m_Script, sref, ip);

            ex.CallStack = Debugger_GetCallStack(sref);
        }


    }
}
