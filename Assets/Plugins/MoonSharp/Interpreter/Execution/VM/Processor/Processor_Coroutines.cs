﻿using System;

namespace MoonSharp.Interpreter.Execution.VM
{
    // This part is practically written procedural style - it looks more like C than C#.
    // This is intentional so to avoid this-calls and virtual-calls as much as possible.
    // Same reason for the "sealed" declaration.
    internal sealed partial class Processor
    {
        public DynValue Coroutine_Create(Closure closure)
        {
            // create a processor instance
            Processor P = new Processor(this);

            // Put the closure as first value on the stack, for future reference
            _ = P.m_ValueStack.Push(DynValue.NewClosure(closure));

            // Return the coroutine handle
            return DynValue.NewCoroutine(new Coroutine(P));
        }

        public CoroutineState State { get; private set; }
        public Coroutine AssociatedCoroutine { get; set; }

        public DynValue Coroutine_Resume(DynValue[] args)
        {
            EnterProcessor();

            try
            {
                int entrypoint = 0;

                if (this.State != CoroutineState.NotStarted && this.State != CoroutineState.Suspended && this.State != CoroutineState.ForceSuspended)
                    throw ScriptRuntimeException.CannotResumeNotSuspended(this.State);

                if (this.State == CoroutineState.NotStarted)
                {
                    entrypoint = PushClrToScriptStackFrame(CallStackItemFlags.ResumeEntryPoint, null, args);
                }
                else if (this.State == CoroutineState.Suspended)
                {
                    _ = this.m_ValueStack.Push(DynValue.NewTuple(args));
                    entrypoint = this.m_SavedInstructionPtr;
                }
                else if (this.State == CoroutineState.ForceSuspended)
                {
                    if (args != null && args.Length > 0)
                        throw new ArgumentException("When resuming a force-suspended coroutine, args must be empty.");

                    entrypoint = this.m_SavedInstructionPtr;
                }

                this.State = CoroutineState.Running;
                DynValue retVal = Processing_Loop(entrypoint);

                if (retVal.Type == DataType.YieldRequest)
                {
                    if (retVal.YieldRequest.Forced)
                    {
                        this.State = CoroutineState.ForceSuspended;
                        return retVal;
                    }
                    else
                    {
                        this.State = CoroutineState.Suspended;
                        return DynValue.NewTuple(retVal.YieldRequest.ReturnValues);
                    }
                }
                else
                {
                    this.State = CoroutineState.Dead;
                    return retVal;
                }
            }
            catch (Exception)
            {
                // Unhandled exception - move to dead
                this.State = CoroutineState.Dead;
                throw;
            }
            finally
            {
                LeaveProcessor();
            }
        }



    }

}
