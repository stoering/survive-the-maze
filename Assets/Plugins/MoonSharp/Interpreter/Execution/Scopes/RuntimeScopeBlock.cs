﻿using System;

namespace MoonSharp.Interpreter.Execution
{
    internal class RuntimeScopeBlock
    {
        public int From { get; internal set; }
        public int To { get; internal set; }
        public int ToInclusive { get; internal set; }

        public override string ToString() => String.Format("ScopeBlock : {0} -> {1} --> {2}", this.From, this.To, this.ToInclusive);
    }
}
