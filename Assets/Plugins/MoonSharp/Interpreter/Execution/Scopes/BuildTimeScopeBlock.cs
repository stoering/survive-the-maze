﻿using System;
using System.Collections.Generic;
using MoonSharp.Interpreter.Tree.Statements;

namespace MoonSharp.Interpreter.Execution.Scopes
{
    internal class BuildTimeScopeBlock
    {
        internal BuildTimeScopeBlock Parent { get; private set; }
        internal List<BuildTimeScopeBlock> ChildNodes { get; private set; }

        internal RuntimeScopeBlock ScopeBlock { get; private set; }

        private Dictionary<string, SymbolRef> m_DefinedNames = new Dictionary<string, SymbolRef>();



        internal void Rename(string name)
        {
            SymbolRef sref = this.m_DefinedNames[name];
            _ = this.m_DefinedNames.Remove(name);
            this.m_DefinedNames.Add(string.Format("@{0}_{1}", name, Guid.NewGuid().ToString("N")), sref);
        }

        internal BuildTimeScopeBlock(BuildTimeScopeBlock parent)
        {
            this.Parent = parent;
            this.ChildNodes = new List<BuildTimeScopeBlock>();
            this.ScopeBlock = new RuntimeScopeBlock();
        }


        internal BuildTimeScopeBlock AddChild()
        {
            BuildTimeScopeBlock block = new BuildTimeScopeBlock(this);
            this.ChildNodes.Add(block);
            return block;
        }

        internal SymbolRef Find(string name) => this.m_DefinedNames.GetOrDefault(name);

        internal SymbolRef Define(string name)
        {
            SymbolRef l = SymbolRef.Local(name, -1);
            this.m_DefinedNames.Add(name, l);
            this.m_LastDefinedName = name;
            return l;
        }

        internal int ResolveLRefs(BuildTimeScopeFrame buildTimeScopeFrame)
        {
            int firstVal = -1;
            int lastVal = -1;

            foreach (SymbolRef lref in this.m_DefinedNames.Values)
            {
                int pos = buildTimeScopeFrame.AllocVar(lref);

                if (firstVal < 0)
                    firstVal = pos;

                lastVal = pos;
            }

            this.ScopeBlock.From = firstVal;
            this.ScopeBlock.ToInclusive = this.ScopeBlock.To = lastVal;

            if (firstVal < 0)
                this.ScopeBlock.From = buildTimeScopeFrame.GetPosForNextVar();

            foreach (BuildTimeScopeBlock child in this.ChildNodes)
            {
                this.ScopeBlock.ToInclusive = Math.Max(this.ScopeBlock.ToInclusive, child.ResolveLRefs(buildTimeScopeFrame));
            }

            if (this.m_LocalLabels != null)
                foreach (LabelStatement label in this.m_LocalLabels.Values)
                    label.SetScope(this.ScopeBlock);

            return this.ScopeBlock.ToInclusive;
        }

        private List<GotoStatement> m_PendingGotos;
        private Dictionary<string, LabelStatement> m_LocalLabels;
        private string m_LastDefinedName;

        internal void DefineLabel(LabelStatement label)
        {
            if (this.m_LocalLabels == null)
                this.m_LocalLabels = new Dictionary<string, LabelStatement>();

            if (this.m_LocalLabels.ContainsKey(label.Label))
            {
                throw new SyntaxErrorException(label.NameToken, "label '{0}' already defined on line {1}", label.Label, this.m_LocalLabels[label.Label].SourceRef.FromLine);
            }
            else
            {
                this.m_LocalLabels.Add(label.Label, label);
                label.SetDefinedVars(this.m_DefinedNames.Count, this.m_LastDefinedName);
            }
        }

        internal void RegisterGoto(GotoStatement gotostat)
        {
            if (this.m_PendingGotos == null)
                this.m_PendingGotos = new List<GotoStatement>();

            this.m_PendingGotos.Add(gotostat);
            gotostat.SetDefinedVars(this.m_DefinedNames.Count, this.m_LastDefinedName);
        }

        internal void ResolveGotos()
        {
            if (this.m_PendingGotos == null)
                return;

            foreach (GotoStatement gotostat in this.m_PendingGotos)
            {

                if (this.m_LocalLabels != null && this.m_LocalLabels.TryGetValue(gotostat.Label, out LabelStatement label))
                {
                    if (label.DefinedVarsCount > gotostat.DefinedVarsCount)
                        throw new SyntaxErrorException(gotostat.GotoToken,
                            "<goto {0}> at line {1} jumps into the scope of local '{2}'", gotostat.Label,
                            gotostat.GotoToken.FromLine,
                            label.LastDefinedVarName);

                    label.RegisterGoto(gotostat);
                }
                else
                {
                    if (this.Parent == null)
                        throw new SyntaxErrorException(gotostat.GotoToken, "no visible label '{0}' for <goto> at line {1}", gotostat.Label,
                            gotostat.GotoToken.FromLine);

                    this.Parent.RegisterGoto(gotostat);
                }
            }

            this.m_PendingGotos.Clear();
        }
    }
}
