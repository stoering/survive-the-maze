﻿using System.Collections.Generic;

namespace MoonSharp.Interpreter.Execution
{
    internal class RuntimeScopeFrame
    {
        public List<SymbolRef> DebugSymbols { get; private set; }
        public int Count => this.DebugSymbols.Count;
        public int ToFirstBlock { get; internal set; }

        public RuntimeScopeFrame() => this.DebugSymbols = new List<SymbolRef>();

        public override string ToString() => string.Format("ScopeFrame : #{0}", this.Count);
    }
}
