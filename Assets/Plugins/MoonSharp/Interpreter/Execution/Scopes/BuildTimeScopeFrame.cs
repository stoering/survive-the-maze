﻿using MoonSharp.Interpreter.Tree.Statements;

namespace MoonSharp.Interpreter.Execution.Scopes
{
    internal class BuildTimeScopeFrame
    {
        private BuildTimeScopeBlock m_ScopeTreeRoot;
        private BuildTimeScopeBlock m_ScopeTreeHead;
        private RuntimeScopeFrame m_ScopeFrame = new RuntimeScopeFrame();

        public bool HasVarArgs { get; private set; }

        internal BuildTimeScopeFrame(bool hasVarArgs)
        {
            this.HasVarArgs = hasVarArgs;
            this.m_ScopeTreeHead = this.m_ScopeTreeRoot = new BuildTimeScopeBlock(null);
        }

        internal void PushBlock() => this.m_ScopeTreeHead = this.m_ScopeTreeHead.AddChild();

        internal RuntimeScopeBlock PopBlock()
        {
            BuildTimeScopeBlock tree = this.m_ScopeTreeHead;

            this.m_ScopeTreeHead.ResolveGotos();

            this.m_ScopeTreeHead = this.m_ScopeTreeHead.Parent;

            if (this.m_ScopeTreeHead == null)
                throw new InternalErrorException("Can't pop block - stack underflow");

            return tree.ScopeBlock;
        }

        internal RuntimeScopeFrame GetRuntimeFrameData()
        {
            if (this.m_ScopeTreeHead != this.m_ScopeTreeRoot)
                throw new InternalErrorException("Misaligned scope frames/blocks!");

            this.m_ScopeFrame.ToFirstBlock = this.m_ScopeTreeRoot.ScopeBlock.To;

            return this.m_ScopeFrame;
        }

        internal SymbolRef Find(string name)
        {
            for (BuildTimeScopeBlock tree = this.m_ScopeTreeHead; tree != null; tree = tree.Parent)
            {
                SymbolRef l = tree.Find(name);

                if (l != null)
                    return l;
            }

            return null;
        }

        internal SymbolRef DefineLocal(string name) => this.m_ScopeTreeHead.Define(name);

        internal SymbolRef TryDefineLocal(string name)
        {
            if (this.m_ScopeTreeHead.Find(name) != null)
            {
                this.m_ScopeTreeHead.Rename(name);
            }

            return this.m_ScopeTreeHead.Define(name);
        }

        internal void ResolveLRefs()
        {
            this.m_ScopeTreeRoot.ResolveGotos();

            _ = this.m_ScopeTreeRoot.ResolveLRefs(this);
        }

        internal int AllocVar(SymbolRef var)
        {
            var.i_Index = this.m_ScopeFrame.DebugSymbols.Count;
            this.m_ScopeFrame.DebugSymbols.Add(var);
            return var.i_Index;
        }

        internal int GetPosForNextVar() => this.m_ScopeFrame.DebugSymbols.Count;

        internal void DefineLabel(LabelStatement label) => this.m_ScopeTreeHead.DefineLabel(label);

        internal void RegisterGoto(GotoStatement gotostat) => this.m_ScopeTreeHead.RegisterGoto(gotostat);
    }
}
