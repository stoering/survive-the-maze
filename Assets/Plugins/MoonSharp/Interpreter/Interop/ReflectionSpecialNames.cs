﻿namespace MoonSharp.Interpreter.Interop
{

    /// <summary>
    /// Helps identifying a reflection special name
    /// </summary>
    public enum ReflectionSpecialNameType
    {
        IndexGetter,
        IndexSetter,
        ImplicitCast,
        ExplicitCast,

        OperatorTrue,
        OperatorFalse,

        PropertyGetter,
        PropertySetter,
        AddEvent,
        RemoveEvent,

        OperatorAdd,
        OperatorAnd,
        OperatorOr,
        OperatorDec,
        OperatorDiv,
        OperatorEq,
        OperatorXor,
        OperatorGt,
        OperatorGte,
        OperatorInc,
        OperatorNeq,
        OperatorLt,
        OperatorLte,
        OperatorNot,
        OperatorMod,
        OperatorMul,
        OperatorCompl,
        OperatorSub,
        OperatorNeg,
        OperatorUnaryPlus,
    }

    /// <summary>
    /// Class helping identifying special names found with reflection
    /// </summary>
    public struct ReflectionSpecialName
    {
        public ReflectionSpecialNameType Type { get; private set; }
        public string Argument { get; private set; }

        public ReflectionSpecialName(ReflectionSpecialNameType type, string argument = null)
            : this()
        {
            this.Type = type;
            this.Argument = argument;
        }

        public ReflectionSpecialName(string name)
            : this()
        {
            if (name.Contains("."))
            {
                string[] split = name.Split('.');
                name = split[split.Length - 1];
            }

            switch (name)
            {
                case "op_Explicit":
                    this.Type = ReflectionSpecialNameType.ExplicitCast;
                    return;
                case "op_Implicit":
                    this.Type = ReflectionSpecialNameType.ImplicitCast;
                    return;
                case "set_Item":
                    this.Type = ReflectionSpecialNameType.IndexSetter;
                    return;
                case "get_Item":
                    this.Type = ReflectionSpecialNameType.IndexGetter;
                    return;
                case "op_Addition":
                    this.Type = ReflectionSpecialNameType.OperatorAdd;
                    this.Argument = "+";
                    return;
                case "op_BitwiseAnd":
                    this.Type = ReflectionSpecialNameType.OperatorAnd;
                    this.Argument = "&";
                    return;
                case "op_BitwiseOr":
                    this.Type = ReflectionSpecialNameType.OperatorOr;
                    this.Argument = "|";
                    return;
                case "op_Decrement":
                    this.Type = ReflectionSpecialNameType.OperatorDec;
                    this.Argument = "--";
                    return;
                case "op_Division":
                    this.Type = ReflectionSpecialNameType.OperatorDiv;
                    this.Argument = "/";
                    return;
                case "op_Equality":
                    this.Type = ReflectionSpecialNameType.OperatorEq;
                    this.Argument = "==";
                    return;
                case "op_ExclusiveOr":
                    this.Type = ReflectionSpecialNameType.OperatorXor;
                    this.Argument = "^";
                    return;
                case "op_False":
                    this.Type = ReflectionSpecialNameType.OperatorFalse;
                    return;
                case "op_GreaterThan":
                    this.Type = ReflectionSpecialNameType.OperatorGt;
                    this.Argument = ">";
                    return;
                case "op_GreaterThanOrEqual":
                    this.Type = ReflectionSpecialNameType.OperatorGte;
                    this.Argument = ">=";
                    return;
                case "op_Increment":
                    this.Type = ReflectionSpecialNameType.OperatorInc;
                    this.Argument = "++";
                    return;
                case "op_Inequality":
                    this.Type = ReflectionSpecialNameType.OperatorNeq;
                    this.Argument = "!=";
                    return;
                case "op_LessThan":
                    this.Type = ReflectionSpecialNameType.OperatorLt;
                    this.Argument = "<";
                    return;
                case "op_LessThanOrEqual":
                    this.Type = ReflectionSpecialNameType.OperatorLte;
                    this.Argument = "<=";
                    return;
                case "op_LogicalNot":
                    this.Type = ReflectionSpecialNameType.OperatorNot;
                    this.Argument = "!";
                    return;
                case "op_Modulus":
                    this.Type = ReflectionSpecialNameType.OperatorMod;
                    this.Argument = "%";
                    return;
                case "op_Multiply":
                    this.Type = ReflectionSpecialNameType.OperatorMul;
                    this.Argument = "*";
                    return;
                case "op_OnesComplement":
                    this.Type = ReflectionSpecialNameType.OperatorCompl;
                    this.Argument = "~";
                    return;
                case "op_Subtraction":
                    this.Type = ReflectionSpecialNameType.OperatorSub;
                    this.Argument = "-";
                    return;
                case "op_True":
                    this.Type = ReflectionSpecialNameType.OperatorTrue;
                    return;
                case "op_UnaryNegation":
                    this.Type = ReflectionSpecialNameType.OperatorNeg;
                    this.Argument = "-";
                    return;
                case "op_UnaryPlus":
                    this.Type = ReflectionSpecialNameType.OperatorUnaryPlus;
                    this.Argument = "+";
                    return;
            }

            if (name.StartsWith("get_"))
            {
                this.Type = ReflectionSpecialNameType.PropertyGetter;
                this.Argument = name.Substring(4);
            }
            else if (name.StartsWith("set_"))
            {
                this.Type = ReflectionSpecialNameType.PropertySetter;
                this.Argument = name.Substring(4);
            }
            else if (name.StartsWith("add_"))
            {
                this.Type = ReflectionSpecialNameType.AddEvent;
                this.Argument = name.Substring(4);
            }
            else if (name.StartsWith("remove_"))
            {
                this.Type = ReflectionSpecialNameType.RemoveEvent;
                this.Argument = name.Substring(7);
            }
        }
    }
}

