﻿using System;

namespace MoonSharp.Interpreter.Interop.StandardDescriptors
{
    internal class EventFacade : IUserDataType
    {
        private Func<object, ScriptExecutionContext, CallbackArguments, DynValue> m_AddCallback;
        private Func<object, ScriptExecutionContext, CallbackArguments, DynValue> m_RemoveCallback;
        private object m_Object;

        public EventFacade(EventMemberDescriptor parent, object obj)
        {
            this.m_Object = obj;
            this.m_AddCallback = parent.AddCallback;
            this.m_RemoveCallback = parent.RemoveCallback;
        }

        public EventFacade(Func<object, ScriptExecutionContext, CallbackArguments, DynValue> addCallback, Func<object, ScriptExecutionContext, CallbackArguments, DynValue> removeCallback, object obj)
        {
            this.m_Object = obj;
            this.m_AddCallback = addCallback;
            this.m_RemoveCallback = removeCallback;
        }

        public DynValue Index(Script script, DynValue index, bool isDirectIndexing)
        {
            if (index.Type == DataType.String)
            {
                if (index.String == "add")
                    return DynValue.NewCallback((c, a) => this.m_AddCallback(this.m_Object, c, a));
                else if (index.String == "remove")
                    return DynValue.NewCallback((c, a) => this.m_RemoveCallback(this.m_Object, c, a));
            }

            throw new ScriptRuntimeException("Events only support add and remove methods");
        }

        public bool SetIndex(Script script, DynValue index, DynValue value, bool isDirectIndexing) => throw new ScriptRuntimeException("Events do not have settable fields");

        public DynValue MetaIndex(Script script, string metaname) => null;
    }
}
