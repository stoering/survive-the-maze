﻿#if !(DOTNET_CORE || NETFX_CORE) 

using System;
using System.Reflection;

namespace MoonSharp.Interpreter.Compatibility.Frameworks
{
    internal abstract class FrameworkClrBase : FrameworkReflectionBase
    {
        private BindingFlags BINDINGFLAGS_MEMBER = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static;
        private BindingFlags BINDINGFLAGS_INNERCLASS = BindingFlags.Public | BindingFlags.NonPublic;

        public override Type GetTypeInfoFromType(Type t) => t;

        public override MethodInfo GetAddMethod(EventInfo ei) => ei.GetAddMethod(true);

        public override ConstructorInfo[] GetConstructors(Type type) => type.GetConstructors(this.BINDINGFLAGS_MEMBER);

        public override EventInfo[] GetEvents(Type type) => type.GetEvents(this.BINDINGFLAGS_MEMBER);

        public override FieldInfo[] GetFields(Type type) => type.GetFields(this.BINDINGFLAGS_MEMBER);

        public override Type[] GetGenericArguments(Type type) => type.GetGenericArguments();

        public override MethodInfo GetGetMethod(PropertyInfo pi) => pi.GetGetMethod(true);

        public override Type[] GetInterfaces(Type t) => t.GetInterfaces();

        public override MethodInfo GetMethod(Type type, string name) => type.GetMethod(name);

        public override MethodInfo[] GetMethods(Type type) => type.GetMethods(this.BINDINGFLAGS_MEMBER);

        public override Type[] GetNestedTypes(Type type) => type.GetNestedTypes(this.BINDINGFLAGS_INNERCLASS);

        public override PropertyInfo[] GetProperties(Type type) => type.GetProperties(this.BINDINGFLAGS_MEMBER);

        public override PropertyInfo GetProperty(Type type, string name) => type.GetProperty(name);

        public override MethodInfo GetRemoveMethod(EventInfo ei) => ei.GetRemoveMethod(true);

        public override MethodInfo GetSetMethod(PropertyInfo pi) => pi.GetSetMethod(true);


        public override bool IsAssignableFrom(Type current, Type toCompare) => current.IsAssignableFrom(toCompare);

        public override bool IsInstanceOfType(Type t, object o) => t.IsInstanceOfType(o);


        public override MethodInfo GetMethod(Type resourcesType, string name, Type[] types) => resourcesType.GetMethod(name, types);

        public override Type[] GetAssemblyTypes(Assembly asm) => asm.GetTypes();

    }
}

#endif