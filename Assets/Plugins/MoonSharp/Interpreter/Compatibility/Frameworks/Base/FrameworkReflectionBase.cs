﻿using System;
using System.Linq;
using System.Reflection;

#if DOTNET_CORE || NETFX_CORE
	using TTypeInfo = System.Reflection.TypeInfo;
#else
using TTypeInfo = System.Type;
#endif

namespace MoonSharp.Interpreter.Compatibility.Frameworks
{
    internal abstract class FrameworkReflectionBase : FrameworkBase
    {
        public abstract TTypeInfo GetTypeInfoFromType(Type t);

        public override Assembly GetAssembly(Type t) => GetTypeInfoFromType(t).Assembly;

        public override Type GetBaseType(Type t) => GetTypeInfoFromType(t).BaseType;


        public override bool IsValueType(Type t) => GetTypeInfoFromType(t).IsValueType;

        public override bool IsInterface(Type t) => GetTypeInfoFromType(t).IsInterface;

        public override bool IsNestedPublic(Type t) => GetTypeInfoFromType(t).IsNestedPublic;
        public override bool IsAbstract(Type t) => GetTypeInfoFromType(t).IsAbstract;

        public override bool IsEnum(Type t) => GetTypeInfoFromType(t).IsEnum;

        public override bool IsGenericTypeDefinition(Type t) => GetTypeInfoFromType(t).IsGenericTypeDefinition;

        public override bool IsGenericType(Type t) => GetTypeInfoFromType(t).IsGenericType;

        public override Attribute[] GetCustomAttributes(Type t, bool inherit) => GetTypeInfoFromType(t).GetCustomAttributes(inherit).OfType<Attribute>().ToArray();

        public override Attribute[] GetCustomAttributes(Type t, Type at, bool inherit) => GetTypeInfoFromType(t).GetCustomAttributes(at, inherit).OfType<Attribute>().ToArray();


    }
}
