﻿#if !(DOTNET_CORE || NETFX_CORE) && !PCL

using System;
using System.Linq;

namespace MoonSharp.Interpreter.Compatibility.Frameworks
{
    internal class FrameworkCurrent : FrameworkClrBase
    {
        public override bool IsDbNull(object o) => o != null && Convert.IsDBNull(o);


        public override bool StringContainsChar(string str, char chr) => str.Contains(chr);

        public override Type GetInterface(Type type, string name) => type.GetInterface(name);
    }
}

#endif
