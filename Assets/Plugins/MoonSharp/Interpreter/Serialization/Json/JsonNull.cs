﻿namespace MoonSharp.Interpreter.Serialization.Json
{
    /// <summary>
    /// UserData representing a null value in a table converted from Json
    /// </summary>
    public sealed class JsonNull
    {
        public static bool isNull() => true;

        [MoonSharpHidden]
        public static bool IsJsonNull(DynValue v)
        {
            return v.Type == DataType.UserData &&
                v.UserData.Descriptor != null &&
                v.UserData.Descriptor.Type == typeof(JsonNull);
        }

        [MoonSharpHidden]
        public static DynValue Create() => UserData.CreateStatic<JsonNull>();
    }
}
