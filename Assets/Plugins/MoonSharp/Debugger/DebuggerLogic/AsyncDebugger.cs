﻿#if (!PCL) && ((!UNITY_5) || UNITY_STANDALONE)

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using MoonSharp.Interpreter;
using MoonSharp.Interpreter.Debugging;

namespace MoonSharp.VsCodeDebugger.DebuggerLogic
{
    internal class AsyncDebugger : IDebugger
    {
        private static object s_AsyncDebuggerIdLock = new object();
        private static int s_AsyncDebuggerIdCounter = 0;
        private object m_Lock = new object();
        private IAsyncDebuggerClient m_Client__;
        private DebuggerAction m_PendingAction = null;
        private List<WatchItem>[] m_WatchItems;
        private Dictionary<int, SourceCode> m_SourcesMap = new Dictionary<int, SourceCode>();
        private Dictionary<int, string> m_SourcesOverride = new Dictionary<int, string>();
        private Func<SourceCode, string> m_SourceFinder;


        public DebugService DebugService { get; private set; }

        public Regex ErrorRegex { get; set; }

        public Script Script { get; private set; }

        public bool PauseRequested { get; set; }

        public string Name { get; set; }

        public int Id { get; private set; }


        public AsyncDebugger(Script script, Func<SourceCode, string> sourceFinder, string name)
        {
            lock (s_AsyncDebuggerIdLock)
                this.Id = s_AsyncDebuggerIdCounter++;

            this.m_SourceFinder = sourceFinder;
            this.ErrorRegex = new Regex(@"\A.*\Z");
            this.Script = script;
            this.m_WatchItems = new List<WatchItem>[(int)WatchType.MaxValue];
            this.Name = name;

            for (int i = 0; i < this.m_WatchItems.Length; i++)
                this.m_WatchItems[i] = new List<WatchItem>(64);
        }


        public IAsyncDebuggerClient Client
        {
            get => this.m_Client__;
            set
            {
                lock (this.m_Lock)
                {
                    if (this.m_Client__ != null && this.m_Client__ != value)
                    {
                        this.m_Client__.Unbind();
                    }

                    if (value != null)
                    {
                        for (int i = 0; i < this.Script.SourceCodeCount; i++)
                            if (this.m_SourcesMap.ContainsKey(i))
                                value.OnSourceCodeChanged(i);
                    }

                    this.m_Client__ = value;
                }
            }
        }

        DebuggerAction IDebugger.GetAction(int ip, SourceRef sourceref)
        {
            this.PauseRequested = false;

            lock (this.m_Lock)
                if (this.Client != null)
                {
                    this.Client.SendStopEvent();
                }

            while (true)
            {
                lock (this.m_Lock)
                {
                    if (this.Client == null)
                    {
                        return new DebuggerAction() { Action = DebuggerAction.ActionType.Run };
                    }

                    if (this.m_PendingAction != null)
                    {
                        DebuggerAction action = this.m_PendingAction;
                        this.m_PendingAction = null;
                        return action;
                    }
                }

                Sleep(10);
            }
        }


        public void QueueAction(DebuggerAction action)
        {
            while (true)
            {
                lock (this.m_Lock)
                    if (this.m_PendingAction == null)
                    {
                        this.m_PendingAction = action;
                        break;
                    }

                Sleep(10);
            }
        }

        private void Sleep(int v)
        {
#if DOTNET_CORE
			System.Threading.Tasks.Task.Delay(10).Wait();
#else
            System.Threading.Thread.Sleep(10);
#endif
        }

        private DynamicExpression CreateDynExpr(string code)
        {
            try
            {
                return this.Script.CreateDynamicExpression(code);
            }
            catch (Exception ex)
            {
                return this.Script.CreateConstantDynamicExpression(code, DynValue.NewString(ex.Message));
            }
        }

        List<DynamicExpression> IDebugger.GetWatchItems() => new List<DynamicExpression>();

        bool IDebugger.IsPauseRequested() => this.PauseRequested;

        void IDebugger.RefreshBreakpoints(IEnumerable<SourceRef> refs)
        {

        }

        void IDebugger.SetByteCode(string[] byteCode)
        {

        }

        void IDebugger.SetSourceCode(SourceCode sourceCode)
        {
            this.m_SourcesMap[sourceCode.SourceID] = sourceCode;

            bool invalidFile = false;

            string file = this.m_SourceFinder(sourceCode);

            if (!string.IsNullOrEmpty(file))
            {
                try
                {
                    if (!File.Exists(file))
                        invalidFile = true;
                }
                catch
                {
                    invalidFile = true;
                }
            }
            else
            {
                invalidFile = true;
            }

            if (invalidFile)
            {
                file = Path.Combine(Path.GetTempPath(), Guid.NewGuid().ToString("N") + ".lua");
                File.WriteAllText(file, sourceCode.Code + GetFooterForTempFile());
                this.m_SourcesOverride[sourceCode.SourceID] = file;
            }
            else if (file != sourceCode.Name)
            {
                this.m_SourcesOverride[sourceCode.SourceID] = file;
            }


            lock (this.m_Lock)
                if (this.Client != null)
                    this.Client.OnSourceCodeChanged(sourceCode.SourceID);
        }

        private string GetFooterForTempFile()
        {
            return "\n\n" +
                "----------------------------------------------------------------------------------------------------------\n" +
                "-- This file has been generated by the debugger as a placeholder for a script snippet stored in memory. --\n" +
                "-- If you restart the host process, the contents of this file are not valid anymore.                    --\n" +
                "----------------------------------------------------------------------------------------------------------\n";
        }

        public string GetSourceFile(int sourceId)
        {
            if (this.m_SourcesOverride.ContainsKey(sourceId))
                return this.m_SourcesOverride[sourceId];
            else if (this.m_SourcesMap.ContainsKey(sourceId))
                return this.m_SourcesMap[sourceId].Name;
            return null;
        }

        public bool IsSourceOverride(int sourceId) => (this.m_SourcesOverride.ContainsKey(sourceId));


        void IDebugger.SignalExecutionEnded()
        {
            lock (this.m_Lock)
                if (this.Client != null)
                    this.Client.OnExecutionEnded();
        }

        bool IDebugger.SignalRuntimeException(ScriptRuntimeException ex)
        {
            lock (this.m_Lock)
                if (this.Client == null)
                    return false;

            this.Client.OnException(ex);
            this.PauseRequested = this.ErrorRegex.IsMatch(ex.Message);
            return this.PauseRequested;
        }

        void IDebugger.Update(WatchType watchType, IEnumerable<WatchItem> items)
        {
            List<WatchItem> list = this.m_WatchItems[(int)watchType];

            list.Clear();
            list.AddRange(items);

            lock (this.m_Lock)
                if (this.Client != null)
                    this.Client.OnWatchesUpdated(watchType);
        }


        public List<WatchItem> GetWatches(WatchType watchType) => this.m_WatchItems[(int)watchType];

        public SourceCode GetSource(int id)
        {
            if (this.m_SourcesMap.ContainsKey(id))
                return this.m_SourcesMap[id];

            return null;
        }

        public SourceCode FindSourceByName(string path)
        {
            // we use case insensitive match - be damned if you have files which differ only by 
            // case in the same directory on Unix.
            path = path.Replace('\\', '/').ToUpperInvariant();

            foreach (KeyValuePair<int, string> kvp in this.m_SourcesOverride)
            {
                if (kvp.Value.Replace('\\', '/').ToUpperInvariant() == path)
                    return this.m_SourcesMap[kvp.Key];
            }

            return this.m_SourcesMap.Values.FirstOrDefault(s => s.Name.Replace('\\', '/').ToUpperInvariant() == path);
        }

        void IDebugger.SetDebugService(DebugService debugService) => this.DebugService = debugService;

        public DynValue Evaluate(string expression)
        {
            DynamicExpression expr = CreateDynExpr(expression);
            return expr.Evaluate();
        }

        DebuggerCaps IDebugger.GetDebuggerCaps() => DebuggerCaps.CanDebugSourceCode | DebuggerCaps.HasLineBasedBreakpoints;
    }
}

#endif