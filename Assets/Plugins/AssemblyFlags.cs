﻿using System.Diagnostics.CodeAnalysis;

// Used to share Start, Update, etc. with unit tests without allowing classes to call each other's Unity funcitons.
[assembly: SuppressMessage("Style", "IDE0044:Add readonly modifier", Justification = "Unity accesses private variables during serialization, so they are not truly readonly.")]
[assembly: SuppressMessage("Style", "IDE1006:Naming Styles", Justification = "Not fixing this for Plugins")]
[assembly: SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "Not fixing this for Plugins")]
[assembly: SuppressMessage("Style", "IDE0059:Value assigned to symbol is never used", Justification = "Not fixing this for Plugins")]
[assembly: SuppressMessage("Style", "IDE0016:Use 'throw' expression", Justification = "Not fixing this for Plugins")]
