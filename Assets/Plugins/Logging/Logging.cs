﻿using UnityEngine;

public enum LoggingLevels
{
    None = 0,
    Error = 1,
    Warning = 2,
    Important = 3,
    Information = 4,
    Verbose = 5,
}

public class Logging
{
    public static LoggingLevels LogLevel { get; set; } = LoggingLevels.Verbose;

    public static void Log(object message, Object context, LoggingLevels loggingLevel = LoggingLevels.Information)
    {
        if (LogLevel >= loggingLevel)
            Debug.Log(message, context);
    }
    public static void Log(object message, LoggingLevels loggingLevel = LoggingLevels.Information)
    {
        if (LogLevel >= loggingLevel)
            Debug.Log(message);
    }

    public static void LogFormat(LogType logType, LogOption logOptions, Object context, string format, params object[] args) => LogFormat(logType, logOptions, context, format, LoggingLevels.Information, args);
    public static void LogFormat(Object context, string format, params object[] args) => LogFormat(context, format, LoggingLevels.Information, args);
    public static void LogFormat(string format, params object[] args) => LogFormat(format, LoggingLevels.Information, args);
    public static void LogFormat(LogType logType, LogOption logOptions, Object context, string format, LoggingLevels loggingLevel, params object[] args)
    {
        if (LogLevel >= loggingLevel)
            Debug.LogFormat(logType, logOptions, context, format, args);
    }
    public static void LogFormat(Object context, string format, LoggingLevels loggingLevel, params object[] args)
    {
        if (LogLevel >= loggingLevel)
            Debug.LogFormat(context, format, args);
    }
    public static void LogFormat(string format, LoggingLevels loggingLevel, params object[] args)
    {
        if (LogLevel >= loggingLevel)
            Debug.LogFormat(format, args);
    }

    public static void LogError(object message, Object context)
    {
        if (LogLevel >= LoggingLevels.Error)
            Debug.LogError(message, context);
    }
    public static void LogError(object message)
    {
        if (LogLevel >= LoggingLevels.Error)
            Debug.LogError(message);
    }

    public static void LogException(System.Exception exception)
    {
        if (LogLevel >= LoggingLevels.Error)
            Debug.LogException(exception);
    }
    public static void LogException(System.Exception exception, Object context)
    {
        if (LogLevel >= LoggingLevels.Error)
            Debug.LogException(exception, context);
    }

    public static void LogWarning(object message)
    {
        if (LogLevel >= LoggingLevels.Warning)
            Debug.LogWarning(message);
    }
    public static void LogWarning(object message, Object context)
    {
        if (LogLevel >= LoggingLevels.Warning)
            Debug.LogWarning(message, context);
    }
}
