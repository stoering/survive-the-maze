@ECHO OFF
IF NOT DEFINED UNITYPATH (GOTO :PrintFail)

IF "%*"=="" (
	ECHO . This script will run a Progressive Seed Unit Test by default.
	ECHO .    The default starting seed is 0. You can pass in an alternative 
	ECHO .    starting seed if desired. Parameter 1 is the seed.
	ECHO .
	ECHO . The script currently repeats the test forever, recording any failed results.
	ECHO . Passing results are not recorded. To set a test limit, pass in a count via
	ECHO .    the second parameter.
	ECHO .
	ECHO . Usage Example: SequentialSeedRegression.bat 10 20
	ECHO .    The above will start at seed 10, and run 20 tests, stopping at seed 30.   
)

:TestSetup
SET SEEDVALUE="%1"
SET TESTLENGTH="%2%"
IF %SEEDVALUE%=="" ( SET SEEDVALUE=0 )
IF %TESTLENGTH%=="" ( SET TESTLENGTH=1 )
SET /a TESTLIMIT=%SEEDVALUE%+%TESTLENGTH%
SET SETTINGSPATH="%~dp0\..\Assets\Tests\Temp\RandomSeedSettings.txt"
SET OUTPUT="%~dp0\Temp\SequentialTestResults.txt"
SET RERUNLIST="%~dp0\Temp\RerunFailedTests.bat"
SET SEEDTYPE=4

ECHO { > %SETTINGSPATH%
ECHO 	"seed": %SEEDVALUE%, >> %SETTINGSPATH%
ECHO 	"seedType": %SEEDTYPE% >> %SETTINGSPATH%
ECHO } >> %SETTINGSPATH%

ECHO Summary of all units tests in the sequential seed regression test: > %OUTPUT%

:RunTest
"%UNITYPATH%" -projectPath "%~dp0\.." -batchmode -runTests -testCategory "Random Seed" -testResults "UnitTests\Temp\results.xml"
IF errorlevel 1 (
 ECHO !TEST %SEEDVALUE% FAILED!
 ECHO !TEST %SEEDVALUE% FAILED! >> %OUTPUT%
 ECHO CALL %0 %SEEDVALUE:"=% >> %RERUNLIST%
 ren "%~dp0\Temp\results.xml" FailedTest%SEEDVALUE%.xml
) ELSE (
 ECHO TEST %SEEDVALUE% PASSED
 ECHO TEST %SEEDVALUE% PASSED >> %OUTPUT%
)
SET /a SEEDVALUE=%SEEDVALUE%+1
IF %SEEDVALUE% LSS %TESTLIMIT% (GOTO :RunTest)
GOTO :EOF

:PrintFail
 ECHO UNITYPATH is not defined, please set it before calling.
 ECHO If Unity is installed normally, try running
 ECHO SET UNITYPATH="C:\Program Files\Unity\Hub\Editor\2019.1.2f1\Editor\Unity.exe"
 ECHO or for PowerShell:
 ECHO Set-Item ENV:UNITYPATH "C:\Program Files\Unity\Hub\Editor\2019.1.2f1\Editor\Unity.exe"
 ECHO Set-Item ENV:UNITYPATH "D:\ProgramFiles\Editor\Unity.exe"